<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {    
         Schema::disableForeignKeyConstraints();
         $this->call(BackupSeeder::class);
         $this->call(SmartAlertTableSeeder::class);
         $this->call(NotificationTableSeeder::class);
         $this->call(HubackupObjectsTableSeeder::class);
         Schema::enableForeignKeyConstraints();
       
    }
}
