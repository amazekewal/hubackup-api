<?php

use Illuminate\Database\Seeder;
use App\Notification;

class NotificationTableSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        $check = Notification::where('user_id', 0)->first();

        //check if dummy already inserted or not
        if ($check === null) {
            Notification::create(['user_id' => 0,
                'status' => 'Backup Started',
                'type' => 'email',
                'num_of_records' => null,
                'is_read' => 0,
            ]);
            Notification::create(['user_id' => 0,
                'status' => 'Companies Added',
                'type' => 'email',
                'num_of_records' => 15,
                'is_read' => 0,
            ]);
            Notification::create(['user_id' => 0,
                'status' => 'Contacts Added',
                'type' => 'email',
                'num_of_records' => 12,
                'is_read' => 0,
            ]);
            Notification::create(['user_id' => 0,
                'status' => 'Backup completed',
                'type' => 'email',
                'num_of_records' => null,
                'is_read' => 0,
            ]);
            Notification::create(['user_id' => 0,
                'status' => 'Backup Started',
                'type' => 'email',
                'num_of_records' => null,
                'is_read' => 0,
            ]);
            Notification::create(['user_id' => 0,
                'status' => 'Backup completed',
                'type' => 'email',
                'num_of_records' => null,
                'is_read' => 0,
            ]);
            Notification::create(['user_id' => 0,
                'status' => 'Backup Started',
                'type' => 'email',
                'num_of_records' => null,
                'is_read' => 0,
            ]);
            Notification::create(['user_id' => 0,
                'status' => 'Backup completed',
                'type' => 'email',
                'num_of_records' => null,
                'is_read' => 0,
            ]);
            Notification::create(['user_id' => 0,
                'status' => 'Backup Started',
                'type' => 'email',
                'num_of_records' => null,
                'is_read' => 0,
            ]);
            Notification::create(['user_id' => 0,
                'status' => 'Backup completed',
                'type' => 'email',
                'num_of_records' => null,
                'is_read' => 0,
            ]);
        } else {
            echo 'Dummy data is already added';
        }
    }

}
