<?php

use Illuminate\Database\Seeder;
use App\SmartAlert;
use App\SmartAlertHistory;
class SmartAlertTableSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        $check = SmartAlert::where('user_id', 0)->first();

        //check if dummy already inserted or not
        if ($check === null) {
            
            $company_alert = SmartAlert::create(['user_id' => 0,
                's_object' => 'Companies',
                'action' => 'Added',
                'operator' => 'More than',
                'amount' => 10,
                'unit' => 'records']);
            
            //create smart alert history
            SmartAlertHistory::create(['user_id' => 0,
                                    'smart_alert_id' => $company_alert->id,
                                              ]);
            
            $contact_alert = SmartAlert::create(['user_id' => 0,
                's_object' => 'Contacts',
                'action' => 'Added',
                'operator' => 'More than',
                'amount' => 10,
                'unit' => 'records']);
            
            //create smart alert history
            SmartAlertHistory::create(['user_id' => 0,
                                    'smart_alert_id' => $contact_alert->id,
                                              ]);
            
            
            SmartAlert::create(['user_id' => 0,
                's_object' => 'Deals',
                'action' => 'Added',
                'operator' => 'More than',
                'amount' => 10,
                'unit' => 'records']);
            SmartAlert::create(['user_id' => 0,
                's_object' => 'Tickets',
                'action' => 'Added',
                'operator' => 'More than',
                'amount' => 10,
                'unit' => 'records']);
            SmartAlert::create(['user_id' => 0,
                's_object' => 'Companies',
                'action' => 'Deleted',
                'operator' => 'More than',
                'amount' => 10,
                'unit' => 'records']);
            SmartAlert::create(['user_id' => 0,
                's_object' => 'Contacts',
                'action' => 'Deleted',
                'operator' => 'More than',
                'amount' => 10,
                'unit' => 'records']);
            
            SmartAlert::create(['user_id' => 0,
                's_object' => 'Deals',
                'action' => 'Deleted',
                'operator' => 'More than',
                'amount' => 10,
                'unit' => 'records']);
            SmartAlert::create(['user_id' => 0,
                's_object' => 'Tickets',
                'action' => 'Deleted',
                'operator' => 'More than',
                'amount' => 10,
                'unit' => 'records']);
        } else {
            echo 'Dummy data is already added';
        }
    }

}
