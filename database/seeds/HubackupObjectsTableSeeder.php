<?php

use Illuminate\Database\Seeder;
use App\HubackupObject;

class HubackupObjectsTableSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {

        $check = HubackupObject::where('object', 'Broadcast Messages')->first();

        //check if dummy already inserted or not
        if ($check === null) {
            $objects = ['Analytics','Broadcast Messages', 'Calendar Events', 'Campaigns', 'CMS Blog Authors', 'CMS Blog Comments',
                'CMS Blog Posts','CMS Blog Topics', 'CMS Blogs', 'CMS Domains', 'CMS Files', 'CMS HubDB',
                'CMS Layouts', 'CMS Page Publishing','CMS Pipelines', 'CMS Site Maps', 'CMS Templates',
                'CMS Url Mappings','Companies','Contacts', 'Deals', 'Email Events', 'Email Subscriptions',
                'Engagements','Events', 'Forms','Marketing Emails', 'Owners', 'Products', 'Publishing Channels',
                'Tickets', 'Workflows'];
            foreach ($objects as $object) {
                HubackupObject::create(['object' => $object]);
            }
        } else {
            echo 'Objects are already added';
        }
    }

}
