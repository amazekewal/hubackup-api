<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEngagementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('engagements', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('user_id')->unsigned();
            $table->bigInteger('backup_progress_id')->unsigned();
            $table->bigInteger('engagement_id')->unsigned();
            $table->integer('portal_id')->unsigned();
            $table->tinyInteger('active')->nullable();
            $table->integer('created_by')->unsigned();
            $table->integer('modified_by')->unsigned();
            $table->integer('owner_id')->unsigned();
            $table->string('type')->nullable();
            $table->json('associations_contactIds')->nullable();
            $table->json('associations_companyIds')->nullable();
            $table->json('associations_dealIds')->nullable();
            $table->json('associations_ownerIds')->nullable();
            $table->json('associations_workflowIds')->nullable();
            $table->json('associations_ticketIds')->nullable();
            $table->json('attachments')->nullable();
            $table->datetime('createdate')->nullable();
            $table->datetime('last_updated')->nullable();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('backup_progress_id')->references('id')->on('backup_progress')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('engagements');
    }
}
