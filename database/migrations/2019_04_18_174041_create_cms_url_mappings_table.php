<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCmsUrlMappingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cms_url_mappings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('user_id')->unsigned();
            $table->bigInteger('backup_progress_id')->unsigned();
            $table->bigInteger('cms_url_mapping_id')->nullable();
            $table->bigInteger('portal_id')->nullable();
            $table->string('name')->nullable();
            $table->string('route_prefix')->nullable();
            $table->string('destination')->nullable();
            $table->integer('redirect_style')->nullable();
            $table->string('content_group_id')->nullable();
            $table->boolean('is_only_after_notfound')->default('0');
            $table->boolean('is_regex')->default('0');
            $table->boolean('is_match_fullurl')->default('0');
            $table->boolean('is_match_query_string')->default('0');
            $table->boolean('is_pattern')->default('0');
            $table->boolean('is_trailing_slash_optional')->default('0');
            $table->boolean('is_protocol_agnostic')->default('0');
            $table->integer('precedence')->nullable();
            $table->string('deleted_at')->nullable();
            $table->datetime('created')->nullable();
            $table->datetime('updated')->nullable();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('backup_progress_id')->references('id')->on('backup_progress')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cms_url_mappings');
    }
}
