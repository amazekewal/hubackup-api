<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('firstName');
            $table->string('email')->unique();
            $table->string('password');
            $table->string('lastName')->nullable();
            $table->string('accesstoken')->nullable();
            $table->string('refreshtoken')->nullable();
            $table->integer('app_id')->nullable();
            $table->integer('hub_id')->nullable();
            $table->string('region')->nullable();
            $table->string('companyName')->nullable();
            $table->string('address')->nullable();
            $table->string('address1')->nullable();
            $table->string('address2')->nullable();
            $table->string('billingAddress')->nullable();
            $table->string('billingAddress1')->nullable();
            $table->string('billingAddress2')->nullable();
            $table->string('zipcode')->nullable();
            $table->string('city')->nullable();
            $table->string('country')->nullable();
            $table->string('phoneNumber')->nullable();
            $table->string('state')->nullable();
            $table->string('timeZone')->nullable();
            $table->string('lastLogin')->nullable();
            $table->tinyInteger('payment')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('users');
    }

}
