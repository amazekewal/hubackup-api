<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCmsDomainsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cms_domains', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('user_id')->unsigned();
            $table->bigInteger('backup_progress_id')->unsigned();
            $table->bigInteger('cms_domain_id')->nullable();
            $table->bigInteger('portal_id')->nullable();
            $table->string('actual_cname')->nullable();
            $table->string('correct_cname')->nullable();
            $table->string('actual_ip')->nullable();
            $table->integer('consecutive_non_resolving_count')->nullable();
            $table->string('domain')->nullable();
            $table->string('full_category_key')->nullable();
            $table->string('name')->nullable();
            $table->boolean('is_any_primary')->default('0');
            $table->boolean('is_dns_correct')->default('0');
            $table->boolean('is_internal_domain')->default('0');
            $table->boolean('is_legacy')->default('0');
            $table->boolean('is_legacy_domain')->default('0');
            $table->boolean('is_resolving')->default('0');
            $table->boolean('is_ssl_enabled')->default('0');
            $table->boolean('is_ssl_only')->default('0');
            $table->boolean('is_used_for_blog_post')->default('0');
            $table->boolean('is_used_for_site_page')->default('0');
            $table->boolean('is_used_for_landing_page')->default('0');
            $table->boolean('is_used_for_email')->default('0');
            $table->boolean('is_used_for_knowledge')->default('0');
            $table->boolean('is_setup_complete')->default('0');
            $table->boolean('manually_marked_as_resolving')->default('0');
            $table->boolean('primary_blog')->default('0');
            $table->boolean('primary_blog_post')->default('0');
            $table->boolean('primary_email')->default('0');
            $table->boolean('primary_landing_page')->default('0');
            $table->boolean('primary_legacy_page')->default('0');
            $table->boolean('primary_knowledge')->default('0');
            $table->boolean('primary_site_page')->default('0');
            $table->string('secondary_to_domain')->nullable();
            $table->boolean('primary_click_tracking')->default('0');
            $table->string('apex_domain')->nullable();
            $table->string('public_suffix')->nullable();
            $table->integer('site_id')->nullable();
            $table->string('brand_id')->nullable();
            $table->boolean('deletable')->default('0');
            $table->string('cos_object_type')->nullable();
            $table->boolean('is_resolving_internal_property')->default('0');
            $table->boolean('is_resolving_ignoring_manually_marked_as_resolving')->default('0');
            $table->boolean('is_used_for_any_content_type')->default('0');
            $table->datetime('created')->nullable();
            $table->datetime('updated')->nullable();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('backup_progress_id')->references('id')->on('backup_progress')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cms_domains');
    }
}
