<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCmsHubdbTableColumnsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cms_hubdb_table_columns', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('cms_hubdb_table_db_id')->unsigned();
            $table->string('column_id')->nullable();
            $table->string('column_name')->nullable();
            $table->string('column_label')->nullable();
            $table->string('column_type')->nullable();
            $table->foreign('cms_hubdb_table_db_id')->references('id')->on('cms_hubdb_tables')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cms_hubdb_table_columns');
    }
}
