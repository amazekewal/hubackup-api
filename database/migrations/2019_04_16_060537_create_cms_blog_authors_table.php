<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCmsBlogAuthorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cms_blog_authors', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('user_id')->unsigned();
            $table->bigInteger('backup_progress_id')->unsigned();
            $table->bigInteger('blog_author_id')->nullable();
            $table->bigInteger('portal_id')->nullable();
            $table->datetime('created')->nullable();
            $table->datetime('updated')->nullable();
            $table->integer('deleted_at')->nullable();
            $table->string('full_name')->nullable();
            $table->string('email')->nullable();
            $table->string('user_name')->nullable();
            $table->string('slug')->nullable();
            $table->string('display_name')->nullable();
            $table->string('google_plus')->nullable();
            $table->text('bio')->nullable();
            $table->string('website')->nullable();
            $table->string('twitter')->nullable();
            $table->string('linkedin')->nullable();
            $table->string('facebook')->nullable();
            $table->string('avatar')->nullable();
            $table->string('gravatar_url')->nullable();
            $table->string('twitter_username')->nullable();
            $table->boolean('has_social_profiles')->default('0');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('backup_progress_id')->references('id')->on('backup_progress')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cms_blog_authors');
    }
}
