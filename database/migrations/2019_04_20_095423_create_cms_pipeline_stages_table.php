<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCmsPipelineStagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cms_pipeline_stages', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('pipeline_db_id');
            $table->string('stage_id');
            $table->string('label')->nullable();
            $table->integer('display_order')->nullable();
            $table->string('ticket_state')->nullable();
            $table->string('probability')->nullable();
            $table->boolean('is_closed')->default('0');
            $table->boolean('active')->default('0');
            $table->datetime('created_date')->nullable();
            $table->datetime('updated_date')->nullable();
           
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cms_pipeline_stages');
    }
}
