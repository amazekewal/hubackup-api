<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCmsTemplatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cms_templates', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('user_id')->unsigned();
            $table->bigInteger('backup_progress_id')->unsigned();
            $table->bigInteger('cms_template_id')->nullable();
            $table->bigInteger('portal_id')->nullable();
            $table->boolean('amp_validated')->default('0');
            $table->integer('attached_amp_stylesheet');
            $table->integer('category_id');
            $table->string('cdn_minified_url')->nullable();
            $table->string('cdn_url')->nullable();
            $table->integer('cloned_from')->nullable();
            $table->integer('generated_from_layout_id')->nullable();
            $table->string('linked_style_id')->nullable();
            $table->integer('thumbnail_width')->nullable();
            $table->integer('created_by_id')->nullable();
            $table->boolean('css_or_js_template')->default('0');
            $table->boolean('default')->default('0');
            $table->string('description_i18n_key')->nullable();
            $table->boolean('deleted_at')->default('0');
            $table->string('folder')->nullable();
            $table->integer('folder_id')->nullable();
            $table->json('google_fonts')->nullable(); 
            $table->boolean('has_style_tag')->default('0');
            $table->json('host_template_types')->nullable(); //array
            $table->boolean('is_available_for_new_content')->default('0');
            $table->boolean('is_from_layout')->default('0');
            $table->boolean('is_read_only')->default('0');
            $table->integer('marketplace_delivery_id')->nullable();
            $table->integer('marketplace_version')->nullable();
            $table->boolean('has_buffered_changes')->default('0');
            $table->boolean('missing')->default('0');
            $table->boolean('purchased')->default('0');
            $table->string('release_id')->nullable();
            $table->integer('size')->nullable();
            $table->text('source')->nullable();
            $table->integer('template_type')->nullable();
            $table->string('type')->nullable();
            $table->integer('updated_by_id')->nullable();
            $table->datetime('updated')->nullable();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('backup_progress_id')->references('id')->on('backup_progress')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cms_templates');
    }
}
