<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOwnersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('owners', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('user_id')->unsigned();
            $table->bigInteger('backup_progress_id')->unsigned();
            $table->bigInteger('owner_id')->nullable();
            $table->bigInteger('portal_id')->nullable();
            $table->string('first_name')->nullable();
            $table->string('last_name')->nullable();
            $table->string('email')->nullable();
            $table->boolean('has_contacts_access')->default('0');
            $table->integer('active_user_id')->nullable();
            $table->boolean('is_active')->default('0');
            $table->integer('user_id_including_inactive')->nullable();
            $table->integer('remote_list_id')->nullable();
            $table->integer('remote_list_portal_id')->nullable();
            $table->integer('remote_list_owner_id')->nullable();
            $table->integer('remote_list_remote_id')->nullable();
            $table->string('remote_list_remote_type')->nullable();
            $table->boolean('remote_list_active')->default('0');
            $table->datetime('created')->nullable();
            $table->datetime('updated')->nullable();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('backup_progress_id')->references('id')->on('backup_progress')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('owners');
    }
}
