<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCmsBlogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cms_blogs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('user_id')->unsigned();
            $table->bigInteger('backup_progress_id')->unsigned();
            $table->bigInteger('cms_blog_id')->nullable();
            $table->bigInteger('portal_id')->nullable();
            $table->string('absolute_url')->nullable();
            $table->boolean('allow_comments')->default('0');
            $table->string('amp_body_color')->nullable();
            $table->string('amp_body_font')->nullable();
            $table->integer('amp_body_font_size')->nullable();
            $table->text('amp_custom_css')->nullable();
            $table->string('amp_header_background_color')->nullable();
            $table->string('amp_header_color')->nullable();
            $table->string('amp_header_font')->nullable();
            $table->integer('amp_header_font_size')->nullable();
            $table->string('amp_link_color')->nullable();
            $table->string('amp_logo_alt')->nullable();
            $table->integer('amp_logo_height')->nullable();
            $table->string('amp_logo_src')->nullable();
            $table->integer('amp_logo_width')->nullable();
            $table->integer('analytics_page_id')->nullable();
            $table->json('attached_stylesheets')->nullable();
            $table->integer('captcha_after_days')->nullable();
            $table->boolean('captcha_always')->default('0');
            $table->integer('category_id')->nullable();
            $table->integer('close_comments_older')->nullable();
            $table->string('comment_date_format')->nullable();
            $table->string('comment_form_guid')->nullable();
            $table->integer('comment_max_thread_depth')->nullable();
            $table->boolean('comment_moderation')->default('0');
            $table->json('comment_notification_emails')->nullable();
            $table->boolean('comment_should_create_contact')->default('0');
            $table->string('comment_verification_text')->nullable();
            $table->string('cos_object_type')->nullable();
            $table->datetime('created_date_time')->nullable();
            $table->string('daily_notification_email_id')->nullable();
            $table->integer('default_group_style_id')->nullable();
            $table->string('default_notification_from_name')->nullable();
            $table->string('default_notification_reply_to')->nullable();
            $table->boolean('deleted_at')->default('0');
            $table->text('description')->nullable();
            $table->string('domain')->nullable();
            $table->string('domain_when_published')->nullable();
            $table->integer('email_api_subscription_id')->nullable();
            $table->boolean('enable_google_amp_output')->default('0');
            $table->boolean('enable_social_auto_publishing')->default('0');
            $table->text('html_footer')->nullable();
            $table->boolean('html_footer_is_shared')->default('0');
            $table->text('html_head')->nullable();
            $table->boolean('html_head_is_shared')->default('0');
            $table->json('html_keywords')->nullable();
            $table->string('html_title')->nullable();
            $table->integer('instant_notification_email_id')->nullable();
            $table->integer('item_layout_id')->nullable();
            $table->boolean('item_template_is_shared')->default('0');
            $table->string('item_template_path')->nullable();
            $table->string('label')->nullable();
            $table->integer('listing_layout_id')->nullable();
            $table->string('listing_template_path')->nullable();
            $table->string('live_domain')->nullable();
            $table->string('month_filter_format')->nullable();
            $table->integer('monthly_notification_email_id')->nullable();
            $table->string('name')->nullable();
            $table->string('post_html_footer')->nullable();
            $table->string('post_html_head')->nullable();
            $table->integer('posts_per_listing_page')->nullable();
            $table->integer('posts_per_rss_feed')->nullable();
            $table->json('public_access_rules')->nullable();
            $table->boolean('public_access_rules_enabled')->default('0');
            $table->string('public_title')->nullable();
            $table->string('publish_date_format')->nullable();
            $table->string('resolved_domain')->nullable();
            $table->string('root_url')->nullable();
            $table->boolean('show_social_link_facebook')->default('0');
            $table->boolean('show_social_link_linkedin')->default('0');
            $table->boolean('show_social_link_twitter')->default('0');
            $table->boolean('show_summary_in_emails')->default('0');
            $table->boolean('show_summary_in_listing')->default('0');
            $table->boolean('show_summary_in_rss')->default('0');
            $table->string('slug')->nullable();
            $table->string('social_account_twitter')->nullable();
            $table->string('subscription_contacts_property')->nullable();
            $table->string('subscription_form_guid')->nullable();
            $table->integer('subscription_lists_by_type_daily')->nullable();
            $table->integer('subscription_lists_by_type_instant')->nullable();
            $table->integer('subscription_lists_by_type_monthly')->nullable();
            $table->integer('subscription_lists_by_type_weekly')->nullable();
            $table->datetime('updated_date_time')->nullable();
            $table->string('url_base')->nullable();
            $table->json('url_segments')->nullable();
            $table->boolean('use_featured_image_in_summary')->default('0');
            $table->boolean('uses_default_template')->default('0');
            $table->string('social_sharing_linkedin')->nullable();
            $table->string('header')->nullable();
            $table->integer('legacy_tab_id')->nullable();
            $table->string('subscription_email_type')->nullable();
            $table->string('social_sharing_digg')->nullable();
            $table->string('social_sharing_twitter')->nullable();
            $table->string('rss_item_header')->nullable();
            $table->string('social_sharing_twitter_account')->nullable();
            $table->string('social_sharing_reddit')->nullable();
            $table->integer('weekly_notification_email_id')->nullable();
            $table->string('legacy_guid')->nullable();
            $table->string('social_sharing_googleplusone')->nullable();
            $table->string('rss_custom_feed')->nullable();
            $table->string('social_sharing_googlebuzz')->nullable();
            $table->string('social_sharing_email')->nullable();
            $table->string('social_sharing_facebook_like')->nullable();
            $table->integer('legacy_module_id')->nullable();
            $table->string('language')->nullable();
            $table->text('social_publishing_slug')->nullable();
            $table->text('rss_description')->nullable();
            $table->string('rss_item_footer')->nullable();
            $table->string('social_sharing_facebook_send')->nullable();
            $table->string('social_sharing_delicious')->nullable();
            $table->string('social_sharing_stumbleupon')->nullable();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('backup_progress_id')->references('id')->on('backup_progress')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cms_blogs');
    }
}
