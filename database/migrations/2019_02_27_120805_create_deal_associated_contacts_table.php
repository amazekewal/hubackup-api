<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDealAssociatedContactsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('deal_associated_contacts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('deal_db_id')->unsigned();
            $table->bigInteger('deal_associated_vid')->unsigned();
            $table->foreign('deal_db_id')->references('id')->on('deals')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('deal_associated_contacts');
    }
}
