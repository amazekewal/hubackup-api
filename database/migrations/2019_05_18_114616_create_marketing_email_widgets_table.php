<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMarketingEmailWidgetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('marketing_email_widgets', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('marketing_email_db_id')->unsigned();
            $table->string('name')->nullable();
            $table->string('label')->nullable();
            $table->json('body')->nullable();
            $table->json('child_css')->nullable();
            $table->json('css')->nullable();
            $table->integer('order')->nullable();
            $table->string('smart_type')->nullable();
            $table->string('type')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('marketing_email_widgets');
    }
}
