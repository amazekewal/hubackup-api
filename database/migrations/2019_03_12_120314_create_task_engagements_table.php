<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTaskEngagementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('task_engagements', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('engagement_db_id')->unsigned();
            $table->text('body')->nullable();
            $table->string('status')->nullable();
            $table->string('subject')->nullable();
            $table->string('task_type')->nullable();
            $table->datetime('reminder')->nullable();
            $table->tinyInteger('send_default_reminder')->nullable();
            $table->foreign('engagement_db_id')->references('id')->on('engagements')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('task_engagements');
    }
}
