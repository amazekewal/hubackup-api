<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCmsFilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cms_files', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('user_id')->unsigned();
            $table->bigInteger('backup_progress_id')->unsigned();
            $table->bigInteger('cms_file_id')->nullable();
            $table->bigInteger('portal_id')->nullable();
            $table->string('alt_key')->nullable();
            $table->string('alt_key_hash')->nullable();
            $table->string('alt_url')->nullable();
            $table->boolean('archived')->default('0');
            $table->integer('deleted_at')->nullable();
            $table->string('extension')->nullable();
            $table->string('folder_id')->nullable();
            $table->integer('height')->nullable();
            $table->boolean('is_cta_image')->default('0');
            $table->string('encoding')->nullable();
            $table->integer('size')->nullable();
            $table->string('title')->nullable();
            $table->string('type')->nullable();
            $table->string('url')->nullable();
            $table->integer('version')->nullable();
            $table->integer('width')->nullable();
            $table->string('cloud_key')->nullable();
            $table->string('s3_url')->nullable();
            $table->string('friendly_url')->nullable();
            $table->string('url_scheme')->nullable();
            $table->boolean('allows_anonymous_access')->default('0');
            $table->boolean('hidden')->default('0');
            $table->string('cloud_key_hash')->nullable();
            $table->string('created_by')->nullable();
            $table->string('deleted_by')->nullable();
            $table->boolean('replaceable')->default('0');
            $table->string('cdn_purge_embargo_time')->nullable();
            $table->string('file_hash')->nullable();
            $table->string('meta_thumbs_medium_cloud_key')->nullable();
            $table->string('meta_thumbs_medium_friendly_url')->nullable();
            $table->string('meta_thumbs_medium_s3_url')->nullable();
            $table->string('meta_thumbs_medium_image_name')->nullable();
            $table->string('meta_thumbs_thumb_cloud_key')->nullable();
            $table->string('meta_thumbs_thumb_friendly_url')->nullable();
            $table->string('meta_thumbs_thumb_s3_url')->nullable();
            $table->string('meta_thumbs_thumb_image_name')->nullable();
            $table->string('meta_thumbs_icon_cloud_key')->nullable();
            $table->string('meta_thumbs_icon_friendly_url')->nullable();
            $table->string('meta_thumbs_icon_s3_url')->nullable();
            $table->string('meta_thumbs_icon_image_name')->nullable();
            $table->datetime('created')->nullable();
            $table->datetime('updated')->nullable();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('backup_progress_id')->references('id')->on('backup_progress')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cms_files');
    }
}
