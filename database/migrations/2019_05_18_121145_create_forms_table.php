<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFormsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('forms', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('user_id')->unsigned();
            $table->bigInteger('backup_progress_id')->unsigned();
            $table->integer('portal_id')->nullable();
            $table->string('guid')->nullable();
            $table->string('name')->nullable();
            $table->string('action')->nullable();
            $table->string('method')->nullable();
            $table->string('css_class')->nullable();
            $table->string('redirect')->nullable();
            $table->string('submit_text')->nullable();
            $table->string('follow_up_id')->nullable();
            $table->string('notify_recipients')->nullable();
            $table->integer('lead_nurturing_campaign_id')->nullable();
            $table->string('performable_html')->nullable();
            $table->string('migrated_from')->nullable();
            $table->string('ignore_current_values')->nullable();
            $table->json('meta_data')->nullable(); //array
            $table->boolean('deletable')->default('0');
            $table->text('inline_message')->nullable();
            $table->integer('tms_id')->nullable();
            $table->boolean('captcha_enabled')->default('0');
            $table->string('campaign_guid')->nullable();
            $table->boolean('cloneable')->default('0');
            $table->boolean('editable')->default('0');
            $table->string('form_type')->nullable();
            $table->integer('deleted_at')->nullable();
            $table->string('theme_name')->nullable();
            $table->string('parent_id')->nullable();
            $table->text('style')->nullable();
            $table->boolean('is_published')->default('0');
            $table->datetime('publish_at')->nullable();
            $table->datetime('unpublish_at')->nullable();
            $table->datetime('published_at')->nullable();
            $table->string('winning_variant_id')->nullable();
            $table->string('finished')->nullable();
            $table->string('control_id')->nullable();
            $table->string('kickback_emailworkflow_id')->nullable();
            $table->string('kickback_emails_json')->nullable();
            $table->datetime('created_on')->nullable();
            $table->datetime('updated_on')->nullable();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('backup_progress_id')->references('id')->on('backup_progress')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('forms');
    }
}
