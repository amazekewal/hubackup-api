<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCmsBlogCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cms_blog_comments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('user_id')->unsigned();
            $table->bigInteger('backup_progress_id')->unsigned();
            $table->bigInteger('cms_blog_comment_id');
            $table->integer('portal_id')->nullable();
            $table->bigInteger('content_id')->nullable();
            $table->string('content_title')->nullable();
            $table->string('content_permalink')->nullable();
            $table->bigInteger('collection_id')->nullable();
            $table->integer('deleted_at')->nullable();
            $table->string('user_name')->nullable();
            $table->string('first_name')->nullable();
            $table->string('last_name')->nullable();
            $table->string('user_email')->nullable();
            $table->string('comment')->nullable();
            $table->string('user_url')->nullable();
            $table->string('state')->nullable();
            $table->string('user_ip')->nullable();
            $table->string('user_referrer')->nullable();
            $table->string('user_agent')->nullable();
            $table->string('content_author_email')->nullable();
            $table->string('content_author_name')->nullable();
            $table->string('thread_id')->nullable();
            $table->string('replying_to')->nullable();
            $table->integer('parent_id')->nullable();
            $table->integer('legacy_id')->nullable();
            $table->string('extra_context')->nullable();
            $table->datetime('created_date')->nullable();
            $table->datetime('content_created_at')->nullable();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('backup_progress_id')->references('id')->on('backup_progress')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cms_blog_comments');
    }
}
