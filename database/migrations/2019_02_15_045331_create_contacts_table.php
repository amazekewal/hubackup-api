<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContactsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('contacts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('user_id')->unsigned();
            $table->bigInteger('backup_progress_id')->unsigned();
            $table->bigInteger('vid')->unsigned();
            $table->string('first_name')->nullable();
            $table->string('last_name')->nullable(); //lastname
            $table->string('email')->nullable();
            $table->text('company_size')->nullable();
            $table->text('date_of_birth')->nullable();
            $table->integer('days_to_close')->nullable();
            $table->text('degree')->nullable();
            $table->text('field_of_study')->nullable();
            $table->datetime('first_conversion_date')->nullable();
            $table->text('first_conversion')->nullable(); // first_conversion_event_name
            $table->datetime('first_deal_created_date')->nullable();
            $table->string('gender')->nullable();
            $table->string('graduation_date')->nullable();
            $table->text('additional_email_addresses')->nullable();  //hs_additional_emails
            $table->text('all_vids_for_a_contact')->nullable();  //hs_all_contact_vids
            $table->text('first_touch_converting_campaign')->nullable(); //hs_analytics_first_touch_converting_campaign
            $table->text('last_touch_converting_campaign')->nullable(); //hs_analytics_last_touch_converting_campaign
            $table->text('avatar_filemanager_key')->nullable(); //hs_avatar_filemanager_key
            $table->text('all_form_submissions_for_a_contact')->nullable(); //hs_calculated_form_submissions
            $table->text('merged_vids_with_timestamps_of_a_contact')->nullable(); //hs_calculated_merged_vids
            $table->text('calculated_mobile_number_in_international_format')->nullable(); //hs_calculated_mobile_number
            $table->text('calculated_phone_number_in_international_format')->nullable(); //hs_calculated_phone_number
            $table->text('calculated_phone_number_area_code')->nullable(); //hs_calculated_phone_number_area_code
            $table->text('calculated_phone_number_country_code')->nullable(); //hs_calculated_phone_number_country_code
            $table->text('calculated_phone_number_region_code')->nullable(); //hs_calculated_phone_number_region_code
            $table->boolean('email_confirmed')->nullable(); //hs_content_membership_email_confirmed
            $table->text('membership_notes')->nullable(); //hs_content_membership_notes
            $table->datetime('registered_at')->nullable(); //hs_content_membership_registered_at
            $table->string('domain_to_which_registration_email_was_sent')->nullable(); //hs_content_membership_registration_domain_sent_to
            $table->string('time_registration_email_was_sent')->nullable(); //hs_content_membership_registration_email_sent_at
            $table->string('status')->nullable(); //hs_content_membership_status
            $table->string('conversations_visitor_email')->nullable(); //hs_conversations_visitor_email
            $table->text('created_by_conversations')->nullable(); //hs_created_by_conversations
            $table->datetime('recent_document_revisit_date')->nullable(); //hs_document_last_revisited
            $table->string('email_domain')->nullable(); //hs_email_domain
            $table->boolean('email_address_quarantined')->nullable(); //hs_email_quarantined
            $table->integer('sends_since_last_engagement')->nullable(); //hs_email_sends_since_last_engagement
            $table->text('marketing_email_confirmation_status')->nullable(); //hs_emailconfirmationstatus
            $table->boolean('clicked_facebook_ad')->nullable(); //hs_facebook_ad_clicked
            $table->text('facebook_id')->nullable(); //hs_facebookid
            $table->boolean('google_ad_click_id')->nullable(); //hs_google_click_id
            $table->text('googleplus_id')->nullable(); //hs_googleplusid
            $table->string('ip_timezone')->nullable(); //hs_ip_timezone
            $table->string('is_a_contact',5)->nullable(); //hs_is_contact
            $table->string('lead_status')->nullable(); //hs_lead_status
            $table->text('legal_basis_for_processing_contacts_data')->nullable();//hs_legal_basis
            $table->text('linkedin_id')->nullable();//hs_linkedinid
            $table->datetime('recent_sales_email_clicked_date')->nullable(); //hs_sales_email_last_clicked
            $table->datetime('recent_sales_email_opened_date')->nullable(); //hs_sales_email_last_opened
            $table->string('calculated_mobile_number_with_country_code')->nullable();//hs_searchable_calculated_international_mobile_number
            $table->string('calculated_phone_number_with_country_code')->nullable();//hs_searchable_calculated_international_phone_number
            $table->string('calculated_mobile_number_without_country_code')->nullable();//hs_searchable_calculated_mobile_number
            $table->string('calculated_phone_number_without_country_code')->nullable();//hs_searchable_calculated_phone_number
            $table->string('currently_in_sequence',5)->nullable();//hs_sequences_is_enrolled
            $table->string('twitter_id')->nullable();//hs_twitterid
            $table->datetime('owner_assigned_date')->nullable();//hubspot_owner_assigneddate
            $table->string('ip_city')->nullable();
            $table->string('ip_country')->nullable();
            $table->string('ip_country_code')->nullable();
            $table->text('ip_latitude_and_longitude')->nullable(); //ip_latlon
            $table->string('ip_state_or_region')->nullable(); //ip_state
            $table->string('ip_state_code_or_region_code')->nullable(); //ip_state_code
            $table->string('ip_zipcode')->nullable(); 
            $table->string('job_function')->nullable();
            $table->datetime('last_modified_date')->nullable(); //lastmodifieddate
            $table->string('marital_status')->nullable();
            $table->string('military_status')->nullable();
            $table->integer('associated_deals')->nullable(); //num_associated_deals
            $table->integer('number_of_form_submissions')->nullable(); //num_conversion_events
            $table->integer('number_of_unique_forms_submitted')->nullable(); //num_unique_conversion_events
            $table->datetime('recent_conversion_date')->nullable();
            $table->text('recent_conversion')->nullable(); //recent_conversion_event_name
            $table->string('recent_deal_amount')->nullable();
            $table->datetime('recent_deal_close_date')->nullable();
            $table->string('relationship_status')->nullable();
            $table->text('school')->nullable();
            $table->string('seniority')->nullable();
            $table->datetime('start_date')->nullable();
            $table->string('total_revenue')->nullable();
            $table->string('work_email')->nullable();
            $table->text('first_page_seen')->nullable(); //hs_analytics_first_url
            $table->integer('marketing_emails_delivered')->nullable(); //hubspot_owner_id
            $table->text('opted_out_of_email:_one_to_one')->nullable(); //hs_email_optout_5688626
            $table->string('twitter_username')->nullable(); //twitterhandle
            $table->integer('follower_count')->nullable(); //followercount
            $table->string('last_page_seen')->nullable(); //hs_analytics_last_url
            $table->integer('marketing_emails_opened')->nullable(); //hs_email_open
            $table->integer('number_of_pageviews')->nullable(); //hs_analytics_num_page_views
            $table->integer('marketing_emails_clicked')->nullable(); //hs_email_click
            $table->text('salutation')->nullable();
            $table->text('twitter_profile_photo')->nullable(); //twitterprofilephoto
            $table->integer('number_of_visits')->nullable(); //hs_analytics_num_visits
            $table->integer('marketing_emails_bounced')->nullable(); //hs_email_bounce
            $table->text('persona')->nullable(); //hs_persona
            $table->integer('number_of_event_completions')->nullable(); //hs_analytics_num_event_completions
            $table->text('unsubscribed_from_all_email')->nullable(); //hs_email_optout
            $table->string('mobile_phone_number')->nullable(); //mobilephone
            $table->string('phone_number')->nullable(); //phone
            $table->string('fax_number')->nullable(); //fax
            $table->datetime('time_first_seen')->nullable(); //hs_analytics_first_timestamp
            $table->text('last_marketing_email_name')->nullable(); //hs_email_last_email_name
            $table->datetime('last_marketing_email_send_date')->nullable(); //hs_email_last_send_date
            $table->string('address')->nullable();
            $table->text('last_meeting_booked')->nullable(); //engagements_last_meeting_booked
            $table->text('last_meeting_booked_campaign')->nullable(); //engagements_last_meeting_booked_campaign
            $table->text('last_meeting_booked_medium')->nullable(); //engagements_last_meeting_booked_medium
            $table->text('last_meeting_booked_source')->nullable(); //engagements_last_meeting_booked_source
            $table->datetime('time_of_first_visit')->nullable(); //hs_analytics_first_visit_timestamp
            $table->datetime('last_marketing_email_open_date')->nullable(); //hs_email_last_open_date
            $table->datetime('recent_sales_email_replied_date')->nullable(); //hs_sales_email_last_replied
            $table->integer('contact_owner')->nullable(); //hubspot_owner_id
            $table->datetime('last_contacted')->nullable(); //notes_last_contacted
            $table->datetime('last_activity_date')->nullable(); //notes_last_updated
            $table->datetime('next_activity_date')->nullable(); //notes_next_activity_date
            $table->integer('number_of_times_contacted')->nullable(); //num_contacted_notes
            $table->integer('number_of_sales_activities')->nullable(); //num_notes
            $table->datetime('surveyMonkey_event_last_updated')->nullable(); //surveymonkeyeventlastupdated
            $table->datetime('webinar_event_last_updated')->nullable(); //webinareventlastupdated
            $table->string('city')->nullable();
            $table->datetime('time_last_seen')->nullable(); //hs_analytics_last_timestamp
            $table->datetime('last_marketing_email_click_date')->nullable(); //hs_email_last_click_date
            $table->string('hubspot_team')->nullable(); //hubspot_team_id
            $table->text('linkedin_bio')->nullable(); //linkedinbio
            $table->text('twitter_bio')->nullable(); //twitterbio
            $table->text('all_owner_ids')->nullable(); //hs_all_owner_ids
            $table->datetime('time_of_last_visit')->nullable(); //hs_analytics_last_visit_timestamp
            $table->datetime('first_marketing_email_send_date')->nullable(); //hs_email_first_send_date
            $table->string('state_or_region')->nullable(); //state
            $table->text('all_team_ids')->nullable(); //hs_all_team_ids
            $table->text('original_source')->nullable(); //hs_analytics_source
            $table->datetime('first_marketing_email_open_date')->nullable(); //hs_email_first_open_date
            $table->integer('zip')->nullable();
            $table->string('country')->nullable();
            $table->text('all_accessible_team_ids')->nullable(); //hs_all_accessible_team_ids
            $table->text('original_source_drill-down_1')->nullable(); //hs_analytics_source_data_1
            $table->datetime('first_marketing_email_click_date')->nullable(); //hs_email_first_click_date
            $table->integer('linkedIn_connections')->nullable(); //linkedinconnections
            $table->text('original_source_drill-down_2')->nullable(); //hs_analytics_source_data_2
            $table->boolean('is_globally_ineligible')->nullable(); //hs_email_is_ineligible
            $table->string('preferred_language')->nullable(); //hs_language
            $table->text('klout_score')->nullable(); //kloutscoregeneral
            $table->string('first_referring_site')->nullable(); //hs_analytics_first_referrer
            $table->string('jobtitle')->nullable();
            $table->string('photo')->nullable();
            $table->text('last_referring_site')->nullable(); //hs_analytics_last_referrer
            $table->text('message')->nullable();
            $table->datetime('closedate')->nullable();
            $table->integer('average_page_views')->nullable(); //hs_analytics_average_page_views
            $table->string('event_revenue')->nullable(); //hs_analytics_revenue
            $table->datetime('became_a_lead_date')->nullable(); //hs_lifecyclestage_lead_date
            $table->datetime('became_a_marketing_qualified_lead_date')->nullable(); //hs_lifecyclestage_marketingqualifiedlead_date
            $table->datetime('became_an_opportunity_date')->nullable(); //hs_lifecyclestage_opportunity_date
            $table->text('lifecycle_stage')->nullable(); //lifecyclestage
            $table->datetime('became_a_sales_qualified_lead_date')->nullable(); //hs_lifecyclestage_salesqualifiedlead_date
            $table->datetime('createdate')->nullable();
            $table->datetime('became_an_evangelist_date')->nullable(); //hs_lifecyclestage_evangelist_date
            $table->datetime('became_a_customer_date')->nullable(); //hs_lifecyclestage_customer_date
            $table->integer('hubspotscore')->nullable(); 
            $table->text('company_name')->nullable(); //company
            $table->datetime('became_a_subscriber_date')->nullable(); //hs_lifecyclestage_subscriber_date
            $table->datetime('became_an_other_lifecycle_date')->nullable(); //hs_lifecyclestage_other_date
            $table->text('website_url')->nullable(); //website
            $table->string('number_of_employees')->nullable(); //numemployees
            $table->string('annual_revenue')->nullable(); //annualrevenue
            $table->string('industry')->nullable();
            $table->integer('associated_company_id')->nullable(); //associatedcompanyid
            $table->datetime('associated_company_last_updated')->nullable(); //associatedcompanylastupdated
            $table->integer('number_of_broadcast_clicks')->nullable(); //hs_social_num_broadcast_clicks
            $table->string('email_recipient_fatigue_recovery_time')->nullable(); //hs_email_recipient_fatigue_recovery_time
            $table->integer('linkedin_clicks')->nullable();
            $table->integer('google_plus_clicks')->nullable();
            $table->integer('facebook_clicks')->nullable();
            $table->integer('twitter_clicks')->nullable();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('backup_progress_id')->references('id')->on('backup_progress')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('contacts');
    }

}
