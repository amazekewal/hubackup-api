<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCalendarEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('calendar_events', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('user_id')->unsigned();
            $table->bigInteger('backup_progress_id')->unsigned();
            $table->bigInteger('event_id')->unsigned();
            $table->integer('portal_id')->nullable();
            $table->string('event_type')->nullable();
            $table->datetime('event_date')->nullable();
            $table->string('category')->nullable();
            $table->integer('category_id')->nullable();
            $table->integer('content_id')->nullable();
            $table->string('state')->nullable();
            $table->string('campaign_guid')->nullable();
            $table->string('name')->nullable();
            $table->string('description')->nullable();
            $table->string('url')->nullable();
            $table->integer('owner_id')->nullable();
            $table->integer('created_by')->nullable();
            $table->boolean('create_content')->default('0');
            $table->string('preview_key')->nullable();
            $table->string('template_path')->nullable();
            $table->string('social_username')->nullable();
            $table->string('social_display_name')->nullable();
            $table->text('avatar_url')->nullable();
            $table->boolean('is_recurring')->default('0');
            $table->json('topic_ids')->nullable(); //array
            $table->integer('content_group_id')->nullable();
            $table->integer('group_id')->nullable();
            $table->string('group_order')->nullable();
            $table->string('remote_id')->nullable();
            $table->datetime('started_date')->nullable(); 
            $table->datetime('published_date')->nullable(); 
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('backup_progress_id')->references('id')->on('backup_progress')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('calendar_events');
    }
}
