<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePublishingChannelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('publishing_channels', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('user_id')->unsigned();
            $table->bigInteger('backup_progress_id')->unsigned();
            $table->bigInteger('channel_id')->unsigned();
            $table->integer('portal_id')->nullable();
            $table->string('channel_guid')->nullable();
            $table->string('account_guid')->nullable();
            $table->string('name')->nullable();
            $table->boolean('settings_publish')->default('0');
            $table->string('type')->nullable();
            $table->string('image_url')->nullable();
            $table->string('picture')->nullable();
            $table->string('user_name')->nullable();
            $table->string('real_name')->nullable();
            $table->string('last_name')->nullable();
            $table->string('page_name')->nullable();
            $table->string('profile_url')->nullable();
            $table->string('email')->nullable();
            $table->integer('page_id')->nullable();
            $table->integer('user')->nullable();
            $table->string('full_name')->nullable();
            $table->string('first_name')->nullable();
            $table->string('page_category')->nullable();
            $table->datetime('created_on')->nullable();
            $table->datetime('updated_on')->nullable();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('backup_progress_id')->references('id')->on('backup_progress')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('publishing_channels');
    }
}
