<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCmsTemplateAttachedJsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cms_template_attached_js', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('cms_template_db_id')->unsigned();
            $table->string('js_id')->nullable();
            $table->string('type')->nullable();
            $table->foreign('cms_template_db_id')->references('id')->on('cms_templates')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cms_template_attached_js');
    }
}
