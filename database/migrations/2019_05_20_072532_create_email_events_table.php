<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmailEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('email_events', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('user_id')->unsigned();
            $table->bigInteger('backup_progress_id')->unsigned();
            $table->bigInteger('email_event_id')->unsigned();
            $table->integer('app_id')->nullable();
            $table->integer('portal_id')->nullable();
            $table->string('app_name')->nullable();
            $table->string('browser_family')->nullable();
            $table->string('browser_name')->nullable();
            $table->string('browser_producer')->nullable();
            $table->string('browser_producer_url')->nullable();
            $table->string('browser_type')->nullable();
            $table->string('browser_url')->nullable();
            $table->json('browser_version')->nullable();
            $table->datetime('created')->nullable();
            $table->integer('email_campaign_id')->nullable();
            $table->string('city')->nullable();
            $table->string('country')->nullable();
            $table->string('state')->nullable();
            $table->string('recipient')->nullable();
            $table->string('type')->nullable();
            $table->text('user_agent')->nullable();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('backup_progress_id')->references('id')->on('backup_progress')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('email_events');
    }
}
