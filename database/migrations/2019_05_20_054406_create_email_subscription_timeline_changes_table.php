<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmailSubscriptionTimelineChangesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('email_subscription_timeline_changes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('timeline_db_id')->unsigned();
            $table->integer('portal_id')->unsigned();
            $table->integer('subscription_id')->nullable();
            $table->string('change')->nullable();
            $table->string('source')->nullable();
            $table->string('change_type')->nullable();
            $table->string('caused_by_event_id')->nullable();
            $table->datetime('created')->nullable();
            $table->foreign('timeline_db_id')->references('id')->on('email_subscription_timelines')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('email_subscription_timeline_changes');
    }
}
