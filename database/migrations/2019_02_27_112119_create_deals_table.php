<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDealsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('deals', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('user_id')->unsigned();
            $table->bigInteger('backup_progress_id')->unsigned();
            $table->bigInteger('deal_id')->unsigned();
            $table->string('deal_name')->nullable(); //dealname
            $table->decimal('amount',11,3)->nullable();
            $table->decimal('amount_in_home_currency',11,3)->nullable();
            $table->integer('days_to_close')->nullable();
            $table->text('original_source_type')->nullable(); //hs_analytics_source
            $table->text('original_source_data_1')->nullable(); //hs_analytics_source_data_1
            $table->text('original_source_data_2')->nullable(); //hs_analytics_source_data_2
            $table->text('deal_amount_calculation_preference')->nullable(); //hs_deal_amount_calculation_preference
            $table->datetime('owner_assigned_date')->nullable();//hubspot_owner_assigneddate
            $table->string('deal_stage')->nullable(); //dealstage
            $table->string('pipeline')->nullable();
            $table->datetime('closedate')->nullable();
            $table->text('last_meeting_booked')->nullable(); //engagements_last_meeting_booked
            $table->text('last_meeting_booked_campaign')->nullable(); //engagements_last_meeting_booked_campaign
            $table->text('last_meeting_booked_medium')->nullable(); //engagements_last_meeting_booked_medium
            $table->text('last_meeting_booked_source')->nullable(); //engagements_last_meeting_booked_source
            $table->datetime('recent_sales_email_replied_date')->nullable(); //hs_sales_email_last_replied
            $table->integer('deal_owner')->nullable(); //hubspot_owner_id
            $table->datetime('last_contacted')->nullable(); //notes_last_contacted
            $table->datetime('last_activity_date')->nullable(); //notes_last_updated
            $table->datetime('next_activity_date')->nullable(); //notes_next_activity_date
            $table->integer('number_of_times_contacted')->nullable(); //num_contacted_notes
            $table->integer('number_of_sales_activities')->nullable(); //num_notes
            $table->datetime('createdate')->nullable(); 
            $table->datetime('last_modified_date')->nullable();
            $table->string('hubspot_team')->nullable(); //hubspot_team_id
            $table->string('deal_type')->nullable(); //dealtype
            $table->text('all_owner_ids')->nullable(); //hs_all_owner_ids
            $table->text('deal_description')->nullable(); //description
            $table->text('all_team_ids')->nullable(); //hs_all_team_ids
            $table->text('all_accessible_team_ids')->nullable(); //hs_all_accessible_team_ids
            $table->integer('number_of_contacts')->nullable(); //num_associated_contacts
            $table->string('closed_lost_reason')->nullable();
            $table->string('closed_won_reason')->nullable();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('backup_progress_id')->references('id')->on('backup_progress')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('deals');
    }
}
