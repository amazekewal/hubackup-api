<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAnalyticsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('analytics', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('user_id')->unsigned();
            $table->bigInteger('backup_progress_id')->unsigned();
            $table->string('breakdown')->nullable();
            $table->integer('raw_views')->nullable();
            $table->integer('visits')->nullable();
            $table->integer('visitors')->nullable();
            $table->integer('leads')->nullable();
            $table->integer('contacts')->nullable();
            $table->integer('subscribers')->nullable();
            $table->integer('marketing_qualified_leads')->nullable();
            $table->integer('sales_qualified_leads')->nullable();
            $table->integer('opportunities')->nullable();
            $table->integer('customers')->nullable();
            $table->string('pageviews_per_session')->nullable();
            $table->string('bounce_rate')->nullable();
            $table->string('time_per_session')->nullable();
            $table->string('new_visitor_session_rate')->nullable();
            $table->string('session_to_contact_rate')->nullable();
            $table->string('contact_to_customer_rate')->nullable();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('backup_progress_id')->references('id')->on('backup_progress')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('analytics');
    }
}
