<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAnalyticViewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('analytic_views', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('user_id')->unsigned();
            $table->bigInteger('backup_progress_id')->unsigned();
            $table->bigInteger('analytic_view_id')->unsigned();
            $table->string('title')->nullable();
            $table->integer('creator_id')->nullable();
            $table->integer('updater_id')->nullable();
            $table->tinyInteger('contains_legacy_report_properties')->nullable();
            $table->tinyInteger('deleted_at')->nullable();
            $table->string('report_property_filters_op')->nullable();
            $table->string('report_property_filters_args')->nullable();
            $table->string('report_property_filters_prop')->nullable();
            $table->datetime('created_date')->nullable();
            $table->datetime('updated_date')->nullable();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('backup_progress_id')->references('id')->on('backup_progress')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('analytic_views');
    }
}
