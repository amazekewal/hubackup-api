<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWorkflowMetasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('workflow_metas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('workflow_db_id')->unsigned();
            $table->boolean('internal')->default('0');
            $table->boolean('only_exec_on_biz_days')->default('0');
            $table->boolean('nurture_time_range_enabled')->default('0');
            $table->integer('nurture_time_range_start_hour')->nullable();
            $table->integer('nurture_time_range_stop_hour')->nullable();
            $table->boolean('listening')->default('0');
            $table->string('allow_contact_to_trigger_multiple_times')->nullable();
            $table->string('unenrollment_setting_type')->nullable();
            $table->json('unenrollment_setting_type_excluded_workflows')->nullable();
            $table->string('recurring_setting_type')->nullable();
            $table->boolean('enroll_on_criteria_update')->default('0');
            $table->boolean('only_enrolls_manually')->default('0');
            $table->string('goal_criteria_property_object_type')->nullable();
            $table->string('goal_criteria_filter_family')->nullable();
            $table->string('goal_criteria_within_time_mode')->nullable();
            $table->string('goal_criteria_property')->nullable();
            $table->string('goal_criteria_value')->nullable();
            $table->string('goal_criteria_type')->nullable();
            $table->string('goal_criteria_operator')->nullable();
            $table->json('re_enrollment_trigger_sets')->nullable();
            $table->json('suppression_list_ids')->nullable();
            $table->string('last_updated_by')->nullable();
            $table->json('segment_criteria')->nullable();
            $table->json('triggered_by_workflow_ids')->nullable();
            $table->string('succeeded_list_id')->nullable();
            $table->integer('contact_list_ids_enrolled')->nullable();
            $table->integer('contact_list_ids_active')->nullable();
            $table->json('contact_list_ids_steps')->nullable();
            $table->integer('contact_list_ids_succeeded')->nullable();
            $table->integer('contact_list_ids_completed')->nullable();
            $table->foreign('workflow_db_id')->references('id')->on('workflows')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('workflow_metas');
    }
}
