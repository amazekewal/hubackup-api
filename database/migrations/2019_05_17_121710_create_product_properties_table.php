<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductPropertiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_properties', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('product_db_id')->unsigned();
            $table->string('property_name')->nullable();
            $table->string('value')->nullable();
            $table->datetime('timestamp')->nullable();
            $table->string('source')->nullable();
            $table->string('source_id')->nullable();
            $table->json('source_vid')->nullable();
            $table->string('request_id')->nullable();
            $table->foreign('product_db_id')->references('id')->on('products')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_properties');
    }
}
