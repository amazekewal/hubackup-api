<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCmsLayoutRowsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cms_layout_rows', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('cms_layout_db_id')->unsigned();
            $table->string('row_id')->nullable();
            $table->boolean('cell')->default('0');
            $table->json('cells')->nullable();
            $table->text('css_class')->nullable();
            $table->string('css_id')->nullable();
            $table->string('css_id_str')->nullable();
            $table->string('css_style')->nullable();
            $table->boolean('editable')->default('0');
            $table->boolean('is_container')->default('0');
            $table->boolean('is_content_overridden')->default('0');
            $table->boolean('is_in_container')->default('0');
            $table->string('label')->nullable();
            $table->json('language_overrides')->nullable();
            $table->string('name')->nullable();
            $table->integer('order')->nullable();
            $table->json('params')->nullable();
            $table->boolean('root')->default('0');
            $table->boolean('row')->default('0');
            $table->json('row_meta_data')->nullable();
            $table->string('type')->nullable();
            $table->integer('w')->nullable();
            $table->json('widgets')->nullable();
            $table->integer('x')->nullable();
            $table->foreign('cms_layout_db_id')->references('id')->on('cms_layouts')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cms_layout_rows');
    }
}
