<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTicketsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tickets', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('user_id')->unsigned();
            $table->bigInteger('backup_progress_id')->unsigned();
            $table->bigInteger('ticket_id')->unsigned();
            $table->string('subject')->nullable();
            $table->string('pipeline')->nullable(); //hs_pipeline
            $table->string('pipeline_stage')->nullable(); //hs_pipeline_stage
            $table->text('content')->nullable();
            $table->string('created_by')->nullable();
            $table->string('source_type')->nullable();
            $table->string('status')->nullable();
            $table->datetime('createdate')->nullable();
            $table->datetime('last_modified_date')->nullable(); //hs_lastmodifieddate
            $table->datetime('closedate')->nullable();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('backup_progress_id')->references('id')->on('backup_progress')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tickets');
    }
}
