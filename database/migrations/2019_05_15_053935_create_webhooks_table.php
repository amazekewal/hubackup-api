<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWebhooksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('webhooks', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('user_id')->unsigned();
            $table->integer('object_id')->nullable();
            $table->string('property_name')->nullable();
            $table->string('property_value')->nullable();
            $table->string('change_source')->nullable();
            $table->integer('event_id')->nullable();
            $table->integer('subscription_id')->nullable();
            $table->integer('portal_id')->nullable();
            $table->integer('app_id')->nullable();
            $table->datetime('occurred_at')->nullable();
            $table->string('subscription_type')->nullable();
            $table->integer('attempt_number')->nullable();
            $table->string('change_flag')->nullable();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('webhooks');
    }
}
