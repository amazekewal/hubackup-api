<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMarketingEmailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('marketing_emails', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('user_id')->unsigned();
            $table->bigInteger('backup_progress_id')->unsigned();
            $table->bigInteger('marketing_email_id')->unsigned();
            $table->integer('portal_id')->nullable();
            $table->boolean('ab')->default('0');
            $table->integer('ab_hours_to_wait')->nullable();
            $table->string('ab_sample_size_default')->nullable();
            $table->string('ab_sampling_default')->nullable();
            $table->string('ab_success_metric')->nullable();
            $table->integer('ab_test_percentage')->nullable();
            $table->boolean('ab_variation')->default('0');
            $table->text('absolute_url')->nullable();
            $table->json('all_email_campaign_ids')->nullable();
            $table->integer('analytics_page_id')->nullable();
            $table->string('analytics_page_type')->nullable();
            $table->boolean('archived')->default('0');
            $table->string('author_name')->nullable();
            $table->datetime('author_at')->nullable();
            $table->string('author_email')->nullable();
            $table->integer('author_user_id')->nullable();
            $table->string('blog_rss_settings')->nullable();
            $table->integer('can_spam_settings_id')->nullable();
            $table->integer('category_id')->nullable();
            $table->integer('content_type_category')->nullable();
            $table->boolean('create_page')->default('0');
            $table->string('current_state')->nullable();
            $table->boolean('currently_published')->default('0');
            $table->string('domain')->nullable();
            $table->text('email_body')->nullable();
            $table->string('email_note')->nullable();
            $table->string('email_template_mode')->nullable();
            $table->string('email_type')->nullable();
            $table->string('feedback_email_category')->nullable();
            $table->integer('feedback_survey_id')->nullable();
            $table->json('flex_areas')->nullable();
            $table->datetime('freeze_date')->nullable();
            $table->string('from_name')->nullable();
            $table->string('html_title')->nullable();
            $table->boolean('is_graymail_suppression_enabled')->default('0');
            $table->boolean('is_local_timezone_send')->default('0');
            $table->boolean('is_published')->default('0');
            $table->boolean('is_recipient_fatigue_suppression_enabled')->default('0');
            $table->integer('last_edit_session_id')->nullable();
            $table->integer('last_edit_update_id')->nullable();
            $table->json('layout_sections')->nullable();
            $table->integer('lead_flow_id')->nullable();
            $table->string('live_domain')->nullable();
            $table->json('mailing_lists_excluded')->nullable();
            $table->json('mailing_lists_included')->nullable();
            $table->integer('max_rss_entries')->nullable();
            $table->string('name')->nullable();
            $table->boolean('page_expiry_enabled')->default('0');
            $table->boolean('page_redirected')->default('0');
            $table->string('preview_key')->nullable();
            $table->string('processing_status')->nullable();
            $table->datetime('publish_date')->nullable();
            $table->boolean('publish_immediately')->default('0');
            $table->string('published_url')->nullable();
            $table->string('reply_to')->nullable();
            $table->string('resolved_domain')->nullable();
            $table->string('rss_email_by_text')->nullable();
            $table->string('rss_email_click_through_text')->nullable();
            $table->string('rss_email_comment_text')->nullable();
            $table->boolean('rss_email_entry_template_enabled')->default('0');
            $table->integer('rss_email_image_max_width')->nullable();
            $table->string('rss_email_url')->nullable();
            $table->boolean('scrubs_subscription_links')->default('0');
            $table->string('slug')->nullable();
            $table->text('smart_email_fields')->nullable();
            $table->string('state')->nullable();
            $table->string('style_settings')->nullable();
            $table->string('subcategory')->nullable();
            $table->string('subject')->nullable();
            $table->string('subscription')->nullable();
            $table->string('subscription_name')->nullable();
            $table->json('team_perms')->nullable();
            $table->string('template_path')->nullable();
            $table->boolean('transactional')->default('0');
            $table->datetime('unpublished_at')->nullable();
            $table->string('url')->nullable();
            $table->boolean('use_rss_headline_as_subject')->default('0');
            $table->json('user_perms')->nullable();
            $table->json('vids_excluded')->nullable();
            $table->json('vids_included')->nullable();
            $table->json('workflow_names')->nullable();
            $table->datetime('created')->nullable();
            $table->datetime('updated')->nullable();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('backup_progress_id')->references('id')->on('backup_progress')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('marketing_emails');
    }
}
