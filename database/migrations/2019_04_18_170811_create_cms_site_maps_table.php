<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCmsSiteMapsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cms_site_maps', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('user_id')->unsigned();
            $table->bigInteger('backup_progress_id')->unsigned();
            $table->bigInteger('cms_site_map_id');
            $table->bigInteger('portal_id')->nullable();
            $table->string('name')->nullable();
            $table->json('blog_posts')->nullable();
            $table->json('content_options')->nullable();
            $table->string('domain')->nullable();
            $table->boolean('has_user_changes')->default('0');
            $table->json('image_metas')->nullable();
            $table->datetime('initiated_at')->nullable();
            $table->json('knowledge_articles')->nullable();
            $table->json('landing_pages')->nullable();
            $table->json('site_pages')->nullable();
            $table->json('video_metas')->nullable();
            $table->integer('deleted_at')->nullable();
            $table->datetime('created')->nullable();
            $table->datetime('updated')->nullable();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('backup_progress_id')->references('id')->on('backup_progress')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cms_site_maps');
    }
}
