<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBlogPostWidgetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cms_blog_post_widgets', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('cms_blog_post_db_id')->unsigned();
            $table->text('body_html')->nullable(); 
            $table->json('child_css')->nullable(); 
            $table->json('css')->nullable(); 
            $table->string('label')->nullable();
            $table->string('name')->nullable();
            $table->integer('order')->nullable();
            $table->string('smart_type')->nullable();
            $table->string('type')->nullable();
            $table->foreign('cms_blog_post_db_id')->references('id')->on('cms_blog_posts')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cms_blog_post_widgets');
    }
}
