<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmailCampaignsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('email_campaigns', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('user_id')->unsigned();
            $table->bigInteger('backup_progress_id')->unsigned();
            $table->bigInteger('email_campaign_id')->nullable();
            $table->integer('app_id')->nullable();
            $table->string('app_name')->nullable();
            $table->integer('content_id')->nullable();
            $table->integer('counters_delivered')->nullable();
            $table->integer('counters_open')->nullable();
            $table->integer('counters_processed')->nullable();
            $table->integer('counters_sent')->nullable();
            $table->string('name')->nullable();
            $table->integer('num_included')->nullable();
            $table->integer('num_queued')->nullable();
            $table->string('sub_type')->nullable();
            $table->string('subject')->nullable();
            $table->string('type')->nullable();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('backup_progress_id')->references('id')->on('backup_progress')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('email_campaigns');
    }
}
