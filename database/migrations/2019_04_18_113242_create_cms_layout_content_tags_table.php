<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCmsLayoutContentTagsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cms_layout_content_tags', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('cms_layout_db_id')->unsigned();
            $table->string('name')->nullable();
            $table->string('source')->nullable();
            $table->foreign('cms_layout_db_id')->references('id')->on('cms_layouts')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cms_layout_content_tags');
    }
}
