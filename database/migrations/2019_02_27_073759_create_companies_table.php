<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompaniesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('companies', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('user_id')->unsigned();
            $table->bigInteger('backup_progress_id')->unsigned();
            $table->bigInteger('company_id')->unsigned();
            $table->string('name')->nullable();
            $table->string('website')->nullable();
            $table->text('about_us')->nullable();
            $table->integer('facebook_fans')->nullable(); //facebookfans
            $table->datetime('first_deal_created_date')->nullable();
            $table->string('founded_year')->nullable();
            $table->datetime('time_first_seen')->nullable(); //hs_analytics_first_timestamp
            $table->text('first_touch_converting_campaign')->nullable(); //hs_analytics_first_touch_converting_campaign
            $table->datetime('time_of_first_visit')->nullable(); //hs_analytics_first_visit_timestamp
            $table->datetime('time_last_seen')->nullable(); //hs_analytics_last_timestamp
            $table->text('last_touch_converting_campaign')->nullable(); //hs_analytics_last_touch_converting_campaign
            $table->datetime('time_of_last_session')->nullable(); //hs_analytics_last_visit_timestamp
            $table->integer('number_of_pageviews')->nullable(); //hs_analytics_num_page_views
            $table->integer('number_of_visits')->nullable(); //hs_analytics_num_visits
            $table->text('original_source_type')->nullable(); //hs_analytics_source
            $table->text('original_source_data_1')->nullable(); //hs_analytics_source_data_1
            $table->text('original_source_data_2')->nullable(); //hs_analytics_source_data_2
            $table->text('avatar_filemanager_key')->nullable(); //hs_avatar_filemanager_key
            $table->datetime('last_modified_date')->nullable(); //hs_lastmodifieddate
            $table->string('target_account')->nullable(); //hs_target_account
            $table->datetime('owner_assigned_date')->nullable();//hubspot_owner_assigneddate
            $table->string('is_public')->nullable();
            $table->integer('associated_contacts')->nullable(); //num_associated_contacts
            $table->integer('associated_deals')->nullable(); //num_associated_deals
            $table->string('recent_deal_amount')->nullable();
            $table->datetime('recent_deal_close_date')->nullable();
            $table->string('timezone')->nullable();
            $table->string('total_money_raised')->nullable();
            $table->string('total_revenue')->nullable();
            $table->string('twitter_handle')->nullable(); //twitterhandle
            $table->string('phone_number')->nullable(); //phone
            $table->text('twitter_bio')->nullable(); //twitterbio
            $table->integer('twitterfollowers')->nullable();
            $table->string('address')->nullable();
            $table->string('address2')->nullable();
            $table->string('facebook_company_page')->nullable();
            $table->string('city')->nullable();
            $table->string('linkedin_company_page')->nullable();
            $table->text('linkedin_bio')->nullable(); //linkedinbio
            $table->string('state_or_region')->nullable(); //state
            $table->string('googleplus_page')->nullable();
            $table->text('last_meeting_booked')->nullable(); //engagements_last_meeting_booked
            $table->text('last_meeting_booked_campaign')->nullable(); //engagements_last_meeting_booked_campaign
            $table->text('last_meeting_booked_medium')->nullable(); //engagements_last_meeting_booked_medium
            $table->text('last_meeting_booked_source')->nullable(); //engagements_last_meeting_booked_source
            $table->datetime('recent_sales_email_replied_date')->nullable(); //hs_sales_email_last_replied
            $table->integer('contact_owner')->nullable(); //hubspot_owner_id
            $table->datetime('last_contacted')->nullable(); //notes_last_contacted
            $table->datetime('last_activity_date')->nullable(); //notes_last_updated
            $table->datetime('next_activity_date')->nullable(); //notes_next_activity_date
            $table->integer('number_of_times_contacted')->nullable(); //num_contacted_notes
            $table->integer('number_of_sales_activities')->nullable(); //num_notes
            $table->integer('zip')->nullable();
            $table->string('country')->nullable();
            $table->string('hubspot_team')->nullable(); //hubspot_team_id
            $table->text('all_owner_ids')->nullable(); //hs_all_owner_ids
            $table->text('website_url')->nullable(); //website
            $table->string('company_domain_name')->nullable(); //domain
            $table->text('all_team_ids')->nullable(); //hs_all_team_ids
            $table->text('all_accessible_team_ids')->nullable(); //hs_all_accessible_team_ids
            $table->integer('number_of_employees')->nullable(); //numberofemployees
            $table->string('industry')->nullable();
            $table->string('annual_revenue')->nullable(); //annualrevenue
            $table->string('lifecycle_stage')->nullable(); //lifecyclestage
            $table->string('lead_status')->nullable(); //hs_lead_status
            $table->integer('parent_company')->nullable(); //hs_parent_company_id
            $table->string('type')->nullable();
            $table->text('description')->nullable();
            $table->integer('number_of_child_companies')->nullable(); //num_child_companies
            $table->datetime('createdate')->nullable(); 
            $table->datetime('closedate')->nullable();
            $table->datetime('first_contact_createdate')->nullable();
            $table->integer('days_to_close')->nullable();
            $table->string('web_technologies')->nullable();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('backup_progress_id')->references('id')->on('backup_progress')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('companies');
    }
}
