<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBackupHistoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('backup_history', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('user_id')->unsigned();
            $table->bigInteger('backup_progress_id')->unsigned();
            $table->string('object_name')->nullable();
            $table->integer('num_of_records')->nullable();
            $table->string('object_size')->nullable();
            $table->integer('removed_records')->nullable();
            $table->integer('added_records')->nullable();
            $table->integer('changed_records')->nullable();
            $table->integer('num_of_API_calls')->nullable();
            $table->string('status',20)->nullable();
            $table->string('error_message')->nullable();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('backup_progress_id')->references('id')->on('backup_progress')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('backup_history');
    }
}
