<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCmsLayoutsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cms_layouts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('user_id')->unsigned();
            $table->bigInteger('backup_progress_id')->unsigned();
            $table->bigInteger('layout_id')->nullable();
            $table->bigInteger('portal_id')->nullable();
            $table->integer('attached_amp_stylesheet')->nullable();
            $table->string('author')->nullable();
            $table->datetime('author_at')->nullable();
            $table->boolean('enable_domain_stylesheets')->default('0');
            $table->boolean('include_default_custom_css')->default('0');
            $table->string('folder')->nullable();
            $table->integer('folder_id')->nullable();
            $table->datetime('cdn_purge_embargo_time')->nullable();
            $table->text('custom_head')->nullable();
            $table->string('body_css_id')->nullable();
            $table->text('custom_footer')->nullable();
            $table->string('body_class')->nullable();
            $table->string('body_css')->nullable();
            $table->boolean('builtin')->default('0');
            $table->boolean('purchased')->default('0');
            $table->string('generator')->nullable();
            $table->integer('generated_template_id')->nullable();
            $table->integer('marketplace_good_id')->nullable();
            $table->string('cloned_from')->nullable();
            $table->boolean('body_editable')->default('0');
            $table->string('last_edit_session_id')->nullable();
            $table->string('last_edit_update_id')->nullable();
            $table->boolean('has_user_changes')->nullable();
            $table->integer('marketplace_delivery_id')->nullable();
            $table->boolean('is_read_only')->nullable();
            $table->json('host_template_types')->nullable();
            $table->integer('marketplace_version')->nullable();
            $table->boolean('should_create_hubshot')->default('0');
            $table->boolean('is_default')->default('0');
            $table->integer('category_id')->nullable();
            $table->boolean('deleted_at')->default('0');
            $table->boolean('is_available_for_new_content')->default('0');
            $table->json('languages')->nullable();
            $table->string('label')->nullable();
            $table->boolean('has_buffered_changes')->default('0');
            $table->string('path')->nullable();
            $table->integer('template_type')->nullable();
            $table->string('updated_by')->nullable();
            $table->integer('updated_by_id')->nullable();
            $table->boolean('layout_data_cell')->default('0');
            $table->json('layout_data_cells')->nullable();
            $table->text('layout_data_css_class')->nullable();
            $table->string('layout_data_css_id')->nullable();
            $table->string('layout_data_css_id_str')->nullable();
            $table->string('layout_data_css_style')->nullable();
            $table->boolean('layout_data_editable')->default('0');
            $table->string('layout_data_id')->nullable();
            $table->boolean('layout_data_is_container')->default('0');
            $table->boolean('layout_data_is_content_overridden')->default('0');
            $table->boolean('layout_data_is_in_container')->default('0');
            $table->json('layout_data_language_overrides')->nullable();
            $table->integer('layout_data_order')->nullable();
            $table->json('layout_data_params')->nullable();
            $table->boolean('layout_data_root')->default('0');
            $table->boolean('layout_data_row')->default('0');
            $table->json('layout_data_row_meta_data')->nullable();
            $table->string('layout_data_type')->nullable();
            $table->integer('layout_data_w')->nullable();
            $table->json('layout_data_widgets')->nullable();
            $table->integer('layout_data_x')->nullable();
            $table->datetime('created')->nullable();
            $table->datetime('updated')->nullable();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('backup_progress_id')->references('id')->on('backup_progress')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cms_layouts');
    }
}
