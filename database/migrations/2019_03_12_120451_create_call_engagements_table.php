<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCallEngagementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('call_engagements', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('engagement_db_id')->unsigned();
            $table->string('to_number')->nullable();
            $table->string('from_number')->nullable();
            $table->string('status')->nullable();
            $table->string('external_id')->nullable();
            $table->bigInteger('duration_milliseconds')->nullable();
            $table->string('external_account_id')->nullable();
            $table->string('recording_url')->nullable();
            $table->string('body')->nullable();
            $table->string('disposition')->nullable();
            $table->foreign('engagement_db_id')->references('id')->on('engagements')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('call_engagements');
    }
}
