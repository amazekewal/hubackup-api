<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePagePublishingWidgetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cms_page_publishing_widgets', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('page_publishing_db_id')->unsigned();
            $table->string('widget_container')->nullable();
            $table->integer('widget_id')->nullable();
            $table->string('body_name')->nullable();
            $table->boolean('body_is_widget_block')->default('0');
            $table->string('body_type')->nullable();
            $table->string('body_tag')->nullable();
            $table->string('body_label')->nullable();
            $table->string('body_widget_type')->nullable();
            $table->string('body_html')->nullable();
            $table->boolean('body_overrideable')->default('0');
            $table->boolean('body_export_to_template_context')->default('0');
            $table->string('body_org_tag')->nullable();
            $table->boolean('body_no_wrapper')->default('0');
            $table->integer('group')->nullable();
            $table->string('body_fields_initial')->nullable();
            $table->string('body_fields_type')->nullable();
            $table->string('body_fields_name')->nullable();
            $table->string('body_fields_label')->nullable();
            $table->string('label')->nullable();
            $table->json('meta')->nullable(); 
            $table->string('common_name')->nullable();
            $table->boolean('common_is_widget_block')->default('0');
            $table->string('common_type')->nullable();
            $table->string('common_tag')->nullable();
            $table->string('common_label')->nullable();
            $table->string('common_widget_type')->nullable();
            $table->string('common_html')->nullable();
            $table->boolean('common_overrideable')->default('0');
            $table->boolean('common_export_to_template_context')->default('0');
            $table->string('common_org_tag')->nullable();
            $table->boolean('common_no_wrapper')->default('0');
            $table->string('key')->nullable();
            $table->integer('order')->nullable();
            $table->string('type')->nullable();
            $table->foreign('page_publishing_db_id')->references('id')->on('cms_page_publishing')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cms_page_publishing_widgets');
    }
}
