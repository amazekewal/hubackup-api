<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBroadcastMessagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('broadcast_messages', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('user_id')->unsigned();
            $table->bigInteger('backup_progress_id')->unsigned();
            $table->integer('portal_id')->unsigned();
            $table->string('broadcast_guid')->nullable();
            $table->string('group_guid')->nullable();
            $table->string('campaign_guid')->nullable();
            $table->string('channel_guid')->nullable();
            $table->string('client_tag')->nullable();
            $table->string('status')->nullable();
            $table->string('message')->nullable();
            $table->text('body')->nullable();
            $table->boolean('link_preview_suppressed')->default('0');
            $table->string('link')->nullable();
            $table->text('original_body')->nullable();
            $table->string('uncompressed_links')->nullable();
            $table->string('link_guid')->nullable();
            $table->string('message_url')->nullable();
            $table->integer('foreign_id')->nullable();
            $table->string('task_queue_id')->nullable();
            $table->string('link_task_queue_id')->nullable();
            $table->string('remote_content_id')->nullable();
            $table->string('remote_content_type')->nullable();
            $table->integer('clicks')->nullable();
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->text('interactions')->nullable();
            $table->text('interaction_counts')->nullable();
            $table->datetime('created_on')->nullable();
            $table->datetime('trigger_at')->nullable();
            $table->datetime('finished_at')->nullable();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('backup_progress_id')->references('id')->on('backup_progress')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('broadcast_messages');
    }
}
