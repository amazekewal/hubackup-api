<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePagePublishingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cms_page_publishing', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('user_id')->unsigned();
            $table->bigInteger('backup_progress_id')->unsigned();
            $table->bigInteger('page_publishing_id')->nullable();
            $table->string('ab_status')->nullable();
            $table->string('ab_test_id')->nullable();
            $table->integer('analytics_page_id')->nullable();
            $table->boolean('archived')->default('0');
            $table->json('attached_stylesheets')->nullable(); 
            $table->integer('author_user_id')->nullable();
            $table->integer('blueprint_type_id')->nullable();
            $table->string('campaign')->nullable();
            $table->string('campaign_name')->nullable();
            $table->string('cloned_from')->nullable();
            $table->text('compose_body')->nullable();
            $table->json('css')->nullable(); 
            $table->string('css_text')->nullable();
            $table->string('current_live_domain')->nullable();
            $table->boolean('deleted_at')->default('0');
            $table->string('deleted_by')->nullable();
            $table->string('domain')->nullable();
            $table->boolean('enable_domain_stylesheets')->default('0');
            $table->boolean('enable_layout_stylesheets')->default('0');
            $table->string('featured_image')->nullable();
            $table->string('featured_image_alt_text')->nullable();
            $table->json('flex_areas')->nullable(); 
            $table->string('folder_id')->nullable();
            $table->string('footer_html')->nullable();
            $table->datetime('freeze_date')->nullable();
            $table->boolean('has_smart_content')->default('0');
            $table->boolean('has_user_changes')->default('0');
            $table->string('head_html')->nullable();
            $table->string('html_title')->nullable();
            $table->string('include_default_custom_css')->nullable();
            $table->boolean('is_draft')->default('0');
            $table->json('keywords')->nullable(); 
            $table->string('language')->nullable();
            $table->string('last_edit_session_id')->nullable();
            $table->string('last_edit_update_id')->nullable();
            $table->string('legacy_blog_tabid')->nullable();
            $table->string('meta_description')->nullable();
            $table->string('meta_keywords')->nullable();
            $table->string('name')->nullable();
            $table->datetime('page_expiry_date')->nullable();
            $table->string('page_expiry_enabled')->nullable();
            $table->string('page_expiry_redirect_id')->nullable();
            $table->string('page_expiry_redirect_url')->nullable();
            $table->string('page_redirected')->nullable();
            $table->string('password')->nullable();
            $table->string('performable_url')->nullable();
            $table->json('personas')->nullable(); 
            $table->integer('portal_id')->nullable();
            $table->datetime('publish_date')->nullable();
            $table->boolean('publish_immediately')->default('0');
            $table->string('published_url')->nullable();
            $table->boolean('rss_email_author_line_template')->default('0');
            $table->integer('rss_email_blog_image_max_width')->nullable();
            $table->string('rss_email_by_text')->nullable();
            $table->string('rss_email_click_through_text')->nullable();
            $table->string('rss_email_comment_text')->nullable();
            $table->string('rss_email_entry_template')->nullable();
            $table->boolean('rss_email_entry_template_enabled')->default('0');
            $table->integer('rss_email_image_max_width')->nullable();
            $table->datetime('scheduled_update_date')->nullable();
            $table->string('slug')->nullable();
            $table->string('staged_from')->nullable();
            $table->string('style_override_id')->nullable();
            $table->string('subcategory')->nullable();
            $table->string('template_path')->nullable();
            $table->string('tms_id')->nullable();
            $table->string('translated_from_id')->nullable();
            $table->datetime('unpublished_at')->nullable();
            $table->string('url')->nullable();
            $table->datetime('created')->nullable();
            $table->datetime('updated')->nullable();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('backup_progress_id')->references('id')->on('backup_progress')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cms_page_publishing');
    }
}
