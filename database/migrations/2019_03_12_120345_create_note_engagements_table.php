<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNoteEngagementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('note_engagements', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('engagement_db_id')->unsigned();
            $table->text('body')->nullable();
            $table->foreign('engagement_db_id')->references('id')->on('engagements')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('note_engagements');
    }
}
