<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFormFieldsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('form_fields', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('form_db_id')->unsigned();
            $table->string('name')->nullable();
            $table->string('label')->nullable();
            $table->string('type')->nullable();
            $table->string('field_type')->nullable();
            $table->string('description')->nullable();
            $table->string('group_name')->nullable();
            $table->integer('display_order')->nullable();
            $table->json('selected_options')->nullable(); //array
            $table->json('options')->nullable(); //array
            $table->string('validation_name')->nullable();
            $table->string('validation_message')->nullable();
            $table->string('validation_data')->nullable();
            $table->boolean('validation_use_default_block_list')->default('0');
            $table->json('validation_blocked_email_addresses')->nullable(); //array
            $table->boolean('hidden')->default('0');
            $table->boolean('enabled')->default('0');
            $table->string('default_value')->nullable();
            $table->boolean('is_smart_field')->default('0');
            $table->string('unselected_label')->nullable();
            $table->string('placeholder')->nullable();
            $table->json('dependent_field_filters')->nullable(); //array
            $table->boolean('label_hidden')->default('0');
            $table->string('property_object_type')->nullable();
            $table->boolean('default')->default('0');
            $table->boolean('is_smart_group')->default('0');
            $table->string('rich_text_content')->nullable();
            $table->foreign('form_db_id')->references('id')->on('forms')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('form_fields');
    }
}
