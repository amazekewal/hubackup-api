<?php

use Dingo\Api\Routing\Router;

/** @var Router $api */
$api = app(Router::class);

$api->version('v1', function (Router $api) {

    // *** in app / authenticated  user routes *** // 
   //send access token and refresh token from hubspot authentication
   $api->get('hubspot-connection', 'App\\Api\\V1\\Controllers\\HubSpotController@getConnection');

    //send access token and refresh token from hubspot authentication
    $api->post('hubspot-authentication', 'App\\Api\\V1\\Controllers\\HubSpotController@authentication');

    //go to backup page
    $api->get('hubspot-backup', 'App\\Api\\V1\\Controllers\\HubSpotController@backup');

    //go to backup history page
    $api->get('hubspot-backup-history/{id}', 'App\\Api\\V1\\Controllers\\HubSpotController@backup_history');
   

    //get progress of backup in dashboard page
    $api->get('hubspot-backup-progress', 'App\\Api\\V1\\Controllers\\HubSpotController@backup_progress');
    
     //request to backup now
    $api->get('backup-now', 'App\\Api\\V1\\Controllers\\HubSpotController@backup_now');
    
    //request to backup now
    $api->get('find-backup-dates', 'App\\Api\\V1\\Controllers\\HubSpotController@get_find_backup_dates');
    
    //request to backup now
    $api->post('search', 'App\\Api\\V1\\Controllers\\HubSpotController@search');
    
    
    /** dashboard routes **/
    
    $api->group(['prefix' => 'dashboard'], function(Router $api) {
        
        //get all backups
        $api->post('/all-backups', 'App\\Api\\V1\\Controllers\\DashboardController@all_backups')->name('all-backups');

        //get success backups
        $api->post('/success-backups', 'App\\Api\\V1\\Controllers\\DashboardController@success_backups')->name('success-backups');
        
        //get warning backups
        $api->post('/warning-backups', 'App\\Api\\V1\\Controllers\\DashboardController@warning_backups')->name('warning-backups');
        
        //get error backups
        $api->post('/error-backups', 'App\\Api\\V1\\Controllers\\DashboardController@error_backups')->name('error-backups');
        
        //get running backups
        $api->post('/running-backups', 'App\\Api\\V1\\Controllers\\DashboardController@running_backups')->name('running-backups');
        
        //get specific backup
        $api->get('/get-backup/{id}', 'App\\Api\\V1\\Controllers\\DashboardController@get_backup')->name('get-backup');
        });

     /** notification routes * */
   
    $api->group(['prefix' => 'notifications'], function(Router $api) {
        //go to notifications page
        $api->get('/', 'App\\Api\\V1\\Controllers\\HubSpotController@notifications')->name('notifications');

        //request to delete selected notifications
        $api->post('/destroy', 'App\\Api\\V1\\Controllers\\HubSpotController@notifications_destroy')->name('destroy-notifications');

        //show notification
        $api->get('/show/{id}', 'App\\Api\\V1\\Controllers\\HubSpotController@show_notification')->name('show-notification');
        
        //unread notifications
        $api->get('/unread', 'App\\Api\\V1\\Controllers\\HubSpotController@unread_notifications')->name('unread-notifications');
        

    });

    //smart alert routes
    $api->group(['prefix' => 'smartalert'], function(Router $api) {
        //get smart alerts
        $api->get('/', 'App\\Api\\V1\\Controllers\\SmartAlertController@index')->name('smart-alert');

        //create smart alert
        $api->post('/create', 'App\\Api\\V1\\Controllers\\SmartAlertController@store')->name('create-smart-alert');

        //delete smart alert
        $api->post('/destroy', 'App\\Api\\V1\\Controllers\\SmartAlertController@destroy')->name('destroy-smart-alert');

        //get  history of smart alert
        $api->get('/history', 'App\\Api\\V1\\Controllers\\SmartAlertController@history')->name('history-smart-alert');

        //delete hostory of smart alert
        $api->post('/history/destroy', 'App\\Api\\V1\\Controllers\\SmartAlertController@history_destroy')->name('destroy-history-smart-alert');
    
        //get objects for smart alerts
        $api->get('/objects', 'App\\Api\\V1\\Controllers\\SmartAlertController@hubackup_objects')->name('objects');
    });
    
    //get webhook notifications from hubspot
   // $api->post('webhook', 'App\\Api\\V1\\Controllers\\WebhookController@index');
    
    //stripe payment gateway
    $api->post('stripe', 'App\\Api\\V1\\Controllers\\StripeController@index');
    
    
    //admin routes
    $api->group(['prefix' => 'admin'], function(Router $api) {
        
        //get all users
        $api->get('/users', 'App\\Api\\V1\\Controllers\\AdminController@index')->name('users');
        
        //get user by id
        $api->get('/user/{id}', 'App\\Api\\V1\\Controllers\\AdminController@user')->name('user');
        
        //delete user
        $api->post('/user/destroy', 'App\\Api\\V1\\Controllers\\AdminController@destroy')->name('destroy-user');
    });
    $api->group(['prefix' => 'auth'], function(Router $api) {
        $api->post('signup', 'App\\Api\\V1\\Controllers\\SignUpController@signUp');
        $api->post('login', 'App\\Api\\V1\\Controllers\\LoginController@login');

        $api->post('forgot', 'App\\Api\\V1\\Controllers\\ForgotPasswordController@sendResetEmail');
        $api->post('reset', 'App\\Api\\V1\\Controllers\\ResetPasswordController@resetPassword');

        $api->post('logout', 'App\\Api\\V1\\Controllers\\LogoutController@logout');
        $api->post('refresh', 'App\\Api\\V1\\Controllers\\RefreshController@refresh');
        $api->get('user', 'App\\Api\\V1\\Controllers\\UserController@me');
        $api->post('update', 'App\\Api\\V1\\Controllers\\UserController@update');
    });

    $api->group(['middleware' => 'jwt.auth'], function(Router $api) {
        $api->get('protected', function() {
            return response()->json([
                        'message' => 'Access to protected resources granted! You are seeing this text as you provided the token correctly.'
            ]);
        });

        $api->get('refresh', [
            'middleware' => 'jwt.refresh',
            function() {
                return response()->json([
                            'message' => 'By accessing this endpoint, you can refresh your access token at each request. Check out this response headers!'
                ]);
            }
                ]);
            });

            $api->get('hello', function() {
                return response()->json([
                            'message' => 'This is a simple example of item returned by your APIs. Everyone can see it.'
                ]);
            });
        });
        