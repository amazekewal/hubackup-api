<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CmsBlogAuthor extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'cms_blog_authors';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id','backup_progress_id', 'blog_author_id','portal_id','created', 'updated', 'deleted_at','full_name','email',
        'user_name','slug','display_name','google_plus','bio','website','twitter','linkedin','facebook','avatar','gravatar_url',
        'twitter_username','has_social_profiles'];
    
    protected $hidden = ['id','user_id','created_at','updated_at','backup_progress_id'];
    
    public function user() {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }
}
