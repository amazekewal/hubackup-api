<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Engagement extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'engagements';
    
     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    
    protected $fillable = ['user_id','backup_progress_id','engagement_id','portal_id','active','created_by',
                           'modified_by','owner_id','type','associations_contactIds',
                           'associations_companyIds','associations_dealIds','associations_ownerIds',
                           'associations_workflowIds','associations_ticketIds','attachments','createdate',
                           'last_updated'];
    
    protected $hidden = ['id','user_id','created_at', 'updated_at','backup_progress_id'];
    
    protected $casts = [
        'associations_contactIds' => 'array',
        'associations_companyIds' => 'array',
        'associations_dealIds' => 'array',
        'associations_ownerIds' => 'array',
        'associations_workflowIds' => 'array',
        'associations_ticketIds' => 'array',
        'attachments' => 'array',
    ];
    
    public function company_engagements()
    {
        return $this->hasMany('App\CompanyAssociatedEngagement', 'engagement_db_id', 'id');
    }
    
    public function contact_engagements()
    {
        return $this->hasMany('App\ContactAssociatedEngagement', 'engagement_db_id', 'id');
    }
    
    public function deal_engagements()
    {
        return $this->hasMany('App\DealAssociatedEngagement', 'engagement_db_id', 'id');
    }
    
    public function task_engagements()
    {
        return $this->hasOne('App\TaskEngagement', 'engagement_db_id', 'id');
    }
    
    public function note_engagements()
    {
        return $this->hasOne('App\NoteEngagement', 'engagement_db_id', 'id');
    }
    
    public function email_engagements()
    {
        return $this->hasOne('App\EmailEngagement', 'engagement_db_id', 'id');
    }
    
    public function call_engagements()
    {
        return $this->hasOne('App\CallEngagement', 'engagement_db_id', 'id');
    }
    
    public function meeting_engagements()
    {
        return $this->hasOne('App\MeetingEngagement', 'engagement_db_id', 'id');
    }
    
}
