<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model {

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'notifications';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id','status','type','num_of_records','is_read'];
    
    public function user() {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }
   
}
