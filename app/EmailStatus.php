<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmailStatus extends Model
{
   /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'email_status';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id','portal_id','email','status','subscribed','marked_as_spam','bounced',
        'subscription_statuses'];
    
    public function user() {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }
}
