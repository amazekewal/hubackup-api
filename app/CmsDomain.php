<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CmsDomain extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'cms_domains';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id','backup_progress_id', 'cms_domain_id','portal_id','actual_cname','correct_cname','actual_ip',
        'consecutive_non_resolving_count','domain','full_category_key','name','is_any_primary','is_dns_correct',
        'is_internal_domain','is_legacy','is_legacy_domain','is_resolving','is_ssl_enabled','is_ssl_only',
        'is_used_for_blog_post','is_used_for_site_page','is_used_for_landing_page','is_used_for_email',
        'is_used_for_knowledge','is_setup_complete','manually_marked_as_resolving','primary_blog','primary_blog_post',
        'primary_email','primary_landing_page','primary_legacy_page','primary_knowledge','primary_site_page',
        'secondary_to_domain','primary_click_tracking','apex_domain','public_suffix','site_id','brand_id','deletable',
        'cos_object_type','is_resolving_internal_property','is_resolving_ignoring_manually_marked_as_resolving',
        'is_used_for_any_content_type','created', 'updated'];
    
    
    protected $hidden = ['id','user_id','created_at','updated_at','backup_progress_id'];

    
    public function user() {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }
}
