<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CallEngagement extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'call_engagements';
    
     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
     
    protected $fillable = ['engagement_db_id','to_number','from_number','status','external_id',
                           'duration_milliseconds','external_account_id','recording_url','body','disposition'];
    
    protected $hidden = ['id','engagement_db_id','created_at', 'updated_at'];
    
    public function engagement() {
        return $this->belongsTo('App\Engagement', 'engagement_db_id', 'id');
    }
}
