<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CmsHubdbTableColumn extends Model
{
   /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'cms_hubdb_tables';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['cms_hubdb_table_db_id', 'column_id','column_name','column_label','column_type'];
    
    protected $hidden = ['cms_hubdb_table_db_id','created_at','updated_at'];

    public function hubdb_table() {
        return $this->belongsTo('App\CmsHubdbTable', 'cms_hubdb_table_db_id', 'id');
    }
}
