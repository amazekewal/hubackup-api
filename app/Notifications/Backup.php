<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class Backup extends Notification
{
    use Queueable;
    protected $notificatoion;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($notificatoion)
    {
        $this->notificatoion = $notificatoion;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $backup_date = \Carbon\Carbon::parse($this->notificatoion->created_at)->format('M d, Y');
        $backup_time = \Carbon\Carbon::parse($this->notificatoion->created_at)->format('g:i A');
        
        return (new MailMessage)
                    ->greeting('Hello!')
                    ->line('Backup'. $this->notificatoion->status.' at ' .$backup_date.' '.$backup_time);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
