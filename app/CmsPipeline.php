<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CmsPipeline extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'cms_pipelines';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id','backup_progress_id', 'pipeline_id','object_type','object_type_id','label','display_order','active',
        'created_date','updated_date'];
    
    protected $hidden = ['id','user_id','created_at','updated_at','pipeline_db_id','backup_progress_id'];

    
    public function user() {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }
    
    public function pipeline_stages()
    {
        return $this->hasMany('App\CmsPipelineStage', 'pipeline_db_id', 'id');
    }
    
}
