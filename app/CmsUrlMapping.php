<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CmsUrlMapping extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'cms_url_mappings';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id','backup_progress_id', 'cms_url_mapping_id','portal_id','name','route_prefix','destination',
        'redirect_style','content_group_id','is_only_after_notfound','is_regex','is_match_fullurl','is_match_query_string',
        'is_pattern','is_trailing_slash_optional','is_protocol_agnostic','precedence','deleted_at','created','updated'];
    
    protected $hidden = ['id','user_id','created_at','updated_at','backup_progress_id'];
    
    public function user() {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }
}
