<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductProperty extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'product_properties';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['product_db_id', 'property_name','value','timestamp','source','source_id','source_vid','request_id'];
    
    protected $hidden = ['product_db_id','created_at','updated_at'];
    
    public function product() {
        return $this->belongsTo('App\Product', 'product_db_id', 'id');
    }
}
