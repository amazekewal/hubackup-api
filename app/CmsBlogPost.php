<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CmsBlogPost extends Model
{
     /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'cms_blog_posts';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id','backup_progress_id', 'blog_post_id','ab','ab_variation','absolute_url','analytics_page_id','analytics_page_type',
        'archived','are_comments_allowed','attached_stylesheets','author','author_at','author_email','author_name','author_user_id',
        'author_username','blog_author_id','blog_post_author_id','blueprint_type_id','campaign','campaign_name','category_id',
        'comment_count','composition_id','content_group_id','content_type_category_id','created','created_by_id','created_time',
        'css','css_text','ctas','current_state','currently_published','deleted_at','domain','enable_google_amp_output_override',
        'featured_image','featured_image_alt_text','featured_image_height','featured_image_length','featured_image_width',
        'flex_areas','freeze_date','has_content_access_rules','has_user_changes','html_title','is_captcha_required','is_draft',
        'is_instant_email_enabled','is_published','is_social_publishing_enabled','keywords','label','layout_sections','link_rel_canonical_url',
        'list_template','live_domain','mab','mab_master','mab_variant','meta_post_summary','meta_last_edit_session_id',
        'meta_last_edit_update_id','tag_ids','topic_ids','page_redirected','personas','placement_guids','public_access_rules',
        'public_access_rules_enabled','tweet_immediately','unpublished_at','use_featured_image','rss_summary',
        'blog_publish_to_social_media_task','blog_post_schedule_task_uid','meta_description','name','page_title','parent_blog_id',
        'past_mab_experiment_ids','portal_id','post_body_rss','post_email_content','post_list_summary_featured_image',
        'post_rss_content','post_template','preview_key','processing_status','publish_date','publish_date_local_time',
        'publish_date_local_format','publish_immediately','published_at','published_by_email','published_by_id','published_by_name',
        'published_url','resolved_domain','rss_body','rss_summary_featured_image','slug','state','subcategory','synced_with_blog_root',
        'tag_list','tag_names','team_perms','template_path','template_path_for_render','title','topic_list','topic_names',
        'translated_content','updated','updated_by_id','upsize_featured_image','url','views','user_perms','widget_containers',
        'widgetcontainers'];
    
    protected $hidden = ['id','user_id','created_at','updated_at','backup_progress_id'];

    public function user() {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }
    
    public function post_widgets()
    {
        return $this->hasMany('App\CmsBlogPostWidget', 'cms_blog_post_db_id', 'id');
    }
}
