<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\BadResponseException;
use Illuminate\Support\Facades\Artisan;
// models
use App\CmsTemplate;
use App\CmsTemplateAttachedJs;
use App\CmsTemplateAttachedStylesheet;
use App\CmsTemplateContentTag;
use App\BackupHistory;
use App\User;
use Illuminate\Support\Facades\Log;
use App\BackupProgress;
use App\Notifications\Backup;
use App\Notification;
use App\SmartAlert;
use App\SmartAlertHistory;
use Carbon\Carbon;

class CmsTemplatesData extends Command {

    /**
     * The name of the apiUrl
     * @var type 
     */
    protected $api_url;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cms-templates:data {userId}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command will get all CMS Templates data';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();

        //get api key from env file
        $this->api_url = env('HUBSPOT_URL');
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        $this->info('CMS Templates Data cron started....' . $this->api_url);

        $status = '';
        $error_msg = '';
        $created_rows = 0;
        $updated_rows = 0;
        $hubspot_cms_template_ids = [];

        $userId = $this->argument('userId');
        $user = User::find($userId);

        $backup_progress = BackupProgress::where('user_id', $user['id'])->where('status', 'Inprogress')->orderByDesc('id')->first();



        try {

            Log::error('Fetching CMS Templates data started |||| Start >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> userId - ' . $userId);

            //start time
            $time_start = microtime(true);


            //user token
            $token = $user->accesstoken;

            //using GuzzleHttp client to get request
            $client = new \GuzzleHttp\Client(['http_errors' => false]);
            $headers = [
                'Authorization' => 'Bearer ' . $token,
                'Accept' => 'application/json',
            ];

            $get_data = $this->getCmsTemplatesData($headers, 0, $client, $backup_progress, $user, $hubspot_cms_template_ids, $status, $error_msg, $created_rows, $updated_rows);

            $created_rows = $get_data['created_rows'];
            $updated_rows = $get_data['updated_rows'];
            $hubspot_cms_template_ids = $get_data['hubspot_cms_template_ids'];
            $error_msg = $get_data['error_msg'];
            $status = $get_data['status'];


            //end time
            $time_end = microtime(true);
            //execution time
            $execution_time = ($time_end - $time_start);

            Log::error('time taken for CMS Templates data ' . $execution_time);
            Log::error('Fetching CMS Templates data completed |||| End <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< userId - ' . $userId);
        } catch (BadResponseException $ex) {
            //if request is invalid
            $response = $ex->getResponse();
            $jsonBody = (string) $response->getBody();
            $this->error($jsonBody);

            $data = json_decode($response->getBody(), true);
            $status = $data['status'];
            $error_msg = $data['message'];
            Log::error('CMS Templates data  |||| Error - ' . $jsonBody);
        }


        //get cms templates from database
        $cms_templates_in_database = CmsTemplate::where('backup_progress_id', $backup_progress['id'] - 1)->get()->pluck('cms_template_id')->toArray();

        //check if empty result 
        if (count($hubspot_cms_template_ids) == 0) {
            $removed_rows = 0;
        } else {
            $removed_cms_templates = array_diff($cms_templates_in_database, $hubspot_cms_template_ids);
            $removed_rows = count($removed_cms_templates);
        }

        //creating BackupHistory record
        $backup_history = BackupHistory::create([
                    'backup_progress_id' => $backup_progress['id'],
                    'user_id' => $user->id,
                    'object_name' => 'Fetched CMS Templates data',
                    'num_of_records' => $created_rows + $removed_rows,
                    'removed_records' => $removed_rows,
                    'added_records' => $created_rows,
                    'changed_records' => $updated_rows,
                    'num_of_API_calls' => 1,
                    'status' => $status,
                    'error_message' => $error_msg]);

        //get smart alerts for action 'added'
        $added_alert = SmartAlert::where('user_id', $user['id'])
                        ->where('s_object', 'CMS Templates')
                        ->where('action', 'Added')->first();

        if (!empty($added_alert)) {
            if ($created_rows > $added_alert->amount) {

                //notification when CMS Template added
                $add_notificatoion = Notification::create(['user_id' => $user['id'], 'backup_progress_id' => $backup_progress['id'],
                            'status' => 'CMS Templates Added', 'type' => 'email', 'num_of_records' => $created_rows]);

                //create history
                SmartAlertHistory::create(['user_id' => $user['id'], 'smart_alert_id' => $added_alert->id]);

                //notify user
                //$user->notify(new Backup($notificatoion));
            }
        }

        //get smart alerts for action 'Changed'
        $changed_alert = SmartAlert::where('user_id', $user['id'])
                        ->where('s_object', 'CMS Templates')
                        ->where('action', 'Changed')->first();

        if (!empty($changed_alert)) {
            if ($updated_rows > $changed_alert->amount) {

                //notification when CMS Template Changed
                $change_notificatoion = Notification::create(['user_id' => $user['id'], 'backup_progress_id' => $backup_progress['id'],
                            'status' => 'CMS Templates Changed', 'type' => 'email', 'num_of_records' => $updated_rows]);

                //create history
                SmartAlertHistory::create(['user_id' => $user['id'], 'smart_alert_id' => $changed_alert->id]);

                //notify user
                //$user->notify(new Backup($notificatoion));
            }
        }
        //get smart alerts for action 'Removed'
        $removed_alert = SmartAlert::where('user_id', $user['id'])
                        ->where('s_object', 'CMS Templates')
                        ->where('action', 'Removed')->first();

        if (!empty($removed_alert)) {
            if ($removed_rows > $removed_alert->amount) {

                //notification when CMS Template Removed
                $remove_notificatoion = Notification::create(['user_id' => $user['id'], 'backup_progress_id' => $backup_progress['id'],
                            'status' => 'CMS Templates Removed', 'type' => 'email', 'num_of_records' => $removed_rows]);

                //create history
                SmartAlertHistory::create(['user_id' => $user['id'], 'smart_alert_id' => $removed_alert->id]);

                //notify user
                //$user->notify(new Backup($notificatoion));
            }
        }
    }

    public function getCmsTemplatesData($headers, $offset, $client, $backup_progress, $user, $hubspot_cms_template_ids, $status, $error_msg, $created_rows, $updated_rows) {

        $url = $this->api_url . '/content/api/v2/templates?limit=100&offset=' . $offset;

        //get all cms Templates data
        $response = $client->request('GET', $url, ['headers' => $headers]);


        $body = $response->getBody();
        $data = json_decode($body, true);


        if (!empty($data['status']) == 'error') {
            $status = $data['status'];
            $error_msg = $data['message'];
            Log::error('CMS Templates data  |||| Error - ' . $error_msg);
        } else {
            $results = $data['objects'];
            $status = 'successed';

            if (!empty($results)) {
                foreach ($results as $value) {

                    //get all  CMS Template ids
                    $hubspot_cms_template_ids[] = $value['id'];



                    $check_cms_template = CmsTemplate::where('user_id', $user->id)
                                    ->where('cms_template_id', $value['id'])
                                    ->where('backup_progress_id', $backup_progress['id'] - 1)->first();
                    $cms_template = CmsTemplate::create([
                                'user_id' => $user->id,
                                'backup_progress_id' => $backup_progress['id'],
                                'cms_template_id' => $value['id'],
                                'portal_id' => $value['portal_id'],
                                'amp_validated' => isset($value['amp_validated']) ? $value['amp_validated'] : 0,
                                'attached_amp_stylesheet' => isset($value['attached_amp_stylesheet']) ? $value['attached_amp_stylesheet'] : 0,
                                'category_id' => isset($value['category_id']) ? $value['category_id'] : 0,
                                'cdn_minified_url' => isset($value['cdn_minified_url']) ? $value['cdn_minified_url'] : "",
                                'cdn_url' => isset($value['cdn_url']) ? $value['cdn_url'] : "",
                                'cloned_from' => isset($value['cloned_from']) ? $value['cloned_from'] : 0,
                                'generated_from_layout_id' => isset($value['body']['generated_from_layout_id']) ? $value['body']['generated_from_layout_id'] : 0,
                                'linked_style_id' => isset($value['body']['linked_style_id']) ? $value['body']['linked_style_id'] : "",
                                'thumbnail_width' => isset($value['body']['thumbnail_width']) ? $value['body']['thumbnail_width'] : 0,
                                'created_by_id' => isset($value['created_by_id']) ? $value['created_by_id'] : 0,
                                'css_or_js_template' => isset($value['css_or_js_template']) ? $value['css_or_js_template'] : 0,
                                'default' => isset($value['default']) ? $value['default'] : 0,
                                'description_i18n_key' => isset($value['description_i18n_key']) ? $value['description_i18n_key'] : "",
                                'deleted_at' => isset($value['deleted_at']) ? $value['deleted_at'] : 0,
                                'folder' => isset($value['folder']) ? $value['folder'] : "",
                                'folder_id' => isset($value['folder_id']) ? $value['folder_id'] : 0,
                                'google_fonts' => isset($value['google_fonts']) ? $value['google_fonts'] : NULL,
                                'has_style_tag' => isset($value['has_style_tag']) ? $value['has_style_tag'] : 0,
                                'host_template_types' => isset($value['host_template_types']) ? $value['host_template_types'] : NULL,
                                'is_available_for_new_content' => isset($value['is_available_for_new_content']) ? $value['is_available_for_new_content'] : 0,
                                'is_from_layout' => isset($value['is_from_layout']) ? $value['is_from_layout'] : 0,
                                'is_read_only' => isset($value['is_read_only']) ? $value['is_read_only'] : 0,
                                'marketplace_delivery_id' => isset($value['marketplace_delivery_id']) ? $value['marketplace_delivery_id'] : 0,
                                'marketplace_version' => isset($value['marketplace_version']) ? $value['marketplace_version'] : 0,
                                'has_buffered_changes' => isset($value['meta']['hasBufferedChanges']) ? $value['meta']['hasBufferedChanges'] : 0,
                                'missing' => isset($value['missing']) ? $value['missing'] : 0,
                                'purchased' => isset($value['purchased']) ? $value['purchased'] : 0,
                                'release_id' => isset($value['release_id']) ? $value['release_id'] : "",
                                'size' => isset($value['size']) ? $value['size'] : 0,
                                'source' => isset($value['source']) ? $value['source'] : "",
                                'template_type' => isset($value['template_type']) ? $value['template_type'] : 0,
                                'type' => isset($value['type']) ? $value['type'] : "",
                                'updated_by_id' => isset($value['updated_by_id']) ? $value['updated_by_id'] : 0,
                                'updated' => isset($value['updated']) ? Carbon::createFromTimestampMs($value['updated'])->format('Y-m-d H:i:s') : NULL,
                    ]);

                    if (!empty($value['attached_js'])) {
                        foreach ($value['attached_js'] as $js) {

                            $attached_js = CmsTemplateAttachedJs::create([
                                        'cms_template_db_id' => $cms_template->id,
                                        'js_id' => $js['id'],
                                        'type' => $js['type'],
                            ]);
                        }
                    }

                    if (!empty($value['attached_stylesheets'])) {
                        foreach ($value['attached_stylesheets'] as $stylesheet) {

                            $attached_stylesheets = CmsTemplateAttachedStylesheet::create([
                                        'cms_template_db_id' => $cms_template->id,
                                        'stylesheet_id' => $stylesheet['id'],
                                        'type' => $stylesheet['type'],
                            ]);
                        }
                    }

                    if (!empty($value['content_tags'])) {
                        foreach ($value['content_tags'] as $content_tag) {

                            $content_tags = CmsTemplateContentTag::create([
                                        'cms_template_db_id' => $cms_template->id,
                                        'name' => $content_tag['name'],
                                        'source' => $content_tag['source'],
                            ]);
                        }
                    }
                    //if cms template is created
                    if ($cms_template) {
                        $created_rows +=1;
                    }

                    //check if cms Template  is already inserted or not
                    if ($check_cms_template === null) {
                        
                    } else {
                        $last_updated_date = Carbon::createFromTimestampMs($value['updated'])->format('Y-m-d H:i:s');

                        //check if any data is changed
                        if ($last_updated_date != $check_cms_template->updated) {

                            $updated_rows +=1;
                        }
                    }
                }
            }
        }

        if (isset($data['offset']) && !empty($data['offset'] !== 0)) {

            return $this->getCmsTemplatesData($headers, $data['offset'], $client, $backup_progress, $user, $hubspot_cms_template_ids, $status, $error_msg, $created_rows, $updated_rows);
        } else {

            $data = ['created_rows' => $created_rows, 'updated_rows' => $updated_rows,
                'hubspot_cms_template_ids' => $hubspot_cms_template_ids, 'status' => $status, 'error_msg' => $error_msg];

            return $data;
        }
    }

}
