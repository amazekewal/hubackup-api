<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
// models
use App\Analytic;
use App\AnalyticView;
use App\BackupHistory;
use App\User;
use Illuminate\Support\Facades\Log;
use App\BackupProgress;
use App\Notifications\Backup;
use App\Notification;
use App\SmartAlert;
use App\SmartAlertHistory;
use Carbon\Carbon;

class AnalyticsData extends Command {

    /**
     * The name of the apiUrl
     * @var type 
     */
    protected $api_url;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'analytics:data {userId}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command will get all Analytics data';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();

        //get api key from env file
        $this->api_url = env('HUBSPOT_URL');
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        $this->info('Analytics Data cron started....' . $this->api_url);

        $status = '';
        $error_msg = '';
        $created_rows = 0;
        $updated_rows = 0;
        $hubspot_analytic_breakdowns = [];

        $userId = $this->argument('userId');
        $user = User::find($userId);

        $backup_progress = BackupProgress::where('user_id', $user['id'])->where('status', 'Inprogress')->orderByDesc('id')->first();

        try {

            Log::error('Fetching Analytics data started |||| Start >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> userId - ' . $userId);


            //start time
            $time_start = microtime(true);


            //user token
            $token = $user->accesstoken;

            //using GuzzleHttp client to get request
            $client = new \GuzzleHttp\Client(['http_errors' => false]);
            $headers = [
                'Authorization' => 'Bearer ' . $token,
                'Accept' => 'application/json',
            ];

            $get_data = $this->getAnalyticsData($headers, 0, $client, $backup_progress, $user, $hubspot_analytic_breakdowns, $status, $error_msg, $created_rows, $updated_rows);

            $created_rows = $get_data['created_rows'];
            $updated_rows = $get_data['updated_rows'];
            $hubspot_analytic_breakdowns = $get_data['hubspot_analytic_breakdowns'];
            $error_msg = $get_data['error_msg'];
            $status = $get_data['status'];


            //end time
            $time_end = microtime(true);
            //execution time
            $execution_time = ($time_end - $time_start);

            Log::error('time taken for Analytics data ' . $execution_time);
            Log::error('Fetching Analytics data completed |||| End <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< userId - ' . $userId);
        } catch (BadResponseException $ex) {
            //if request is invalid
            $response = $ex->getResponse();
            $jsonBody = (string) $response->getBody();
            $this->error($jsonBody);

            $data = json_decode($response->getBody(), true);
            $status = $data['status'];
            $error_msg = $data['message'];
            Log::error('Analytics data  |||| Error - ' . $jsonBody);
        }

        //get Analytics from database
        $analytics_in_database = Analytic::where('backup_progress_id', $backup_progress['id'] - 1)->get()->pluck('breakdown')->toArray();

        //check if empty result 
        if (count($hubspot_analytic_breakdowns) == 0) {
            $removed_rows = 0;
        } else {
            $removed_analytics = array_diff($analytics_in_database, $hubspot_analytic_breakdowns);
            $removed_rows = count($removed_analytics);
        }

        //creating BackupHistory record
        $backup_history = BackupHistory::create([
                    'backup_progress_id' => $backup_progress['id'],
                    'user_id' => $user->id,
                    'object_name' => 'Fetched Analytics data',
                    'num_of_records' => $created_rows + $removed_rows,
                    'removed_records' => $removed_rows,
                    'added_records' => $created_rows,
                    'changed_records' => $updated_rows,
                    'num_of_API_calls' => 1,
                    'status' => $status,
                    'error_message' => $error_msg]);

        //get smart alerts for action 'added'
        $added_alert = SmartAlert::where('user_id', $user['id'])
                        ->where('s_object', 'Analytics')
                        ->where('action', 'Added')->first();

        if (!empty($added_alert)) {
            if ($created_rows > $added_alert->amount) {

                //notification when Analytic added
                $add_notificatoion = Notification::create(['user_id' => $user['id'], 'backup_progress_id' => $backup_progress['id'],
                            'status' => 'Analytics Added', 'type' => 'email', 'num_of_records' => $created_rows]);

                //create history
                SmartAlertHistory::create(['user_id' => $user['id'], 'smart_alert_id' => $added_alert->id]);

                //notify user
                //$user->notify(new Backup($notificatoion));
            }
        }

        //get smart alerts for action 'Changed'
        $changed_alert = SmartAlert::where('user_id', $user['id'])
                        ->where('s_object', 'Analytics')
                        ->where('action', 'Changed')->first();

        if (!empty($changed_alert)) {
            if ($updated_rows > $changed_alert->amount) {


                //notification when Analytic Changed
                $change_notificatoion = Notification::create(['user_id' => $user['id'], 'backup_progress_id' => $backup_progress['id'],
                            'status' => 'Analytics Changed', 'type' => 'email', 'num_of_records' => $updated_rows]);

                //create history
                SmartAlertHistory::create(['user_id' => $user['id'], 'smart_alert_id' => $changed_alert->id]);

                //notify user
                //$user->notify(new Backup($notificatoion));
            }
        }

        //get smart alerts for action 'Removed'
        $removed_alert = SmartAlert::where('user_id', $user['id'])
                        ->where('s_object', 'Analytics')
                        ->where('action', 'Removed')->first();

        if (!empty($removed_alert)) {
            if ($removed_rows > $removed_alert->amount) {

                //notification when Analytic Removed
                $remove_notificatoion = Notification::create(['user_id' => $user['id'], 'backup_progress_id' => $backup_progress['id'],
                            'status' => 'Analytics Removed', 'type' => 'email', 'num_of_records' => $removed_rows]);

                //create history
                SmartAlertHistory::create(['user_id' => $user['id'], 'smart_alert_id' => $removed_alert->id]);

                //notify user
                //$user->notify(new Backup($notificatoion));
            }
        }
    }

    public function getAnalyticsData($headers, $offset, $client, $backup_progress, $user, $hubspot_analytic_breakdowns, $status, $error_msg, $created_rows, $updated_rows) {

        $enddate = Carbon::now()->format('Ymd');
        $startdate = Carbon::now()->subYears(1)->format('Ymd');

        $url = $this->api_url . '/analytics/v2/reports/totals/total?start=' . $startdate . '&end=' . $enddate . '&limit=100&offset=' . $offset;

        //get all Analytics data
        $response = $client->request('GET', $url, ['headers' => $headers]);


        $body = $response->getBody();
        $results = json_decode($body, true);

        if (!empty($results['status']) == 'error') {
            $status = $results['status'];
            $error_msg = $results['message'];
            Log::error('Analytics data  |||| Error - ' . $error_msg);
        } else {
            $status = 'successed';
            if (!empty($results)) {
                $create_analtics = [];
                foreach ($results['breakdowns'] as $value) {

                    //get all Analytic breakdowns 
                    $hubspot_analytic_breakdowns[] = $value['breakdown'];


                    $check_analytic = Analytic::where('user_id', $user->id)
                                    ->where('breakdown', $value['breakdown'])
                                    ->where('backup_progress_id', $backup_progress['id'] - 1)->first();

                    $create_analtics[] = [
                        'user_id' => $user->id,
                        'backup_progress_id' => $backup_progress['id'],
                        'breakdown' => $value['breakdown'],
                        'raw_views' => isset($value['rawViews']) ? $value['rawViews'] : 0,
                        'visits' => isset($value['visits']) ? $value['visits'] : 0,
                        'visitors' => isset($value['visitors']) ? $value['visitors'] : 0,
                        'leads' => isset($value['leads']) ? $value['leads'] : 0,
                        'contacts' => isset($value['contacts']) ? $value['contacts'] : 0,
                        'subscribers' => isset($value['subscribers']) ? $value['subscribers'] : 0,
                        'marketing_qualified_leads' => isset($value['marketingQualifiedLeads']) ? $value['marketingQualifiedLeads'] : 0,
                        'sales_qualified_leads' => isset($value['salesQualifiedLeads']) ? $value['salesQualifiedLeads'] : 0,
                        'opportunities' => isset($value['opportunities']) ? $value['opportunities'] : 0,
                        'customers' => isset($value['customers']) ? $value['customers'] : 0,
                        'pageviews_per_session' => isset($value['pageviewsPerSession']) ? $value['pageviewsPerSession'] : "",
                        'bounce_rate' => isset($value['bounceRate']) ? $value['bounceRate'] : "",
                        'time_per_session' => isset($value['timePerSession']) ? $value['timePerSession'] : "",
                        'new_visitor_session_rate' => isset($value['newVisitorSessionRate']) ? $value['newVisitorSessionRate'] : "",
                        'session_to_contact_rate' => isset($value['sessionToContactRate']) ? $value['sessionToContactRate'] : "",
                        'contact_to_customer_rate' => isset($value['contactToCustomerRate']) ? $value['contactToCustomerRate'] : "",
                        'created_at' => Carbon::now()->toDateTimeString(),
                        'updated_at' => Carbon::now()->toDateTimeString()
                    ];
                    //check if Analytic breakdown  is already inserted or not
                    if ($check_analytic === null) {
                        
                    } else {

                        //check if any data is changed
                        if ($check_analytic->raw_views != $value['rawViews'] || $check_analytic->visits != $value['visits'] ||
                                $check_analytic->visitors != $value['visitors'] || $check_analytic->leads != $value['leads'] ||
                                $check_analytic->contacts != $value['contacts'] || $check_analytic->subscribers != $value['subscribers'] ||
                                $check_analytic->marketing_qualified_leads != $value['marketingQualifiedLeads'] ||
                                $check_analytic->sales_qualified_leads != $value['salesQualifiedLeads'] ||
                                $check_analytic->opportunities != $value['opportunities'] ||
                                $check_analytic->customers != $value['customers'] ||
                                $check_analytic->pageviews_per_session != $value['pageviewsPerSession'] ||
                                $check_analytic->bounce_rate != $value['bounceRate'] ||
                                $check_analytic->time_per_session != $value['timePerSession'] ||
                                $check_analytic->new_visitor_session_rate != $value['newVisitorSessionRate'] ||
                                $check_analytic->session_to_contact_rate != $value['sessionToContactRate'] ||
                                $check_analytic->contact_to_customer_rate != $value['contactToCustomerRate']) {

                            $updated_rows +=1;
                        }
                    }
                }

                //insert analytics in bulk
                $company = Analytic::insert($create_analtics);
                $created_rows += count($create_analtics);
            }
        }

        if (isset($data['offset']) && !empty($data['offset'] !== 0)) {

            return $this->getAnalyticsData($headers, $data['offset'], $client, $backup_progress, $user, $hubspot_analytic_breakdowns, $status, $error_msg, $created_rows, $updated_rows);
        } else {

            $data = ['created_rows' => $created_rows, 'updated_rows' => $updated_rows,
                'hubspot_analytic_breakdowns' => $hubspot_analytic_breakdowns, 'status' => $status, 'error_msg' => $error_msg];

            return $data;
        }
    }

}
