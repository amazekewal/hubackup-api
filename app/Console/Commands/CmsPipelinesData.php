<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\BadResponseException;
use Illuminate\Support\Facades\Artisan;
// models
use App\CmsPipeline;
use App\CmsPipelineStage;
use App\BackupHistory;
use App\User;
use Illuminate\Support\Facades\Log;
use App\BackupProgress;
use App\Notifications\Backup;
use App\Notification;
use App\SmartAlert;
use App\SmartAlertHistory;
use Carbon\Carbon;

class CmsPipelinesData extends Command {

    /**
     * The name of the apiUrl
     * @var type 
     */
    protected $api_url;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cms-pipelines:data {userId}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command will get all CMS Pipelines data';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();

        //get api key from env file
        $this->api_url = env('HUBSPOT_URL');
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        $this->info('CMS Pipelines Data cron started....' . $this->api_url);

        $status = '';
        $error_msg = '';
        $ticket_created_rows = 0;
        $ticket_updated_rows = 0;
        $deal_created_rows = 0;
        $deal_updated_rows = 0;
        $hubspot_cms_pipeline_ids = [];

        $userId = $this->argument('userId');
        $user = User::find($userId);

        $backup_progress = BackupProgress::where('user_id', $user['id'])->where('status', 'Inprogress')->orderByDesc('id')->first();


        try {

            Log::error('Fetching CMS Pipelines data started |||| Start >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> userId - ' . $userId);

            //start time
            $time_start = microtime(true);


            //user token
            $token = $user->accesstoken;

            //using GuzzleHttp client to get request
            $client = new \GuzzleHttp\Client(['http_errors' => false]);
            $headers = [
                'Authorization' => 'Bearer ' . $token,
                'Accept' => 'application/json',
            ];

            //for ticket pipelines
            $tickets_url = $this->api_url . '/crm-pipelines/v1/pipelines/tickets';

            //get all cms ticket pipelines data
            $tickets_response = $client->request('GET', $tickets_url, ['headers' => $headers]);


            $tickets_body = $tickets_response->getBody();
            $tickets_data = json_decode($tickets_body, true);


            if (!empty($tickets_data['status']) == 'error') {
                $status = $tickets_data['status'];
                $error_msg = $tickets_data['message'];
                Log::error('CMS Pipelines data  |||| Error - ' . $error_msg);
            } else {

                $status = 'successed';

                $all_tickets_results = $tickets_data['results'];

                //turn data into collection
                $results_collection = collect($all_tickets_results);
                $results_chunks = $results_collection->chunk(500); //chunk into smaller pieces


                foreach ($results_chunks as $results_chunk) {

                    $tickets_results = $results_chunk->toArray();

                    if (!empty($tickets_results)) {
                        foreach ($tickets_results as $value) {

                            //get all cms pipeline ids
                            $hubspot_cms_pipeline_ids[] = $value['pipelineId'];


                            $check_ticket_pipeline = CmsPipeline::where('user_id', $user->id)
                                            ->where('pipeline_id', $value['pipelineId'])
                                            ->where('backup_progress_id', $backup_progress['id'] - 1)->first();

                            $ticket_pipeline = CmsPipeline::create([
                                        'user_id' => $user->id,
                                        'backup_progress_id' => $backup_progress['id'],
                                        'pipeline_id' => $value['pipelineId'],
                                        'object_type' => isset($value['objectType']) ? $value['objectType'] : "",
                                        'object_type_id' => isset($value['objectTypeId']) ? $value['objectTypeId'] : "",
                                        'label' => isset($value['label']) ? $value['label'] : "",
                                        'display_order' => isset($value['displayOrder']) ? $value['displayOrder'] : 0,
                                        'active' => isset($value['active']) ? $value['active'] : 0,
                                        'created_date' => isset($value['createdAt']) ? Carbon::createFromTimestampMs($value['createdAt'])->format('Y-m-d H:i:s') : NULL,
                                        'updated_date' => isset($value['updatedAt']) ? Carbon::createFromTimestampMs($value['updatedAt'])->format('Y-m-d H:i:s') : NULL,
                            ]);

                            if (!empty($value['stages'])) {
                                foreach ($value['stages'] as $stage) {

                                    $ticket_pipeline_stage = CmsPipelineStage::create([
                                                'pipeline_db_id' => $ticket_pipeline->id,
                                                'stage_id' => isset($stage['stageId']) ? $stage['stageId'] : "",
                                                'label' => isset($stage['label']) ? $stage['label'] : "",
                                                'display_order' => isset($stage['displayOrder']) ? $stage['displayOrder'] : 0,
                                                'ticket_state' => isset($stage['ticketState']) ? $stage['ticketState'] : "",
                                                'probability' => isset($stage['probability']) ? $stage['probability'] : "",
                                                'is_closed' => isset($stage['isClosed']) ? $stage['isClosed'] : 0,
                                                'active' => isset($stage['active']) ? $stage['active'] : 0,
                                                'created_date' => isset($stage['createdAt']) ? Carbon::createFromTimestampMs($stage['createdAt'])->format('Y-m-d H:i:s') : NULL,
                                                'updated_date' => isset($stage['updatedAt']) ? Carbon::createFromTimestampMs($stage['updatedAt'])->format('Y-m-d H:i:s') : NULL,
                                    ]);
                                }
                            }
                            //if deal pipeline is created
                            if ($ticket_pipeline) {
                                $ticket_created_rows +=1;
                            }
                            //check if  ticket pipeline  is already inserted or not
                            if ($check_ticket_pipeline === null) {
                                
                            } else {
                                $last_updated_date = Carbon::createFromTimestampMs($value['updatedAt'])->format('Y-m-d H:i:s');
                                //check if any data is changed
                                if ($value['updatedAt'] != null) {

                                    if ($last_updated_date != $check_ticket_pipeline->updated_date) {

                                        $ticket_updated_rows +=1;
                                    }
                                }
                            }
                        }
                    }
                }
                //for deal pipelines
                $deal_url = $this->api_url . '/crm-pipelines/v1/pipelines/deals';

                //get all cms deal pipelines data
                $deals_response = $client->request('GET', $deal_url, ['headers' => $headers]);


                $deals_body = $deals_response->getBody();
                $deals_data = json_decode($deals_body, true);


                if (!empty($deals_data['status']) == 'error') {
                    $status = $deals_data['status'];
                    $error_msg = $deals_data['message'];
                } else {
                    $status = 'successed';

                    $all_deals_results = $deals_data['results'];

                    //turn data into collection
                    $results_collection = collect($all_deals_results);
                    $results_chunks = $results_collection->chunk(500); //chunk into smaller pieces


                    foreach ($results_chunks as $results_chunk) {

                        $deals_results = $results_chunk->toArray();

                        if (!empty($deals_results)) {
                            foreach ($deals_results as $value) {

                                //get all cms pipeline ids
                                $hubspot_cms_pipeline_ids[] = $value['pipelineId'];


                                $check_deal_pipeline = CmsPipeline::where('user_id', $user->id)
                                                ->where('pipeline_id', $value['pipelineId'])
                                                ->where('backup_progress_id', $backup_progress['id'] - 1)->first();

                                $deal_pipeline = CmsPipeline::create([
                                            'user_id' => $user->id,
                                            'backup_progress_id' => $backup_progress['id'],
                                            'pipeline_id' => $value['pipelineId'],
                                            'object_type' => isset($value['objectType']) ? $value['objectType'] : "",
                                            'object_type_id' => isset($value['objectTypeId']) ? $value['objectTypeId'] : "",
                                            'label' => isset($value['label']) ? $value['label'] : "",
                                            'display_order' => isset($value['displayOrder']) ? $value['displayOrder'] : 0,
                                            'active' => isset($value['active']) ? $value['active'] : 0,
                                            'created_date' => isset($value['createdAt']) ? Carbon::createFromTimestampMs($value['createdAt'])->format('Y-m-d H:i:s') : NULL,
                                            'updated_date' => isset($value['updatedAt']) ? Carbon::createFromTimestampMs($value['updatedAt'])->format('Y-m-d H:i:s') : NULL,
                                ]);

                                if (!empty($value['stages'])) {
                                    foreach ($value['stages'] as $stage) {

                                        $deal_pipeline_stage = CmsPipelineStage::create([
                                                    'pipeline_db_id' => $deal_pipeline->id,
                                                    'stage_id' => isset($stage['stageId']) ? $stage['stageId'] : "",
                                                    'label' => isset($stage['label']) ? $stage['label'] : "",
                                                    'display_order' => isset($stage['displayOrder']) ? $stage['displayOrder'] : 0,
                                                    'ticket_state' => isset($stage['ticketState']) ? $stage['ticketState'] : "",
                                                    'probability' => isset($stage['probability']) ? $stage['probability'] : "",
                                                    'is_closed' => isset($stage['isClosed']) ? $stage['isClosed'] : 0,
                                                    'active' => isset($stage['active']) ? $stage['active'] : 0,
                                                    'created_date' => isset($stage['createdAt']) ? Carbon::createFromTimestampMs($stage['createdAt'])->format('Y-m-d H:i:s') : NULL,
                                                    'updated_date' => isset($stage['updatedAt']) ? Carbon::createFromTimestampMs($stage['updatedAt'])->format('Y-m-d H:i:s') : NULL,
                                        ]);
                                    }
                                }
                                //if deal pipeline is created
                                if ($deal_pipeline) {
                                    $deal_created_rows +=1;
                                }
                                //check if  deal pipeline  is already inserted or not
                                if ($check_deal_pipeline === null) {
                                    
                                } else {
                                    $last_updated_date = Carbon::createFromTimestampMs($value['updatedAt'])->format('Y-m-d H:i:s');

                                    //check if any data is changed
                                    if ($value['updatedAt'] != null) {
                                        if ($last_updated_date != $check_deal_pipeline->updated_date) {
                                            $deal_updated_rows +=1;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            $created_rows = $ticket_created_rows + $deal_created_rows;
            $updated_rows = $ticket_updated_rows + $deal_updated_rows;

            //end time
            $time_end = microtime(true);
            //execution time
            $execution_time = ($time_end - $time_start);

            Log::error('time taken for CMS Pipelines data ' . $execution_time);
            Log::error('Fetching CMS Pipelines data completed |||| End <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< userId - ' . $userId);
        } catch (BadResponseException $ex) {
            //if request is invalid
            $response = $ex->getResponse();
            $jsonBody = (string) $response->getBody();
            $this->error($jsonBody);

            $data = json_decode($response->getBody(), true);
            $status = $data['status'];
            $error_msg = $data['message'];
            Log::error('CMS Pipelines data  |||| Error - ' . $jsonBody);
        }

        //get cms pipelines from database
        $cms_pipelines_in_database = CmsPipeline::where('backup_progress_id', $backup_progress['id'] - 1)->get()->pluck('pipeline_id')->toArray();

        //check if empty result 
        if (count($hubspot_cms_pipeline_ids) == 0) {
            $removed_rows = 0;
        } else {
            $removed_cms_pipelines = array_diff($cms_pipelines_in_database, $hubspot_cms_pipeline_ids);
            $removed_rows = count($removed_cms_pipelines);
        }

        //creating BackupHistory record
        $backup_history = BackupHistory::create([
                    'backup_progress_id' => $backup_progress['id'],
                    'user_id' => $user->id,
                    'object_name' => 'Fetched CMS Pipelines data',
                    'num_of_records' => $created_rows + $removed_rows,
                    'removed_records' => $removed_rows,
                    'added_records' => $created_rows,
                    'changed_records' => $updated_rows,
                    'num_of_API_calls' => 2,
                    'status' => $status,
                    'error_message' => $error_msg]);

        //get smart alerts for action 'added'
        $added_alert = SmartAlert::where('user_id', $user['id'])
                        ->where('s_object', 'CMS Pipelines')
                        ->where('action', 'Added')->first();

        if (!empty($added_alert)) {
            if ($created_rows > $added_alert->amount) {

                //notification when CMS Pipeline added
                $add_notificatoion = Notification::create(['user_id' => $user['id'], 'backup_progress_id' => $backup_progress['id'],
                            'status' => 'CMS Pipelines Added', 'type' => 'email', 'num_of_records' => $created_rows]);

                //create history
                SmartAlertHistory::create(['user_id' => $user['id'], 'smart_alert_id' => $added_alert->id]);

                //notify user
                //$user->notify(new Backup($notificatoion));
            }
        }

        //get smart alerts for action 'Changed'
        $changed_alert = SmartAlert::where('user_id', $user['id'])
                        ->where('s_object', 'CMS Pipelines')
                        ->where('action', 'Changed')->first();

        if (!empty($changed_alert)) {
            if ($updated_rows > $changed_alert->amount) {

                //notification when CMS  Pipelines Changed
                $change_notificatoion = Notification::create(['user_id' => $user['id'], 'backup_progress_id' => $backup_progress['id'],
                            'status' => 'CMS Pipelines Changed', 'type' => 'email', 'num_of_records' => $updated_rows]);

                //create history
                SmartAlertHistory::create(['user_id' => $user['id'], 'smart_alert_id' => $changed_alert->id]);

                //notify user
                //$user->notify(new Backup($notificatoion));
            }
        }

        //get smart alerts for action 'Removed'
        $removed_alert = SmartAlert::where('user_id', $user['id'])
                        ->where('s_object', 'CMS Pipelines')
                        ->where('action', 'Removed')->first();

        if (!empty($removed_alert)) {
            if ($removed_rows > $removed_alert->amount) {

                //notification when CMS Pipeline Removed
                $remove_notificatoion = Notification::create(['user_id' => $user['id'], 'backup_progress_id' => $backup_progress['id'],
                            'status' => 'CMS Pipelines Removed', 'type' => 'email', 'num_of_records' => $removed_rows]);

                //create history
                SmartAlertHistory::create(['user_id' => $user['id'], 'smart_alert_id' => $removed_alert->id]);

                //notify user
                //$user->notify(new Backup($notificatoion));
            }
        }
    }

}
