<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
// models
use App\EmailCampaign;
use App\BackupHistory;
use App\User;
use Illuminate\Support\Facades\Log;
use App\BackupProgress;
use App\Notifications\Backup;
use App\Notification;
use App\SmartAlert;
use App\SmartAlertHistory;
use Carbon\Carbon;

class CampaignsData extends Command {

    /**
     * The name of the apiUrl
     * @var type 
     */
    protected $api_url;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'email-campaigns:data {userId}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command will get email campaigns data';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();

        //get api key from env file
        $this->api_url = env('HUBSPOT_URL');
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        $this->info('Email Campaigns Data cron started....' . $this->api_url);

        $status = '';
        $error_msg = '';
        $created_rows = 0;
        $updated_rows = 0;
        $hubspot_email_campaign_ids = [];

        $userId = $this->argument('userId');
        $user = User::find($userId);

        $backup_progress = BackupProgress::where('user_id', $user['id'])->where('status', 'Inprogress')->orderByDesc('id')->first();


        try {


            Log::error('Fetching Email Campaigns data started |||| Start >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> userId - ' . $userId);

            //start time
            $time_start = microtime(true);


            //user token
            $token = $user->accesstoken;

            //using GuzzleHttp client to get request
            $client = new \GuzzleHttp\Client(['http_errors' => false]);
            $headers = [
                'Authorization' => 'Bearer ' . $token,
                'Accept' => 'application/json',
            ];

            $get_data = $this->getCampaignsData($headers, 0, $client, $backup_progress, $user, $hubspot_email_campaign_ids, $status, $error_msg, $created_rows, $updated_rows);

            $created_rows = $get_data['created_rows'];
            $updated_rows = $get_data['updated_rows'];
            $hubspot_email_campaign_ids = $get_data['hubspot_email_campaign_ids'];
            $error_msg = $get_data['error_msg'];
            $status = $get_data['status'];

            //end time
            $time_end = microtime(true);
            //execution time
            $execution_time = ($time_end - $time_start);

            Log::error('time taken for Email Campaigns data ' . $execution_time);
            Log::error('Fetching Email Campaigns data completed |||| End <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< userId - ' . $userId);
        } catch (BadResponseException $ex) {
            //if request is invalid
            $response = $ex->getResponse();
            $jsonBody = (string) $response->getBody();
            $this->error($jsonBody);

            $data = json_decode($response->getBody(), true);
            $status = $data['status'];
            $error_msg = $data['message'];
            Log::error('CMS Email Campaigns data  |||| Error - ' . $jsonBody);
        }

        //get email campaigns from database
        $email_campaigns_in_database = EmailCampaign::where('backup_progress_id', $backup_progress['id'] - 1)->get()->pluck('email_campaign_id')->toArray();

        //check if empty result 
        if (count($hubspot_email_campaign_ids) == 0) {
            $removed_rows = 0;
        } else {
            $removed_email_campaigns = array_diff($email_campaigns_in_database, $hubspot_email_campaign_ids);
            $removed_rows = count($removed_email_campaigns);
        }

        //creating BackupHistory record
        $backup_history = BackupHistory::create([
                    'backup_progress_id' => $backup_progress['id'],
                    'user_id' => $user->id,
                    'object_name' => 'Fetched Email Campaigns data',
                    'num_of_records' => $created_rows + $removed_rows,
                    'removed_records' => $removed_rows,
                    'added_records' => $created_rows,
                    'changed_records' => $updated_rows,
                    'num_of_API_calls' => 2,
                    'status' => $status,
                    'error_message' => $error_msg]);

        //get smart alerts for action 'added'
        $added_alert = SmartAlert::where('user_id', $user['id'])
                        ->where('s_object', 'Campaigns')
                        ->where('action', 'Added')->first();

        if (!empty($added_alert)) {
            if ($created_rows > $added_alert->amount) {

                //notification when Campaign added
                $add_notificatoion = Notification::create(['user_id' => $user['id'], 'backup_progress_id' => $backup_progress['id'],
                            'status' => 'Campaigns Added', 'type' => 'email', 'num_of_records' => $created_rows]);

                //create history
                SmartAlertHistory::create(['user_id' => $user['id'], 'smart_alert_id' => $added_alert->id]);

                //notify user
                //$user->notify(new Backup($notificatoion));
            }
        }

        //get smart alerts for action 'Changed'
        $changed_alert = SmartAlert::where('user_id', $user['id'])
                        ->where('s_object', 'Campaigns')
                        ->where('action', 'Changed')->first();

        if (!empty($changed_alert)) {
            if ($updated_rows > $changed_alert->amount) {

                //notification when Email Campaign Changed
                $change_notificatoion = Notification::create(['user_id' => $user['id'], 'backup_progress_id' => $backup_progress['id'],
                            'status' => 'Campaigns Changed', 'type' => 'email', 'num_of_records' => $updated_rows]);

                //create history
                SmartAlertHistory::create(['user_id' => $user['id'], 'smart_alert_id' => $changed_alert->id]);

                //notify user
                //$user->notify(new Backup($notificatoion));
            }
        }

        //get smart alerts for action 'Removed'
        $removed_alert = SmartAlert::where('user_id', $user['id'])
                        ->where('s_object', 'Campaigns')
                        ->where('action', 'Removed')->first();

        if (!empty($removed_alert)) {
            if ($removed_rows > $removed_alert->amount) {

                //notification when Email Campaign Removed
                $remove_notificatoion = Notification::create(['user_id' => $user['id'], 'backup_progress_id' => $backup_progress['id'],
                            'status' => 'Campaigns Removed', 'type' => 'email', 'num_of_records' => $removed_rows]);

                //create history
                SmartAlertHistory::create(['user_id' => $user['id'], 'smart_alert_id' => $removed_alert->id]);

                //notify user
                //$user->notify(new Backup($notificatoion));
            }
        }
    }

    public function getCampaignsData($headers, $offset, $client, $backup_progress, $user, $hubspot_email_campaign_ids, $status, $error_msg, $created_rows, $updated_rows) {

        if ($offset == 0) {
            $off = '';
        } else {
            $off = '&offset=' . $offset;
        }
        $campaign_url = $this->api_url . '/email/public/v1/campaigns/by-id?limit=100' . $off;

        //get all email campaigns data
        $campaign_response = $client->request('GET', $campaign_url, ['headers' => $headers]);


        $campaign_body = $campaign_response->getBody();
        $campaign_data = json_decode($campaign_body, true);


        if (!empty($campaign_data['status']) == 'error') {
            $status = $campaign_data['status'];
            $error_msg = $campaign_data['message'];
            Log::error('Email Campaigns data  |||| Error - ' . $error_msg);
        } else {
            $campaign_results = $campaign_data['campaigns'];
            $status = 'successed';
            if (!empty($campaign_results)) {
                $campaigns = [];
                foreach ($campaign_results as $value) {


                    $compaign_meta_url = $this->api_url . '/email/public/v1/campaigns/' . $value['id'];

                    //get complete details of each campaign
                    $response_compaign_meta = $client->request('GET', $compaign_meta_url, ['headers' => $headers]);
                    $body_compaign_meta = $response_compaign_meta->getBody();
                    $data_compaign_meta = json_decode($body_compaign_meta, true);

                    if (!empty($data_compaign_meta['status']) == 'error') {
                        $status = 'warning';
                        $error_msg = $data_compaign_meta['message'];
                        Log::error('Email Campaigns data  |||| Warning - ' . $error_msg);
                    } else {


                        $check_compaign = EmailCampaign::where('user_id', $user->id)
                                        ->where('email_campaign_id', $value['id'])
                                        ->where('backup_progress_id', $backup_progress['id'] - 1)->first();
                        $campaigns[] = [
                            'user_id' => $user->id,
                            'backup_progress_id' => $backup_progress['id'],
                            'email_campaign_id' => $value['id'],
                            'app_id' => isset($value['appId']) ? $value['appId'] : 0,
                            'app_name' => isset($value['appName']) ? $value['appName'] : "",
                            'content_id' => isset($data_compaign_meta['contentId']) ? $data_compaign_meta['contentId'] : 0,
                            'counters_delivered' => isset($data_compaign_meta['counters']['delivered']) ? $data_compaign_meta['counters']['delivered'] : 0,
                            'counters_open' => isset($data_compaign_meta['counters']['open']) ? $data_compaign_meta['counters']['open'] : 0,
                            'counters_processed' => isset($data_compaign_meta['counters']['processed']) ? $data_compaign_meta['counters']['processed'] : 0,
                            'counters_sent' => isset($data_compaign_meta['counters']['sent']) ? $data_compaign_meta['counters']['sent'] : 0,
                            'name' => isset($data_compaign_meta['name']) ? $data_compaign_meta['name'] : "",
                            'num_included' => isset($data_compaign_meta['numIncluded']) ? $data_compaign_meta['numIncluded'] : "",
                            'num_queued' => isset($data_compaign_meta['numQueued']) ? $data_compaign_meta['numQueued'] : "",
                            'sub_type' => isset($data_compaign_meta['subType']) ? $data_compaign_meta['subType'] : "",
                            'subject' => isset($data_compaign_meta['subject']) ? $data_compaign_meta['subject'] : "",
                            'type' => isset($data_compaign_meta['type']) ? $data_compaign_meta['type'] : "",
                            'created_at' => Carbon::now()->toDateTimeString(),
                            'updated_at' => Carbon::now()->toDateTimeString()
                        ];

                        //check if campaign  is already inserted or not
                        if ($check_compaign === null) {
                            
                        } else {


                            //check if any data is changed
                            if ($check_compaign->app_name != $data_compaign_meta['appName'] ||
                                    $check_compaign->content_id != $data_compaign_meta['contentId'] ||
                                    $check_compaign->counters_delivered != $data_compaign_meta['counters']['delivered'] ||
                                    $check_compaign->counters_open != $data_compaign_meta['counters']['open'] ||
                                    $check_compaign->counters_processed != $data_compaign_meta['counters']['processed'] ||
                                    $check_compaign->counters_sent != $data_compaign_meta['counters']['sent'] ||
                                    $check_compaign->name != $data_compaign_meta['name'] ||
                                    $check_compaign->num_included != $data_compaign_meta['numIncluded'] ||
                                    $check_compaign->num_queued != $data_compaign_meta['numQueued'] ||
                                    $check_compaign->sub_type != $data_compaign_meta['subType'] ||
                                    $check_compaign->subject != $data_compaign_meta['subject'] ||
                                    $check_compaign->type != $data_compaign_meta['type']) {

                                $updated_rows +=1;
                            }
                        }
                    }
                }
                //insert EmailCampaign in bulk
                EmailCampaign::insert($campaigns);
                $created_rows += count($campaigns);
            }
        }

        if (isset($data['hasMore']) && !empty($data['hasMore'] == true)) {

            return $this->getCampaignsData($headers, $data['offset'], $client, $backup_progress, $user, $hubspot_email_campaign_ids, $status, $error_msg, $created_rows, $updated_rows);
        } else {

            $data = ['created_rows' => $created_rows, 'updated_rows' => $updated_rows,
                'hubspot_email_campaign_ids' => $hubspot_email_campaign_ids, 'status' => $status, 'error_msg' => $error_msg];

            return $data;
        }
    }

}
