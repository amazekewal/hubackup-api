<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
// models
use App\Form;
use App\FormField;
use App\BackupHistory;
use App\User;
use Illuminate\Support\Facades\Log;
use App\BackupProgress;
use App\Notifications\Backup;
use App\Notification;
use App\SmartAlert;
use App\SmartAlertHistory;
use Carbon\Carbon;

class FormsData extends Command {

    /**
     * The name of the apiUrl
     * @var type 
     */
    protected $api_url;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'forms:data {userId}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command will get all forms data';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();

        //get api key from env file
        $this->api_url = env('HUBSPOT_URL');
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        $this->info('Forms Data cron started....' . $this->api_url);

        $status = '';
        $error_msg = '';
        $created_rows = 0;
        $updated_rows = 0;
        $hubspot_form_ids = [];

        $userId = $this->argument('userId');
        $user = User::find($userId);

        $backup_progress = BackupProgress::where('user_id', $user['id'])->where('status', 'Inprogress')->orderByDesc('id')->first();


        try {

            Log::error('Fetching Forms data started |||| Start >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> userId - ' . $userId);

            //start time
            $time_start = microtime(true);


            //user token
            $token = $user->accesstoken;

            //using GuzzleHttp client to get request
            $client = new \GuzzleHttp\Client(['http_errors' => false]);
            $headers = [
                'Authorization' => 'Bearer ' . $token,
                'Accept' => 'application/json',
            ];

            $url = $this->api_url . '/forms/v2/forms';

            //get all forms data
            $response = $client->request('GET', $url, ['headers' => $headers]);


            $body = $response->getBody();
            $all_results = json_decode($body, true);

            if (!empty($all_results['status']) == 'error') {
                $status = $all_results['status'];
                $error_msg = $all_results['message'];
                Log::error('Forms data  |||| Error - ' . $error_msg);
            } else {
                $status = 'successed';

                //turn data into collection
                $results_collection = collect($all_results);
                $results_chunks = $results_collection->chunk(500); //chunk into smaller pieces


                foreach ($results_chunks as $results_chunk) {

                    $results = $results_chunk->toArray();

                    if (!empty($results)) {
                        foreach ($results as $value) {

                            //get all forms ids
                            $hubspot_form_ids[] = $value['guid'];



                            $check_form = Form::where('user_id', $user->id)
                                            ->where('guid', $value['guid'])
                                            ->where('backup_progress_id', $backup_progress['id'] - 1)->first();
                            $form = Form::create([
                                        'user_id' => $user->id,
                                        'backup_progress_id' => $backup_progress['id'],
                                        'portal_id' => $value['portalId'],
                                        'guid' => isset($value['guid']) ? $value['guid'] : "",
                                        'name' => isset($value['name']) ? $value['name'] : "",
                                        'action' => isset($value['action']) ? $value['action'] : "",
                                        'method' => isset($value['method']) ? $value['method'] : "",
                                        'css_class' => isset($value['cssClass']) ? $value['cssClass'] : "",
                                        'redirect' => isset($value['redirect']) ? $value['redirect'] : "",
                                        'submit_text' => isset($value['submitText']) ? $value['submitText'] : "",
                                        'follow_up_id' => isset($value['followUpId']) ? $value['followUpId'] : "",
                                        'notify_recipients' => isset($value['notifyRecipients']) ? $value['notifyRecipients'] : "",
                                        'lead_nurturing_campaign_id' => isset($value['leadNurturingCampaignId']) ? $value['leadNurturingCampaignId'] : 0,
                                        'performable_html' => isset($value['performableHtml']) ? $value['performableHtml'] : "",
                                        'migrated_from' => isset($value['migratedFrom']) ? $value['migratedFrom'] : "",
                                        'ignore_current_values' => isset($value['ignoreCurrentValues']) ? $value['ignoreCurrentValues'] : "",
                                        'meta_data' => !empty($value['metaData']) ? $value['metaData'] : NULL,
                                        'deletable' => isset($value['deletable']) ? $value['deletable'] : 0,
                                        'inline_message' => isset($value['inlineMessage']) ? $value['inlineMessage'] : "",
                                        'tms_id' => isset($value['tmsId']) ? $value['tmsId'] : 0,
                                        'captcha_enabled' => isset($value['captchaEnabled']) ? $value['captchaEnabled'] : 0,
                                        'campaign_guid' => isset($value['campaignGuid']) ? $value['campaignGuid'] : "",
                                        'cloneable' => isset($value['cloneable']) ? $value['cloneable'] : 0,
                                        'editable' => isset($value['editable']) ? $value['editable'] : 0,
                                        'form_type' => isset($value['formType']) ? $value['formType'] : 0,
                                        'deleted_at' => isset($value['deletedAt']) ? $value['deletedAt'] : 0,
                                        'theme_name' => isset($value['themeName']) ? $value['themeName'] : "",
                                        'parent_id' => isset($value['parentId']) ? $value['parentId'] : "",
                                        'style' => isset($value['style']) ? $value['style'] : "",
                                        'is_published' => isset($value['isPublished']) ? $value['isPublished'] : 0,
                                        'publish_at' => isset($value['publishAt']) ? Carbon::createFromTimestampMs($value['publishAt'])->format('Y-m-d H:i:s') : NULL,
                                        'unpublish_at' => isset($value['unpublishAt']) ? Carbon::createFromTimestampMs($value['unpublishAt'])->format('Y-m-d H:i:s') : NULL,
                                        'published_at' => isset($value['publishedAt']) ? Carbon::createFromTimestampMs($value['publishedAt'])->format('Y-m-d H:i:s') : NULL,
                                        'winning_variant_id' => isset($value['multivariateTest']['winningVariantId']) ? $value['multivariateTest']['winningVariantId'] : "",
                                        'finished' => isset($value['multivariateTest']['finished']) ? $value['multivariateTest']['finished'] : "",
                                        'control_id' => isset($value['multivariateTest']['controlId']) ? $value['multivariateTest']['controlId'] : "",
                                        'kickback_emailworkflow_id' => isset($value['kickbackEmailWorkflowId']) ? $value['kickbackEmailWorkflowId'] : "",
                                        'kickback_emails_json' => isset($value['kickbackEmailsJson']) ? $value['kickbackEmailsJson'] : "",
                                        'created_on' => isset($value['createdAt']) ? Carbon::createFromTimestampMs($value['createdAt'])->format('Y-m-d H:i:s') : NULL,
                                        'updated_on' => isset($value['updatedAt']) ? Carbon::createFromTimestampMs($value['updatedAt'])->format('Y-m-d H:i:s') : NULL,
                            ]);

                            if (!empty($value['formFieldGroups'])) {
                                foreach ($value['formFieldGroups'] as $form_field) {

                                    $form_field = FormField::create([
                                                'form_db_id' => $form->id,
                                                'name' => isset($form_field['fields'][0]['name']) ? $form_field['fields'][0]['name'] : "",
                                                'label' => isset($form_field['fields'][0]['label']) ? $form_field['fields'][0]['label'] : "",
                                                'type' => isset($form_field['fields'][0]['type']) ? $form_field['fields'][0]['type'] : "",
                                                'field_type' => isset($form_field['fields'[0]]['fieldType']) ? $form_field['fields'][0]['fieldType'] : "",
                                                'description' => isset($form_field['fields'][0]['description']) ? $form_field['fields'][0]['description'] : "",
                                                'group_name' => isset($form_field['fields'][0]['groupName']) ? $form_field['fields'][0]['groupName'] : "",
                                                'display_order' => isset($form_field['fields'][0]['displayOrder']) ? $form_field['fields'][0]['displayOrder'] : 0,
                                                'selected_options' => !empty($form_field['fields'][0]['selectedOptions']) ? $form_field['fields'][0]['selectedOptions'] : NULL,
                                                'options' => isset($form_field['fields'][0]['options']) ? $form_field['fields'][0]['options'] : NULL,
                                                'validation_name' => isset($form_field['fields'][0]['validation']['name']) ? $form_field['fields'][0]['validation']['name'] : "",
                                                'validation_message' => isset($form_field['fields'][0]['validation']['message']) ? $form_field['fields'][0]['validation']['message'] : "",
                                                'validation_data' => isset($form_field['fields'][0]['validation']['data']) ? $form_field['fields'][0]['validation']['data'] : "",
                                                'validation_use_default_block_list' => isset($form_field['fields'][0]['validation']['useDefaultBlockList']) ? $form_field['fields'][0]['validation']['useDefaultBlockList'] : 0,
                                                'validation_blocked_email_addresses' => !empty($form_field['fields'][0]['validation']['blockedEmailAddresses']) ? $form_field['fields'][0]['validation']['blockedEmailAddresses'] : NULL,
                                                'hidden' => isset($form_field['fields']['hidden']) ? $form_field['fields'][0]['hidden'] : 0,
                                                'enabled' => isset($form_field['fields']['enabled']) ? $form_field['fields'][0]['enabled'] : 0,
                                                'default_value' => isset($form_field['fields']['defaultValue']) ? $form_field['fields'][0]['defaultValue'] : "",
                                                'is_smart_field' => isset($form_field['fields']['isSmartField']) ? $form_field['fields'][0]['isSmartField'] : 0,
                                                'unselected_label' => isset($form_field['fields']['unselectedLabel']) ? $form_field['fields'][0]['unselectedLabel'] : "",
                                                'placeholder' => isset($form_field['fields']['placeholder']) ? $form_field['fields'][0]['placeholder'] : "",
                                                'dependent_field_filters' => !empty($form_field['fields'][0]['dependentFieldFilters']) ? $form_field['fields'][0]['dependentFieldFilters'] : NULL,
                                                'label_hidden' => isset($form_field['fields'][0]['labelHidden']) ? $form_field['fields'][0]['labelHidden'] : 0,
                                                'property_object_type' => isset($form_field['fields'][0]['propertyObjectType']) ? $form_field['fields'][0]['propertyObjectType'] : "",
                                                'default' => isset($form_field['default']) ? $form_field['default'] : 0,
                                                'is_smart_group' => isset($form_field['isSmartGroup']) ? $form_field['isSmartGroup'] : 0,
                                                'rich_text_content' => isset($form_field['richText']['content']) ? $form_field['richText']['content'] : "",
                                    ]);
                                }
                            }
                            //if form is created
                            if ($form) {
                                $created_rows +=1;
                            }

                            //check if form  is already inserted or not
                            if ($check_form === null) {
                                
                            } else {
                                $last_updated_date = Carbon::createFromTimestampMs($value['updatedAt'])->format('Y-m-d H:i:s');

                                //check if any data is changed
                                if ($last_updated_date != $check_form->updated_on) {
                                    $updated_rows +=1;
                                }
                            }
                        }
                    }
                }
            }
            //end time
            $time_end = microtime(true);
            //execution time
            $execution_time = ($time_end - $time_start);

            Log::error('time taken for Forms data ' . $execution_time);
            Log::error('Fetching Forms data completed |||| End <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< userId - ' . $userId);
        } catch (BadResponseException $ex) {
            //if request is invalid
            $response = $ex->getResponse();
            $jsonBody = (string) $response->getBody();
            $this->error($jsonBody);

            $data = json_decode($response->getBody(), true);
            $status = $data['status'];
            $error_msg = $data['message'];
            Log::error('Forms data  |||| Error - ' . $jsonBody);
        }

        //get forms from database
        $forms_in_database = Form::where('backup_progress_id', $backup_progress['id'] - 1)->get()->pluck('guid')->toArray();
        
        //check if empty result 
        if (count($hubspot_form_ids) == 0) {
            $removed_rows = 0;
        } else {
        $removed_forms = array_diff($forms_in_database, $hubspot_form_ids);
        $removed_rows = count($removed_forms);
        }

        //creating BackupHistory record
        $backup_history = BackupHistory::create([
                    'backup_progress_id' => $backup_progress['id'],
                    'user_id' => $user->id,
                    'object_name' => 'Fetched Forms',
                    'num_of_records' => $created_rows + $removed_rows,
                    'removed_records' => $removed_rows,
                    'added_records' => $created_rows,
                    'changed_records' => $updated_rows,
                    'num_of_API_calls' => 1,
                    'status' => $status,
                    'error_message' => $error_msg]);

        //get smart alerts for action 'added'
        $added_alert = SmartAlert::where('user_id', $user['id'])
                        ->where('s_object', 'Forms')
                        ->where('action', 'Added')->first();

        if (!empty($added_alert)) {
            if ($created_rows > $added_alert->amount) {

                //notification when Form added
                $add_notificatoion = Notification::create(['user_id' => $user['id'], 'backup_progress_id' => $backup_progress['id'],
                            'status' => 'Forms Added', 'type' => 'email', 'num_of_records' => $created_rows]);

                //create history
                SmartAlertHistory::create(['user_id' => $user['id'], 'smart_alert_id' => $added_alert->id]);

                //notify user
                //$user->notify(new Backup($notificatoion));
            }
        }

        //get smart alerts for action 'Changed'
        $changed_alert = SmartAlert::where('user_id', $user['id'])
                        ->where('s_object', 'Forms')
                        ->where('action', 'Changed')->first();

        if (!empty($changed_alert)) {
            if ($updated_rows > $changed_alert->amount) {

                //notification when Forms Changed
                $change_notificatoion = Notification::create(['user_id' => $user['id'], 'backup_progress_id' => $backup_progress['id'],
                            'status' => 'Forms Changed', 'type' => 'email', 'num_of_records' => $updated_rows]);

                //create history
                SmartAlertHistory::create(['user_id' => $user['id'], 'smart_alert_id' => $changed_alert->id]);

                //notify user
                //$user->notify(new Backup($notificatoion));
            }
        }

        //get smart alerts for action 'Removed'
        $removed_alert = SmartAlert::where('user_id', $user['id'])
                        ->where('s_object', 'Forms')
                        ->where('action', 'Removed')->first();

        if (!empty($removed_alert)) {
            if ($removed_rows > $removed_alert->amount) {

                //notification when Form Removed
                $remove_notificatoion = Notification::create(['user_id' => $user['id'], 'backup_progress_id' => $backup_progress['id'],
                            'status' => 'Forms Removed', 'type' => 'email', 'num_of_records' => $removed_rows]);

                //create history
                SmartAlertHistory::create(['user_id' => $user['id'], 'smart_alert_id' => $removed_alert->id]);

                //notify user
                //$user->notify(new Backup($notificatoion));
            }
        }
    }

}
