<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
// models
use App\Owner;
use App\BackupHistory;
use App\User;
use Illuminate\Support\Facades\Log;
use App\BackupProgress;
use App\Notifications\Backup;
use App\Notification;
use App\SmartAlert;
use App\SmartAlertHistory;
use Carbon\Carbon;

class OwnersData extends Command {

    /**
     * The name of the apiUrl
     * @var type 
     */
    protected $api_url;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'owners:data {userId}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command will get all owners data';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();

        //get api key from env file
        $this->api_url = env('HUBSPOT_URL');
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        $this->info('Owners Data cron started....' . $this->api_url);

        $status = '';
        $error_msg = '';
        $created_rows = 0;
        $updated_rows = 0;
        $hubspot_owner_ids = [];

        $userId = $this->argument('userId');
        $user = User::find($userId);

        $backup_progress = BackupProgress::where('user_id', $user['id'])->where('status', 'Inprogress')->orderByDesc('id')->first();


        try {

            Log::error('Fetching Owners data started |||| Start >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> userId - ' . $userId);

            //start time
            $time_start = microtime(true);


            //user token
            $token = $user->accesstoken;

            //using GuzzleHttp client to get request
            $client = new \GuzzleHttp\Client(['http_errors' => false]);
            $headers = [
                'Authorization' => 'Bearer ' . $token,
                'Accept' => 'application/json',
            ];

            $url = $this->api_url . '/owners/v2/owners';

            //get all owners data
            $response = $client->request('GET', $url, ['headers' => $headers]);


            $body = $response->getBody();
            $all_results = json_decode($body, true);

            if (!empty($all_results['status']) == 'error') {
                $status = $all_results['status'];
                $error_msg = $all_results['message'];
                Log::error('Owners data  |||| Error - ' . $error_msg);
            } else {
                $status = 'successed';


                //turn data into collection
                $results_collection = collect($all_results);
                $results_chunks = $results_collection->chunk(500); //chunk into smaller pieces


                foreach ($results_chunks as $results_chunk) {

                    $results = $results_chunk->toArray();

                    if (!empty($results)) {
                        $create_owners = [];
                        foreach ($results as $value) {

                            //get all owners ids
                            $hubspot_owner_ids[] = $value['ownerId'];



                            $check_owner = Owner::where('user_id', $user->id)
                                            ->where('owner_id', $value['ownerId'])
                                            ->where('backup_progress_id', $backup_progress['id'] - 1)->first();

                            $create_owners[] = [
                                'user_id' => $user->id,
                                'backup_progress_id' => $backup_progress['id'],
                                'owner_id' => $value['ownerId'],
                                'portal_id' => isset($value['portalId']) ? $value['portalId'] : 0,
                                'first_name' => isset($value['firstName']) ? $value['firstName'] : "",
                                'last_name' => isset($value['lastName']) ? $value['lastName'] : "",
                                'email' => isset($value['email']) ? $value['email'] : "",
                                'has_contacts_access' => isset($value['hasContactsAccess']) ? $value['hasContactsAccess'] : 0,
                                'active_user_id' => isset($value['activeUserId']) ? $value['activeUserId'] : 0,
                                'is_active' => isset($value['isActive']) ? $value['isActive'] : 0,
                                'user_id_including_inactive' => isset($value['userIdIncludingInactive']) ? $value['userIdIncludingInactive'] : 0,
                                'remote_list_id' => isset($value['remoteList']['id']) ? $value['remoteList']['id'] : 0,
                                'remote_list_portal_id' => isset($value['remoteList']['portalId']) ? $value['remoteList']['portalId'] : 0,
                                'remote_list_owner_id' => isset($value['remoteList']['ownerId']) ? $value['remoteList']['ownerId'] : 0,
                                'remote_list_remote_id' => isset($value['remoteList']['remoteId']) ? $value['remoteList']['remoteId'] : 0,
                                'remote_list_remote_type' => isset($value['remoteList']['remoteType']) ? $value['remoteList']['remoteType'] : "",
                                'remote_list_active' => isset($value['remoteList']['active']) ? $value['remoteList']['active'] : 0,
                                'created' => isset($value['createdAt']) ? Carbon::createFromTimestampMs($value['createdAt'])->format('Y-m-d H:i:s') : NULL,
                                'updated' => isset($value['updatedAt']) ? Carbon::createFromTimestampMs($value['updatedAt'])->format('Y-m-d H:i:s') : NULL,
                                'created_at' => Carbon::now()->toDateTimeString(),
                                'updated_at' => Carbon::now()->toDateTimeString()
                            ];

                            //check if owner  is already inserted or not
                            if ($check_owner === null) {
                                
                            } else {
                                $last_updated_date = Carbon::createFromTimestampMs($value['updatedAt'])->format('Y-m-d H:i:s');

                                //check if any data is changed
                                if ($last_updated_date != $check_owner->updated) {
                                    $updated_rows +=1;
                                }
                            }
                        }
                        //insert owners in bulk
                        Owner::insert($create_owners);
                        $created_rows += count($create_owners);
                    }
                }
            }
            //end time
            $time_end = microtime(true);
            //execution time
            $execution_time = ($time_end - $time_start);

            Log::error('time taken for Owners data ' . $execution_time);
            Log::error('Fetching Owners data completed |||| End <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< userId - ' . $userId);
        } catch (BadResponseException $ex) {
            //if request is invalid
            $response = $ex->getResponse();
            $jsonBody = (string) $response->getBody();
            $this->error($jsonBody);

            $data = json_decode($response->getBody(), true);
            $status = $data['status'];
            $error_msg = $data['message'];
            Log::error('CMS Owners data  |||| Error - ' . $jsonBody);
        }

        //get owners from database
        $owners_in_database = Owner::where('backup_progress_id', $backup_progress['id'] - 1)->get()->pluck('owner_id')->toArray();
        
        //check if empty result 
        if (count($hubspot_owner_ids) == 0) {
            $removed_rows = 0;
        } else {
        $removed_owners = array_diff($owners_in_database, $hubspot_owner_ids);
        $removed_rows = count($removed_owners);
        }

        //creating BackupHistory record
        $backup_history = BackupHistory::create([
                    'backup_progress_id' => $backup_progress['id'],
                    'user_id' => $user->id,
                    'object_name' => 'Fetched Owners data',
                    'num_of_records' => $created_rows + $removed_rows,
                    'removed_records' => $removed_rows,
                    'added_records' => $created_rows,
                    'changed_records' => $updated_rows,
                    'num_of_API_calls' => 1,
                    'status' => $status,
                    'error_message' => $error_msg]);

        //get smart alerts for action 'added'
        $added_alert = SmartAlert::where('user_id', $user['id'])
                        ->where('s_object', 'Owners')
                        ->where('action', 'Added')->first();

        if (!empty($added_alert)) {
            if ($created_rows > $added_alert->amount) {

                //notification when Owner added
                $add_notificatoion = Notification::create(['user_id' => $user['id'], 'backup_progress_id' => $backup_progress['id'],
                            'status' => 'Owners Added', 'type' => 'email', 'num_of_records' => $created_rows]);

                //create history
                SmartAlertHistory::create(['user_id' => $user['id'], 'smart_alert_id' => $added_alert->id]);

                //notify user
                //$user->notify(new Backup($notificatoion));
            }
        }

        //get smart alerts for action 'Changed'
        $changed_alert = SmartAlert::where('user_id', $user['id'])
                        ->where('s_object', 'Owners')
                        ->where('action', 'Changed')->first();

        if (!empty($changed_alert)) {
            if ($updated_rows > $changed_alert->amount) {

                //notification when Owner Changed
                $change_notificatoion = Notification::create(['user_id' => $user['id'], 'backup_progress_id' => $backup_progress['id'],
                            'status' => 'Owners Changed', 'type' => 'email', 'num_of_records' => $updated_rows]);

                //create history
                SmartAlertHistory::create(['user_id' => $user['id'], 'smart_alert_id' => $changed_alert->id]);

                //notify user
                //$user->notify(new Backup($notificatoion));
            }
        }

        //get smart alerts for action 'Removed'
        $removed_alert = SmartAlert::where('user_id', $user['id'])
                        ->where('s_object', 'Owners')
                        ->where('action', 'Removed')->first();

        if (!empty($removed_alert)) {
            if ($removed_rows > $removed_alert->amount) {

                //notification when Owner Removed
                $remove_notificatoion = Notification::create(['user_id' => $user['id'], 'backup_progress_id' => $backup_progress['id'],
                            'status' => 'Owners Removed', 'type' => 'email', 'num_of_records' => $removed_rows]);

                //create history
                SmartAlertHistory::create(['user_id' => $user['id'], 'smart_alert_id' => $removed_alert->id]);

                //notify user
                //$user->notify(new Backup($notificatoion));
            }
        }
    }

}
