<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\User;
use App\BackupProgress;
use App\Notification;
use Carbon\Carbon;
class BackupStarted extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'backup:started {userId}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'backup is started';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $userId = $this->argument('userId');
        $user = User::find($userId);
        
        // start backup progress
         $backup_progress = BackupProgress::create(['user_id' => $user['id'],
                     'status' => 'Inprogress',
                     'start_date' => Carbon::now()]);


         //create notification when backup started
         $notificatoion = Notification::create(['user_id' => $user['id'],
                     'backup_progress_id' => $backup_progress['id'],
                     'status' => 'Backup Started', 'type' => 'email']);

         //notify user
         //$user->notify(new Backup($notificatoion));
         
    }
}
