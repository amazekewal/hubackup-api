<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
// models
use App\PublishingChannel;
use App\BackupHistory;
use App\User;
use Illuminate\Support\Facades\Log;
use App\BackupProgress;
use App\Notifications\Backup;
use App\Notification;
use App\SmartAlert;
use App\SmartAlertHistory;
use Carbon\Carbon;

class PublishingChannelsData extends Command {

    /**
     * The name of the apiUrl
     * @var type 
     */
    protected $api_url;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'publishing-channels:data {userId}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command will get all publishing channels data';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();

        //get api key from env file
        $this->api_url = env('HUBSPOT_URL');
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        $this->info('Publishing channels Data cron started....' . $this->api_url);

        $status = '';
        $error_msg = '';
        $created_rows = 0;
        $updated_rows = 0;
        $hubspot_channel_ids = [];
        
        $userId = $this->argument('userId');
        $user = User::find($userId);

        $backup_progress = BackupProgress::where('user_id', $user['id'])->where('status', 'Inprogress')->orderByDesc('id')->first();

        try {
           
            Log::error('Fetching Publishing channels data started |||| Start >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> userId - ' . $userId);

            //start time
            $time_start = microtime(true);


            //user token
            $token = $user->accesstoken;

            //using GuzzleHttp client to get request
            $client = new \GuzzleHttp\Client(['http_errors' => false]);
            $headers = [
                'Authorization' => 'Bearer ' . $token,
                'Accept' => 'application/json',
            ];

            $url = $this->api_url . '/broadcast/v1/channels/setting/publish/current';

            //get all Publishing channels data
            $response = $client->request('GET', $url, ['headers' => $headers]);


            $body = $response->getBody();
            $results = json_decode($body, true);

            if (!empty($results['status']) == 'error') {
                $status = $results['status'];
                $error_msg = $results['message'];
                Log::error('Publishing channels data  |||| Error - ' . $error_msg);
            } else {
                $status = 'successed';
                if (!empty($results)) {
                    $create_channels = [];
                    foreach ($results as $value) {

                        //get all channels ids
                        $hubspot_channel_ids[] = $value['channelId'];

                        

                        $check_channel = PublishingChannel::where('user_id', $user->id)
                                        ->where('channel_id', $value['channelId'])
                                        ->where('backup_progress_id', $backup_progress['id'] - 1)->first();

                        $create_channels = [
                                        'user_id' => $user->id,
                                        'backup_progress_id' => $backup_progress['id'],
                                        'channel_id' => $value['channelId'],
                                        'portal_id' => isset($value['portalId']) ? $value['portalId'] : 0,
                                        'channel_guid' => isset($value['channelGuid']) ? $value['channelGuid'] : "",
                                        'account_guid' => isset($value['accountGuid']) ? $value['accountGuid'] : "",
                                        'name' => isset($value['name']) ? $value['name'] : "",
                                        'settings_publish' => isset($value['settings']['publish']) ? $value['settings']['publish'] : 0,
                                        'type' => isset($value['type']) ? $value['type'] : "",
                                        'image_url' => isset($value['dataMap']['imageUrl']) ? $value['dataMap']['imageUrl'] : "",
                                        'picture' => isset($value['dataMap']['picture']) ? $value['dataMap']['picture'] : "",
                                        'user_name' => isset($value['dataMap']['userName']) ? $value['dataMap']['userName'] : "",
                                        'real_name' => isset($value['dataMap']['realName']) ? $value['dataMap']['realName'] : "",
                                        'last_name' => isset($value['dataMap']['lastName']) ? $value['dataMap']['lastName'] : "",
                                        'page_name' => isset($value['dataMap']['pageName']) ? $value['dataMap']['pageName'] : "",
                                        'profile_url' => isset($value['dataMap']['profileUrl']) ? $value['dataMap']['profileUrl'] : "",
                                        'email' => isset($value['dataMap']['email']) ? $value['dataMap']['email'] : "",
                                        'user' => isset($value['dataMap']['userId']) ? $value['dataMap']['userId'] : "",
                                        'full_name' => isset($value['dataMap']['fullName']) ? $value['dataMap']['fullName'] : "",
                                        'first_name' => isset($value['dataMap']['firstName']) ? $value['dataMap']['firstName'] : "",
                                        'page_category' => isset($value['dataMap']['pageCategory']) ? $value['dataMap']['pageCategory'] : "",
                                        'created_on' => isset($value['createdAt']) ? Carbon::createFromTimestampMs($value['createdAt'])->format('Y-m-d H:i:s') : NULL,
                                        'updated_on' => isset($value['updatedAt']) ? Carbon::createFromTimestampMs($value['updatedAt'])->format('Y-m-d H:i:s') : NULL,
                                        'created_at' => Carbon::now()->toDateTimeString(),
                                        'updated_at' => Carbon::now()->toDateTimeString()
                                ];
                        //check if publishing channel  is already inserted or not
                        if ($check_channel === null) {

                                
                           
                        } else {
                           
                            $last_updated_date = Carbon::createFromTimestampMs($value['updatedAt'])->format('Y-m-d H:i:s');

                            //check if any data is changed
                            if ($last_updated_date != $check_channel->updated) {
                                
                                $updated_rows +=1;
                            }
                        }
                        
                    }
                    
                    //insert publishing channels in bulk
                    PublishingChannel::insert($create_channels);
                    $created_rows += count($create_channels);
                }
            }
            //end time
            $time_end = microtime(true);
            //execution time
            $execution_time = ($time_end - $time_start);
            
            Log::error('time taken for Publishing channels data ' . $execution_time);
            Log::error('Fetching Publishing channels data completed |||| End <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< userId - ' . $userId);
        } catch (BadResponseException $ex) {
            //if request is invalid
            $response = $ex->getResponse();
            $jsonBody = (string) $response->getBody();
            $this->error($jsonBody);

            $data = json_decode($response->getBody(), true);
            $status = $data['status'];
            $error_msg = $data['message'];
            Log::error('CMS Publishing channels data  |||| Error - ' . $jsonBody);
        }


        //get channels from database
        $channels_in_database = PublishingChannel::where('backup_progress_id', $backup_progress['id'] - 1)->get()->pluck('channel_id')->toArray();
        
        //check if empty result 
        if (count($hubspot_channel_ids) == 0) {
            $removed_rows = 0;
        } else {
        $removed_channels = array_diff($channels_in_database, $hubspot_channel_ids);
        $removed_rows = count($removed_channels);
        }
       
        //creating BackupHistory record
        $backup_history = BackupHistory::create([
                    'backup_progress_id' => $backup_progress['id'],
                    'user_id' => $user->id,
                    'object_name' => 'Fetched Publishing channels data',
                    'num_of_records' => $created_rows + $removed_rows,
                    'removed_records' => $removed_rows,
                    'added_records' => $created_rows,
                    'changed_records' => $updated_rows,
                    'num_of_API_calls' => 1,
                    'status' => $status,
                    'error_message' => $error_msg]);

        //get smart alerts for action 'added'
        $added_alert = SmartAlert::where('user_id', $user['id'])
                        ->where('s_object', 'Publishing channels')
                        ->where('action', 'Added')->first();

        if (!empty($added_alert)) {
            if ($created_rows > $added_alert->amount) {

                //notification when Publishing channel added
                $add_notificatoion = Notification::create(['user_id' => $user['id'],'backup_progress_id' => $backup_progress['id'],
                            'status' => 'Publishing channels Added', 'type' => 'email', 'num_of_records' => $created_rows]);

                //create history
                SmartAlertHistory::create(['user_id' => $user['id'], 'smart_alert_id' => $added_alert->id]);

                //notify user
                //$user->notify(new Backup($notificatoion));
            }
        }

        //get smart alerts for action 'Changed'
        $changed_alert = SmartAlert::where('user_id', $user['id'])
                        ->where('s_object', 'Publishing channels')
                        ->where('action', 'Changed')->first();

        if (!empty($changed_alert)) {
            if ($updated_rows > $changed_alert->amount) {

                //notification when Publishing channel Changed
                $change_notificatoion = Notification::create(['user_id' => $user['id'],'backup_progress_id' => $backup_progress['id'],
                            'status' => 'Publishing channels Changed', 'type' => 'email', 'num_of_records' => $updated_rows]);

                //create history
                SmartAlertHistory::create(['user_id' => $user['id'], 'smart_alert_id' => $changed_alert->id]);

                //notify user
                //$user->notify(new Backup($notificatoion));
            }
        }

        //get smart alerts for action 'Removed'
        $removed_alert = SmartAlert::where('user_id', $user['id'])
                        ->where('s_object', 'Publishing channels')
                        ->where('action', 'Removed')->first();

        if (!empty($removed_alert)) {
            if ($removed_rows > $removed_alert->amount) {

                //notification when Publishing channel Removed
                $remove_notificatoion = Notification::create(['user_id' => $user['id'],'backup_progress_id' => $backup_progress['id'],
                            'status' => 'Publishing channels Removed', 'type' => 'email', 'num_of_records' => $removed_rows]);

                //create history
                SmartAlertHistory::create(['user_id' => $user['id'], 'smart_alert_id' => $removed_alert->id]);

                //notify user
                //$user->notify(new Backup($notificatoion));
            }
        }
    }

}
