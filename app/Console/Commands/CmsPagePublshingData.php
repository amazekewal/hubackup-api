<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\BadResponseException;
use Illuminate\Support\Facades\Artisan;
// models
use App\CmsPagePublshing;
use App\CmsPagePublshingWidget;
use App\BackupHistory;
use App\User;
use Illuminate\Support\Facades\Log;
use App\BackupProgress;
use App\Notifications\Backup;
use App\Notification;
use App\SmartAlert;
use App\SmartAlertHistory;
use Carbon\Carbon;

class CmsPagePublshingData extends Command {

    /**
     * The name of the apiUrl
     * @var type 
     */
    protected $api_url;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cms-page-publishing:data {userId}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command will get all Cms Page Publishing data';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();

        //get api key from env file
        $this->api_url = env('HUBSPOT_URL');
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        $this->info('CMS Page Publishing Data cron started....' . $this->api_url);

        $status = '';
        $error_msg = '';
        $created_rows = 0;
        $updated_rows = 0;
        $hubspot_page_publishing_ids = [];

        $userId = $this->argument('userId');
        $user = User::find($userId);

        $backup_progress = BackupProgress::where('user_id', $user['id'])->where('status', 'Inprogress')->orderByDesc('id')->first();


        try {

            Log::error('Fetching CMS Page Publishing data started |||| Start >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> userId - ' . $userId);

            //start time
            $time_start = microtime(true);


            //user token
            $token = $user->accesstoken;

            //using GuzzleHttp client to get request
            $client = new \GuzzleHttp\Client(['http_errors' => false]);
            $headers = [
                'Authorization' => 'Bearer ' . $token,
                'Accept' => 'application/json',
            ];

            $get_data = $this->getCmsPagePublshingData($headers, 0, $client, $backup_progress, $user, $hubspot_page_publishing_ids, $status, $error_msg, $created_rows, $updated_rows);

            $created_rows = $get_data['created_rows'];
            $updated_rows = $get_data['updated_rows'];
            $hubspot_page_publishing_ids = $get_data['hubspot_page_publishing_ids'];
            $error_msg = $get_data['error_msg'];
            $status = $get_data['status'];

            //end time
            $time_end = microtime(true);
            //execution time
            $execution_time = ($time_end - $time_start);

            Log::error('time taken for CMS Page Publishing data ' . $execution_time);
            Log::error('Fetching CMS Page Publishing data completed |||| End <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< userId - ' . $userId);
        } catch (BadResponseException $ex) {
            //if request is invalid
            $response = $ex->getResponse();
            $jsonBody = (string) $response->getBody();
            $this->error($jsonBody);

            $data = json_decode($response->getBody(), true);
            $status = $data['status'];
            $error_msg = $data['message'];
            Log::error('CMS Page Publishing data  |||| Error - ' . $jsonBody);
        }

        //get Page Publishing from database
        $cms_page_publishing_in_database = CmsPagePublshing::where('backup_progress_id', $backup_progress['id'] - 1)->get()->pluck('page_publishing_id')->toArray();

        //check if empty result 
        if (count($hubspot_page_publishing_ids) == 0) {
            $removed_rows = 0;
        } else {
            $removed_page_publishing = array_diff($cms_page_publishing_in_database, $hubspot_page_publishing_ids);
            $removed_rows = count($removed_page_publishing);
        }
        //creating BackupHistory record
        $backup_history = BackupHistory::create([
                    'backup_progress_id' => $backup_progress['id'],
                    'user_id' => $user->id,
                    'object_name' => 'Fetched CMS Page Publishing data',
                    'num_of_records' => $created_rows + $removed_rows,
                    'removed_records' => $removed_rows,
                    'added_records' => $created_rows,
                    'changed_records' => $updated_rows,
                    'num_of_API_calls' => 1,
                    'status' => $status,
                    'error_message' => $error_msg]);

        //get smart alerts for action 'added'
        $added_alert = SmartAlert::where('user_id', $user['id'])
                        ->where('s_object', 'CMS Page Publishing')
                        ->where('action', 'Added')->first();

        if (!empty($added_alert)) {
            if ($created_rows > $added_alert->amount) {

                //notification when Page Publishing added
                $add_notificatoion = Notification::create(['user_id' => $user['id'],
                            'backup_progress_id' => $backup_progress['id'],
                            'status' => 'CMS Page Publishing Added',
                            'type' => 'email',
                            'num_of_records' => $created_rows]);

                //create history
                SmartAlertHistory::create(['user_id' => $user['id'], 'smart_alert_id' => $added_alert->id]);

                //notify user
                //$user->notify(new Backup($notificatoion));
            }
        }

        //get smart alerts for action 'Changed'
        $changed_alert = SmartAlert::where('user_id', $user['id'])
                        ->where('s_object', 'CMS Page Publishing')
                        ->where('action', 'Changed')->first();

        if (!empty($changed_alert)) {
            if ($updated_rows > $changed_alert->amount) {



                //notification when Page Publishing Changed
                $change_notificatoion = Notification::create(['user_id' => $user['id'],
                            'backup_progress_id' => $backup_progress['id'],
                            'status' => 'CMS Page Publishing Changed',
                            'type' => 'email',
                            'num_of_records' => $updated_rows]);

                //create history
                SmartAlertHistory::create(['user_id' => $user['id'], 'smart_alert_id' => $changed_alert->id]);

                //notify user
                //$user->notify(new Backup($notificatoion));
            }
        }

        //get smart alerts for action 'Removed'
        $removed_alert = SmartAlert::where('user_id', $user['id'])
                        ->where('s_object', 'CMS Page Publishing')
                        ->where('action', 'Removed')->first();

        if (!empty($removed_alert)) {
            if ($removed_rows > $removed_alert->amount) {

                //notification when Page Publishing Removed
                $remove_notificatoion = Notification::create(['user_id' => $user['id'],
                            'backup_progress_id' => $backup_progress['id'],
                            'status' => 'CMS Page Publishing Removed',
                            'type' => 'email',
                            'num_of_records' => $removed_rows]);

                //create history
                SmartAlertHistory::create(['user_id' => $user['id'], 'smart_alert_id' => $removed_alert->id]);

                //notify user
                //$user->notify(new Backup($notificatoion));
            }
        }
    }

    public function getCmsPagePublshingData($headers, $offset, $client, $backup_progress, $user, $hubspot_page_publishing_ids, $status, $error_msg, $created_rows, $updated_rows) {

        $url = $this->api_url . '/content/api/v2/pages?limit=100&offset=' . $offset;

        //get all Cms Page Publishing data
        $response = $client->request('GET', $url, ['headers' => $headers]);


        $body = $response->getBody();
        $data = json_decode($body, true);


        if (!empty($data['status']) == 'error') {
            $status = $data['status'];
            $error_msg = $data['message'];
            Log::error('CMS Page Publishing data  |||| Error - ' . $error_msg);
        } else {
            $results = $data['objects'];
            $status = 'successed';

            if (!empty($results)) {
                foreach ($results as $value) {

                    //get all page publishing ids
                    $hubspot_page_publishing_ids[] = $value['id'];

                    $check_page_publishing = CmsPagePublshing::where('user_id', $user->id)
                                    ->where('page_publishing_id', $value['id'])
                                    ->where('backup_progress_id', $backup_progress['id'] - 1)->first();

                    $page_publishing = CmsPagePublshing::create([
                                'user_id' => $user->id,
                                'backup_progress_id' => $backup_progress['id'],
                                'page_publishing_id' => $value['id'],
                                'ab_status' => isset($value['ab_status']) ? $value['ab_status'] : "",
                                'ab_test_id' => isset($value['ab_test_id']) ? $value['ab_test_id'] : "",
                                'analytics_page_id' => isset($value['analytics_page_id']) ? $value['analytics_page_id'] : 0,
                                'archived' => isset($value['archived']) ? $value['archived'] : 0,
                                'attached_stylesheets' => isset($value['attached_stylesheets']) ? $value['attached_stylesheets'] : NULL,
                                'author_user_id' => isset($value['author_user_id']) ? $value['author_user_id'] : 0,
                                'blueprint_type_id' => isset($value['blueprint_type_id']) ? $value['blueprint_type_id'] : 0,
                                'campaign' => isset($value['campaign']) ? $value['campaign'] : "",
                                'campaign_name' => isset($value['campaign_name']) ? $value['campaign_name'] : "",
                                'cloned_from' => isset($value['cloned_from']) ? $value['cloned_from'] : "",
                                'compose_body' => isset($value['compose_body']) ? $value['compose_body'] : "",
                                'css' => isset($value['css']) ? $value['css'] : NULL,
                                'css_text' => isset($value['css_text']) ? $value['css_text'] : "",
                                'current_live_domain' => isset($value['current_live_domain']) ? $value['current_live_domain'] : "",
                                'deleted_at' => isset($value['deleted_at']) ? $value['deleted_at'] : 0,
                                'deleted_by' => isset($value['deleted_by']) ? $value['deleted_by'] : "",
                                'domain' => isset($value['domain']) ? $value['domain'] : "",
                                'enable_domain_stylesheets' => isset($value['enable_domain_stylesheets']) ? $value['enable_domain_stylesheets'] : 0,
                                'enable_layout_stylesheets' => isset($value['enable_layout_stylesheets']) ? $value['enable_layout_stylesheets'] : 0,
                                'featured_image' => isset($value['featured_image']) ? $value['featured_image'] : "",
                                'featured_image_alt_text' => isset($value['featured_image_alt_text']) ? $value['featured_image_alt_text'] : "",
                                'flex_areas' => isset($value['flex_areas']) ? $value['flex_areas'] : NULL,
                                'folder_id' => isset($value['folder_id']) ? $value['folder_id'] : "",
                                'footer_html' => isset($value['footer_html']) ? $value['footer_html'] : "",
                                'freeze_date' => isset($value['freeze_date']) ? $value['freeze_date'] : "",
                                'has_smart_content' => isset($value['has_smart_content']) ? $value['has_smart_content'] : 0,
                                'has_user_changes' => isset($value['has_user_changes']) ? $value['has_user_changes'] : 0,
                                'head_html' => isset($value['head_html']) ? $value['head_html'] : "",
                                'html_title' => isset($value['html_title']) ? $value['html_title'] : "",
                                'include_default_custom_css' => isset($value['include_default_custom_css']) ? $value['include_default_custom_css'] : "",
                                'is_draft' => isset($value['is_draft']) ? $value['is_draft'] : 0,
                                'keywords' => isset($value['keywords']) ? $value['keywords'] : NULL,
                                'language' => isset($value['language']) ? $value['language'] : "",
                                'last_edit_session_id' => isset($value['last_edit_session_id']) ? $value['last_edit_session_id'] : "",
                                'last_edit_update_id' => isset($value['last_edit_update_id']) ? $value['last_edit_update_id'] : "",
                                'legacy_blog_tabid' => isset($value['legacy_blog_tabid']) ? $value['legacy_blog_tabid'] : "",
                                'meta_description' => isset($value['meta_description']) ? $value['meta_description'] : "",
                                'meta_keywords' => isset($value['meta_keywords']) ? $value['meta_keywords'] : "",
                                'name' => isset($value['name']) ? $value['name'] : "",
                                'page_expiry_date' => isset($value['page_expiry_date']) ? Carbon::createFromTimestampMs($value['page_expiry_date'])->format('Y-m-d H:i:s') : NULL,
                                'page_expiry_enabled' => isset($value['page_expiry_enabled']) ? $value['page_expiry_enabled'] : "",
                                'page_expiry_redirect_id' => isset($value['page_expiry_redirect_id']) ? $value['page_expiry_redirect_id'] : "",
                                'page_expiry_redirect_url' => isset($value['page_expiry_redirect_url']) ? $value['page_expiry_redirect_url'] : "",
                                'page_redirected' => isset($value['page_redirected']) ? $value['page_redirected'] : "",
                                'password' => isset($value['password']) ? $value['password'] : "",
                                'performable_url' => isset($value['performable_url']) ? $value['performable_url'] : "",
                                'personas' => isset($value['personas']) ? $value['personas'] : NULL,
                                'portal_id' => isset($value['portal_id']) ? $value['portal_id'] : 0,
                                'publish_date' => isset($value['publish_date']) ? Carbon::createFromTimestampMs($value['publish_date'])->format('Y-m-d H:i:s') : NULL,
                                'publish_immediately' => isset($value['publish_immediately']) ? $value['publish_immediately'] : 0,
                                'published_url' => isset($value['published_url']) ? $value['published_url'] : "",
                                'rss_email_author_line_template' => isset($value['rss_email_author_line_template']) ? $value['rss_email_author_line_template'] : 0,
                                'rss_email_blog_image_max_width' => isset($value['rss_email_blog_image_max_width']) ? $value['rss_email_blog_image_max_width'] : 0,
                                'rss_email_by_text' => isset($value['rss_email_by_text']) ? $value['rss_email_by_text'] : "",
                                'rss_email_click_through_text' => isset($value['rss_email_click_through_text']) ? $value['rss_email_click_through_text'] : "",
                                'rss_email_comment_text' => isset($value['rss_email_comment_text']) ? $value['rss_email_comment_text'] : "",
                                'rss_email_entry_template' => isset($value['rss_email_entry_template']) ? $value['rss_email_entry_template'] : "",
                                'rss_email_entry_template_enabled' => isset($value['rss_email_entry_template_enabled']) ? $value['rss_email_entry_template_enabled'] : 0,
                                'rss_email_image_max_width' => isset($value['rss_email_image_max_width']) ? $value['rss_email_image_max_width'] : 0,
                                'scheduled_update_date' => isset($value['scheduled_update_date']) ? Carbon::createFromTimestampMs($value['scheduled_update_date'])->format('Y-m-d H:i:s') : NULL,
                                'slug' => isset($value['slug']) ? $value['slug'] : "",
                                'staged_from' => isset($value['staged_from']) ? $value['staged_from'] : "",
                                'style_override_id' => isset($value['style_override_id']) ? $value['style_override_id'] : "",
                                'subcategory' => isset($value['subcategory']) ? $value['subcategory'] : "",
                                'template_path' => isset($value['template_path']) ? $value['template_path'] : "",
                                'tms_id' => isset($value['tms_id']) ? $value['tms_id'] : "",
                                'translated_from_id' => isset($value['translated_from_id']) ? $value['translated_from_id'] : "",
                                'unpublished_at' => isset($value['unpublished_at']) ? Carbon::createFromTimestampMs($value['unpublished_at'])->format('Y-m-d H:i:s') : NULL,
                                'url' => isset($value['url']) ? $value['url'] : "",
                                'created' => isset($value['createdDate']) ? Carbon::createFromTimestampMs($value['created'])->format('Y-m-d H:i:s') : NULL,
                                'updated' => isset($value['updatedDate']) ? Carbon::createFromTimestampMs($value['updated'])->format('Y-m-d H:i:s') : NULL,
                    ]);

                    if (!empty($value['widget_containers'])) {
                        foreach ($value['widget_containers'] as $key => $widgets) {
                            foreach ($widgets as $widget) {
                                $page_publishing_widgets = CmsPagePublshingWidget::create([
                                            'page_publishing_db_id' => $page_publishing->id,
                                            'widget_container' => isset($key) ? $key : "",
                                            'widget_id' => isset($widget['id']) ? $widget['id'] : 0,
                                            'body_name' => isset($widget['body']['name']) ? $widget['body']['name'] : "",
                                            'body_is_widget_block' => isset($widget['body']['is_widget_block']) ? $widget['body']['is_widget_block'] : 0,
                                            'body_type' => isset($widget['body']['type']) ? $widget['body']['type'] : "",
                                            'body_tag' => isset($widget['body']['tag']) ? $widget['body']['tag'] : "",
                                            'body_label' => isset($widget['body']['label']) ? $widget['body']['label'] : "",
                                            'body_widget_type' => isset($widget['body']['widget_type']) ? $widget['body']['widget_type'] : "",
                                            'body_html' => isset($widget['body']['html']) ? $widget['body']['html'] : "",
                                            'body_overrideable' => isset($widget['body']['overrideable']) ? $widget['body']['overrideable'] : 0,
                                            'body_export_to_template_context' => isset($widget['body']['export_to_template_context']) ? $widget['body']['export_to_template_context'] : 0,
                                            'body_org_tag' => isset($widget['body']['org_tag']) ? $widget['body']['org_tag'] : "",
                                            'body_no_wrapper' => isset($widget['body']['no_wrapper']) ? $widget['body']['no_wrapper'] : 0,
                                            'group' => isset($widget['group']) ? $widget['group'] : 0,
                                            'body_fields_initial' => isset($widget['body_fields']['initial']) ? $widget['body_fields']['initial'] : "",
                                            'body_fields_type' => isset($widget['body_fields']['type']) ? $widget['body_fields']['type'] : "",
                                            'body_fields_name' => isset($widget['body_fields']['name']) ? $widget['body_fields']['name'] : "",
                                            'body_fields_label' => isset($widget['body_fields']['label']) ? $widget['body_fields']['label'] : "",
                                            'label' => isset($widget['label']) ? $widget['label'] : "",
                                            'meta' => isset($widget['meta']) ? $widget['meta'] : NULL,
                                            'common_name' => isset($widget['common_name']) ? $widget['common_name'] : "",
                                            'common_is_widget_block' => isset($widget['common']['is_widget_block']) ? $widget['common']['is_widget_block'] : 0,
                                            'common_type' => isset($widget['common']['type']) ? $widget['common']['type'] : "",
                                            'common_tag' => isset($widget['common']['tag']) ? $widget['common']['tag'] : "",
                                            'common_label' => isset($widget['common']['label']) ? $widget['common']['label'] : "",
                                            'common_widget_type' => isset($widget['common']['widget_type']) ? $widget['common']['widget_type'] : "",
                                            'common_html' => isset($widget['common']['html']) ? $widget['common']['html'] : "",
                                            'common_overrideable' => isset($widget['common']['overrideable']) ? $widget['common']['overrideable'] : 0,
                                            'common_export_to_template_context' => isset($widget['common']['export_to_template_context']) ? $widget['common']['export_to_template_context'] : 0,
                                            'common_org_tag' => isset($widget['common']['org_tag']) ? $widget['common']['org_tag'] : "",
                                            'common_no_wrapper' => isset($widget['common']['no_wrapper']) ? $widget['common']['no_wrapper'] : 0,
                                            'key' => isset($widget['key']) ? $widget['key'] : "",
                                            'order' => isset($widget['order']) ? $widget['order'] : 0,
                                            'type' => isset($widget['type']) ? $widget['type'] : "",
                                ]);
                            }
                        }
                    }

                    //if engagement is created
                    if ($page_publishing) {
                        $created_rows +=1;
                    }

                    //check if cms page publishing  is already inserted or not
                    if ($check_page_publishing === null) {
                        
                    } else {

                        $last_updated_date = Carbon::createFromTimestampMs($value['updated'])->format('Y-m-d H:i:s');

                        //check if any data is changed
                        if ($last_updated_date != $check_page_publishing->updated) {

                            $updated_rows +=1;
                        }
                    }
                }
            }
        }

        if (isset($data['offset']) && !empty($data['offset'] !== 0)) {

            return $this->getCmsPagePublshingData($headers, $data['offset'], $client, $backup_progress, $user, $hubspot_page_publishing_ids, $status, $error_msg, $created_rows, $updated_rows);
        } else {

            $data = ['created_rows' => $created_rows, 'updated_rows' => $updated_rows,
                'hubspot_page_publishing_ids' => $hubspot_page_publishing_ids, 'status' => $status, 'error_msg' => $error_msg];

            return $data;
        }
    }

}
