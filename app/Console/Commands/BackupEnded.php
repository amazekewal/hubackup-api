<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\User;
use App\BackupProgress;
use App\Notification;
use Illuminate\Support\Facades\Log;
use Carbon\Carbon;
class BackupEnded extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'backup:ended {userId}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'backup is ended';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $userId = $this->argument('userId');
        $user = User::find($userId);
        
        //after all commands save backup progress as done
        $backup_last_command = BackupProgress::where('user_id', $user['id'])->where('status', 'Inprogress')->orderByDesc('id')->first();

        //update as done
        $backup_last_command->status = 'done';
        $backup_last_command->end_date = Carbon::now();
        $backup_last_command->save();
        
        
        //create notification when backup complete
        $notificatoion = Notification::create(['user_id' => $user['id'],'backup_progress_id' => $backup_last_command['id'],
                    'status' => 'Backup completed', 'type' => 'email']);

//        //notify user
//        $user->notify(new Backup($notificatoion));

        Log::error(PHP_EOL);
    }
}
