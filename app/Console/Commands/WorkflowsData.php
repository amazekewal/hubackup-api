<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\BadResponseException;
use Illuminate\Support\Facades\Artisan;
// models
use App\Workflow;
use App\WorkflowMeta;
use App\BackupHistory;
use App\User;
use Illuminate\Support\Facades\Log;
use App\BackupProgress;
use App\Notifications\Backup;
use App\Notification;
use App\SmartAlert;
use App\SmartAlertHistory;
use Carbon\Carbon;

class WorkflowsData extends Command {

    /**
     * The name of the apiUrl
     * @var type 
     */
    protected $api_url;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'workflows:data {userId}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command will get Workflows data';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();

        //get api key from env file
        $this->api_url = env('HUBSPOT_URL');
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        $this->info('Workflows Data cron started....' . $this->api_url);

        $status = '';
        $error_msg = '';
        $created_rows = 0;
        $updated_rows = 0;
        $hubspot_workflow_ids = [];

        $userId = $this->argument('userId');
        $user = User::find($userId);

        $backup_last_command = BackupProgress::where('user_id', $user['id'])->where('status', 'Inprogress')->orderByDesc('id')->first();

        try {

            Log::error('Fetching Workflows data started |||| Start >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> userId - ' . $userId);

            //start time
            $time_start = microtime(true);


            //user token
            $token = $user->accesstoken;

            //using GuzzleHttp client to get request
            $client = new \GuzzleHttp\Client(['http_errors' => false]);
            $headers = [
                'Authorization' => 'Bearer ' . $token,
                'Accept' => 'application/json',
            ];

            $url = $this->api_url . '/automation/v3/workflows';

            //get all workflows data
            $response = $client->request('GET', $url, ['headers' => $headers]);


            $body = $response->getBody();
            $data = json_decode($body, true);

            if (!empty($data['status']) == 'error') {
                $status = $data['status'];
                $error_msg = $data['message'];
                Log::error('Workflows data  |||| Error - ' . $error_msg);
            } else {

                $status = 'successed';

                $all_results = $data['workflows'];

                //turn data into collection
                $results_collection = collect($all_results);
                $results_chunks = $results_collection->chunk(500); //chunk into smaller pieces


                foreach ($results_chunks as $results_chunk) {

                    $results = $results_chunk->toArray();

                    if (!empty($results)) {
                        foreach ($results as $value) {

                            //get all workflows ids
                            $hubspot_workflow_ids[] = $value['id'];


                            $workflow_meta_url = $this->api_url . '/automation/v3/workflows/' . $value['id'];

                            //get complete details of each workflow
                            $response_workflow_meta = $client->request('GET', $workflow_meta_url, ['headers' => $headers]);
                            $body_workflow_meta = $response_workflow_meta->getBody();
                            $data_workflow_meta = json_decode($body_workflow_meta, true);

                            if (!empty($data_workflow_meta['status']) == 'error') {
                                $status = 'warning';
                                $error_msg = $data_workflow_meta['message'];
                                Log::error('Workflows data  |||| Warning - ' . $error_msg);
                            } else {
                                $check_workflow = Workflow::where('user_id', $user->id)
                                                ->where('workflow_id', $value['id'])
                                                ->where('backup_progress_id', $backup_last_command['id'] - 1)->first();
                                $workflow = Workflow::create([
                                            'user_id' => $user->id,
                                            'backup_progress_id' => $backup_last_command['id'],
                                            'workflow_id' => $value['id'],
                                            'portal_id' => isset($value['portalId']) ? $value['portalId'] : 0,
                                            'name' => isset($value['name']) ? $value['name'] : "",
                                            'type' => isset($value['type']) ? $value['type'] : "",
                                            'enabled' => isset($value['enabled']) ? $value['enabled'] : "",
                                            'persona_tag_ids' => isset($value['personaTagIds']) ? $value['personaTagIds'] : NULL,
                                            'inserted_at' => isset($value['insertedAt']) ? Carbon::createFromTimestampMs($value['insertedAt'])->format('Y-m-d H:i:s') : NULL,
                                            'updated_on' => isset($value['updatedAt']) ? Carbon::createFromTimestampMs($value['updatedAt'])->format('Y-m-d H:i:s') : NULL,
                                ]);

                                $workflow_db_id = $workflow->id;
                                $hubspot_workflow_id = $workflow->workflow_id;

                                $workflow_meta = WorkflowMeta::create([
                                            'workflow_db_id' => $workflow->id,
                                            'internal' => isset($data_workflow_meta['internal']) ? $data_workflow_meta['internal'] : 0,
                                            'only_exec_on_biz_days' => isset($data_workflow_meta['onlyExecOnBizDays']) ? $data_workflow_meta['onlyExecOnBizDays'] : 0,
                                            'nurture_time_range_enabled' => isset($data_workflow_meta['nurtureTimeRange']['enabled']) ? $data_workflow_meta['nurtureTimeRange']['enabled'] : 0,
                                            'nurture_time_range_start_hour' => isset($data_workflow_meta['nurtureTimeRange']['startHour']) ? $data_workflow_meta['nurtureTimeRange']['startHour'] : 0,
                                            'nurture_time_range_stop_hour' => isset($data_workflow_meta['nurtureTimeRange']['stopHour']) ? $data_workflow_meta['nurtureTimeRange']['stopHour'] : 0,
                                            'allow_contact_to_trigger_multiple_times' => isset($data_workflow_meta['allowContactToTriggerMultipleTimes']) ? $data_workflow_meta['allowContactToTriggerMultipleTimes'] : "",
                                            'unenrollment_setting_type' => isset($data_workflow_meta['unenrollmentSetting']['type']) ? $data_workflow_meta['unenrollmentSetting']['type'] : "",
                                            'unenrollment_setting_type_excluded_workflows' => isset($data_workflow_meta['unenrollmentSetting']['excludedWorkflows']) ? ['unenrollmentSetting']['excludedWorkflows'] : NULL,
                                            'recurring_setting_type' => isset($data_workflow_meta['recurringSetting']['type']) ? $data_workflow_meta['recurringSetting']['type'] : "",
                                            'enroll_on_criteria_update' => isset($data_workflow_meta['enrollOnCriteriaUpdate']) ? $data_workflow_meta['enrollOnCriteriaUpdate'] : 0,
                                            'only_enrolls_manually' => isset($data_workflow_meta['onlyEnrollsManually']) ? $data_workflow_meta['onlyEnrollsManually'] : 0,
                                            'goal_criteria_property_object_type' => isset($data_workflow_meta['goalCriteria'][0]['propertyObjectType']) ? $data_workflow_meta['goalCriteria'][0]['propertyObjectType'] : "",
                                            'goal_criteria_filter_family' => isset($data_workflow_meta['goalCriteria'][0]['filterFamily']) ? $data_workflow_meta['goalCriteria'][0]['filterFamily'] : "",
                                            'goal_criteria_within_time_mode' => isset($data_workflow_meta['goalCriteria'][0]['withinTimeMode']) ? $data_workflow_meta['goalCriteria'][0]['withinTimeMode'] : "",
                                            'goal_criteria_property' => isset($data_workflow_meta['goalCriteria'][0]['property']) ? $data_workflow_meta['goalCriteria'][0]['property'] : "",
                                            'goal_criteria_value' => isset($data_workflow_meta['goalCriteria'][0]['value']) ? $data_workflow_meta['goalCriteria'][0]['value'] : "",
                                            'goal_criteria_type' => isset($data_workflow_meta['goalCriteria'][0]['type']) ? $data_workflow_meta['goalCriteria'][0]['type'] : "",
                                            'goal_criteria_operator' => isset($data_workflow_meta['goalCriteria'][0]['operator']) ? $data_workflow_meta['goalCriteria'][0]['operator'] : "",
                                            're_enrollment_trigger_sets' => isset($data_workflow_meta['reEnrollmentTriggerSets']) ? $data_workflow_meta['reEnrollmentTriggerSets'] : NULL,
                                            'suppression_list_ids' => isset($data_workflow_meta['suppressionListIds']) ? $data_workflow_meta['suppressionListIds'] : NULL,
                                            'last_updated_by' => isset($data_workflow_meta['lastUpdatedBy']) ? $data_workflow_meta['lastUpdatedBy'] : "",
                                            'segment_criteria' => isset($data_workflow_meta['segmentCriteria']) ? $data_workflow_meta['segmentCriteria'] : NULL,
                                            'triggered_by_workflow_ids' => isset($data_workflow_meta['metaData']['triggeredByWorkflowIds']) ? $data_workflow_meta['metaData']['triggeredByWorkflowIds'] : NULL,
                                            'succeeded_list_id' => isset($data_workflow_meta['metaData']['succeededListId']) ? $data_workflow_meta['metaData']['succeededListId'] : "",
                                            'contact_list_ids_enrolled' => isset($data_workflow_meta['contactListIds']['enrolled']) ? $data_workflow_meta['contactListIds']['enrolled'] : 0,
                                            'contact_list_ids_active' => isset($data_workflow_meta['contactListIds']['active']) ? $data_workflow_meta['contactListIds']['active'] : "",
                                            'contact_list_ids_steps' => isset($value['contactListIds']['steps']) ? $value['contactListIds']['steps'] : NULL,
                                            'contact_list_ids_succeeded' => isset($data_workflow_meta['contactListIds']['succeeded']) ? $data_workflow_meta['contactListIds']['succeeded'] : "",
                                            'contact_list_ids_completed' => isset($data_workflow_meta['contactListIds']['completed']) ? $data_workflow_meta['contactListIds']['completed'] : "",
                                ]);

                                //if workflow is created
                                if ($workflow) {
                                    $created_rows +=1;
                                }
                                //check if workflow  is already inserted or not
                                if ($check_workflow === null) {
                                    
                                } else {
                                    $last_updated_date = Carbon::createFromTimestampMs($value['updatedAt'])->format('Y-m-d H:i:s');

                                    //check if any data is changed
                                    if ($last_updated_date != $check_workflow->updated_date) {

                                        $updated_rows +=1;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            //end time
            $time_end = microtime(true);
            //execution time
            $execution_time = ($time_end - $time_start);

            Log::error('time taken for Workflows data ' . $execution_time);
            Log::error('Fetching Workflows data completed |||| End <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< userId - ' . $userId);
        } catch (BadResponseException $ex) {
            //if request is invalid
            $response = $ex->getResponse();
            $jsonBody = (string) $response->getBody();
            $this->error($jsonBody);

            $data = json_decode($response->getBody(), true);
            $status = $data['status'];
            $error_msg = $data['message'];
            Log::error('Workflows data  |||| Error - ' . $jsonBody);
        }

        //get workflows from database
        $workflows_in_database = Workflow::where('backup_progress_id', $backup_last_command['id'] - 1)->get()->pluck('workflow_id')->toArray();
        
        //check if empty result 
        if (count($hubspot_workflow_ids) == 0) {
            $removed_rows = 0;
        } else {
        $removed_workflows = array_diff($workflows_in_database, $hubspot_workflow_ids);
        $removed_rows = count($removed_workflows);
        }

        //creating BackupHistory record
        $backup_history = BackupHistory::create([
                    'backup_progress_id' => $backup_last_command['id'],
                    'user_id' => $user->id,
                    'object_name' => 'Fetched Workflows data',
                    'num_of_records' => $created_rows + $removed_rows,
                    'removed_records' => $removed_rows,
                    'added_records' => $created_rows,
                    'changed_records' => $updated_rows,
                    'num_of_API_calls' => 2,
                    'status' => $status,
                    'error_message' => $error_msg]);



        //get smart alerts for action 'added'
        $added_alert = SmartAlert::where('user_id', $user['id'])
                        ->where('s_object', 'Workflows')
                        ->where('action', 'Added')->first();

        if (!empty($added_alert)) {
            if ($created_rows > $added_alert->amount) {
                if ($created_rows == 1) {
                    $added_data = 'Workflow';
                } else {
                    $added_data = 'Workflows';
                }

                //notification when Workflow added
                $add_notificatoion = Notification::create(['user_id' => $user['id'], 'backup_progress_id' => $backup_last_command['id'],
                            'status' => $added_data . ' Added', 'type' => 'email', 'num_of_records' => $created_rows]);

                //create history
                SmartAlertHistory::create(['user_id' => $user['id'], 'smart_alert_id' => $added_alert->id]);

                //notify user
                //$user->notify(new Backup($notificatoion));
            }
        }

        //get smart alerts for action 'Changed'
        $changed_alert = SmartAlert::where('user_id', $user['id'])
                        ->where('s_object', 'Workflows')
                        ->where('action', 'Changed')->first();

        if (!empty($changed_alert)) {
            if ($updated_rows > $changed_alert->amount) {

                if ($updated_rows == 1) {
                    $changed_data = 'Workflow';
                } else {
                    $changed_data = 'Workflows';
                }

                //notification when Workflow Changed
                $change_notificatoion = Notification::create(['user_id' => $user['id'], 'backup_progress_id' => $backup_last_command['id'],
                            'status' => $changed_data . ' Changed', 'type' => 'email', 'num_of_records' => $updated_rows]);

                //create history
                SmartAlertHistory::create(['user_id' => $user['id'], 'smart_alert_id' => $changed_alert->id]);

                //notify user
                //$user->notify(new Backup($notificatoion));
            }
        }

        //get smart alerts for action 'Removed'
        $removed_alert = SmartAlert::where('user_id', $user['id'])
                        ->where('s_object', 'Workflows')
                        ->where('action', 'Removed')->first();

        if (!empty($removed_alert)) {
            if ($removed_rows > $removed_alert->amount) {

                //notification when Workflow Removed
                $remove_notificatoion = Notification::create(['user_id' => $user['id'], 'backup_progress_id' => $backup_last_command['id'],
                            'status' => 'Workflows Removed', 'type' => 'email', 'num_of_records' => $removed_rows]);

                //create history
                SmartAlertHistory::create(['user_id' => $user['id'], 'smart_alert_id' => $removed_alert->id]);

                //notify user
                //$user->notify(new Backup($notificatoion));
            }
        }
    }

}
