<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
// models
use App\Product;
use App\ProductProperty;
use App\BackupHistory;
use App\User;
use Illuminate\Support\Facades\Log;
use App\BackupProgress;
use App\Notifications\Backup;
use App\Notification;
use App\SmartAlert;
use App\SmartAlertHistory;
use Carbon\Carbon;

class ProductsData extends Command {

    /**
     * The name of the apiUrl
     * @var type 
     */
    protected $api_url;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'products:data {userId}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command will get all Products data';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();

        //get api key from env file
        $this->api_url = env('HUBSPOT_URL');
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        $this->info('Products cron started....' . $this->api_url);

        $status = '';
        $error_msg = '';
        $created_rows = 0;
        $updated_rows = 0;
        $hubspot_product_ids = [];

        $userId = $this->argument('userId');
        $user = User::find($userId);

        $backup_progress = BackupProgress::where('user_id', $user['id'])->where('status', 'Inprogress')->orderByDesc('id')->first();


        try {

            Log::error('Fetching Products data started |||| Start >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> userId - ' . $userId);

            //start time
            $time_start = microtime(true);


            //user token
            $token = $user->accesstoken;

            //using GuzzleHttp client to get request
            $client = new \GuzzleHttp\Client(['http_errors' => false]);
            $headers = [
                'Authorization' => 'Bearer ' . $token,
                'Accept' => 'application/json',
            ];

            $get_data = $this->getProductsData($headers, 0, $client, $backup_progress, $user, $hubspot_product_ids, $status, $error_msg, $created_rows, $updated_rows);

            $created_rows = $get_data['created_rows'];
            $updated_rows = $get_data['updated_rows'];
            $hubspot_product_ids = $get_data['hubspot_product_ids'];
            $error_msg = $get_data['error_msg'];
            $status = $get_data['status'];

            //end time
            $time_end = microtime(true);
            //execution time
            $execution_time = ($time_end - $time_start);

            Log::error('time taken for Products data ' . $execution_time);
            Log::error('Fetching Products data completed |||| End <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< userId - ' . $userId);
        } catch (BadResponseException $ex) {
            //if request is invalid
            $response = $ex->getResponse();
            $jsonBody = (string) $response->getBody();
            $this->error($jsonBody);

            $data = json_decode($response->getBody(), true);
            $status = $data['status'];
            $error_msg = $data['message'];
            Log::error('CMS Products data  |||| Error - ' . $jsonBody);
        }

        //get products from database
        $products_in_database = Product::where('backup_progress_id', $backup_progress['id'] - 1)->get()->pluck('product_id')->toArray();
        
        //check if empty result 
        if (count($hubspot_product_ids) == 0) {
            $removed_rows = 0;
        } else {
        $removed_products = array_diff($products_in_database, $hubspot_product_ids);
        $removed_rows = count($removed_products);
        }
        
        //creating BackupHistory record
        $backup_history = BackupHistory::create([
                    'backup_progress_id' => $backup_progress['id'],
                    'user_id' => $user->id,
                    'object_name' => 'Fetched Products data',
                    'num_of_records' => $created_rows + $removed_rows,
                    'removed_records' => $removed_rows,
                    'added_records' => $created_rows,
                    'changed_records' => $updated_rows,
                    'num_of_API_calls' => 1,
                    'status' => $status,
                    'error_message' => $error_msg]);

        //get smart alerts for action 'added'
        $added_alert = SmartAlert::where('user_id', $user['id'])
                        ->where('s_object', 'Products')
                        ->where('action', 'Added')->first();

        if (!empty($added_alert)) {
            if ($created_rows > $added_alert->amount) {

                //notification when Product added
                $add_notificatoion = Notification::create(['user_id' => $user['id'], 'backup_progress_id' => $backup_progress['id'],
                            'status' => 'Products Added', 'type' => 'email', 'num_of_records' => $created_rows]);

                //create history
                SmartAlertHistory::create(['user_id' => $user['id'], 'smart_alert_id' => $added_alert->id]);

                //notify user
                //$user->notify(new Backup($notificatoion));
            }
        }

        //get smart alerts for action 'Changed'
        $changed_alert = SmartAlert::where('user_id', $user['id'])
                        ->where('s_object', 'Products')
                        ->where('action', 'Changed')->first();

        if (!empty($changed_alert)) {
            if ($updated_rows > $changed_alert->amount) {


                //notification when Product Changed
                $change_notificatoion = Notification::create(['user_id' => $user['id'], 'backup_progress_id' => $backup_progress['id'],
                            'status' => 'Products Changed', 'type' => 'email', 'num_of_records' => $updated_rows]);

                //create history
                SmartAlertHistory::create(['user_id' => $user['id'], 'smart_alert_id' => $changed_alert->id]);

                //notify user
                //$user->notify(new Backup($notificatoion));
            }
        }

        //get smart alerts for action 'Removed'
        $removed_alert = SmartAlert::where('user_id', $user['id'])
                        ->where('s_object', 'Products')
                        ->where('action', 'Removed')->first();

        if (!empty($removed_alert)) {
            if ($removed_rows > $removed_alert->amount) {

                //notification when Product Removed
                $remove_notificatoion = Notification::create(['user_id' => $user['id'], 'backup_progress_id' => $backup_progress['id'],
                            'status' => 'Products Removed', 'type' => 'email', 'num_of_records' => $removed_rows]);

                //create history
                SmartAlertHistory::create(['user_id' => $user['id'], 'smart_alert_id' => $removed_alert->id]);

                //notify user
                //$user->notify(new Backup($notificatoion));
            }
        }
    }

    public function getProductsData($headers, $offset, $client, $backup_progress, $user, $hubspot_product_ids, $status, $error_msg, $created_rows, $updated_rows) {

        $property_text = "";
        $properties = ['subject',
            'name',
            'description',
            'price'
        ];
        //last value
        $last_value = end($properties);
        foreach ($properties as $property) {

            //check if value is not last value
            if ($property != $last_value) {
                $property_text .= 'properties=' . $property . '&';
            } else {
                $property_text .= 'properties=' . $property;
            }
        }
        $url = $this->api_url . '/crm-objects/v1/objects/products/paged?' . $property_text . '&offset=' . $offset;

        //get all products data
        $response = $client->request('GET', $url, ['headers' => $headers]);


        $body = $response->getBody();
        $data = json_decode($body, true);


        if (!empty($data['status']) == 'error') {
            $status = $data['status'];
            $error_msg = $data['message'];
            Log::error('Products data  |||| Error - ' . $error_msg);
        } else {
            $results = $data['objects'];
            $status = 'successed';

            if (!empty($results)) {
                foreach ($results as $value) {

                    //get all products ids
                    $hubspot_product_ids[] = $value['objectId'];



                    $check_product = Product::where('user_id', $user->id)
                                    ->where('product_id', $value['objectId'])
                                    ->where('backup_progress_id', $backup_progress['id'] - 1)->first();

                    $product = Product::create([
                                'user_id' => $user->id,
                                'backup_progress_id' => $backup_progress['id'],
                                'product_id' => $value['objectId'],
                                'portal_id' => isset($value['portalId']) ? $value['portalId'] : 0,
                                'object_type' => isset($value['objectType']) ? $value['objectType'] : "",
                                'version' => isset($value['version']) ? $value['version'] : 0,
                                'is_deleted' => isset($value['isDeleted']) ? $value['isDeleted'] : 0,
                    ]);

                    if (!empty($value['properties'])) {
                        foreach ($value['properties'] as $key => $property) {

                            $properties = ProductProperty::create([
                                        'product_db_id' => $product->id,
                                        'property_name' => isset($key) ? $key : "",
                                        'value' => isset($property['value']) ? $property['value'] : "",
                                        'timestamp' => isset($property['timestamp']) ? Carbon::createFromTimestampMs($property['timestamp'])->format('Y-m-d H:i:s') : NULL,
                                        'source' => isset($property['source']) ? $property['source'] : "",
                                        'source_id' => isset($property['sourceId']) ? $property['sourceId'] : "",
                                        'source_vid' => isset($property['versions'][0]['sourceVid']) ? $property['versions'][0]['sourceVid'] : NULL,
                                        'request_id' => isset($property['versions'][0]['requestId']) ? $property['versions'][0]['requestId'] : "",
                            ]);
                        }
                    }
                    //if engagement is created
                    if ($product) {
                        $created_rows +=1;
                    }
                    //check if product  is already inserted or not
                    if ($check_product === null) {
                        
                    } else {

                        //check if any data is changed
                        if ($value['version'] > $check_product->version) {
                            $updated_rows +=1;
                        }
                    }
                }
            }
        }

        if (isset($data['hasMore']) && !empty($data['hasMore'] == true)) {

            return $this->getProductsData($headers, $data['offset'], $client, $backup_progress, $user, $hubspot_product_ids, $status, $error_msg, $created_rows, $updated_rows);
        } else {

            $data = ['created_rows' => $created_rows, 'updated_rows' => $updated_rows,
                'hubspot_product_ids' => $hubspot_product_ids, 'status' => $status, 'error_msg' => $error_msg];

            return $data;
        }
    }

}
