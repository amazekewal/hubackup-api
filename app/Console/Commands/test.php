<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Contact;

use App\Company;
;
use App\CmsPipeline;
use App\Form;;
use DB;
use App\User;
use App\Engagement;
use Carbon\Carbon;
class test extends Command {

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'test:run';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {

       dd(Carbon::now()->toDateTimeString());


        //key not equal to company db id
        $key_not = '';
        $search_term = '';


        //get form columns
               $start_date = Carbon::parse('2019-08-01T06:18:42.000Z')->toDateString();
            $end_date = Carbon::parse('2019-08-22T06:18:42.000Z')->toDateString();
            
            
           
           $engagement = new Engagement();
            $engagement_columns = $engagement->getFillable();

            $key_not = ''; 
            
            $associated = ['metadata']; 
            
            $columns = array_merge($engagement_columns,$associated);
            
            $query = Engagement::with('note_engagements')
                                ->with('email_engagements')
                                ->with('task_engagements')
                                ->with('call_engagements')
                                ->with('meeting_engagements');

            //search term in all columns
            foreach ($engagement_columns as $column) {
                $query->orWhere($column, 'LIKE', '%' . $search_term . '%')
                      ->whereBetween('created_at', [$start_date, $end_date]);
            }
            
        $query->where('user_id', 1)->select('*');
        
                
        $data = $query->get()->toArray();
        
         $data_only = [];
        $i = 0;

        foreach ($data as $data_value) {
            $data_only[$i] = array();
            foreach ($data_value as $key => $value) {
                
                if($key == 'deal_associated_contacts' || $key == 'deal_associated_companies' ||
                    $key == 'deal_associated_tickets'){
                    $ass_ids = [];
                    foreach($value as $associated_values){
                        foreach($associated_values as $associated_ids){
                            $ass_ids[] = $associated_ids;
                        }
                        
                    }
                 $data_only[$i][] = $ass_ids;   
                }elseif($key == 'note_engagements' || $key == 'email_engagements' ||
                        $key == 'task_engagements' || $key == 'call_engagements' ||
                        $key == 'meeting_engagements'){
                    if($data_value[$key] != null ){
                        $data_only[$i][] = $value;
                    }
                    
                }else{
                   $data_only[$i][] = $value; 
                }
                
            }
            $i++;
        }
        
        dd(Carbon::now()->format('Ymd'));
        $key_column = [];
        foreach ($columns as $getcolumn) {

            if ($getcolumn != 'user_id' && $getcolumn != $key_not) {
                $key_column[] = strtoupper(str_replace('_', ' ', $getcolumn));
            }
        }


        //return $datatable->make(true);

        dd($data_only);
        $json_data = array(
            "draw" => 0,
            "recordsTotal" => count($data),
            "data" => $data_only,
            "fields" => $key_column
        );

        dd($json_data);
        return response()->json([
                    'data' => $json_data]);
    }

}
