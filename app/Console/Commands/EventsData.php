<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
// models
use App\Event;
use App\BackupHistory;
use App\User;
use Illuminate\Support\Facades\Log;
use App\BackupProgress;
use App\Notifications\Backup;
use App\Notification;
use App\SmartAlert;
use App\SmartAlertHistory;
use Carbon\Carbon;

class EventsData extends Command {

    /**
     * The name of the apiUrl
     * @var type 
     */
    protected $api_url;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'events:data {userId}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command will get all events data';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();

        //get api key from env file
        $this->api_url = env('HUBSPOT_URL');
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        $this->info('Events Data cron started....' . $this->api_url);

        $status = '';
        $error_msg = '';
        $created_rows = 0;
        $updated_rows = 0;
        $hubspot_event_ids = [];

        $userId = $this->argument('userId');
        $user = User::find($userId);

        $backup_progress = BackupProgress::where('user_id', $user['id'])->where('status', 'Inprogress')->orderByDesc('id')->first();


        try {

            Log::error('Fetching Events data started |||| Start >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> userId - ' . $userId);

            //start time
            $time_start = microtime(true);


            //user token
            $token = $user->accesstoken;

            //using GuzzleHttp client to get request
            $client = new \GuzzleHttp\Client(['http_errors' => false]);
            $headers = [
                'Authorization' => 'Bearer ' . $token,
                'Accept' => 'application/json',
            ];

            $url = $this->api_url . '/reports/v2/events';

            //get all events data
            $response = $client->request('GET', $url, ['headers' => $headers]);


            $body = $response->getBody();
            $all_results = json_decode($body, true);

            if (!empty($all_results['status']) == 'error') {
                $status = $all_results['status'];
                $error_msg = $all_results['message'];
                Log::error('Events data  |||| Error - ' . $error_msg);
            } else {
                $status = 'successed';

                //turn data into collection
                $results_collection = collect($all_results);
                $results_chunks = $results_collection->chunk(500); //chunk into smaller pieces

                $create_events = [];

                foreach ($results_chunks as $results_chunk) {

                    $results = $results_chunk->toArray();

                    if (!empty($results)) {
                        foreach ($results as $value) {

                            //get all events ids
                            $hubspot_event_ids[] = $value['id'];



                            $check_event = Event::where('user_id', $user->id)
                                            ->where('event_id', $value['id'])
                                            ->where('backup_progress_id', $backup_progress['id'] - 1)->first();

                            $create_events[] = [
                                'user_id' => $user->id,
                                'backup_progress_id' => $backup_progress['id'],
                                'event_id' => $value['id'],
                                'name' => isset($value['name']) ? $value['name'] : "",
                                'label' => isset($value['label']) ? $value['label'] : "",
                                'status' => isset($value['status']) ? $value['status'] : "",
                                'created_at' => Carbon::now()->toDateTimeString(),
                                'updated_at' => Carbon::now()->toDateTimeString()
                            ];

                            //check if event  is already inserted or not
                            if ($check_event === null) {
                                
                            } else {

                                //check if any data is changed
                                if ($check_event->name != $value['name'] || $check_event->label != $value['label'] || $check_event->status != $value['status']) {

                                    $updated_rows +=1;
                                }
                            }
                        }
                        //insert  events in bulk
                        Event::insert($create_events);
                        $created_rows += count($create_events);
                    }
                }
            }

            //end time
            $time_end = microtime(true);
            //execution time
            $execution_time = ($time_end - $time_start);

            Log::error('time taken for Events data ' . $execution_time);
            Log::error('Fetching Events data completed |||| End <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< userId - ' . $userId);
        } catch (BadResponseException $ex) {
            //if request is invalid
            $response = $ex->getResponse();
            $jsonBody = (string) $response->getBody();
            $this->error($jsonBody);

            $data = json_decode($response->getBody(), true);
            $status = $data['status'];
            $error_msg = $data['message'];
            Log::error('CMS Events data  |||| Error - ' . $jsonBody);
        }

        //get events from database
        $events_in_database = Event::where('backup_progress_id', $backup_progress['id'] - 1)->get()->pluck('event_id')->toArray();

        //check if empty result 
        if (count($hubspot_event_ids) == 0) {
            $removed_rows = 0;
        } else {
            $removed_events = array_diff($events_in_database, $hubspot_event_ids);
            $removed_rows = count($removed_events);
        }
        
        //creating BackupHistory record
        $backup_history = BackupHistory::create([
                    'backup_progress_id' => $backup_progress['id'],
                    'user_id' => $user->id,
                    'object_name' => 'Fetched Events data',
                    'num_of_records' => $created_rows + $removed_rows,
                    'removed_records' => $removed_rows,
                    'added_records' => $created_rows,
                    'changed_records' => $updated_rows,
                    'num_of_API_calls' => 1,
                    'status' => $status,
                    'error_message' => $error_msg]);

        //get smart alerts for action 'added'
        $added_alert = SmartAlert::where('user_id', $user['id'])
                        ->where('s_object', 'Events')
                        ->where('action', 'Added')->first();

        if (!empty($added_alert)) {
            if ($created_rows > $added_alert->amount) {

                //notification when Event added
                $add_notificatoion = Notification::create(['user_id' => $user['id'], 'backup_progress_id' => $backup_progress['id'],
                            'status' => 'Events Added', 'type' => 'email', 'num_of_records' => $created_rows]);

                //create history
                SmartAlertHistory::create(['user_id' => $user['id'], 'smart_alert_id' => $added_alert->id]);

                //notify user
                //$user->notify(new Backup($notificatoion));
            }
        }

        //get smart alerts for action 'Changed'
        $changed_alert = SmartAlert::where('user_id', $user['id'])
                        ->where('s_object', 'Events')
                        ->where('action', 'Changed')->first();

        if (!empty($changed_alert)) {
            if ($updated_rows > $changed_alert->amount) {


                //notification when Event Changed
                $change_notificatoion = Notification::create(['user_id' => $user['id'], 'backup_progress_id' => $backup_progress['id'],
                            'status' => 'Events Changed', 'type' => 'email', 'num_of_records' => $updated_rows]);

                //create history
                SmartAlertHistory::create(['user_id' => $user['id'], 'smart_alert_id' => $changed_alert->id]);

                //notify user
                //$user->notify(new Backup($notificatoion));
            }
        }

        //get smart alerts for action 'Removed'
        $removed_alert = SmartAlert::where('user_id', $user['id'])
                        ->where('s_object', 'Events')
                        ->where('action', 'Removed')->first();

        if (!empty($removed_alert)) {
            if ($removed_rows > $removed_alert->amount) {

                //notification when Event Removed
                $remove_notificatoion = Notification::create(['user_id' => $user['id'], 'backup_progress_id' => $backup_progress['id'],
                            'status' => 'Events Removed', 'type' => 'email', 'num_of_records' => $removed_rows]);

                //create history
                SmartAlertHistory::create(['user_id' => $user['id'], 'smart_alert_id' => $removed_alert->id]);

                //notify user
                //$user->notify(new Backup($notificatoion));
            }
        }
    }

}
