<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\BadResponseException;
use Illuminate\Support\Facades\Artisan;
// models
use App\CmsHubdbTable;
use App\CmsHubdbTableColumn;
use App\BackupHistory;
use App\User;
use Illuminate\Support\Facades\Log;
use App\BackupProgress;
use App\Notifications\Backup;
use App\Notification;
use App\SmartAlert;
use App\SmartAlertHistory;
use Carbon\Carbon;

class CmsHubdbTablesData extends Command {

    /**
     * The name of the apiUrl
     * @var type 
     */
    protected $api_url;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cms-hubdb:data {userId}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command will get all CMS Hubdb Tables data';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();

        //get api key from env file
        $this->api_url = env('HUBSPOT_URL');
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        $this->info('CMS HubDB Data cron started....' . $this->api_url);

        $status = '';
        $error_msg = '';
        $created_rows = 0;
        $updated_rows = 0;
        $hubspot_hubdb_table_ids = [];

        $userId = $this->argument('userId');
        $user = User::find($userId);

        $backup_progress = BackupProgress::where('user_id', $user['id'])->where('status', 'Inprogress')->orderByDesc('id')->first();


        try {

            Log::error('Fetching CMS HubDB data started |||| Start >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> userId - ' . $userId);

            //start time
            $time_start = microtime(true);


            //user token
            $token = $user->accesstoken;

            //using GuzzleHttp client to get request
            $client = new \GuzzleHttp\Client(['http_errors' => false]);
            $headers = [
                'Authorization' => 'Bearer ' . $token,
                'Accept' => 'application/json',
            ];

            $url = $this->api_url . '/hubdb/api/v2/tables';

            //get all cms Hubdb Tables data
            $response = $client->request('GET', $url, ['headers' => $headers]);


            $body = $response->getBody();
            $data = json_decode($body, true);


            if (!empty($data['status']) == 'error') {
                $status = $data['status'];
                $error_msg = $data['message'];
                Log::error('CMS HubDB data  |||| Error - ' . $error_msg);
            } else {

                $status = 'successed';
                $all_results = $data['objects'];

                //turn data into collection
                $results_collection = collect($all_results);
                $results_chunks = $results_collection->chunk(500); //chunk into smaller pieces


                foreach ($results_chunks as $results_chunk) {

                    $results = $results_chunk->toArray();


                    if (!empty($results)) {
                        foreach ($results as $value) {

                            //get all CMS Hubdb Tables ids
                            $hubspot_hubdb_table_ids[] = $value['id'];



                            $check_cms_hubdb = CmsHubdbTable::where('user_id', $user->id)
                                            ->where('table_id', $value['id'])
                                            ->where('backup_progress_id', $backup_progress['id'] - 1)->first();

                            $cms_hubdb_table = CmsHubdbTable::create([
                                        'user_id' => $user->id,
                                        'backup_progress_id' => $backup_progress['id'],
                                        'table_id' => $value['id'],
                                        'created_date' => isset($value['createdDate']) ? Carbon::createFromTimestampMs($value['createdDate'])->format('Y-m-d H:i:s') : NULL,
                                        'updated_date' => isset($value['updatedDate']) ? Carbon::createFromTimestampMs($value['updatedDate'])->format('Y-m-d H:i:s') : NULL,
                                        'deleted' => isset($value['deleted']) ? $value['deleted'] : 0,
                                        'use_for_pages' => isset($value['useForPages']) ? $value['useForPages'] : 0,
                                        'row_count' => isset($value['rowCount']) ? $value['rowCount'] : 0,
                                        'created_by' => isset($value['createdBy']) ? $value['createdBy'] : "",
                                        'updated_by' => isset($value['updatedBy']) ? $value['updatedBy'] : "",
                                        'column_count' => isset($value['columnCount']) ? $value['columnCount'] : 0,
                            ]);

                            if (!empty($value['columns'])) {
                                foreach ($value['columns'] as $column) {

                                    $hubdb_table_columns = CmsHubdbTableColumn::create([
                                                'cms_hubdb_table_db_id' => $cms_hubdb_tables->id,
                                                'column_id' => $column['id'],
                                                'column_name' => $column['name'],
                                                'column_label' => $column['label'],
                                                'column_type' => $column['type']
                                    ]);
                                }
                            }
                            if ($cms_hubdb_table) {
                                $created_rows +=1;
                            }
                            //check if Hubdb Table  is already inserted or not
                            if ($check_cms_hubdb === null) {
                                
                            } else {
                                $last_updated_date = Carbon::createFromTimestampMs($value['updatedDate'])->format('Y-m-d H:i:s');

                                //check if any data is changed
                                if ($last_updated_date != $check_cms_hubdb->updated_date) {

                                    $updated_rows +=1;
                                }
                            }
                        }
                    }
                }
            }
            //end time
            $time_end = microtime(true);
            //execution time
            $execution_time = ($time_end - $time_start);

            Log::error('time taken for CMS Hubdb Tables data ' . $execution_time);
            Log::error('Fetching CMS Hubdb Tables data completed |||| End <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< userId - ' . $userId);
        } catch (BadResponseException $ex) {
            //if request is invalid
            $response = $ex->getResponse();
            $jsonBody = (string) $response->getBody();
            $this->error($jsonBody);

            $data = json_decode($response->getBody(), true);
            $status = $data['status'];
            $error_msg = $data['message'];
            Log::error('CMS Hubdb Tables data  |||| Error - ' . $jsonBody);
        }

        //get cms hubdb tables from database
        $cms_hubdb_tables_in_database = CmsHubdbTable::where('backup_progress_id', $backup_progress['id'] - 1)->get()->pluck('table_id')->toArray();

        //check if empty result 
        if (count($hubspot_hubdb_table_ids) == 0) {
            $removed_rows = 0;
        } else {
            $removed_cms_hubdb_tables = array_diff($cms_hubdb_tables_in_database, $hubspot_hubdb_table_ids);
            $removed_rows = count($removed_cms_hubdb_tables);
        }

        //creating BackupHistory record
        $backup_history = BackupHistory::create([
                    'backup_progress_id' => $backup_progress['id'],
                    'user_id' => $user->id,
                    'object_name' => 'Fetched CMS HubDB Tables data',
                    'num_of_records' => $created_rows + $removed_rows,
                    'removed_records' => $removed_rows,
                    'added_records' => $created_rows,
                    'changed_records' => $updated_rows,
                    'num_of_API_calls' => 1,
                    'status' => $status,
                    'error_message' => $error_msg]);

        //get smart alerts for action 'added'
        $added_alert = SmartAlert::where('user_id', $user['id'])
                        ->where('s_object', 'CMS HubDB')
                        ->where('action', 'Added')->first();

        if (!empty($added_alert)) {
            if ($created_rows > $added_alert->amount) {

                //notification when CMS Hubdb Table added
                $add_notificatoion = Notification::create(['user_id' => $user['id'],
                            'backup_progress_id' => $backup_progress['id'],
                            'status' => 'CMS HubDB Tables Added',
                            'type' => 'email',
                            'num_of_records' => $created_rows]);

                //create history
                SmartAlertHistory::create(['user_id' => $user['id'], 'smart_alert_id' => $added_alert->id]);

                //notify user
                //$user->notify(new Backup($notificatoion));
            }
        }

        //get smart alerts for action 'Changed'
        $changed_alert = SmartAlert::where('user_id', $user['id'])
                        ->where('s_object', 'CMS HubDB')
                        ->where('action', 'Changed')->first();

        if (!empty($changed_alert)) {
            if ($updated_rows > $changed_alert->amount) {



                //notification when CMS  Hubdb Tables Changed
                $change_notificatoion = Notification::create(['user_id' => $user['id'],
                            'backup_progress_id' => $backup_progress['id'],
                            'status' => 'CMS HubDB Tables Changed',
                            'type' => 'email',
                            'num_of_records' => $updated_rows]);

                //create history
                SmartAlertHistory::create(['user_id' => $user['id'], 'smart_alert_id' => $changed_alert->id]);

                //notify user
                //$user->notify(new Backup($notificatoion));
            }
        }

        //get smart alerts for action 'Removed'
        $removed_alert = SmartAlert::where('user_id', $user['id'])
                        ->where('s_object', 'CMS HubDB')
                        ->where('action', 'Removed')->first();

        if (!empty($removed_alert)) {
            if ($removed_rows > $removed_alert->amount) {

                //notification when CMS Hubdb Table Removed
                $remove_notificatoion = Notification::create(['user_id' => $user['id'],
                            'backup_progress_id' => $backup_progress['id'],
                            'status' => 'CMS HubDB Tables Removed',
                            'type' => 'email',
                            'num_of_records' => $removed_rows]);

                //create history
                SmartAlertHistory::create(['user_id' => $user['id'], 'smart_alert_id' => $removed_alert->id]);

                //notify user
                //$user->notify(new Backup($notificatoion));
            }
        }
    }

}
