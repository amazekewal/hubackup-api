<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Engagement;
use App\TaskEngagement;
use App\NoteEngagement;
use App\NoteAttachment;
use App\EmailEngagement;
use App\CallEngagement;
use App\MeetingEngagement;
use App\BackupHistory;
use App\User;
use App\BackupProgress;
use App\SmartAlert;
use App\SmartAlertHistory;
use App\Notification;
use Illuminate\Support\Facades\Log;
use Carbon\Carbon;

class EngagementsData extends Command {

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'engagements:data {userId}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command will get all Engagements data';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
        //get api key from env file
        $this->api_url = env('HUBSPOT_URL');
        $this->api_key = env('HUBSPOT_KEY');
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {

        $status = '';
        $error_msg = '';
        $created_rows = 0;
        $updated_rows = 0;
        $hubspot_engagement_ids = [];

        $userId = $this->argument('userId');
        $user = User::find($userId);

        $backup_progress = BackupProgress::where('user_id', $user['id'])->where('status', 'Inprogress')->orderByDesc('id')->first();

        try {

            Log::error('Fetching Engagements data started |||| Start >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> userId - ' . $userId);

            //start time
            $time_start = microtime(true);


            $token = $user->accesstoken;

            $client = new \GuzzleHttp\Client(['http_errors' => false]);
            $headers = [
                'Authorization' => 'Bearer ' . $token,
                'Accept' => 'application/json',
            ];

            $get_data = $this->getEngagementsData($headers, 0, $client, $backup_progress, $user, $hubspot_engagement_ids, $status, $error_msg, $created_rows, $updated_rows);

            $created_rows = $get_data['created_rows'];
            $updated_rows = $get_data['updated_rows'];
            $hubspot_engagement_ids = $get_data['hubspot_engagement_ids'];
            $error_msg = $get_data['error_msg'];
            $status = $get_data['status'];


            //end time
            $time_end = microtime(true);

            //execution time
            $execution_time = ($time_end - $time_start);

            Log::error('time taken for Engagements data ' . $execution_time);
            Log::error('Fetching Engagements data completed |||| End <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< userId - ' . $userId);
        } catch (BadResponseException $ex) {
            //if request is invalid
            $response = $ex->getResponse();
            $jsonBody = (string) $response->getBody();
            $this->error($jsonBody);

            $data = json_decode($response->getBody(), true);
            $status = $data['status'];
            $error_msg = $data['message'];
            Log::error('Engagements data  |||| Error - ' . $jsonBody);
        }

        //get contacts from database
        $engagements_in_database = Engagement::where('backup_progress_id', $backup_progress['id'] - 1)->get()->pluck('engagement_id')->toArray();

        //check if empty result 
        if (count($hubspot_engagement_ids) == 0) {
            $removed_rows = 0;
        } else {
            $removed_engagements = array_diff($engagements_in_database, $hubspot_engagement_ids);
            $removed_rows = count($removed_engagements);
        }

        // creating BackupHistory record
        $backup_history = BackupHistory::create([
                    'backup_progress_id' => $backup_progress['id'],
                    'user_id' => $user->id,
                    'object_name' => 'Fetched Engagements data',
                    'num_of_records' => $created_rows + $removed_rows,
                    'removed_records' => $removed_rows,
                    'added_records' => $created_rows,
                    'changed_records' => $updated_rows,
                    'num_of_API_calls' => 1,
                    'status' => $status,
                    'error_message' => $error_msg]);

        //get smart alerts for action 'added'
        $added_alert = SmartAlert::where('user_id', $user['id'])
                        ->where('s_object', 'Engagements')
                        ->where('action', 'Added')->first();

        if (!empty($added_alert)) {
            if ($created_rows > $added_alert->amount) {

                //notification when Engagement added
                $add_notificatoion = Notification::create(['user_id' => $user['id'], 'backup_progress_id' => $backup_progress['id'],
                            'status' => 'Engagements Added', 'type' => 'email', 'num_of_records' => $created_rows]);

                //create history
                SmartAlertHistory::create(['user_id' => $user['id'], 'smart_alert_id' => $added_alert->id]);

                //notify user
                //$user->notify(new Backup($notificatoion));
            }
        }

        //get smart alerts for action 'Changed'
        $changed_alert = SmartAlert::where('user_id', $user['id'])
                        ->where('s_object', 'Engagements')
                        ->where('action', 'Changed')->first();

        if (!empty($changed_alert)) {
            if ($updated_rows > $changed_alert->amount) {

                //notification when Engagements Changed
                $change_notificatoion = Notification::create(['user_id' => $user['id'], 'backup_progress_id' => $backup_progress['id'],
                            'status' => 'Engagements Changed', 'type' => 'email', 'num_of_records' => $updated_rows]);

                //create history
                SmartAlertHistory::create(['user_id' => $user['id'], 'smart_alert_id' => $changed_alert->id]);

                //notify user
                //$user->notify(new Backup($notificatoion));
            }
        }

        //get smart alerts for action 'Removed'
        $removed_alert = SmartAlert::where('user_id', $user['id'])
                        ->where('s_object', 'Engagements')
                        ->where('action', 'Removed')->first();

        if (!empty($removed_alert)) {
            if ($removed_rows > $removed_alert->amount) {

                //notification when Engagements Removed
                $remove_notificatoion = Notification::create(['user_id' => $user['id'], 'backup_progress_id' => $backup_progress['id'],
                            'status' => 'Engagements Removed', 'type' => 'email', 'num_of_records' => $removed_rows]);

                //create history
                SmartAlertHistory::create(['user_id' => $user['id'], 'smart_alert_id' => $removed_alert->id]);

                //notify user
                //$user->notify(new Backup($notificatoion));
            }
        }
    }

    public function getEngagementsData($headers, $offset, $client, $backup_progress, $user, $hubspot_engagement_ids, $status, $error_msg, $created_rows, $updated_rows) {


        $url = $this->api_url . '/engagements/v1/engagements/paged?limit=100&offset=' . $offset;

        //get all Engagements data
        $response = $client->request('GET', $url, ['headers' => $headers]);

        $body = $response->getBody();
        $data = json_decode($body, true);

        if (!empty($data['status']) == 'error') {
            $status = $data['status'];
            $error_msg = $data['message'];
            Log::error('Engagements data  |||| Error - ' . $error_msg);
        } else {
            $results = $data['results'];
            $status = 'successed';
            if (!empty($results)) {
                foreach ($results as $result) {

                    //get all Engagement ids
                    $hubspot_engagement_ids[] = $result['engagement']['id'];



                    //engagement id  
                    $engagement_id = $result['engagement']['id'];
                    $portal_id = $result['engagement']['portalId'];
                    $active = $result['engagement']['active'];
                    $created_by = $result['engagement']['createdBy'];
                    $modified_by = $result['engagement']['modifiedBy'];
                    $owner_id = $result['engagement']['ownerId'];
                    $type = $result['engagement']['type'];
                    $last_updated = Carbon::createFromTimestampMs($result['engagement']['lastUpdated'])->format('Y-m-d H:i:s');



                    //check if any engagements exists
                    $engagement_check = Engagement::where('user_id', $user->id)
                                    ->where('engagement_id', $engagement_id)
                                    ->where('backup_progress_id', $backup_progress['id'] - 1)->first();
                    $engagement = Engagement::create([
                                'user_id' => $user->id,
                                'backup_progress_id' => $backup_progress['id'],
                                'engagement_id' => $engagement_id,
                                'portal_id' => $portal_id,
                                'active' => $active,
                                'created_by' => $created_by,
                                'modified_by' => $modified_by,
                                'owner_id' => $owner_id,
                                'type' => $type,
                                'associations_contactIds' => isset($result['associations']['contactIds']) ? $result['associations']['contactIds'] : NULL,
                                'associations_companyIds' => isset($result['associations']['companyIds']) ? $result['associations']['companyIds'] : NULL,
                                'associations_dealIds' => isset($result['associations']['dealIds']) ? $result['associations']['dealIds'] : NULL,
                                'associations_ownerIds' => isset($result['associations']['ownerIds']) ? $result['associations']['ownerIds'] : NULL,
                                'associations_workflowIds' => isset($result['associations']['workflowIds']) ? $result['associations']['workflowIds'] : NULL,
                                'associations_ticketIds' => isset($result['associations']['ticketIds']) ? $result['associations']['ticketIds'] : NULL,
                                'attachments' => isset($attachements) ? $result['attachments'] : NULL,
                                'createdate' => isset($result['engagement']['createdAt']) ? Carbon::createFromTimestampMs($result['engagement']['createdAt'])->format('Y-m-d H:i:s') : NULL,
                                'last_updated' => isset($result['engagement']['lastUpdated']) ? Carbon::createFromTimestampMs($result['engagement']['lastUpdated'])->format('Y-m-d H:i:s') : NULL,
                    ]);
                    $engagement_db_id = $engagement->id;
                    $table = 'create';



                    //Task engagements are used by users to manage tasks, and are available on the tasks dashboard as well as on associated object records.
                    //if engagement is TASK
                    if ($type == 'TASK') {

                        //create task engagements
                        TaskEngagement::create([
                            'engagement_db_id' => $engagement_db_id,
                            'body' => isset($result['engagement']['bodyPreview']) ? $result['engagement']['bodyPreview'] : '',
                            'status' => isset($result['metadata']['status']) ? $result['metadata']['status'] : '',
                            'subject' => isset($result['metadata']['subject']) ? $result['metadata']['subject'] : '',
                            'task_type' => isset($result['metadata']['taskType']) ? $result['metadata']['taskType'] : '',
                            'reminder' => isset($result['metadata']['reminders'][0]) ? Carbon::createFromTimestampMs($result['metadata']['reminders'][0])->format('Y-m-d H:i:s') : NULL,
                            'send_default_reminder' => isset($result['metadata']['sendDefaultReminder']) ? $result['metadata']['sendDefaultReminder'] : 0,
                        ]);
                    }

                    //Email engagements are used to track emails sent on an object record.
                    //if engagement is EMAIL
                    if ($type == 'EMAIL') {

                        //create  email engagements
                        EmailEngagement::create([
                            'engagement_db_id' => $engagement_db_id,
                            'sender_email' => isset($result['metadata']['from']['email']) ? $result['metadata']['from']['email'] : '',
                            'sender_first_name' => isset($result['metadata']['from']['firstName']) ? $result['metadata']['from']['firstName'] : '',
                            'sender_last_name' => isset($result['metadata']['from']['lastName']) ? $result['metadata']['from']['lastName'] : '',
                            'recipient_email' => isset($result['metadata']['to']['email']) ? $result['metadata']['to']['email'] : '',
                            'subject' => isset($result['metadata']['subject']) ? $result['metadata']['subject'] : '',
                            'html' => isset($result['metadata']['html']) ? $result['metadata']['html'] : '',
                            'text' => isset($result['metadata']['text']) ? $result['metadata']['text'] : '',
                        ]);
                    }


                    //Call engagements are used to track calls made by users on an object record.
                    //if engagement is CALL
                    if ($type == 'CALL') {

                        //create  call engagements
                        CallEngagement::create([
                            'engagement_db_id' => $engagement_db_id,
                            'to_number' => isset($result['metadata']['toNumber']) ? $result['metadata']['toNumber'] : '',
                            'from_number' => isset($result['metadata']['fromNumber']) ? $result['metadata']['fromNumber'] : '',
                            'status' => isset($result['metadata']['status']) ? $result['metadata']['status'] : '',
                            'external_id' => isset($result['metadata']['externalId']) ? $result['metadata']['externalId'] : '',
                            'duration_milliseconds' => isset($result['metadata']['durationMilliseconds']) ? $result['metadata']['durationMilliseconds'] : '',
                            'external_account_id' => isset($result['metadata']['externalAccountId']) ? $result['metadata']['externalAccountId'] : '',
                            'recording_url' => isset($result['metadata']['recordingUrl']) ? $result['metadata']['recordingUrl'] : '',
                            'body' => isset($result['metadata']['body']) ? $result['metadata']['body'] : '',
                            'disposition' => isset($result['metadata']['disposition']) ? $result['metadata']['disposition'] : '',
                        ]);
                    }


                    //Meeting engagements are used to track face-to-face meetings on an object record.
                    //if engagement is MEETING
                    if ($type == 'MEETING') {

                        //create meeting engagements
                        MeetingEngagement::create([
                            'engagement_db_id' => $engagement_db_id,
                            'body' => isset($result['metadata']['body']) ? $result['metadata']['body'] : '',
                            'start_time' => isset($result['metadata']['startTime']) ? Carbon::createFromTimestampMs($result['metadata']['startTime'])->format('Y-m-d H:i:s') : NULL,
                            'end_time' => isset($result['metadata']['endTime']) ? Carbon::createFromTimestampMs($result['metadata']['endTime'])->format('Y-m-d H:i:s') : NULL,
                            'title' => isset($result['metadata']['title']) ? $result['metadata']['title'] : '',
                        ]);
                    }
                    //Note engagements can hold simple text information about an object record.
                    //if engagement is NOTE
                    if ($type == 'NOTE') {

                        //create note engagements
                        $note = NoteEngagement::create([
                                    'engagement_db_id' => $engagement_db_id,
                                    'body' => isset($result['metadata']['body']) ? $result['metadata']['body'] : '',
                        ]);
                        $note_id = $note->id;
                    }

                    //if engagement is created
                    if ($engagement) {
                        $created_rows +=1;
                    }
                    if ($engagement_check == null) {
                        
                    } else {
                        //check if any data is changed
                        if ($last_updated != $engagement_check->last_updated) {

                            $updated_rows +=1;
                        }
                    }
                }
            }
        }
        if (isset($data['hasMore']) && !empty($data['hasMore'] == true)) {

            return $this->getEngagementsData($headers, $data['offset'], $client, $backup_progress, $user, $hubspot_engagement_ids, $status, $error_msg, $created_rows, $updated_rows);
        } else {

            $data = ['created_rows' => $created_rows, 'updated_rows' => $updated_rows,
                'hubspot_engagement_ids' => $hubspot_engagement_ids, 'status' => $status, 'error_msg' => $error_msg];

            return $data;
        }
    }

}
