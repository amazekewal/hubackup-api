<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\BadResponseException;
use Illuminate\Support\Facades\Artisan;
// models
use App\MarketingEmail;
use App\MarketingEmailWidget;
use App\BackupHistory;
use App\User;
use Illuminate\Support\Facades\Log;
use App\BackupProgress;
use App\Notifications\Backup;
use App\Notification;
use App\SmartAlert;
use App\SmartAlertHistory;
use Carbon\Carbon;

class MarketingEmailsData extends Command {

    /**
     * The name of the apiUrl
     * @var type 
     */
    protected $api_url;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'marketing-emails:data {userId}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command will get all Marketing Emails data';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();

        //get api key from env file
        $this->api_url = env('HUBSPOT_URL');
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        $this->info('Marketing Emails Data cron started....' . $this->api_url);

        $status = '';
        $error_msg = '';
        $created_rows = 0;
        $updated_rows = 0;
        $hubspot_marketing_email_ids = [];

        $userId = $this->argument('userId');
        $user = User::find($userId);

        $backup_progress = BackupProgress::where('user_id', $user['id'])->where('status', 'Inprogress')->orderByDesc('id')->first();

        try {

            Log::error('Fetching Marketing Emails data started |||| Start >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> userId - ' . $userId);

            //start time
            $time_start = microtime(true);


            //user token
            $token = $user->accesstoken;

            //using GuzzleHttp client to get request
            $client = new \GuzzleHttp\Client(['http_errors' => false]);
            $headers = [
                'Authorization' => 'Bearer ' . $token,
                'Accept' => 'application/json',
            ];

            $get_data = $this->getMarketingEmailsData($headers, 0, $client, $backup_progress, $user, $hubspot_marketing_email_ids, $status, $error_msg, $created_rows, $updated_rows);

            $created_rows = $get_data['created_rows'];
            $updated_rows = $get_data['updated_rows'];
            $hubspot_marketing_email_ids = $get_data['hubspot_marketing_email_ids'];
            $error_msg = $get_data['error_msg'];
            $status = $get_data['status'];


            //end time
            $time_end = microtime(true);
            //execution time
            $execution_time = ($time_end - $time_start);

            Log::error('time taken for Marketing Emails data ' . $execution_time);
            Log::error('Fetching Marketing Emails data completed |||| End <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< userId - ' . $userId);
        } catch (BadResponseException $ex) {
            //if request is invalid
            $response = $ex->getResponse();
            $jsonBody = (string) $response->getBody();
            $this->error($jsonBody);

            $data = json_decode($response->getBody(), true);
            $status = $data['status'];
            $error_msg = $data['message'];
            Log::error('CMS Marketing Emails data  |||| Error - ' . $jsonBody);
        }


        //get marketing emails from database
        $marketing_emails_in_database = MarketingEmail::where('backup_progress_id', $backup_progress['id'] - 1)->get()->pluck('marketing_email_id')->toArray();
        
        //check if empty result 
        if (count($hubspot_marketing_email_ids) == 0) {
            $removed_rows = 0;
        } else {
        $removed_marketing_emails = array_diff($marketing_emails_in_database, $hubspot_marketing_email_ids);
        $removed_rows = count($removed_marketing_emails);
        }
        
        //creating BackupHistory record
        $backup_history = BackupHistory::create([
                    'backup_progress_id' => $backup_progress['id'],
                    'user_id' => $user->id,
                    'object_name' => 'Fetched Marketing Emails data',
                    'num_of_records' => $created_rows + $removed_rows,
                    'removed_records' => $removed_rows,
                    'added_records' => $created_rows,
                    'changed_records' => $updated_rows,
                    'num_of_API_calls' => 1,
                    'status' => $status,
                    'error_message' => $error_msg]);

        //get smart alerts for action 'added'
        $added_alert = SmartAlert::where('user_id', $user['id'])
                        ->where('s_object', 'Marketing Emails')
                        ->where('action', 'Added')->first();

        if (!empty($added_alert)) {
            if ($created_rows > $added_alert->amount) {

                //notification when Marketing Email added
                $add_notificatoion = Notification::create(['user_id' => $user['id'], 'backup_progress_id' => $backup_progress['id'],
                            'status' => 'Marketing Emails Added', 'type' => 'email', 'num_of_records' => $created_rows]);

                //create history
                SmartAlertHistory::create(['user_id' => $user['id'], 'smart_alert_id' => $added_alert->id]);

                //notify user
                //$user->notify(new Backup($notificatoion));
            }
        }

        //get smart alerts for action 'Changed'
        $changed_alert = SmartAlert::where('user_id', $user['id'])
                        ->where('s_object', 'Marketing Emails')
                        ->where('action', 'Changed')->first();

        if (!empty($changed_alert)) {
            if ($updated_rows > $changed_alert->amount) {

                //notification when Marketing Email Changed
                $change_notificatoion = Notification::create(['user_id' => $user['id'], 'backup_progress_id' => $backup_progress['id'],
                            'status' => 'Marketing Emails Changed', 'type' => 'email', 'num_of_records' => $updated_rows]);

                //create history
                SmartAlertHistory::create(['user_id' => $user['id'], 'smart_alert_id' => $changed_alert->id]);

                //notify user
                //$user->notify(new Backup($notificatoion));
            }
        }

        //get smart alerts for action 'Removed'
        $removed_alert = SmartAlert::where('user_id', $user['id'])
                        ->where('s_object', 'Marketing Emails')
                        ->where('action', 'Removed')->first();

        if (!empty($removed_alert)) {
            if ($removed_rows > $removed_alert->amount) {

                //notification when Marketing Email Removed
                $remove_notificatoion = Notification::create(['user_id' => $user['id'], 'backup_progress_id' => $backup_progress['id'],
                            'status' => 'Marketing Emails Removed', 'type' => 'email', 'num_of_records' => $removed_rows]);

                //create history
                SmartAlertHistory::create(['user_id' => $user['id'], 'smart_alert_id' => $removed_alert->id]);

                //notify user
                //$user->notify(new Backup($notificatoion));
            }
        }
    }

    public function getMarketingEmailsData($headers, $offset, $client, $backup_progress, $user, $hubspot_marketing_email_ids, $status, $error_msg, $created_rows, $updated_rows) {

        $url = $this->api_url . '/marketing-emails/v1/emails?limit=100&offset=' . $offset;

        //get all Marketing Emails data
        $response = $client->request('GET', $url, ['headers' => $headers]);


        $body = $response->getBody();
        $data = json_decode($body, true);


        if (!empty($data['status']) == 'error') {
            $status = $data['status'];
            $error_msg = $data['message'];
            Log::error('Marketing Emails data  |||| Error - ' . $error_msg);
        } else {
            $results = $data['objects'];
            $status = 'successed';
            if (!empty($results)) {
                foreach ($results as $value) {

                    //get all marketing emails ids
                    $hubspot_marketing_email_ids[] = $value['id'];



                    $check_marketing_email = MarketingEmail::where('user_id', $user->id)
                                    ->where('marketing_email_id', $value['id'])
                                    ->where('backup_progress_id', $backup_progress['id'] - 1)->first();
                    $marketing_email = MarketingEmail::create([
                                'user_id' => $user->id,
                                'backup_progress_id' => $backup_progress['id'],
                                'marketing_email_id' => $value['id'],
                                'portal_id' => isset($value['portalId']) ? $value['portalId'] : 0,
                                'ab' => isset($value['ab']) ? $value['ab'] : 0,
                                'ab_hours_to_wait' => isset($value['abHoursToWait']) ? $value['abHoursToWait'] : 0,
                                'ab_sample_size_default' => isset($value['abSampleSizeDefault']) ? $value['abSampleSizeDefault'] : "",
                                'ab_sampling_default' => isset($value['abSamplingDefault']) ? $value['abSamplingDefault'] : "",
                                'ab_success_metric' => isset($value['abSuccessMetric']) ? $value['abSuccessMetric'] : "",
                                'ab_test_percentage' => isset($value['abTestPercentage']) ? $value['abTestPercentage'] : 0,
                                'ab_variation' => isset($value['abVariation']) ? $value['abVariation'] : 0,
                                'absolute_url' => isset($value['absoluteUrl']) ? $value['absoluteUrl'] : "",
                                'all_email_campaign_ids' => isset($value['allEmailCampaignIds']) ? $value['allEmailCampaignIds'] : NULL,
                                'analytics_page_id' => isset($value['analyticsPageId']) ? $value['analyticsPageId'] : 0,
                                'analytics_page_type' => isset($value['analyticsPageType']) ? $value['analyticsPageType'] : "",
                                'archived' => isset($value['archived']) ? $value['archived'] : 0,
                                'author_name' => isset($value['authorName']) ? $value['authorName'] : "",
                                'author_at' => isset($value['authorAt']) ? Carbon::createFromTimestampMs($value['authorAt'])->format('Y-m-d H:i:s') : NULL,
                                'author_email' => isset($value['authorEmail']) ? $value['authorEmail'] : "",
                                'author_user_id' => isset($value['authorUserId']) ? $value['authorUserId'] : 0,
                                'blog_rss_settings' => isset($value['blogRssSettings']) ? $value['blogRssSettings'] : "",
                                'can_spam_settings_id' => isset($value['canSpamSettingsId']) ? $value['canSpamSettingsId'] : 0,
                                'category_id' => isset($value['categoryId']) ? $value['categoryId'] : 0,
                                'content_type_category' => isset($value['contentTypeCategory']) ? $value['contentTypeCategory'] : 0,
                                'create_page' => isset($value['createPage']) ? $value['createPage'] : 0,
                                'current_state' => isset($value['currentState']) ? $value['currentState'] : "",
                                'currently_published' => isset($value['currentlyPublished']) ? $value['currentlyPublished'] : 0,
                                'domain' => isset($value['domain']) ? $value['domain'] : "",
                                'email_body' => isset($value['emailBody']) ? $value['emailBody'] : "",
                                'email_note' => isset($value['emailNote']) ? $value['emailNote'] : "",
                                'email_template_mode' => isset($value['emailTemplateMode']) ? $value['emailTemplateMode'] : "",
                                'email_type' => isset($value['emailType']) ? $value['emailType'] : "",
                                'feedback_email_category' => isset($value['feedbackEmailCategory']) ? $value['feedbackEmailCategory'] : "",
                                'feedback_survey_id' => isset($value['feedbackSurveyId']) ? $value['feedbackSurveyId'] : 0,
                                'flex_areas' => isset($value['flexAreas']) ? $value['flexAreas'] : NULL,
                                'freeze_date' => isset($value['freezeDate']) ? Carbon::createFromTimestampMs($value['freezeDate'])->format('Y-m-d H:i:s') : NULL,
                                'from_name' => isset($value['fromName']) ? $value['fromName'] : "",
                                'html_title' => isset($value['htmlTitle']) ? $value['htmlTitle'] : "",
                                'is_graymail_suppression_enabled' => isset($value['isGraymailSuppressionEnabled']) ? $value['isGraymailSuppressionEnabled'] : 0,
                                'is_local_timezone_send' => isset($value['isLocalTimezoneSend']) ? $value['isLocalTimezoneSend'] : 0,
                                'is_published' => isset($value['isPublished']) ? $value['isPublished'] : 0,
                                'is_recipient_fatigue_suppression_enabled' => isset($value['isRecipientFatigueSuppressionEnabled']) ? $value['isRecipientFatigueSuppressionEnabled'] : 0,
                                'last_edit_session_id' => isset($value['lastEditSessionId']) ? $value['lastEditSessionId'] : 0,
                                'last_edit_update_id' => isset($value['lastEditUpdateId']) ? $value['lastEditUpdateId'] : 0,
                                'layout_sections' => isset($value['layoutSections']) ? $value['layoutSections'] : NULL,
                                'lead_flow_id' => isset($value['leadFlowId']) ? $value['leadFlowId'] : 0,
                                'live_domain' => isset($value['liveDomain']) ? $value['liveDomain'] : "",
                                'mailing_lists_excluded' => isset($value['mailingListsExcluded']) ? $value['mailingListsExcluded'] : NULL,
                                'mailing_lists_included' => isset($value['mailingListsIncluded']) ? $value['mailingListsIncluded'] : NULL,
                                'max_rss_entries' => isset($value['maxRssEntries']) ? $value['maxRssEntries'] : 0,
                                'name' => isset($value['name']) ? $value['name'] : "",
                                'page_expiry_enabled' => isset($value['pageExpiryEnabled']) ? $value['pageExpiryEnabled'] : 0,
                                'page_redirected' => isset($value['pageRedirected']) ? $value['pageRedirected'] : 0,
                                'preview_key' => isset($value['previewKey']) ? $value['previewKey'] : "",
                                'processing_status' => isset($value['processingStatus']) ? $value['processingStatus'] : "",
                                'publish_date' => isset($value['publishDate']) ? Carbon::createFromTimestampMs($value['publishDate'])->format('Y-m-d H:i:s') : NULL,
                                'publish_immediately' => isset($value['publishImmediately']) ? $value['publishImmediately'] : 0,
                                'published_url' => isset($value['publishedUrl']) ? $value['publishedUrl'] : "",
                                'reply_to' => isset($value['replyTo']) ? $value['replyTo'] : "",
                                'resolved_domain' => isset($value['resolvedDomain']) ? $value['resolvedDomain'] : "",
                                'rss_email_by_text' => isset($value['rssEmailByText']) ? $value['rssEmailByText'] : "",
                                'rss_email_click_through_text' => isset($value['rssEmailClickThroughText']) ? $value['rssEmailClickThroughText'] : "",
                                'rss_email_comment_text' => isset($value['rssEmailCommentText']) ? $value['rssEmailCommentText'] : "",
                                'rss_email_entry_template_enabled' => isset($value['rssEmailEntryTemplateEnabled']) ? $value['rssEmailEntryTemplateEnabled'] : 0,
                                'rss_email_image_max_width' => isset($value['rssEmailImageMaxWidth']) ? $value['rssEmailImageMaxWidth'] : 0,
                                'rss_email_url' => isset($value['rssEmailUrl']) ? $value['rssEmailUrl'] : "",
                                'scrubs_subscription_links' => isset($value['scrubsSubscriptionLinks']) ? $value['scrubsSubscriptionLinks'] : 0,
                                'slug' => isset($value['slug']) ? $value['slug'] : "",
                                'smart_email_fields' => isset($value['smartEmailFields']) ? $value['smartEmailFields'] : "",
                                'state' => isset($value['state']) ? $value['state'] : "",
                                'style_settings' => isset($value['styleSettings']) ? $value['styleSettings'] : "",
                                'subcategory' => isset($value['subcategory']) ? $value['subcategory'] : "",
                                'subject' => isset($value['subject']) ? $value['subject'] : "",
                                'subscription' => isset($value['subscription']) ? $value['subscription'] : "",
                                'subscription_name' => isset($value['subscriptionName']) ? $value['subscriptionName'] : "",
                                'team_perms' => isset($value['teamPerms']) ? $value['teamPerms'] : NULL,
                                'template_path' => isset($value['templatePath']) ? $value['templatePath'] : "",
                                'transactional' => isset($value['transactional']) ? $value['transactional'] : 0,
                                'unpublished_at' => isset($value['unpublishedAt']) ? Carbon::createFromTimestampMs($value['unpublishedAt'])->format('Y-m-d H:i:s') : NULL,
                                'url' => isset($value['url']) ? $value['url'] : "",
                                'use_rss_headline_as_subject' => isset($value['useRssHeadlineAsSubject']) ? $value['useRssHeadlineAsSubject'] : 0,
                                'user_perms' => isset($value['userPerms']) ? $value['userPerms'] : NULL,
                                'vids_excluded' => isset($value['vidsExcluded']) ? $value['vidsExcluded'] : NULL,
                                'vids_included' => isset($value['vidsIncluded']) ? $value['vidsIncluded'] : NULL,
                                'workflow_names' => isset($value['workflowNames']) ? $value['workflowNames'] : NULL,
                                'created' => isset($value['createdDate']) ? Carbon::createFromTimestampMs($value['createdDate'])->format('Y-m-d H:i:s') : NULL,
                                'updated' => isset($value['updatedDate']) ? Carbon::createFromTimestampMs($value['updatedDate'])->format('Y-m-d H:i:s') : NULL,
                    ]);

                    if (!empty($value['widgets'])) {
                        foreach ($value['widgets'] as $key => $widget) {

                            $widgets = MarketingEmailWidget::create([
                                        'marketing_email_db_id' => $marketing_email->id,
                                        'portal_id' => isset($value['portalId']) ? $value['portalId'] : 0,
                                        'name' => isset($key) ? $key : "",
                                        'label' => isset($value['label']) ? $value['label'] : "",
                                        'body' => isset($value['body']) ? value['body'] : NULL,
                                        'child_css' => isset($value['child_css']) ? $value['child_css'] : NULL,
                                        'css' => isset($value['css']) ? $value['css'] : NULL,
                                        'order' => isset($value['order']) ? $value['order'] : 0,
                                        'smart_type' => isset($value['smart_type']) ? $value['smart_type'] : "",
                                        'type' => isset($value['type']) ? $value['type'] : "",
                            ]);
                        }
                    }
                    //if marketing_email is created
                    if ($marketing_email) {
                        $created_rows +=1;
                    }
                    //check if marketing email  is already inserted or not
                    if ($check_marketing_email === null) {
                        
                    } else {

                        $last_updated_date = Carbon::createFromTimestampMs($value['updated'])->format('Y-m-d H:i:s');

                        //check if any data is changed
                        if ($last_updated_date != $check_marketing_email->updated_) {

                            $updated_rows +=1;
                        }
                    }
                }
            }
        }

        if (isset($data['offset']) && !empty($data['offset'] !== 0)) {

            return $this->getMarketingEmailsData($headers, $data['offset'], $client, $backup_progress, $user, $hubspot_marketing_email_ids, $status, $error_msg, $created_rows, $updated_rows);
        } else {

            $data = ['created_rows' => $created_rows, 'updated_rows' => $updated_rows,
                'hubspot_marketing_email_ids' => $hubspot_marketing_email_ids, 'status' => $status, 'error_msg' => $error_msg];

            return $data;
        }
    }

}
