<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
// models
use App\EmailCampaign;
use App\EmailEvent;
use App\BackupHistory;
use App\User;
use Illuminate\Support\Facades\Log;
use App\BackupProgress;
use App\Notifications\Backup;
use App\Notification;
use App\SmartAlert;
use App\SmartAlertHistory;
use Carbon\Carbon;

class EmailEventsData extends Command {

    /**
     * The name of the apiUrl
     * @var type 
     */
    protected $api_url;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'email-events:data {userId}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command will get email events data';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();

        //get api key from env file
        $this->api_url = env('HUBSPOT_URL');
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        $this->info('Email Events Data cron started....' . $this->api_url);

        $status = '';
        $error_msg = '';
        $created_rows = 0;
        $updated_rows = 0;
        $hubspot_email_event_ids = [];

        $userId = $this->argument('userId');
        $user = User::find($userId);

        $backup_progress = BackupProgress::where('user_id', $user['id'])->where('status', 'Inprogress')->orderByDesc('id')->first();

        $email_campaigns = EmailCampaign::where('backup_progress_id', $backup_progress['id'])->get()->toArray();

        if (!empty($email_campaigns)) {

            Log::error('Fetching Email Events data started |||| Start >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> userId - ' . $userId);

            //start time
            $time_start = microtime(true);


            //user token
            $token = $user->accesstoken;

            //using GuzzleHttp client to get request
            $client = new \GuzzleHttp\Client(['http_errors' => false]);
            $headers = [
                'Authorization' => 'Bearer ' . $token,
                'Accept' => 'application/json',
            ];
            foreach ($email_campaigns as $email_campaign) {
                $get_data = $this->getEmailEventsData($headers, 0, $client, $backup_progress, $user, $email_campaign['email_campaign_id'], $hubspot_email_event_ids, $status, $error_msg, $created_rows, $updated_rows);
                $created_rows += $get_data['created_rows'];
                $updated_rows += $get_data['updated_rows'];
                $hubspot_email_event_ids[] = $get_data['hubspot_email_event_ids'];
                $error_msg = $get_data['error_msg'];
                $status = $get_data['status'];

                //end time
                $time_end = microtime(true);
                //execution time
                $execution_time = ($time_end - $time_start);

                Log::error('time taken for Email Events data ' . $execution_time);
                Log::error('Fetching Email Events data completed |||| End <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< userId - ' . $userId);
            }
        } else {

            $status = 'error';
            $error_msg = 'No Campaigns found';
            Log::error('CMS Email Events data  |||| Error - ' . $error_msg);
        }


        //get email events from database
        $email_events_in_database = EmailEvent::where('backup_progress_id', $backup_progress['id'] - 1)->get()->pluck('email_event_id')->toArray();

        //check if empty result 
        if (count($hubspot_email_event_ids) == 0) {
            $removed_rows = 0;
        } else {
            $removed_email_events = array_diff($email_events_in_database, $hubspot_email_event_ids);
            $removed_rows = count($removed_email_events);
        }

        $backup_progress = BackupProgress::where('user_id', $user['id'])->where('status', 'Inprogress')->orderByDesc('id')->first();

        //creating BackupHistory record
        $backup_history = BackupHistory::create([
                    'backup_progress_id' => $backup_progress['id'],
                    'user_id' => $user->id,
                    'object_name' => 'Fetched Email Events data',
                    'num_of_records' => $created_rows + $removed_rows,
                    'removed_records' => $removed_rows,
                    'added_records' => $created_rows,
                    'changed_records' => $updated_rows,
                    'num_of_API_calls' => 1,
                    'status' => $status,
                    'error_message' => $error_msg]);

        //get smart alerts for action 'added'
        $added_alert = SmartAlert::where('user_id', $user['id'])
                        ->where('s_object', 'Email Events')
                        ->where('action', 'Added')->first();

        if (!empty($added_alert)) {
            if ($created_rows > $added_alert->amount) {

                //notification when Event added
                $add_notificatoion = Notification::create(['user_id' => $user['id'], 'backup_progress_id' => $backup_progress['id'],
                            'status' => 'Email Events Added', 'type' => 'email', 'num_of_records' => $created_rows]);

                //create history
                SmartAlertHistory::create(['user_id' => $user['id'], 'smart_alert_id' => $added_alert->id]);

                //notify user
                //$user->notify(new Backup($notificatoion));
            }
        }

        //get smart alerts for action 'Changed'
        $changed_alert = SmartAlert::where('user_id', $user['id'])
                        ->where('s_object', 'Email Events')
                        ->where('action', 'Changed')->first();

        if (!empty($changed_alert)) {
            if ($updated_rows > $changed_alert->amount) {

                //notification when Email Event Changed
                $change_notificatoion = Notification::create(['user_id' => $user['id'], 'backup_progress_id' => $backup_progress['id'],
                            'status' => 'Email Events Changed', 'type' => 'email', 'num_of_records' => $updated_rows]);

                //create history
                SmartAlertHistory::create(['user_id' => $user['id'], 'smart_alert_id' => $changed_alert->id]);

                //notify user
                //$user->notify(new Backup($notificatoion));
            }
        }

        //get smart alerts for action 'Removed'
        $removed_alert = SmartAlert::where('user_id', $user['id'])
                        ->where('s_object', 'Email Events')
                        ->where('action', 'Removed')->first();

        if (!empty($removed_alert)) {
            if ($removed_rows > $removed_alert->amount) {

                //notification when Email Event Removed
                $remove_notificatoion = Notification::create(['user_id' => $user['id'], 'backup_progress_id' => $backup_progress['id'],
                            'status' => 'Email Events Removed', 'type' => 'email', 'num_of_records' => $removed_rows]);

                //create history
                SmartAlertHistory::create(['user_id' => $user['id'], 'smart_alert_id' => $removed_alert->id]);

                //notify user
                //$user->notify(new Backup($notificatoion));
            }
        }
    }

    public function getEmailEventsData($headers, $offset, $client, $backup_progress, $user, $hubspot_compaign_id, $hubspot_email_event_ids, $status, $error_msg, $created_rows, $updated_rows) {

        $event_url = $this->api_url . 'email/public/v1/events?campaignId=' . $hubspot_compaign_id . '&limit=100&offset=' . $offset;


        $response_event = $client->request('GET', $event_url, ['headers' => $headers]);
        $body_event = $response_event->getBody();
        $data_event = json_decode($body_event, true);

        if (!empty($data_event['status']) == 'error') {
            $status = $data_event['status'];
            $error_msg = $data_event['message'];
            Log::error('Email Events data  |||| Error - ' . $error_msg);
        } else {
            $event_results = $data_event['events'];
            $status = 'successed';
            if (!empty($event_results)) {

                $create_events = [];
                foreach ($event_results as $value) {

                    //get all email events ids
                    $hubspot_email_event_ids[] = $value['id'];


                    $check_event = EmailEvent::where('user_id', $user->id)
                                    ->where('email_event_id', $value['id'])
                                    ->where('backup_progress_id', $backup_progress['id'] - 1)->first();

                    $create_events[] = [
                        'user_id' => $user->id,
                        'backup_progress_id' => $backup_progress['id'],
                        'email_event_id' => $value['id'],
                        'email_campaign_id' => $value['emailCampaignId'],
                        'app_id' => isset($value['appId']) ? $value['appId'] : 0,
                        'app_name' => isset($value['appName']) ? $value['appName'] : "",
                        'browser_family' => isset($value['browser']['family']) ? $value['browser']['family'] : 0,
                        'browser_name' => isset($value['browser']['name']) ? $value['browser']['name'] : 0,
                        'browser_producer' => isset($value['browser']['producer']) ? $value['browser']['producer'] : 0,
                        'browser_producer_url' => isset($value['browser']['producerUrl']) ? $value['browser']['producerUrl'] : 0,
                        'browser_type' => isset($value['browser']['type']) ? $value['browser']['type'] : 0,
                        'browser_url' => isset($value['browser']['url']) ? $value['browser']['url'] : "",
                        'browser_version' => isset($value['browser']['version']) ? $value['browser']['version'] : NULL,
                        'created' => isset($value['created']) ? $value['created'] : "",
                        'city' => isset($value['location']['city']) ? $value['location']['city'] : "",
                        'country' => isset($value['location']['country']) ? $value['location']['country'] : "",
                        'state' => isset($value['location']['state']) ? $value['location']['state'] : "",
                        'recipient' => isset($value['recipient']) ? $value['recipient'] : "",
                        'type' => isset($value['type']) ? $value['type'] : "",
                        'user_agent' => isset($value['userAgent']) ? $value['userAgent'] : "",
                        'created_at' => Carbon::now()->toDateTimeString(),
                        'updated_at' => Carbon::now()->toDateTimeString()
                    ];
                    //check if event  is already inserted or not
                    if ($check_event === null) {
                        
                    } else {

                        //check if any data is changed
                        if ($check_event->app_name != $value['appName'] ||
                                $check_event->browser_family != $value['browser']['family'] ||
                                $check_event->browser_name != $value['browser']['name'] ||
                                $check_event->browser_producer != $value['browser']['producer'] ||
                                $check_event->browser_producer_url != $value['browser']['producerUrl'] ||
                                $check_event->browser_type != $value['browser']['type'] ||
                                $check_event->browser_url != $value['browser']['url'] ||
                                $check_event->browser_version != $value['browser']['version'] ||
                                $check_event->created != $value['created'] ||
                                $check_event->city != $value['location']['city'] ||
                                $check_event->country != $value['location']['country'] ||
                                $check_event->state != $value['location']['state'] ||
                                $check_event->recipient != $value['recipient'] ||
                                $check_event->type != $value['type'] ||
                                $check_event->user_agent != $value['userAgent']) {

                            $updated_rows +=1;
                        }
                    }
                }
                //insert email events in bulk
                EmailEvent::insert($create_events);
                $created_rows += count($create_events);
            }
        }

        if (isset($data['hasMore']) && !empty($data['hasMore'] == true)) {

            return $this->getEmailEventsData($headers, $data['offset'], $client, $backup_progress, $user, $hubspot_compaign_id, $hubspot_email_event_ids, $status, $error_msg, $created_rows, $updated_rows);
        } else {

            $data = ['created_rows' => $created_rows, 'updated_rows' => $updated_rows,
                'hubspot_email_event_ids' => $hubspot_email_event_ids, 'status' => $status, 'error_msg' => $error_msg];

            return $data;
        }
    }

}
