<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
// models
use App\CmsBlogComment;
use App\BackupHistory;
use App\User;
use Illuminate\Support\Facades\Log;
use App\BackupProgress;
use App\Notifications\Backup;
use App\Notification;
use App\SmartAlert;
use App\SmartAlertHistory;
use Carbon\Carbon;

class CmsBlogCommentsData extends Command {

    /**
     * The name of the apiUrl
     * @var type 
     */
    protected $api_url;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cms-blog-comments:data {userId}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command will get all Cms Blog Comments data';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();

        //get api key from env file
        $this->api_url = env('HUBSPOT_URL');
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        $this->info('Cms Blog Comments Data cron started....' . $this->api_url);

        $status = '';
        $error_msg = '';
        $created_rows = 0;
        $updated_rows = 0;
        $hubspot_blog_comment_ids = [];

        $userId = $this->argument('userId');
        $user = User::find($userId);

        $backup_progress = BackupProgress::where('user_id', $user['id'])->where('status', 'Inprogress')->orderByDesc('id')->first();


        try {

            Log::error('Fetching Cms Blog Comments data started |||| Start >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> userId - ' . $userId);

            //start time
            $time_start = microtime(true);


            //user token
            $token = $user->accesstoken;

            //using GuzzleHttp client to get request
            $client = new \GuzzleHttp\Client(['http_errors' => false]);
            $headers = [
                'Authorization' => 'Bearer ' . $token,
                'Accept' => 'application/json',
            ];

            $get_data = $this->getCmsBlogCommentsData($headers, 0, $client, $backup_progress, $user, $hubspot_blog_comment_ids, $status, $error_msg, $created_rows, $updated_rows);

            $created_rows = $get_data['created_rows'];
            $updated_rows = $get_data['updated_rows'];
            $hubspot_blog_comment_ids = $get_data['hubspot_blog_comment_ids'];
            $error_msg = $get_data['error_msg'];
            $status = $get_data['status'];

            //end time
            $time_end = microtime(true);
            //execution time
            $execution_time = ($time_end - $time_start);

            Log::error('time taken for Cms Blog Comments data ' . $execution_time);
            Log::error('Fetching Cms Blog Comments data completed |||| End <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< userId - ' . $userId);
        } catch (BadResponseException $ex) {
            //if request is invalid
            $response = $ex->getResponse();
            $jsonBody = (string) $response->getBody();
            $this->error($jsonBody);

            $data = json_decode($response->getBody(), true);
            $status = $data['status'];
            $error_msg = $data['message'];
            Log::error('Cms Blog Comments data  |||| Error - ' . $jsonBody);
        }

        //get blog comments from database
        $blog_comments_in_database = CmsBlogComment::where('backup_progress_id', $backup_progress['id'] - 1)->get()->pluck('cms_blog_comment_id')->toArray();

        //check if empty result 
        if (count($hubspot_blog_comment_ids) == 0) {
            $removed_rows = 0;
        } else {
            $removed_blog_comments = array_diff($blog_comments_in_database, $hubspot_blog_comment_ids);
            $removed_rows = count($removed_blog_comments);
        }

        //creating BackupHistory record
        $backup_history = BackupHistory::create([
                    'backup_progress_id' => $backup_progress['id'],
                    'user_id' => $user->id,
                    'object_name' => 'Fetched Cms Blog Comments data',
                    'num_of_records' => $created_rows + $removed_rows,
                    'removed_records' => $removed_rows,
                    'added_records' => $created_rows,
                    'changed_records' => $updated_rows,
                    'num_of_API_calls' => 1,
                    'status' => $status,
                    'error_message' => $error_msg]);

        //get smart alerts for action 'added'
        $added_alert = SmartAlert::where('user_id', $user['id'])
                        ->where('s_object', 'Cms Blog Comments')
                        ->where('action', 'Added')->first();

        if (!empty($added_alert)) {
            if ($created_rows > $added_alert->amount) {

                //notification when Cms Blog Comment added
                $add_notificatoion = Notification::create(['user_id' => $user['id'],
                            'backup_progress_id' => $backup_progress['id'],
                            'status' => 'Cms Blog Comments Added',
                            'type' => 'email',
                            'num_of_records' => $created_rows]);

                //create history
                SmartAlertHistory::create(['user_id' => $user['id'], 'smart_alert_id' => $added_alert->id]);

                //notify user
                //$user->notify(new Backup($notificatoion));
            }
        }

        //get smart alerts for action 'Changed'
        $changed_alert = SmartAlert::where('user_id', $user['id'])
                        ->where('s_object', 'Cms Blog Comments')
                        ->where('action', 'Changed')->first();

        if (!empty($changed_alert)) {
            if ($updated_rows > $changed_alert->amount) {

                //notification when Cms Blog Comment Changed
                $change_notificatoion = Notification::create(['user_id' => $user['id'],
                            'backup_progress_id' => $backup_progress['id'],
                            'status' => 'Cms Blog Comments Changed',
                            'type' => 'email',
                            'num_of_records' => $updated_rows]);

                //create history
                SmartAlertHistory::create(['user_id' => $user['id'], 'smart_alert_id' => $changed_alert->id]);

                //notify user
                //$user->notify(new Backup($notificatoion));
            }
        }

        //get smart alerts for action 'Removed'
        $removed_alert = SmartAlert::where('user_id', $user['id'])
                        ->where('s_object', 'Cms Blog Comments')
                        ->where('action', 'Removed')->first();

        if (!empty($removed_alert)) {
            if ($removed_rows > $removed_alert->amount) {

                //notification when Cms Blog Comment Removed
                $remove_notificatoion = Notification::create(['user_id' => $user['id'],
                            'backup_progress_id' => $backup_progress['id'],
                            'status' => 'Cms Blog Comments Removed',
                            'type' => 'email',
                            'num_of_records' => $removed_rows]);

                //create history
                SmartAlertHistory::create(['user_id' => $user['id'], 'smart_alert_id' => $removed_alert->id]);

                //notify user
                //$user->notify(new Backup($notificatoion));
            }
        }
    }

    public function getCmsBlogCommentsData($headers, $offset, $client, $backup_progress, $user, $hubspot_blog_comment_ids, $status, $error_msg, $created_rows, $updated_rows) {

        $url = $this->api_url . '/comments/v3/comments?limit=100&offset=' . $offset;

        //get all Cms Blog Comments data
        $response = $client->request('GET', $url, ['headers' => $headers]);


        $body = $response->getBody();
        $data = json_decode($body, true);


        if (!empty($data['status']) == 'error') {
            $status = $data['status'];
            $error_msg = $data['message'];
            Log::error('CMS Blog Comments data  |||| Error - ' . $error_msg);
        } else {
            $results = $data['objects'];
            $status = 'successed';
            if (!empty($results)) {
                $create_comments = [];
                foreach ($results as $value) {

                    //get all blog comment ids
                    $hubspot_blog_comment_ids[] = $value['id'];


                    $check_comment = CmsBlogComment::where('user_id', $user->id)
                                    ->where('cms_blog_comment_id', $value['id'])
                                    ->where('backup_progress_id', $backup_progress['id'] - 1)->first();

                    $create_comments[] = [
                        'user_id' => $user->id,
                        'backup_progress_id' => $backup_progress['id'],
                        'cms_blog_comment_id' => $value['id'],
                        'portal_id' => isset($value['portalId']) ? $value['portalId'] : 0,
                        'content_id' => isset($value['contentId']) ? $value['contentId'] : 0,
                        'content_title' => isset($value['contentTitle']) ? $value['contentTitle'] : "",
                        'content_permalink' => isset($value['contentPermalink']) ? $value['contentPermalink'] : "",
                        'collection_id' => isset($value['collectionId']) ? $value['collectionId'] : 0,
                        'deleted_at' => isset($value['deletedAt']) ? $value['deletedAt'] : "",
                        'user_name' => isset($value['userName']) ? $value['userName'] : "",
                        'first_name' => isset($value['firstName']) ? $value['firstName'] : "",
                        'last_name' => isset($value['lastName']) ? $value['lastName'] : "",
                        'user_email' => isset($value['userEmail']) ? $value['userEmail'] : "",
                        'comment' => isset($value['comment']) ? $value['comment'] : "",
                        'user_url' => isset($value['userUrl']) ? $value['userUrl'] : "",
                        'state' => isset($value['state']) ? $value['state'] : "",
                        'user_ip' => isset($value['userIp']) ? $value['userIp'] : "",
                        'user_referrer' => isset($value['userReferrer']) ? $value['userReferrer'] : "",
                        'user_agent' => isset($value['userAgent']) ? $value['userAgent'] : "",
                        'content_author_email' => isset($value['contentAuthorEmail']) ? $value['contentAuthorEmail'] : "",
                        'content_author_name' => isset($value['contentAuthorName']) ? $value['contentAuthorName'] : "",
                        'thread_id' => isset($value['threadId']) ? $value['threadId'] : "",
                        'replying_to' => isset($value['replyingTo']) ? $value['replyingTo'] : "",
                        'parent_id' => isset($value['parentId']) ? $value['parentId'] : 0,
                        'legacy_id' => isset($value['legacyId']) ? $value['legacyId'] : 0,
                        'extra_context' => isset($value['extraContext']) ? $value['extraContext'] : "",
                        'created_date' => isset($value['createdAt']) ? Carbon::createFromTimestampMs($value['createdAt'])->format('Y-m-d H:i:s') : NULL,
                        'content_created_at' => isset($value['contentCreatedAt']) ? Carbon::createFromTimestampMs($value['contentCreatedAt'])->format('Y-m-d H:i:s') : NULL,
                        'created_at' => Carbon::now()->toDateTimeString(),
                        'updated_at' => Carbon::now()->toDateTimeString()
                    ];

                    //check if Cms Blog Comment  is already inserted or not
                    if ($check_comment === null) {
                        
                    } else {

                        //check if any data is changed
                        if ($check_comment->content_id != $value['contentId'] || $check_comment->content_title != $value['contentTitle'] ||
                                $check_comment->content_permalink != $value['contentPermalink'] || $check_comment->collection_id != $value['collection_id'] ||
                                $check_comment->deleted_at != $value['deletedAt'] || $check_comment->user_name != $value['userName'] ||
                                $check_comment->first_name != $value['firstName'] || $check_comment->last_name != $value['lastName'] ||
                                $check_comment->user_email != $value['userEmail'] || $check_comment->comment != $value['comment'] ||
                                $check_comment->user_url != $value['userUrl'] || $check_comment->state != $value['state'] ||
                                $check_comment->user_ip != $value['userIp'] || $check_comment->user_referrer != $value['userReferrer'] ||
                                $check_comment->user_agent != $value['userAgent'] || $check_comment->content_author_email != $value['contentAuthorEmail'] ||
                                $check_comment->content_author_name != $value['contentAuthorName'] || $check_comment->thread_id != $value['threadId'] ||
                                $check_comment->replying_to != $value['replyingTo'] || $check_comment->parent_id != $value['parentId'] ||
                                $check_comment->legacy_id != $value['legacyId'] || $check_comment->extra_context != $value['extraContext']
                        ) {

                            $updated_rows +=1;
                        }
                    }
                }
                //insert blog comments in bulk
                CmsBlogComment::insert($create_comments);
                $created_rows += count($create_comments);
            }
        }
        if (isset($data['offset']) && !empty($data['offset'] !== 0)) {

            return $this->getCmsBlogCommentsData($headers, $data['offset'], $client, $backup_progress, $user, $hubspot_blog_comment_ids, $status, $error_msg, $created_rows, $updated_rows);
        } else {

            $data = ['created_rows' => $created_rows, 'updated_rows' => $updated_rows,
                'hubspot_blog_comment_ids' => $hubspot_blog_comment_ids, 'status' => $status, 'error_msg' => $error_msg];

            return $data;
        }
    }

}
