<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\BadResponseException;
use Illuminate\Support\Facades\Artisan;
// models
use App\CmsUrlMapping;
use App\BackupHistory;
use App\User;
use Illuminate\Support\Facades\Log;
use App\BackupProgress;
use App\Notifications\Backup;
use App\Notification;
use App\SmartAlert;
use App\SmartAlertHistory;
use Carbon\Carbon;

class CmsUrlMappingsData extends Command {

    /**
     * The name of the apiUrl
     * @var type 
     */
    protected $api_url;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cms-url-mappings:data {userId}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command will get all CMS Url Mappings data';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();

        //get api key from env file
        $this->api_url = env('HUBSPOT_URL');
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        $this->info('CMS Url Mappings Data cron started....' . $this->api_url);

        $status = '';
        $error_msg = '';
        $created_rows = 0;
        $updated_rows = 0;
        $hubspot_cms_url_mapping_ids = [];


        $userId = $this->argument('userId');
        $user = User::find($userId);

        $backup_progress = BackupProgress::where('user_id', $user['id'])->where('status', 'Inprogress')->orderByDesc('id')->first();


        try {

            Log::error('Fetching CMS Url Mappings data started |||| Start >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> userId - ' . $userId);

            //start time
            $time_start = microtime(true);


            //user token
            $token = $user->accesstoken;

            //using GuzzleHttp client to get request
            $client = new \GuzzleHttp\Client(['http_errors' => false]);
            $headers = [
                'Authorization' => 'Bearer ' . $token,
                'Accept' => 'application/json',
            ];

            $get_data = $this->getCmsUrlMappingsData($headers, 0, $client, $backup_progress, $user, $hubspot_cms_url_mapping_ids, $status, $error_msg, $created_rows, $updated_rows);

            $created_rows = $get_data['created_rows'];
            $updated_rows = $get_data['updated_rows'];
            $hubspot_cms_url_mapping_ids = $get_data['hubspot_cms_url_mapping_ids'];
            $error_msg = $get_data['error_msg'];
            $status = $get_data['status'];


            //end time
            $time_end = microtime(true);
            //execution time
            $execution_time = ($time_end - $time_start);

            Log::error('time taken for CMS Url Mappings data ' . $execution_time);
            Log::error('Fetching CMS Url Mappings data completed |||| End <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< userId - ' . $userId);
        } catch (BadResponseException $ex) {
            //if request is invalid
            $response = $ex->getResponse();
            $jsonBody = (string) $response->getBody();
            $this->error($jsonBody);

            $data = json_decode($response->getBody(), true);
            $status = $data['status'];
            $error_msg = $data['message'];
            Log::error('CMS Url Mappings data  |||| Error - ' . $jsonBody);
        }

        //get cms url mappings from database
        $cms_url_mappings_in_database = CmsUrlMapping::where('backup_progress_id', $backup_progress['id'] - 1)->get()->pluck('cms_url_mapping_id')->toArray();

        //check if empty result 
        if (count($hubspot_cms_url_mapping_ids) == 0) {
            $removed_rows = 0;
        } else {
            $removed_cms_url_mappings = array_diff($cms_url_mappings_in_database, $hubspot_cms_url_mapping_ids);
            $removed_rows = count($removed_cms_url_mappings);
        }

        //creating BackupHistory record
        $backup_history = BackupHistory::create([
                    'backup_progress_id' => $backup_progress['id'],
                    'user_id' => $user->id,
                    'object_name' => 'Fetched CMS Url Mappings data',
                    'num_of_records' => $created_rows + $removed_rows,
                    'removed_records' => $removed_rows,
                    'added_records' => $created_rows,
                    'changed_records' => $updated_rows,
                    'num_of_API_calls' => 1,
                    'status' => $status,
                    'error_message' => $error_msg]);

        //get smart alerts for action 'added'
        $added_alert = SmartAlert::where('user_id', $user['id'])
                        ->where('s_object', 'CMS Url Mappings')
                        ->where('action', 'Added')->first();

        if (!empty($added_alert)) {
            if ($created_rows > $added_alert->amount) {

                //notification when CMS Url Mapping added
                $add_notificatoion = Notification::create(['user_id' => $user['id'], 'backup_progress_id' => $backup_progress['id'],
                            'status' => 'CMS Url Mappings Added', 'type' => 'email', 'num_of_records' => $created_rows]);

                //create history
                SmartAlertHistory::create(['user_id' => $user['id'], 'smart_alert_id' => $added_alert->id]);

                //notify user
                //$user->notify(new Backup($notificatoion));
            }
        }

        //get smart alerts for action 'Changed'
        $changed_alert = SmartAlert::where('user_id', $user['id'])
                        ->where('s_object', 'CMS Url Mappings')
                        ->where('action', 'Changed')->first();

        if (!empty($changed_alert)) {
            if ($updated_rows > $changed_alert->amount) {

                //notification when Url Mapping Changed
                $change_notificatoion = Notification::create(['user_id' => $user['id'], 'backup_progress_id' => $backup_progress['id'],
                            'status' => 'CMS Url Mappings Changed', 'type' => 'email', 'num_of_records' => $updated_rows]);

                //create history
                SmartAlertHistory::create(['user_id' => $user['id'], 'smart_alert_id' => $changed_alert->id]);

                //notify user
                //$user->notify(new Backup($notificatoion));
            }
        }

        //get smart alerts for action 'Removed'
        $removed_alert = SmartAlert::where('user_id', $user['id'])
                        ->where('s_object', 'CMS Url Mappings')
                        ->where('action', 'Removed')->first();

        if (!empty($removed_alert)) {
            if ($removed_rows > $removed_alert->amount) {

                //notification when CMS Url Mapping Removed
                $remove_notificatoion = Notification::create(['user_id' => $user['id'], 'backup_progress_id' => $backup_progress['id'],
                            'status' => 'CMS Url Mappings Removed', 'type' => 'email', 'num_of_records' => $removed_rows]);

                //create history
                SmartAlertHistory::create(['user_id' => $user['id'], 'smart_alert_id' => $removed_alert->id]);

                //notify user
                //$user->notify(new Backup($notificatoion));
            }
        }
    }

    public function getCmsUrlMappingsData($headers, $offset, $client, $backup_progress, $user, $hubspot_cms_url_mapping_ids, $status, $error_msg, $created_rows, $updated_rows) {


        $url = $this->api_url . '/url-mappings/v3/url-mappings?limit=100&offset=' . $offset;

        //get all cms Url Mappings data
        $response = $client->request('GET', $url, ['headers' => $headers]);


        $body = $response->getBody();
        $data = json_decode($body, true);

        if (!empty($data['status']) == 'error') {
            $status = $data['status'];
            $error_msg = $data['message'];
            Log::error('CMS Url Mappings data  |||| Error - ' . $error_msg);
        } else {
            $results = $data['objects'];
            $status = 'successed';

            if (!empty($results)) {
                $create_url_mappings = [];
                foreach ($results as $value) {

                    //get all cms url mapping ids
                    $hubspot_cms_url_mapping_ids[] = $value['id'];



                    $check_url_mapping = CmsUrlMapping::where('user_id', $user->id)
                                    ->where('cms_url_mapping_id', $value['id'])
                                    ->where('backup_progress_id', $backup_progress['id'] - 1)->first();

                    $create_url_mappings[] = [
                        'user_id' => $user->id,
                        'backup_progress_id' => $backup_progress['id'],
                        'cms_url_mapping_id' => $value['id'],
                        'portal_id' => isset($value['portalId']) ? $value['portalId'] : 0,
                        'name' => isset($value['name']) ? $value['name'] : "",
                        'route_prefix' => isset($value['routePrefix']) ? $value['routePrefix'] : "",
                        'destination' => isset($value['destination']) ? $value['destination'] : "",
                        'redirect_style' => isset($value['redirectStyle']) ? $value['redirectStyle'] : 0,
                        'content_group_id' => isset($value['contentGroupId']) ? $value['contentGroupId'] : "",
                        'is_only_after_notfound' => isset($value['isOnlyAfterNotFound']) ? $value['isOnlyAfterNotFound'] : 0,
                        'is_regex' => isset($value['isRegex']) ? $value['isRegex'] : 0,
                        'is_match_fullurl' => isset($value['isMatchFullUrl']) ? $value['isMatchFullUrl'] : 0,
                        'is_match_query_string' => isset($value['isMatchQueryString']) ? $value['isMatchQueryString'] : 0,
                        'is_pattern' => isset($value['isPattern']) ? $value['isPattern'] : 0,
                        'is_trailing_slash_optional' => isset($value['isTrailingSlashOptional']) ? $value['isTrailingSlashOptional'] : 0,
                        'is_protocol_agnostic' => isset($value['isProtocolAgnostic']) ? $value['isProtocolAgnostic'] : 0,
                        'precedence' => isset($value['precedence']) ? $value['precedence'] : 0,
                        'deleted_at' => isset($value['deleted_at']) ? $value['deleted_at'] : 0,
                        'created' => isset($value['created']) ? Carbon::createFromTimestampMs($value['created'])->format('Y-m-d H:i:s') : NULL,
                        'updated' => isset($value['updated']) ? Carbon::createFromTimestampMs($value['updated'])->format('Y-m-d H:i:s') : NULL,
                        'created_at' => Carbon::now()->toDateTimeString(),
                        'updated_at' => Carbon::now()->toDateTimeString()
                    ];

                    //check if Url Mapping  is already inserted or not
                    if ($check_url_mapping === null) {
                        
                    } else {
                        $last_updated_date = Carbon::createFromTimestampMs($value['updated'])->format('Y-m-d H:i:s');

                        //check if any data is changed
                        if ($last_updated_date != $check_url_mapping->updated) {

                            $updated_rows +=1;
                        }
                    }
                }
                //insert cms url mappings in bulk
                CmsUrlMapping::insert($create_url_mappings);
                $created_rows += count($create_url_mappings);
            }
        }

        if (isset($data['offset']) && !empty($data['offset'] !== 0)) {

            return $this->getCmsUrlMappingsData($headers, $data['offset'], $client, $backup_progress, $user, $hubspot_cms_url_mapping_ids, $status, $error_msg, $created_rows, $updated_rows);
        } else {

            $data = ['created_rows' => $created_rows, 'updated_rows' => $updated_rows,
                'hubspot_cms_url_mapping_ids' => $hubspot_cms_url_mapping_ids, 'status' => $status, 'error_msg' => $error_msg];

            return $data;
        }
    }

}
