<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\BadResponseException;
use Illuminate\Support\Facades\Artisan;
use DB;
// models
use App\Company;
use App\CompanyAssociatedContact;
use App\CompanyAssociatedDeal;
use App\CompanyAssociatedTicket;
use App\CompanyAssociatedChildCompany;
use App\CompanyAssociatedEngagement;
use App\Engagement;
use App\TaskEngagement;
use App\NoteEngagement;
use App\NoteAttachment;
use App\EmailEngagement;
use App\CallEngagement;
use App\MeetingEngagement;
use App\BackupHistory;
use App\User;
use App\BackupProgress;
use App\Notification;
use Illuminate\Support\Facades\Log;
use App\Notifications\Backup;
use App\SmartAlert;
use App\SmartAlertHistory;
use Carbon\Carbon;

class CompaniesData extends Command { /**
 * The name of the apiUrl
 * @var type 
 */

    protected $api_url;

    /**
     * The name of the apiKey
     * @var type 
     */
    protected $api_key;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'companies:data {userId}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command will get all Companies data';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
        //get api key from env file
        $this->api_url = env('HUBSPOT_URL');
        $this->api_key = env('HUBSPOT_KEY');
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {

        $this->info('Companies Data cron started....' . $this->api_url);
        $status = '';
        $error_msg = '';
        $created_rows = 0;
        $updated_rows = 0;
        $hubspot_company_ids = [];

        $userId = $this->argument('userId');
        $user = User::find($userId);

        $backup_progress = BackupProgress::where('user_id', $user['id'])->where('status', 'Inprogress')->orderByDesc('id')->first();

        try {

            Log::error('Fetching companies data started |||| Start >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> userId - ' . $userId);

            //start time
            $time_start = microtime(true);

            //user token
            $token = $user->accesstoken;

            //using GuzzleHttp client to get request
            $client = new \GuzzleHttp\Client(['http_errors' => false]);
            $headers = [
                'Authorization' => 'Bearer ' . $token,
                'Accept' => 'application/json',
            ];

            $get_data = $this->getCompaniesData($headers, 0, $client, $backup_progress, $user, $hubspot_company_ids, $status, $error_msg, $created_rows, $updated_rows);

            $created_rows = $get_data['created_rows'];
            $updated_rows = $get_data['updated_rows'];
            $hubspot_company_ids = $get_data['hubspot_company_ids'];
            $error_msg = $get_data['error_msg'];
            $status = $get_data['status'];

            //end time
            $time_end = microtime(true);

            //execution time
            $execution_time = ($time_end - $time_start);

            Log::error('time taken for companies data ' . $execution_time);
            Log::error('Fetching companies data completed |||| End <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< userId - ' . $userId);
        } catch (BadResponseException $ex) {
            //if request is invalid
            $response = $ex->getResponse();
            $jsonBody = (string) $response->getBody();
            $this->error($jsonBody);
            $status = $data['status'];
            $error_msg = $data['message'];
            Log::error('Companies data  |||| Error - ' . $jsonBody);
        }

        //get companies from database
        $companies_in_database = Company::where('backup_progress_id', $backup_progress['id'] - 1)->get()->pluck('company_id')->toArray();

        //check if empty result 
        if (count($hubspot_company_ids) == 0) {
            $removed_rows = 0;
        } else {
            $removed_companies = array_diff($companies_in_database, $hubspot_company_ids);
            $removed_rows = count($removed_companies);
        }


        //creating BackupHistory record
        $backup_history = BackupHistory::create([
                    'backup_progress_id' => $backup_progress['id'],
                    'user_id' => $user->id,
                    'object_name' => 'Fetched Companies data',
                    'num_of_records' => $created_rows + $removed_rows,
                    'removed_records' => $removed_rows,
                    'added_records' => $created_rows,
                    'changed_records' => $updated_rows,
                    'num_of_API_calls' => 2,
                    'status' => $status,
                    'error_message' => $error_msg]);

        //get smart alerts for action 'added'
        $added_alert = SmartAlert::where('user_id', $user['id'])
                        ->where('s_object', 'Companies')
                        ->where('action', 'Added')->first();

        if (!empty($added_alert)) {
            if ($created_rows > $added_alert->amount) {

                //notification when company added
                $add_notificatoion = Notification::create(['user_id' => $user['id'], 'backup_progress_id' => $backup_progress['id'],
                            'status' => 'Companies Added', 'type' => 'email', 'num_of_records' => $created_rows]);

                //create history
                SmartAlertHistory::create(['user_id' => $user['id'], 'smart_alert_id' => $added_alert->id]);

                //notify user
                //$user->notify(new Backup($notificatoion));
            }
        }

        //get smart alerts for action 'Changed'
        $changed_alert = SmartAlert::where('user_id', $user['id'])
                        ->where('s_object', 'Companies')
                        ->where('action', 'Changed')->first();

        if (!empty($changed_alert)) {
            if ($updated_rows > $changed_alert->amount) {

                //notification when company Changed
                $change_notificatoion = Notification::create(['user_id' => $user['id'], 'backup_progress_id' => $backup_progress['id'],
                            'status' => 'Companies Changed', 'type' => 'email', 'num_of_records' => $updated_rows]);

                //create history
                SmartAlertHistory::create(['user_id' => $user['id'], 'smart_alert_id' => $changed_alert->id]);

                //notify user
                //$user->notify(new Backup($notificatoion));
            }
        }

        //get smart alerts for action 'Removed'
        $removed_alert = SmartAlert::where('user_id', $user['id'])
                        ->where('s_object', 'Companies')
                        ->where('action', 'Removed')->first();

        if (!empty($removed_alert)) {
            if ($removed_rows > $removed_alert->amount) {

                //notification when company Removed
                $remove_notificatoion = Notification::create(['user_id' => $user['id'], 'backup_progress_id' => $backup_progress['id'],
                            'status' => 'Companies Removed', 'type' => 'email', 'num_of_records' => $removed_rows]);

                //create history
                SmartAlertHistory::create(['user_id' => $user['id'], 'smart_alert_id' => $removed_alert->id]);

                //notify user
                //$user->notify(new Backup($notificatoion));
            }
        }
    }

    public function getCompaniesData($headers, $offset, $client, $backup_progress, $user, $hubspot_company_ids, $status, $error_msg, $created_rows, $updated_rows) {


        $url = $this->api_url . '/companies/v2/companies/recent/modified?limit=100&offset=' . $offset;

        //get all companies data
        $response = $client->request('GET', $url, ['headers' => $headers]);


        $body = $response->getBody();
        $data = json_decode($body, true);


        if (!empty($data['status']) == 'error') {
            $status = $data['status'];
            $error_msg = $data['message'];
            Log::error('companies data  |||| Error - ' . $error_msg);
        } else {
            $results = $data['results'];
            $status = 'successed';
            if (!empty($results)) {

                $create_companies = [];
                foreach ($results as $value) {

                    //get all company ids
                    $hubspot_company_ids[] = $value['companyId'];


                    $company_meta_url = $this->api_url . '/companies/v2/companies/' . $value['companyId'];

                    //get complete details of each company
                    $response_company_meta = $client->request('GET', $company_meta_url, ['headers' => $headers]);
                    $body_company_meta = $response_company_meta->getBody();
                    $data_company_meta = json_decode($body_company_meta, true);

                    if (!empty($data_company_meta['status']) == 'error') {
                        $status = 'warning';
                        $error_msg = $data_company_meta['message'];
                        Log::error('companies data  |||| Warning - ' . $error_msg);
                    } else {
                        $status = 'successed';
                        $check_company = Company::where('user_id', $user->id)
                                        ->where('company_id', $data_company_meta['companyId'])
                                        ->where('backup_progress_id', $backup_progress['id'] - 1)->first();
                        $create_companies[] = [
                            'user_id' => $user->id,
                            'backup_progress_id' => $backup_progress['id'],
                            'company_id' => $data_company_meta['companyId'],
                            'name' => isset($data_company_meta['properties']['name']['value']) ? $data_company_meta['properties']['name']['value'] : '',
                            'website' => isset($data_company_meta['properties']['website']['value']) ? $data_company_meta['properties']['website']['value'] : '',
                            'about_us' => isset($data_company_meta['properties']['about_us']['value']) ? $data_company_meta['properties']['about_us']['value'] : '',
                            'facebook_fans' => isset($data_company_meta['properties']['facebookfans']['value']) ? $data_company_meta['properties']['facebookfans']['value'] : 0,
                            'first_deal_created_date' => isset($data_company_meta['properties']['first_deal_created_date']['value']) ? Carbon::createFromTimestampMs($data_company_meta['properties']['first_deal_created_date']['value'])->format('Y-m-d H:i:s') : NULL,
                            'founded_year' => isset($data_company_meta['properties']['founded_year']['value']) ? $data_company_meta['properties']['founded_year']['value'] : '',
                            'time_first_seen' => !empty($data_company_meta['properties']['hs_analytics_first_timestamp']['value']) ? Carbon::createFromTimestampMs($data_company_meta['properties']['hs_analytics_first_timestamp']['value'])->format('Y-m-d H:i:s') : NULL,
                            'first_touch_converting_campaign' => isset($data_company_meta['properties']['hs_analytics_first_touch_converting_campaign']['value']) ? $data_company_meta['properties']['hs_analytics_first_touch_converting_campaign']['value'] : '',
                            'time_of_first_visit' => !empty($data_company_meta['properties']['hs_analytics_first_visit_timestamp']['value']) ? Carbon::createFromTimestampMs($data_company_meta['properties']['hs_analytics_first_visit_timestamp']['value'])->format('Y-m-d H:i:s') : NULL,
                            'time_last_seen' => !empty($data_company_meta['properties']['hs_analytics_last_timestamp']['value']) ? Carbon::createFromTimestampMs($data_company_meta['properties']['hs_analytics_last_timestamp']['value'])->format('Y-m-d H:i:s') : NULL,
                            'last_touch_converting_campaign' => isset($data_company_meta['properties']['hs_analytics_last_touch_converting_campaign']['value']) ? $data_company_meta['properties']['hs_analytics_last_touch_converting_campaign']['value'] : '',
                            'time_of_last_session' => !empty($data_company_meta['properties']['hs_analytics_last_visit_timestamp']['value']) ? Carbon::createFromTimestampMs($data_company_meta['properties']['hs_analytics_last_visit_timestamp']['value'])->format('Y-m-d H:i:s') : NULL,
                            'number_of_pageviews' => isset($data_company_meta['properties']['hs_analytics_num_page_views']['value']) ? $data_company_meta['properties']['hs_analytics_num_page_views']['value'] : 0,
                            'number_of_visits' => isset($data_company_meta['properties']['hs_analytics_num_visits']['value']) ? $data_company_meta['properties']['hs_analytics_num_visits']['value'] : 0,
                            'original_source_type' => isset($data_company_meta['properties']['hs_analytics_source']['value']) ? $data_company_meta['properties']['hs_analytics_source']['value'] : '',
                            'original_source_data_1' => isset($data_company_meta['properties']['hs_analytics_source_data_1']['value']) ? $data_company_meta['properties']['hs_analytics_source_data_1']['value'] : '',
                            'original_source_data_2' => isset($data_company_meta['properties']['hs_analytics_source_data_2']['value']) ? $data_company_meta['properties']['hs_analytics_source_data_2']['value'] : '',
                            'avatar_filemanager_key' => isset($data_company_meta['properties']['hs_avatar_filemanager_key']['value']) ? $data_company_meta['properties']['hs_avatar_filemanager_key']['value'] : '',
                            'last_modified_date' => !empty($data_company_meta['properties']['hs_lastmodifieddate']['value']) ? Carbon::createFromTimestampMs($data_company_meta['properties']['hs_lastmodifieddate']['value'])->format('Y-m-d H:i:s') : NULL,
                            'target_account' => isset($data_company_meta['properties']['hs_target_account']['value']) ? $data_company_meta['properties']['hs_target_account']['value'] : '',
                            'owner_assigned_date' => !empty($data_company_meta['properties']['hubspot_owner_assigneddate']['value']) ? Carbon::createFromTimestampMs($data_company_meta['properties']['hubspot_owner_assigneddate']['value'])->format('Y-m-d H:i:s') : NULL,
                            'is_public' => isset($data_company_meta['properties']['is_public']['value']) ? $data_company_meta['properties']['is_public']['value'] : '',
                            'associated_contacts' => isset($data_company_meta['properties']['num_associated_contacts']['value']) ? $data_company_meta['properties']['num_associated_contacts']['value'] : 0,
                            'associated_deals' => isset($data_company_meta['properties']['num_associated_deals']['value']) ? $data_company_meta['properties']['num_associated_deals']['value'] : 0,
                            'recent_deal_amount' => isset($data_company_meta['properties']['recent_deal_amount']['value']) ? $data_company_meta['properties']['recent_deal_amount']['value'] : '',
                            'recent_deal_close_date' => !empty($data_company_meta['properties']['recent_deal_close_date']['value']) ? Carbon::createFromTimestampMs($data_company_meta['properties']['recent_deal_close_date']['value'])->format('Y-m-d H:i:s') : NULL,
                            'timezone' => isset($data_company_meta['properties']['timezone']['value']) ? $data_company_meta['properties']['timezone']['value'] : '',
                            'total_money_raised' => isset($data_company_meta['properties']['total_money_raised']['value']) ? $data_company_meta['properties']['total_money_raised']['value'] : 0,
                            'total_revenue' => isset($data_company_meta['properties']['total_revenue']['value']) ? $data_company_meta['properties']['total_revenue']['value'] : '',
                            'twitter_handle' => isset($data_company_meta['properties']['twitterhandle']['value']) ? $data_company_meta['properties']['twitterhandle']['value'] : '',
                            'phone_number' => isset($data_company_meta['properties']['phone']['value']) ? $data_company_meta['properties']['phone']['value'] : '',
                            'twitter_bio' => isset($data_company_meta['properties']['twitterbio']['value']) ? $data_company_meta['properties']['twitterbio']['value'] : '',
                            'twitterfollowers' => isset($data_company_meta['properties']['twitterfollowers']['value']) ? $data_company_meta['properties']['twitterfollowers']['value'] : 0,
                            'address' => isset($data_company_meta['properties']['address']['value']) ? $data_company_meta['properties']['address']['value'] : '',
                            'address2' => isset($data_company_meta['properties']['address2']['value']) ? $data_company_meta['properties']['address2']['value'] : '',
                            'facebook_company_page' => isset($data_company_meta['properties']['facebook_company_page']['value']) ? $data_company_meta['properties']['facebook_company_page']['value'] : '',
                            'city' => isset($data_company_meta['properties']['city']['value']) ? $data_company_meta['properties']['city']['value'] : '',
                            'linkedin_company_page' => isset($data_company_meta['properties']['linkedin_company_page']['value']) ? $data_company_meta['properties']['linkedin_company_page']['value'] : '',
                            'linkedin_bio' => isset($data_company_meta['properties']['linkedinbio']['value']) ? $data_company_meta['properties']['linkedinbio']['value'] : '',
                            'state_or_region' => isset($data_company_meta['properties']['state']['value']) ? $data_company_meta['properties']['state']['value'] : '',
                            'googleplus_page' => isset($data_company_meta['properties']['googleplus_page']['value']) ? $data_company_meta['properties']['googleplus_page']['value'] : '',
                            'last_meeting_booked' => isset($data_company_meta['properties']['engagements_last_meeting_booked']['value']) ? $data_company_meta['properties']['engagements_last_meeting_booked']['value'] : '',
                            'last_meeting_booked_campaign' => isset($data_company_meta['properties']['engagements_last_meeting_booked_campaign']['value']) ? $data_company_meta['properties']['engagements_last_meeting_booked_campaign']['value'] : '',
                            'last_meeting_booked_medium' => isset($data_company_meta['properties']['engagements_last_meeting_booked_medium']['value']) ? $data_company_meta['properties']['engagements_last_meeting_booked_medium']['value'] : '',
                            'last_meeting_booked_source' => isset($data_company_meta['properties']['engagements_last_meeting_booked_source']['value']) ? $data_company_meta['properties']['engagements_last_meeting_booked_source']['value'] : '',
                            'recent_sales_email_replied_date' => !empty($data_company_meta['properties']['hs_sales_email_last_replied']['value']) ? Carbon::createFromTimestampMs($data_company_meta['properties']['hs_sales_email_last_replied']['value'])->format('Y-m-d H:i:s') : NULL,
                            'contact_owner' => isset($data_company_meta['properties']['hubspot_owner_id']['value']) ? $data_company_meta['properties']['hubspot_owner_id']['value'] : 0,
                            'last_contacted' => !empty($data_company_meta['properties']['notes_last_contacted']['value']) ? Carbon::createFromTimestampMs($data_company_meta['properties']['notes_last_contacted']['value'])->format('Y-m-d H:i:s') : NULL,
                            'last_activity_date' => !empty($data_company_meta['properties']['notes_last_updated']['value']) ? Carbon::createFromTimestampMs($data_company_meta['properties']['notes_last_updated']['value'])->format('Y-m-d H:i:s') : NULL,
                            'next_activity_date' => !empty($data_company_meta['properties']['notes_next_activity_date']['value']) ? Carbon::createFromTimestampMs($data_company_meta['properties']['notes_next_activity_date']['value'])->format('Y-m-d H:i:s') : NULL,
                            'number_of_times_contacted' => isset($data_company_meta['properties']['num_contacted_notes']['value']) ? $data_company_meta['properties']['num_contacted_notes']['value'] : 0,
                            'number_of_sales_activities' => isset($data_company_meta['properties']['num_notes']['value']) ? $data_company_meta['properties']['num_notes']['value'] : 0,
                            'zip' => isset($data_company_meta['properties']['zip']['value']) ? $data_company_meta['properties']['zip']['value'] : 0,
                            'country' => isset($data_company_meta['properties']['country']['value']) ? $data_company_meta['properties']['country']['value'] : '',
                            'hubspot_team' => isset($data_company_meta['properties']['hubspot_team_id']['value']) ? $data_company_meta['properties']['hubspot_team_id']['value'] : 0,
                            'all_owner_ids' => isset($data_company_meta['properties']['hs_all_owner_ids']['value']) ? $data_company_meta['properties']['hs_all_owner_ids']['value'] : '',
                            'website_url' => isset($data_company_meta['properties']['website']['value']) ? $data_company_meta['properties']['website']['value'] : '',
                            'company_domain_name' => isset($data_company_meta['properties']['domain']['value']) ? $data_company_meta['properties']['domain']['value'] : '',
                            'all_team_ids' => isset($data_company_meta['properties']['hs_all_team_ids']['value']) ? $data_company_meta['properties']['hs_all_team_ids']['value'] : '',
                            'all_accessible_team_ids' => isset($data_company_meta['properties']['hs_all_accessible_team_ids']['value']) ? $data_company_meta['properties']['hs_all_accessible_team_ids']['value'] : '',
                            'number_of_employees' => isset($data_company_meta['properties']['numberofemployees']['value']) ? $data_company_meta['properties']['numberofemployees']['value'] : 0,
                            'industry' => isset($data_company_meta['properties']['industry']['value']) ? $data_company_meta['properties']['industry']['value'] : '',
                            'annual_revenue' => isset($data_company_meta['properties']['annualrevenue']['value']) ? $data_company_meta['properties']['annualrevenue']['value'] : 0,
                            'lifecycle_stage' => isset($data_company_meta['properties']['lifecyclestage']['value']) ? $data_company_meta['properties']['lifecyclestage']['value'] : '',
                            'lead_status' => isset($data_company_meta['properties']['hs_lead_status']['value']) ? $data_company_meta['properties']['hs_lead_status']['value'] : '',
                            'parent_company' => isset($data_company_meta['properties']['hs_parent_company_id']['value']) ? $data_company_meta['properties']['hs_parent_company_id']['value'] : '',
                            'type' => isset($data_company_meta['properties']['type']['value']) ? $data_company_meta['properties']['type']['value'] : '',
                            'description' => isset($data_company_meta['properties']['description']['value']) ? $data_company_meta['properties']['description']['value'] : '',
                            'number_of_child_companies' => isset($data_company_meta['properties']['hs_num_child_companies']['value']) ? $data_company_meta['properties']['hs_num_child_companies']['value'] : 0,
                            'createdate' => isset($data_company_meta['properties']['createdate']['value']) ? Carbon::createFromTimestampMs($data_company_meta['properties']['createdate']['value'])->format('Y-m-d H:i:s') : NULL,
                            'closedate' => isset($data_company_meta['properties']['closedate']['value']) ? Carbon::createFromTimestampMs($data_company_meta['properties']['closedate']['value'])->format('Y-m-d H:i:s') : NULL,
                            'first_contact_createdate' => isset($data_company_meta['properties']['first_contact_createdate']['value']) ? Carbon::createFromTimestampMs($data_company_meta['properties']['first_contact_createdate']['value'])->format('Y-m-d H:i:s') : NULL,
                            'days_to_close' => isset($data_company_meta['properties']['days_to_close']['value']) ? $data_company_meta['properties']['days_to_close']['value'] : 0,
                            'web_technologies' => isset($data_company_meta['properties']['web_technologies']['value']) ? $data_company_meta['properties']['web_technologies']['value'] : '',
                            'created_at' => Carbon::now()->toDateTimeString(),
                            'updated_at' => Carbon::now()->toDateTimeString()
                        ];
                        //check if company is already inserted or not
                        if ($check_company === null) {
                            
                        } else {

                            $hs_lastmodifieddate = Carbon::createFromTimestampMs($data_company_meta['properties']['hs_lastmodifieddate']['value'])->format('Y-m-d H:i:s');

                            //check if any data is changed
                            if ($hs_lastmodifieddate != $check_company->last_modified_date) {

                                $updated_rows +=1;
                            }
                        }
                    }
                }
                //insert companies in bulk
                $company = Company::insert($create_companies);
                $created_rows += count($create_companies);
            }
        }
        if (isset($data['hasMore']) && !empty($data['hasMore'] == true)) {
            return $this->getCompaniesData($headers, $data['offset'], $client, $backup_progress, $user, $hubspot_company_ids, $status, $error_msg, $created_rows, $updated_rows);
        } else {

            $data = ['created_rows' => $created_rows, 'updated_rows' => $updated_rows,
                'hubspot_company_ids' => $hubspot_company_ids, 'status' => $status, 'error_msg' => $error_msg];
            return $data;
        }
    }

}
