<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\BadResponseException;
use Illuminate\Support\Facades\Artisan;
// models
use App\Deal;
use App\DealAssociatedContact;
use App\DealAssociatedCompany;
use App\DealAssociatedTicket;
use App\DealAssociatedEngagement;
use App\Engagement;
use App\TaskEngagement;
use App\NoteEngagement;
use App\NoteAttachment;
use App\EmailEngagement;
use App\CallEngagement;
use App\MeetingEngagement;
use App\BackupHistory;
use App\User;
use App\BackupProgress;
use App\SmartAlert;
use App\Notification;
use App\SmartAlertHistory;
use Illuminate\Support\Facades\Log;
use Carbon\Carbon;

class DealsData extends Command {

    /**
     * The name of the apiUrl
     * @var type 
     */
    protected $api_url;

    /**
     * The name of the apiKey
     * @var type 
     */
    protected $api_key;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'deals:data {userId}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command will get all Deals data';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
        //get api key from env file
        $this->api_url = env('HUBSPOT_URL');
        $this->api_key = env('HUBSPOT_KEY');
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {

        $this->info('Deals Data cron started....' . $this->api_url);

        $status = '';
        $error_msg = '';
        $created_rows = 0;
        $updated_rows = 0;
        $hubspot_deal_ids = [];

        $userId = $this->argument('userId');
        $user = User::find($userId);

        $backup_progress = BackupProgress::where('user_id', $user['id'])->where('status', 'Inprogress')->orderByDesc('id')->first();


        try {

            Log::error('Fetching deals data started |||| Start >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> userId - ' . $userId);

            //start time
            $time_start = microtime(true);


            //user token
            $token = $user->accesstoken;

            //using GuzzleHttp client to get request
            $client = new \GuzzleHttp\Client(['http_errors' => false]);
            $headers = [
                'Authorization' => 'Bearer ' . $token,
                'Accept' => 'application/json',
            ];

            $get_data = $this->getDealsData($headers, 0, $client, $backup_progress, $user, $hubspot_deal_ids, $status, $error_msg, $created_rows, $updated_rows);

            $created_rows = $get_data['created_rows'];
            $updated_rows = $get_data['updated_rows'];
            $hubspot_deal_ids = $get_data['hubspot_deal_ids'];
            $error_msg = $get_data['error_msg'];
            $status = $get_data['status'];

            //end time
            $time_end = microtime(true);
            //execution time
            $execution_time = ($time_end - $time_start);

            Log::error('time taken for Deals data ' . $execution_time);
            Log::error('Fetching Deals data completed |||| End <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< userId - ' . $userId);
        } catch (BadResponseException $ex) {
            //if request is invalid
            $response = $ex->getResponse();
            $jsonBody = (string) $response->getBody();
            $this->error($jsonBody);

            $data = json_decode($response->getBody(), true);
            $status = $data['status'];
            $error_msg = $data['message'];
            Log::error('Deals data  |||| Error - ' . $jsonBody);
        }

        //get deals from database
        $deals_in_database = Deal::where('backup_progress_id', $backup_progress['id'] - 1)->get()->pluck('deal_id')->toArray();

        //check if empty result 
        if (count($hubspot_deal_ids) == 0) {
            $removed_rows = 0;
        } else {
            $removed_deals = array_diff($deals_in_database, $hubspot_deal_ids);
            $removed_rows = count($removed_deals);
        }

        //creating BackupHistory record
        $backup_history = BackupHistory::create([
                    'backup_progress_id' => $backup_progress['id'],
                    'user_id' => $user->id,
                    'object_name' => 'Fetched Deals data',
                    'num_of_records' => $created_rows + $removed_rows,
                    'removed_records' => $removed_rows,
                    'added_records' => $created_rows,
                    'changed_records' => $updated_rows,
                    'num_of_API_calls' => 2,
                    'status' => $status,
                    'error_message' => $error_msg]);

        //get smart alerts for action 'added'
        $added_alert = SmartAlert::where('user_id', $user['id'])
                        ->where('s_object', 'Deals')
                        ->where('action', 'Added')->first();

        if (!empty($added_alert)) {
            if ($created_rows > $added_alert->amount) {

                //notification when deal added
                $add_notificatoion = Notification::create(['user_id' => $user['id'], 'backup_progress_id' => $backup_progress['id'],
                            'status' => 'Deals Added', 'type' => 'email', 'num_of_records' => $created_rows]);

                //create history
                SmartAlertHistory::create(['user_id' => $user['id'], 'smart_alert_id' => $added_alert->id]);

                //notify user
                //$user->notify(new Backup($notificatoion));
            }
        }

        //get smart alerts for action 'Changed'
        $changed_alert = SmartAlert::where('user_id', $user['id'])
                        ->where('s_object', 'Deals')
                        ->where('action', 'Changed')->first();

        if (!empty($changed_alert)) {
            if ($updated_rows > $changed_alert->amount) {

                //notification when Deal Changed
                $change_notificatoion = Notification::create(['user_id' => $user['id'], 'backup_progress_id' => $backup_progress['id'],
                            'status' => 'Deals Changed', 'type' => 'email', 'num_of_records' => $updated_rows]);

                //create history
                SmartAlertHistory::create(['user_id' => $user['id'], 'smart_alert_id' => $changed_alert->id]);

                //notify user
                //$user->notify(new Backup($notificatoion));
            }
        }

        //get smart alerts for action 'Removed'
        $removed_alert = SmartAlert::where('user_id', $user['id'])
                        ->where('s_object', 'Deals')
                        ->where('action', 'Removed')->first();

        if (!empty($removed_alert)) {
            if ($removed_rows > $removed_alert->amount) {


                //notification when Deal Removed
                $remove_notificatoion = Notification::create(['user_id' => $user['id'], 'backup_progress_id' => $backup_progress['id'],
                            'status' => $removed_data . 'Deals Removed', 'type' => 'email', 'num_of_records' => $removed_rows]);

                //create history
                SmartAlertHistory::create(['user_id' => $user['id'], 'smart_alert_id' => $removed_alert->id]);

                //notify user
                //$user->notify(new Backup($notificatoion));
            }
        }
    }

    public function getDealsData($headers, $offset, $client, $backup_progress, $user, $hubspot_deal_ids, $status, $error_msg, $created_rows, $updated_rows) {


        $url = $this->api_url . '/deals/v1/deal/paged?limit=100&offset=' . $offset;

        //get all deals data
        $response = $client->request('GET', $url, ['headers' => $headers]);


        $body = $response->getBody();
        $data = json_decode($body, true);


        if (!empty($data['status']) == 'error') {
            $status = $data['status'];
            $error_msg = $data['message'];
            Log::error('deals data  |||| Error - ' . $error_msg);
        } else {
            $results = $data['deals'];
            $status = 'successed';

            if (!empty($results)) {

                foreach ($results as $value) {

                    //get all deal ids
                    $hubspot_deal_ids[] = $value['dealId'];


                    $deal_meta_url = $this->api_url . '/deals/v1/deal/' . $value['dealId'];

                    //get complete details of each deal
                    $response_deal_meta = $client->request('GET', $deal_meta_url, ['headers' => $headers]);
                    $body_deal_meta = $response_deal_meta->getBody();
                    $data_deal_meta = json_decode($body_deal_meta, true);

                    if (!empty($data_deal_meta['status']) == 'error') {
                        $status = 'warning';
                        $error_msg = $data_deal_meta['message'];
                        Log::error('deals data  |||| Warning - ' . $error_msg);
                    } else {
                        $check_deal = Deal::where('user_id', $user->id)
                                        ->where('deal_id', $data_deal_meta['dealId'])
                                        ->where('backup_progress_id', $backup_progress['id'] - 1)->first();

                        $deal = Deal::create([
                                    'user_id' => $user->id,
                                    'backup_progress_id' => $backup_progress['id'],
                                    'deal_id' => $data_deal_meta['dealId'],
                                    'deal_name' => isset($data_deal_meta['properties']['dealname']['value']) ? $data_deal_meta['properties']['dealname']['value'] : '',
                                    'amount' => isset($data_deal_meta['properties']['amount']['value']) ? $data_deal_meta['properties']['amount']['value'] : 0,
                                    'amount_in_home_currency' => isset($data_deal_meta['properties']['amount_in_home_currency']['value']) ? $data_deal_meta['properties']['amount_in_home_currency']['value'] : 0,
                                    'days_to_close' => isset($data_deal_meta['properties']['days_to_close']['value']) ? $data_deal_meta['properties']['days_to_close']['value'] : 0,
                                    'original_source_type' => isset($data1['properties']['hs_analytics_source']['value']) ? $data1['properties']['hs_analytics_source']['value'] : '',
                                    'original_source_data_1' => isset($data1['properties']['hs_analytics_source_data_1']['value']) ? $data1['properties']['hs_analytics_source_data_1']['value'] : '',
                                    'original_source_data_2' => isset($data1['properties']['hs_analytics_source_data_2']['value']) ? $data1['properties']['hs_analytics_source_data_2']['value'] : '',
                                    'deal_amount_calculation_preference' => isset($data_deal_meta['properties']['hs_deal_amount_calculation_preference']['value']) ? $data_deal_meta['properties']['hs_deal_amount_calculation_preference']['value'] : '',
                                    'owner_assigned_date' => isset($data_deal_meta['properties']['hubspot_owner_assigneddate']['value']) ? Carbon::createFromTimestampMs($data_deal_meta['properties']['hubspot_owner_assigneddate']['value'])->format('Y-m-d H:i:s') : NULL,
                                    'deal_stage' => isset($data_deal_meta['properties']['dealstage']['value']) ? $data_deal_meta['properties']['dealstage']['value'] : '',
                                    'pipeline' => isset($data_deal_meta['properties']['pipeline']['value']) ? $data_deal_meta['properties']['pipeline']['value'] : '',
                                    'closedate' => isset($data_deal_meta['properties']['closedate']['value']) ? Carbon::createFromTimestampMs($data_deal_meta['properties']['closedate']['value'])->format('Y-m-d H:i:s') : NULL,
                                    'last_meeting_booked' => isset($data1['properties']['engagements_last_meeting_booked']['value']) ? $data1['properties']['engagements_last_meeting_booked']['value'] : '',
                                    'last_meeting_booked_campaign' => isset($data1['properties']['engagements_last_meeting_booked_campaign']['value']) ? $data1['properties']['engagements_last_meeting_booked_campaign']['value'] : '',
                                    'last_meeting_booked_medium' => isset($data1['properties']['engagements_last_meeting_booked_medium']['value']) ? $data1['properties']['engagements_last_meeting_booked_medium']['value'] : '',
                                    'last_meeting_booked_source' => isset($data1['properties']['engagements_last_meeting_booked_source']['value']) ? $data1['properties']['engagements_last_meeting_booked_source']['value'] : '',
                                    'recent_sales_email_replied_date' => !empty($data1['properties']['hs_sales_email_last_replied']['value']) ? Carbon::createFromTimestampMs($data1['properties']['hs_sales_email_last_replied']['value'])->format('Y-m-d H:i:s') : NULL,
                                    'deal_owner' => isset($data1['properties']['hubspot_owner_id']['value']) ? $data1['properties']['hubspot_owner_id']['value'] : 0,
                                    'last_contacted' => !empty($data1['properties']['notes_last_contacted']['value']) ? Carbon::createFromTimestampMs($data1['properties']['notes_last_contacted']['value'])->format('Y-m-d H:i:s') : NULL,
                                    'last_activity_date' => !empty($data1['properties']['notes_last_updated']['value']) ? Carbon::createFromTimestampMs($data1['properties']['notes_last_updated']['value'])->format('Y-m-d H:i:s') : NULL,
                                    'next_activity_date' => !empty($data1['properties']['notes_next_activity_date']['value']) ? Carbon::createFromTimestampMs($data1['properties']['notes_next_activity_date']['value'])->format('Y-m-d H:i:s') : NULL,
                                    'number_of_times_contacted' => isset($data1['properties']['num_contacted_notes']['value']) ? $data1['properties']['num_contacted_notes']['value'] : 0,
                                    'number_of_sales_activities' => isset($data1['properties']['num_notes']['value']) ? $data1['properties']['num_notes']['value'] : 0,
                                    'createdate' => isset($data_deal_meta['properties']['createdate']['value']) ? Carbon::createFromTimestampMs($data_deal_meta['properties']['createdate']['value'])->format('Y-m-d H:i:s') : NULL,
                                    'last_modified_date' => isset($data_deal_meta['properties']['hs_lastmodifieddate']['value']) ? Carbon::createFromTimestampMs($data_deal_meta['properties']['hs_lastmodifieddate']['value'])->format('Y-m-d H:i:s') : NULL,
                                    'hubspot_team' => isset($data1['properties']['hubspot_team_id']['value']) ? $data1['properties']['hubspot_team_id']['value'] : 0,
                                    'deal_type' => isset($data_deal_meta['properties']['dealtype']['value']) ? $data_deal_meta['properties']['dealtype']['value'] : '',
                                    'all_owner_ids' => isset($data1['properties']['hs_all_owner_ids']['value']) ? $data1['properties']['hs_all_owner_ids']['value'] : '',
                                    'deal_description' => isset($data_deal_meta['properties']['description']['value']) ? $data_deal_meta['properties']['description']['value'] : '',
                                    'all_team_ids' => isset($data1['properties']['hs_all_team_ids']['value']) ? $data1['properties']['hs_all_team_ids']['value'] : '',
                                    'all_accessible_team_ids' => isset($data1['properties']['hs_all_accessible_team_ids']['value']) ? $data1['properties']['hs_all_accessible_team_ids']['value'] : '',
                                    'number_of_contacts' => isset($data_deal_meta['properties']['num_associated_contacts']['value']) ? $data_deal_meta['properties']['num_associated_contacts']['value'] : 0,
                                    'closed_lost_reason' => isset($data_deal_meta['properties']['closed_lost_reason']['value']) ? $data_deal_meta['properties']['closed_lost_reason']['value'] : '',
                                    'closed_won_reason' => isset($data_deal_meta['properties']['closed_won_reason']['value']) ? $data_deal_meta['properties']['closed_won_reason']['value'] : '',
                        ]);


                        $deal_db_id = $deal->id;
                        if ($deal_db_id > 0) {
                            $deal_associated_vids = $data_deal_meta['associations']['associatedVids'];
                            $deal_associated_company_ids = $data_deal_meta['associations']['associatedCompanyIds'];
                            $deal_associated_ticket_ids = $data_deal_meta['associations']['associatedTicketIds'];

                            //deal associated contacts
                            if (count($deal_associated_vids) > 0) {
                                foreach ($deal_associated_vids as $vid) {

                                    $vids_check = DealAssociatedContact::where('deal_db_id', $deal_db_id)
                                                    ->where('deal_associated_vid', $vid)->first();

                                    $deal_associated_contact_create = DealAssociatedContact::create([
                                                'deal_db_id' => $deal_db_id,
                                                'deal_associated_vid' => $vid]);
                                }
                            }

                            //deal associated companies
                            if (count($deal_associated_company_ids) > 0) {
                                foreach ($deal_associated_company_ids as $company_id) {
                                    $company_ids_check = DealAssociatedCompany::where('deal_db_id', $deal_db_id)
                                                    ->where('deal_associated_company_id', $company_id)->first();

                                    $deal_associated_company_create = DealAssociatedCompany::create([
                                                'deal_db_id' => $deal_db_id,
                                                'deal_associated_company_id' => $company_id]);
                                }
                            }

                            //for deal associated tickets
                            //check if any tickets exists
                            if (count($deal_associated_ticket_ids) > 0) {
                                foreach ($deal_associated_ticket_ids as $ticket_id) {

                                    $ticket_check = DealAssociatedTicket::where('deal_db_id', $deal_db_id)
                                                    ->where('deal_associated_ticket_id', $ticket_id)->first();

                                    $deal_associated_ticket_create = DealAssociatedTicket::create([
                                                'deal_db_id' => $deal_db_id,
                                                'deal_associated_ticket_id' => $ticket_id]);
                                }
                            }
                        }
                        //if deal is created
                        if ($deal) {
                            $created_rows +=1;
                        }

                        //check if case is already inserted or not
                        if ($check_deal === null) {
                            
                        } else {
                            $hs_lastmodifieddate = Carbon::createFromTimestampMs($data_deal_meta['properties']['hs_lastmodifieddate']['value'])->format('Y-m-d H:i:s');
                            $table = 'update';

                            //check if any data is changed
                            if ($hs_lastmodifieddate != $check_deal->last_modified_date) {


                                $updated_rows +=1;
                            }
                        }
                    }
                }
            }
        }

        if (isset($data['hasMore']) && !empty($data['hasMore'] == true)) {

            return $this->getDealsData($headers, $data['offset'], $client, $backup_progress, $user, $hubspot_deal_ids, $status, $error_msg, $created_rows, $updated_rows);
        } else {

            $data = ['created_rows' => $created_rows, 'updated_rows' => $updated_rows,
                'hubspot_deal_ids' => $hubspot_deal_ids, 'status' => $status, 'error_msg' => $error_msg];

            return $data;
        }
    }

}
