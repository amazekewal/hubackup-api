<?php

namespace App\Console\Commands;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\BadResponseException;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;
// models
use App\Contact;
use App\ContactAssociatedCompany;
use App\ContactAssociatedDeal;
use App\ContactAssociatedTicket;
use App\ContactAssociatedEngagement;
use App\Engagement;
use App\TaskEngagement;
use App\NoteEngagement;
use App\NoteAttachment;
use App\EmailEngagement;
use App\CallEngagement;
use App\MeetingEngagement;
use App\BackupHistory;
use App\User;
use App\BackupProgress;
use App\SmartAlert;
use App\SmartAlertHistory;
use App\Notification;
use Illuminate\Support\Facades\Log;
use Carbon\Carbon;

class ContactsData extends Command {

    /**
     * The name of the apiUrl
     * @var type 
     */
    protected $api_url;

    /**
     * The name of the apiKey
     * @var type 
     */
    protected $api_key;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'contacts:data {userId}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command will get all Contacts data';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
        //get api key from env file
        $this->api_url = env('HUBSPOT_URL');
        $this->api_key = env('HUBSPOT_KEY');
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {

        $this->info('Updating the server....' . $this->api_url);
        $status = '';
        $error_msg = '';
        $created_rows = 0;
        $updated_rows = 0;
        $hubspot_contact_ids = [];

        $userId = $this->argument('userId');
        $user = User::find($userId);

        $backup_progress = BackupProgress::where('user_id', $user['id'])->where('status', 'Inprogress')->orderByDesc('id')->first();


        try {

            Log::error('Fetching contacts data started |||| Start >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> userId - ' . $userId);

            //start time
            $time_start = microtime(true);


            $token = $user->accesstoken;

            $client = new \GuzzleHttp\Client(['http_errors' => false]);
            $headers = [
                'Authorization' => 'Bearer ' . $token,
                'Accept' => 'application/json',
            ];
            $get_data = $this->getContactsData($headers, 0, $client, $backup_progress, $user, $hubspot_contact_ids, $status, $error_msg, $created_rows, $updated_rows);

            $created_rows = $get_data['created_rows'];
            $updated_rows = $get_data['updated_rows'];
            $hubspot_contact_ids = $get_data['hubspot_contact_ids'];
            $error_msg = $get_data['error_msg'];
            $status = $get_data['status'];

            //end time
            $time_end = microtime(true);

            //execution time
            $execution_time = ($time_end - $time_start);

            Log::error('time taken for contacts data ' . $execution_time);
            Log::error('Fetching contacts data completed |||| End <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< userId - ' . $userId);
        } catch (BadResponseException $ex) {
            //if request is invalid
            $response = $ex->getResponse();
            $jsonBody = (string) $response->getBody();
            $this->error($jsonBody);

            $data = json_decode($response->getBody(), true);
            $status = $data['status'];
            $error_msg = $data['message'];
            Log::error('Contacts data  |||| Error - ' . $jsonBody);
        }

        //get contacts from database
        $contacts_in_database = Contact::where('backup_progress_id', $backup_progress['id'] - 1)->get()->pluck('vid')->toArray();

        //check if empty result 
        if (count($hubspot_contact_ids) == 0) {
            $removed_rows = 0;
        } else {
            $removed_contacts = array_diff($contacts_in_database, $hubspot_contact_ids);
            $removed_rows = count($removed_contacts);
        }

        // creating BackupHistory record
        $backup_history = BackupHistory::create([
                    'backup_progress_id' => $backup_progress['id'],
                    'user_id' => $user->id,
                    'object_name' => 'Fetched Contacts data',
                    'num_of_records' => $created_rows + $removed_rows,
                    'removed_records' => $removed_rows,
                    'added_records' => $created_rows,
                    'changed_records' => $updated_rows,
                    'num_of_API_calls' => 2,
                    'status' => $status,
                    'error_message' => $error_msg]);

        //get smart alerts for action 'added'
        $added_alert = SmartAlert::where('user_id', $user['id'])
                        ->where('s_object', 'Contacts')
                        ->where('action', 'Added')->first();

        if (!empty($added_alert)) {
            if ($created_rows > $added_alert->amount) {

                //notification when contact added
                $add_notificatoion = Notification::create(['user_id' => $user['id'], 'backup_progress_id' => $backup_progress['id'],
                            'status' => 'Contacts Added', 'type' => 'email', 'num_of_records' => $created_rows]);

                //create history
                SmartAlertHistory::create(['user_id' => $user['id'], 'smart_alert_id' => $added_alert->id]);

                //notify user
                //$user->notify(new Backup($notificatoion));
            }
        }

        //get smart alerts for action 'Changed'
        $changed_alert = SmartAlert::where('user_id', $user['id'])
                        ->where('s_object', 'Contacts')
                        ->where('action', 'Changed')->first();

        if (!empty($changed_alert)) {
            if ($updated_rows > $changed_alert->amount) {

                //notification when Contacts Changed
                $change_notificatoion = Notification::create(['user_id' => $user['id'], 'backup_progress_id' => $backup_progress['id'],
                            'status' => 'Contacts Changed', 'type' => 'email', 'num_of_records' => $updated_rows]);

                //create history
                SmartAlertHistory::create(['user_id' => $user['id'], 'smart_alert_id' => $changed_alert->id]);

                //notify user
                //$user->notify(new Backup($notificatoion));
            }
        }

        //get smart alerts for action 'Removed'
        $removed_alert = SmartAlert::where('user_id', $user['id'])
                        ->where('s_object', 'Contacts')
                        ->where('action', 'Removed')->first();

        if (!empty($removed_alert)) {
            if ($removed_rows > $removed_alert->amount) {

                //notification when Contact Removed
                $remove_notificatoion = Notification::create(['user_id' => $user['id'], 'backup_progress_id' => $backup_progress['id'],
                            'status' => 'Contacts Removed', 'type' => 'email', 'num_of_records' => $removed_rows]);

                //create history
                SmartAlertHistory::create(['user_id' => $user['id'], 'smart_alert_id' => $removed_alert->id]);

                //notify user
                //$user->notify(new Backup($notificatoion));
            }
        }
    }

    public function getContactsData($headers, $offset, $client, $backup_progress, $user, $hubspot_contact_ids, $status, $error_msg, $created_rows, $updated_rows) {


        $url = $this->api_url . '/contacts/v1/lists/recently_updated/contacts/recent?count=100&vidOffset=' . $offset;

        //get all contacts data
        $response = $client->request('GET', $url, ['headers' => $headers]);

        $body = $response->getBody();
        $data = json_decode($body, true);


        if (!empty($data['status']) == 'error') {
            $status = $data['status'];
            $error_msg = $data['message'];
            Log::error('contacts data  |||| Error - ' . $error_msg);
        } else {
            $results = $data['contacts'];
            $status = 'successed';
            if (!empty($results)) {
                $create_contacts = [];
                foreach ($results as $value) {

                    //get all contact ids
                    $hubspot_contact_ids[] = $value['vid'];


                    $contact_meta_url = $this->api_url . '/contacts/v1/contact/vid/' . $value['vid'] . '/profile';
                    $response1 = $client->request('GET', $contact_meta_url, ['headers' => $headers]);

                    $body1 = $response1->getBody();
                    $data1 = json_decode($body1, true);

                    if (!empty($data1['status']) == 'error') {
                        $status = 'warning';
                        $error_msg = $data1['message'];
                        Log::error('contacts data  |||| Warning - ' . $error_msg);
                    } else {
                        $status = 'successed';

                        $check_contact = Contact::where('user_id', $user->id)
                                        ->where('vid', $data1['vid'])
                                        ->where('backup_progress_id', $backup_progress['id'] - 1)->first();

                        $create_contacts[] = [
                            'user_id' => $user->id,
                            'vid' => $data1['vid'],
                            'backup_progress_id' => $backup_progress['id'],
                            'company_size' => isset($data1['properties']['company_size']['value']) ? $data1['properties']['company_size']['value'] : '',
                            'date_of_birth' => isset($data1['properties']['date_of_birth']['value']) ? $data1['properties']['date_of_birth']['value'] : '',
                            'days_to_close' => isset($data1['properties']['days_to_close']['value']) ? $data1['properties']['days_to_close']['value'] : 0,
                            'degree' => isset($data1['properties']['degree']['value']) ? $data1['properties']['degree']['value'] : '',
                            'field_of_study' => isset($data1['properties']['field_of_study']['value']) ? $data1['properties']['field_of_study']['value'] : '',
                            'first_conversion_date' => !empty($data1['properties']['first_conversion_date']['value']) ? Carbon::createFromTimestampMs($data1['properties']['first_conversion_date']['value'])->format('Y-m-d H:i:s') : NULL,
                            'first_conversion' => isset($data1['properties']['first_conversion_event_name']['value']) ? $data1['properties']['first_conversion_event_name']['value'] : '',
                            'first_deal_created_date' => !empty($data1['properties']['first_deal_created_date']['value']) ? Carbon::createFromTimestampMs($data1['properties']['first_deal_created_date']['value'])->format('Y-m-d H:i:s') : NULL,
                            'gender' => isset($data1['properties']['gender']['value']) ? $data1['properties']['gender']['value'] : '',
                            'graduation_date' => isset($data1['properties']['graduation_date']['value']) ? $data1['properties']['graduation_date']['value'] : '',
                            'additional_email_addresses' => isset($data1['properties']['hs_additional_emails']['value']) ? $data1['properties']['hs_additional_emails']['value'] : '',
                            'all_vids_for_a_contact' => isset($data1['properties']['hs_all_contact_vids']['value']) ? $data1['properties']['hs_all_contact_vids']['value'] : '',
                            'first_touch_converting_campaign' => isset($data1['properties']['hs_analytics_first_touch_converting_campaign']['value']) ? $data1['properties']['hs_analytics_first_touch_converting_campaign']['value'] : '',
                            'last_touch_converting_campaign' => isset($data1['properties']['hs_analytics_last_touch_converting_campaign']['value']) ? $data1['properties']['hs_analytics_last_touch_converting_campaign']['value'] : '',
                            'avatar_filemanager_key' => isset($data1['properties']['hs_avatar_filemanager_key']['value']) ? $data1['properties']['hs_avatar_filemanager_key']['value'] : '',
                            'all_form_submissions_for_a_contact' => isset($data1['properties']['hs_calculated_form_submissions']['value']) ? $data1['properties']['hs_calculated_form_submissions']['value'] : '',
                            'merged_vids_with_timestamps_of_a_contact' => isset($data1['properties']['hs_calculated_merged_vids']['value']) ? $data1['properties']['hs_calculated_merged_vids']['value'] : '',
                            'calculated_mobile_number_in_international_format' => isset($data1['properties']['hs_calculated_mobile_number']['value']) ? $data1['properties']['hs_calculated_mobile_number']['value'] : '',
                            'calculated_phone_number_in_international_format' => isset($data1['properties']['hs_calculated_phone_number']['value']) ? $data1['properties']['hs_calculated_phone_number']['value'] : '',
                            'calculated_phone_number_area_code' => isset($data1['properties']['hs_calculated_phone_number_area_code']['value']) ? $data1['properties']['hs_calculated_phone_number_area_code']['value'] : '',
                            'calculated_phone_number_country_code' => isset($data1['properties']['hs_calculated_phone_number_country_code']['value']) ? $data1['properties']['hs_calculated_phone_number_country_code']['value'] : '',
                            'calculated_phone_number_region_code' => isset($data1['properties']['hs_calculated_phone_number_region_code']['value']) ? $data1['properties']['hs_calculated_phone_number_region_code']['value'] : '',
                            'email_confirmed' => isset($data1['properties']['hs_content_membership_email_confirmed']['value']) ? $data1['properties']['hs_content_membership_email_confirmed']['value'] : '',
                            'membership_notes' => isset($data1['properties']['hs_content_membership_notes']['value']) ? $data1['properties']['hs_content_membership_notes']['value'] : '',
                            'registered_at' => !empty($data1['properties']['hs_content_membership_registered_at']['value']) ? Carbon::createFromTimestampMs($data1['properties']['hs_content_membership_registered_at']['value'])->format('Y-m-d H:i:s') : NULL,
                            'domain_to_which_registration_email_was_sent' => isset($data1['properties']['hs_content_membership_registration_domain_sent_to']['value']) ? $data1['properties']['hs_content_membership_registration_domain_sent_to']['value'] : '',
                            'time_registration_email_was_sent' => isset($data1['properties']['hs_content_membership_registration_email_sent_at']['value']) ? $data1['properties']['hs_content_membership_registration_email_sent_at']['value'] : '',
                            'status' => isset($data1['properties']['hs_content_membership_status']['value']) ? $data1['properties']['hs_content_membership_status']['value'] : '',
                            'conversations_visitor_email' => isset($data1['properties']['hs_conversations_visitor_email']['value']) ? $data1['properties']['hs_conversations_visitor_email']['value'] : '',
                            'created_by_conversations' => isset($data1['properties']['hs_created_by_conversations']['value']) ? $data1['properties']['hs_created_by_conversations']['value'] : '',
                            'recent_document_revisit_date' => !empty($data1['properties']['hs_document_last_revisited']['value']) ? Carbon::createFromTimestampMs($data1['properties']['hs_document_last_revisited']['value'])->format('Y-m-d H:i:s') : NULL,
                            'email_domain' => isset($data1['properties']['hs_email_domain']['value']) ? $data1['properties']['hs_email_domain']['value'] : 0,
                            'email_address_quarantined' => isset($data1['properties']['hs_email_quarantined']['value']) ? $data1['properties']['hs_email_quarantined']['value'] : '',
                            'sends_since_last_engagement' => isset($data1['properties']['hs_email_sends_since_last_engagement']['value']) ? $data1['properties']['hs_email_sends_since_last_engagement']['value'] : NULL,
                            'marketing_email_confirmation_status' => isset($data1['properties']['hs_emailconfirmationstatus']['value']) ? $data1['properties']['hs_emailconfirmationstatus']['value'] : '',
                            'clicked_facebook_ad' => isset($data1['properties']['hs_facebook_ad_clicked']['value']) ? $data1['properties']['hs_facebook_ad_clicked']['value'] : '',
                            'facebook_id' => isset($data1['properties']['hs_facebookid']['value']) ? $data1['properties']['hs_facebookid']['value'] : '',
                            'google_ad_click_id' => isset($data1['properties']['hs_google_click_id']['value']) ? $data1['properties']['hs_google_click_id']['value'] : '',
                            'googleplus_id' => isset($data1['properties']['hs_googleplusid']['value']) ? $data1['properties']['hs_googleplusid']['value'] : '',
                            'ip_timezone' => isset($data1['properties']['hs_ip_timezone']['value']) ? $data1['properties']['hs_ip_timezone']['value'] : '',
                            'is_a_contact' => isset($data1['properties']['hs_is_contact']['value']) ? $data1['properties']['hs_is_contact']['value'] : '',
                            'lead_status' => isset($data1['properties']['hs_lead_status']['value']) ? $data1['properties']['hs_lead_status']['value'] : '',
                            'legal_basis_for_processing_contacts_data' => isset($data1['properties']['hs_legal_basis']['value']) ? $data1['properties']['hs_legal_basis']['value'] : '',
                            'linkedin_id' => isset($data1['properties']['hs_linkedinid']['value']) ? $data1['properties']['hs_linkedinid']['value'] : '',
                            'recent_sales_email_clicked_date' => !empty($data1['properties']['hs_sales_email_last_clicked']['value']) ? Carbon::createFromTimestampMs($data1['properties']['hs_sales_email_last_clicked']['value'])->format('Y-m-d H:i:s') : NULL,
                            'recent_sales_email_opened_date' => !empty($data1['properties']['hs_sales_email_last_opened']['value']) ? Carbon::createFromTimestampMs($data1['properties']['hs_sales_email_last_opened']['value'])->format('Y-m-d H:i:s') : NULL,
                            'calculated_mobile_number_with_country_code' => isset($data1['properties']['hs_searchable_calculated_international_mobile_number']['value']) ? $data1['properties']['hs_searchable_calculated_international_mobile_number']['value'] : '',
                            'calculated_phone_number_with_country_code' => isset($data1['properties']['hs_searchable_calculated_international_phone_number']['value']) ? $data1['properties']['hs_searchable_calculated_international_phone_number']['value'] : '',
                            'calculated_mobile_number_without_country_code' => isset($data1['properties']['hs_searchable_calculated_mobile_number']['value']) ? $data1['properties']['hs_searchable_calculated_mobile_number']['value'] : '',
                            'calculated_phone_number_without_country_code' => isset($data1['properties']['hs_searchable_calculated_phone_number']['value']) ? $data1['properties']['hs_searchable_calculated_phone_number']['value'] : '',
                            'currently_in_sequence' => isset($data1['properties']['hs_sequences_is_enrolled']['value']) ? $data1['properties']['hs_sequences_is_enrolled']['value'] : '',
                            'twitter_id' => isset($data1['properties']['hs_twitterid']['value']) ? $data1['properties']['hs_twitterid']['value'] : '',
                            'owner_assigned_date' => !empty($data1['properties']['hubspot_owner_assigneddate']['value']) ? Carbon::createFromTimestampMs($data1['properties']['hubspot_owner_assigneddate']['value'])->format('Y-m-d H:i:s') : NULL,
                            'ip_city' => isset($data1['properties']['ip_city']['value']) ? $data1['properties']['ip_city']['value'] : '',
                            'ip_country' => isset($data1['properties']['ip_country']['value']) ? $data1['properties']['ip_country']['value'] : '',
                            'ip_country_code' => isset($data1['properties']['ip_country_code']['value']) ? $data1['properties']['ip_country_code']['value'] : '',
                            'ip_latitude_and_longitude' => isset($data1['properties']['ip_latlon']['value']) ? $data1['properties']['ip_latlon']['value'] : '',
                            'ip_state_or_region' => isset($data1['properties']['ip_state']['value']) ? $data1['properties']['ip_state']['value'] : '',
                            'ip_state_code_or_region_code' => isset($data1['properties']['ip_state_code']['value']) ? $data1['properties']['ip_state_code']['value'] : '',
                            'ip_zipcode' => isset($data1['properties']['ip_zipcode']['value']) ? $data1['properties']['ip_zipcode']['value'] : '',
                            'job_function' => isset($data1['properties']['job_function']['value']) ? $data1['properties']['job_function']['value'] : '',
                            'last_modified_date' => !empty($data1['properties']['lastmodifieddate']['value']) ? Carbon::createFromTimestampMs($data1['properties']['lastmodifieddate']['value'])->format('Y-m-d H:i:s') : NULL,
                            'marital_status' => isset($data1['properties']['marital_status']['value']) ? $data1['properties']['marital_status']['value'] : '',
                            'military_status' => isset($data1['properties']['military_status']['value']) ? $data1['properties']['military_status']['value'] : '',
                            'associated_deals' => isset($data1['properties']['num_associated_deals']['value']) ? $data1['properties']['num_associated_deals']['value'] : 0,
                            'number_of_form_submissions' => isset($data1['properties']['num_conversion_events']['value']) ? $data1['properties']['num_conversion_events']['value'] : 0,
                            'number_of_unique_forms_submitted' => isset($data1['properties']['num_unique_conversion_events']['value']) ? $data1['properties']['num_unique_conversion_events']['value'] : 0,
                            'recent_conversion_date' => !empty($data1['properties']['recent_conversion_date']['value']) ? Carbon::createFromTimestampMs($data1['properties']['recent_conversion_date']['value'])->format('Y-m-d H:i:s') : NULL,
                            'recent_conversion' => isset($data1['properties']['recent_conversion_event_name']['value']) ? $data1['properties']['recent_conversion_event_name']['value'] : '',
                            'recent_deal_amount' => isset($data1['properties']['recent_deal_amount']['value']) ? $data1['properties']['recent_deal_amount']['value'] : '',
                            'recent_deal_close_date' => !empty($data1['properties']['recent_deal_close_date']['value']) ? Carbon::createFromTimestampMs($data1['properties']['recent_deal_close_date']['value'])->format('Y-m-d H:i:s') : NULL,
                            'relationship_status' => isset($data1['properties']['relationship_status']['value']) ? $data1['properties']['relationship_status']['value'] : '',
                            'seniority' => isset($data1['properties']['seniority']['value']) ? $data1['properties']['seniority']['value'] : '',
                            'school' => isset($data1['properties']['school']['value']) ? $data1['properties']['school']['value'] : '',
                            'start_date' => !empty($data1['properties']['start_date']['value']) ? Carbon::parse($data1['properties']['start_date']['value'])->format('Y-m-d H:i:s') : NULL,
                            'total_revenue' => isset($data1['properties']['total_revenue']['value']) ? $data1['properties']['total_revenue']['value'] : '',
                            'work_email' => isset($data1['properties']['work_email']['value']) ? $data1['properties']['work_email']['value'] : '',
                            'first_name' => isset($data1['properties']['firstname']['value']) ? $data1['properties']['firstname']['value'] : '',
                            'first_page_seen' => isset($data1['properties']['hs_analytics_first_url']['value']) ? $data1['properties']['hs_analytics_first_url']['value'] : '',
                            'marketing_emails_delivered' => isset($data1['properties']['hubspot_owner_id']['value']) ? $data1['properties']['hubspot_owner_id']['value'] : 0,
                            'opted_out_of_email:_one_to_one' => isset($data1['properties']['hs_email_optout_5688626']['value']) ? $data1['properties']['hs_email_optout_5688626']['value'] : '',
                            'twitter_username' => isset($data1['properties']['twitterhandle']['value']) ? $data1['properties']['twitterhandle']['value'] : '',
                            'follower_count' => isset($data1['properties']['followercount']['value']) ? $data1['properties']['followercount']['value'] : '',
                            'last_page_seen' => isset($data1['properties']['hs_analytics_last_url']['value']) ? $data1['properties']['hs_analytics_last_url']['value'] : '',
                            'marketing_emails_opened' => isset($data1['properties']['hs_email_open']['value']) ? $data1['properties']['hs_email_open']['value'] : NULL,
                            'last_name' => isset($data1['properties']['lastname']['value']) ? $data1['properties']['lastname']['value'] : '',
                            'number_of_pageviews' => isset($data1['properties']['hs_analytics_num_page_views']['value']) ? $data1['properties']['hs_analytics_num_page_views']['value'] : 0,
                            'marketing_emails_clicked' => isset($data1['properties']['hs_email_click']['value']) ? $data1['properties']['hs_email_click']['value'] : NULL,
                            'salutation' => isset($data1['properties']['salutation']['value']) ? $data1['properties']['salutation']['value'] : '',
                            'twitter_profile_photo' => isset($data1['properties']['twitterprofilephoto']['value']) ? $data1['properties']['twitterprofilephoto']['value'] : '',
                            'email' => isset($data1['properties']['email']['value']) ? $data1['properties']['email']['value'] : '',
                            'number_of_visits' => isset($data1['properties']['hs_analytics_num_visits']['value']) ? $data1['properties']['hs_analytics_num_visits']['value'] : 0,
                            'marketing_emails_bounced' => isset($data1['properties']['hs_email_bounce']['value']) ? $data1['properties']['hs_email_bounce']['value'] : NULL,
                            'persona' => isset($data1['properties']['hs_persona']['value']) ? $data1['properties']['hs_persona']['value'] : '',
                            'number_of_event_completions' => isset($data1['properties']['hs_analytics_num_event_completions']['value']) ? $data1['properties']['hs_analytics_num_event_completions']['value'] : 0,
                            'unsubscribed_from_all_email' => isset($data1['properties']['hs_email_optout']['value']) ? $data1['properties']['hs_email_optout']['value'] : '',
                            'mobile_phone_number' => isset($data1['properties']['mobilephone']['value']) ? $data1['properties']['mobilephone']['value'] : '',
                            'phone_number' => isset($data1['properties']['phone']['value']) ? $data1['properties']['phone']['value'] : '',
                            'fax_number' => isset($data1['properties']['fax']['value']) ? $data1['properties']['fax']['value'] : '',
                            'time_first_seen' => !empty($data1['properties']['hs_analytics_first_timestamp']['value']) ? Carbon::createFromTimestampMs($data1['properties']['hs_analytics_first_timestamp']['value'])->format('Y-m-d H:i:s') : NULL,
                            'last_marketing_email_name' => isset($data1['properties']['hs_email_last_email_name']['value']) ? $data1['properties']['hs_email_last_email_name']['value'] : '',
                            'last_marketing_email_send_date' => !empty($data1['properties']['hs_email_last_send_date']['value']) ? Carbon::createFromTimestampMs($data1['properties']['hs_email_last_send_date']['value'])->format('Y-m-d H:i:s') : NULL,
                            'address' => isset($data1['properties']['address']['value']) ? $data1['properties']['address']['value'] : '',
                            'last_meeting_booked' => isset($data1['properties']['engagements_last_meeting_booked']['value']) ? $data1['properties']['engagements_last_meeting_booked']['value'] : '',
                            'last_meeting_booked_campaign' => isset($data1['properties']['engagements_last_meeting_booked_campaign']['value']) ? $data1['properties']['engagements_last_meeting_booked_campaign']['value'] : '',
                            'last_meeting_booked_medium' => isset($data1['properties']['engagements_last_meeting_booked_medium']['value']) ? $data1['properties']['engagements_last_meeting_booked_medium']['value'] : '',
                            'last_meeting_booked_source' => isset($data1['properties']['engagements_last_meeting_booked_source']['value']) ? $data1['properties']['engagements_last_meeting_booked_source']['value'] : '',
                            'last_marketing_email_open_date' => !empty($data1['properties']['hs_email_last_open_date']['value']) ? Carbon::createFromTimestampMs($data1['properties']['hs_email_last_open_date']['value'])->format('Y-m-d H:i:s') : NULL,
                            'recent_sales_email_replied_date' => !empty($data1['properties']['hs_sales_email_last_replied']['value']) ? Carbon::createFromTimestampMs($data1['properties']['hs_sales_email_last_replied']['value'])->format('Y-m-d H:i:s') : NULL,
                            'contact_owner' => isset($data1['properties']['hubspot_owner_id']['value']) ? $data1['properties']['hubspot_owner_id']['value'] : 0,
                            'last_contacted' => !empty($data1['properties']['notes_last_contacted']['value']) ? Carbon::createFromTimestampMs($data1['properties']['notes_last_contacted']['value'])->format('Y-m-d H:i:s') : NULL,
                            'last_activity_date' => !empty($data1['properties']['notes_last_updated']['value']) ? Carbon::createFromTimestampMs($data1['properties']['notes_last_updated']['value'])->format('Y-m-d H:i:s') : NULL,
                            'next_activity_date' => !empty($data1['properties']['notes_next_activity_date']['value']) ? Carbon::createFromTimestampMs($data1['properties']['notes_next_activity_date']['value'])->format('Y-m-d H:i:s') : NULL,
                            'number_of_times_contacted' => isset($data1['properties']['num_contacted_notes']['value']) ? $data1['properties']['num_contacted_notes']['value'] : 0,
                            'number_of_sales_activities' => isset($data1['properties']['num_notes']['value']) ? $data1['properties']['num_notes']['value'] : 0,
                            'surveyMonkey_event_last_updated' => !empty($data1['properties']['surveymonkeyeventlastupdated']['value']) ? Carbon::createFromTimestampMs($data1['properties']['surveymonkeyeventlastupdated']['value'])->format('Y-m-d H:i:s') : NULL,
                            'webinar_event_last_updated' => !empty($data1['properties']['webinareventlastupdated']['value']) ? Carbon::createFromTimestampMs($data1['properties']['webinareventlastupdated']['value'])->format('Y-m-d H:i:s') : NULL,
                            'city' => isset($data1['properties']['city']['value']) ? $data1['properties']['city']['value'] : '',
                            'time_last_seen' => !empty($data1['properties']['hs_analytics_last_timestamp']['value']) ? Carbon::createFromTimestampMs($data1['properties']['hs_analytics_last_timestamp']['value'])->format('Y-m-d H:i:s') : NULL,
                            'last_marketing_email_click_date' => !empty($data1['properties']['hs_email_last_click_date']['value']) ? Carbon::createFromTimestampMs($data1['properties']['hs_email_last_click_date']['value'])->format('Y-m-d H:i:s') : NULL,
                            'hubspot_team' => isset($data1['properties']['hubspot_team_id']['value']) ? $data1['properties']['hubspot_team_id']['value'] : 0,
                            'linkedin_bio' => isset($data1['properties']['linkedinbio']['value']) ? $data1['properties']['linkedinbio']['value'] : '',
                            'twitter_bio' => isset($data1['properties']['twitterbio']['value']) ? $data1['properties']['twitterbio']['value'] : '',
                            'all_owner_ids' => isset($data1['properties']['hs_all_owner_ids']['value']) ? $data1['properties']['hs_all_owner_ids']['value'] : '',
                            'time_of_last_visit' => !empty($data1['properties']['hs_analytics_last_visit_timestamp']['value']) ? Carbon::createFromTimestampMs($data1['properties']['hs_analytics_last_visit_timestamp']['value'])->format('Y-m-d H:i:s') : NULL,
                            'first_marketing_email_send_date' => !empty($data1['properties']['hs_email_first_send_date']['value']) ? Carbon::createFromTimestampMs($data1['properties']['hs_email_first_send_date']['value'])->format('Y-m-d H:i:s') : NULL,
                            'state_or_region' => isset($data1['properties']['state']['value']) ? $data1['properties']['state']['value'] : '',
                            'all_team_ids' => isset($data1['properties']['hs_all_team_ids']['value']) ? $data1['properties']['hs_all_team_ids']['value'] : '',
                            'original_source' => isset($data1['properties']['hs_analytics_source']['value']) ? $data1['properties']['hs_analytics_source']['value'] : '',
                            'first_marketing_email_open_date' => !empty($data1['properties']['hs_email_first_open_date']['value']) ? Carbon::createFromTimestampMs($data1['properties']['hs_email_first_open_date']['value'])->format('Y-m-d H:i:s') : NULL,
                            'zip' => isset($data1['properties']['zip']['value']) ? $data1['properties']['zip']['value'] : 0,
                            'country' => isset($data1['properties']['country']['value']) ? $data1['properties']['country']['value'] : '',
                            'all_accessible_team_ids' => isset($data1['properties']['hs_all_accessible_team_ids']['value']) ? $data1['properties']['hs_all_accessible_team_ids']['value'] : '',
                            'original_source_drill-down_1' => isset($data1['properties']['hs_analytics_source_data_1']['value']) ? $data1['properties']['hs_analytics_source_data_1']['value'] : '',
                            'first_marketing_email_click_date' => !empty($data1['properties']['hs_email_first_click_date']['value']) ? Carbon::createFromTimestampMs($data1['properties']['hs_email_first_click_date']['value'])->format('Y-m-d H:i:s') : NULL,
                            'linkedIn_connections' => isset($data1['properties']['linkedinconnections']['value']) ? $data1['properties']['linkedinconnections']['value'] : 0,
                            'original_source_drill-down_2' => isset($data1['properties']['hs_analytics_source_data_2']['value']) ? $data1['properties']['hs_analytics_source_data_2']['value'] : '',
                            'is_globally_ineligible' => isset($data1['properties']['hs_email_is_ineligible']['value']) ? $data1['properties']['hs_email_is_ineligible']['value'] : NULL,
                            'preferred_language' => isset($data1['properties']['hs_language']['value']) ? $data1['properties']['hs_language']['value'] : '',
                            'klout_score' => isset($data1['properties']['kloutscoregeneral']['value']) ? $data1['properties']['kloutscoregeneral']['value'] : '',
                            'first_referring_site' => isset($data1['properties']['hs_analytics_first_referrer']['value']) ? $data1['properties']['hs_analytics_first_referrer']['value'] : '',
                            'jobtitle' => isset($data1['properties']['jobtitle']['value']) ? $data1['properties']['jobtitle']['value'] : '',
                            'photo' => isset($data1['properties']['photo']['value']) ? $data1['properties']['photo']['value'] : '',
                            'last_referring_site' => isset($data1['properties']['hs_analytics_last_referrer']['value']) ? $data1['properties']['hs_analytics_last_referrer']['value'] : '',
                            'message' => isset($data1['properties']['message']['value']) ? $data1['properties']['message']['value'] : '',
                            'closedate' => !empty($data1['properties']['closedate']['value']) ? Carbon::createFromTimestampMs($data1['properties']['closedate']['value'])->format('Y-m-d H:i:s') : NULL,
                            'average_page_views' => isset($data1['properties']['hs_analytics_average_page_views']['value']) ? $data1['properties']['hs_analytics_average_page_views']['value'] : 0,
                            'event_revenue' => isset($data1['properties']['hs_analytics_revenue']['value']) ? $data1['properties']['hs_analytics_revenue']['value'] : '',
                            'became_a_lead_date' => !empty($data1['properties']['hs_lifecyclestage_lead_date']['value']) ? Carbon::createFromTimestampMs($data1['properties']['hs_lifecyclestage_lead_date']['value'])->format('Y-m-d H:i:s') : NULL,
                            'became_a_marketing_qualified_lead_date' => !empty($data1['properties']['hs_lifecyclestage_marketingqualifiedlead_date']['value']) ? Carbon::createFromTimestampMs($data1['properties']['hs_lifecyclestage_marketingqualifiedlead_date']['value'])->format('Y-m-d H:i:s') : NULL,
                            'became_an_opportunity_date' => !empty($data1['properties']['hs_lifecyclestage_opportunity_date']['value']) ? Carbon::createFromTimestampMs($data1['properties']['hs_lifecyclestage_opportunity_date']['value'])->format('Y-m-d H:i:s') : NULL,
                            'lifecycle_stage' => isset($data1['properties']['lifecyclestage']['value']) ? $data1['properties']['lifecyclestage']['value'] : '',
                            'became_a_sales_qualified_lead_date' => !empty($data1['properties']['hs_lifecyclestage_salesqualifiedlead_date']['value']) ? Carbon::createFromTimestampMs($data1['properties']['hs_lifecyclestage_salesqualifiedlead_date']['value'])->format('Y-m-d H:i:s') : NULL,
                            'createdate' => !empty($data1['properties']['createdate']['value']) ? Carbon::createFromTimestampMs($data1['properties']['createdate']['value'])->format('Y-m-d H:i:s') : NULL,
                            'became_an_evangelist_date' => !empty($data1['properties']['hs_lifecyclestage_evangelist_date']['value']) ? Carbon::createFromTimestampMs($data1['properties']['hs_lifecyclestage_evangelist_date']['value'])->format('Y-m-d H:i:s') : NULL,
                            'became_a_customer_date' => !empty($data1['properties']['hs_lifecyclestage_customer_date']['value']) ? Carbon::createFromTimestampMs($data1['properties']['hs_lifecyclestage_customer_date']['value'])->format('Y-m-d H:i:s') : NULL,
                            'hubspotscore' => isset($data1['properties']['hubspotscore']['value']) ? $data1['properties']['hubspotscore']['value'] : '',
                            'company_name' => isset($data1['properties']['company']['value']) ? $data1['properties']['company']['value'] : '',
                            'became_a_subscriber_date' => !empty($data1['properties']['hs_lifecyclestage_subscriber_date']['value']) ? Carbon::createFromTimestampMs($data1['properties']['hs_lifecyclestage_subscriber_date']['value'])->format('Y-m-d H:i:s') : NULL,
                            'became_an_other_lifecycle_date' => !empty($data1['properties']['hs_lifecyclestage_other_date']['value']) ? Carbon::createFromTimestampMs($data1['properties']['hs_lifecyclestage_other_date']['value'])->format('Y-m-d H:i:s') : NULL,
                            'website_url' => isset($data1['properties']['website']['value']) ? $data1['properties']['website']['value'] : '',
                            'number_of_employees' => isset($data1['properties']['numemployees']['value']) ? $data1['properties']['numemployees']['value'] : '',
                            'annual_revenue' => isset($data1['properties']['annualrevenue']['value']) ? $data1['properties']['annualrevenue']['value'] : 0,
                            'industry' => isset($data1['properties']['industry']['value']) ? $data1['properties']['industry']['value'] : '',
                            'associated_company_id' => isset($data1['properties']['associatedcompanyid']['value']) ? $data1['properties']['associatedcompanyid']['value'] : NULL,
                            'associated_company_last_updated' => !empty($data1['properties']['associatedcompanylastupdated']['value']) ? Carbon::createFromTimestampMs($data1['properties']['associatedcompanylastupdated']['value'])->format('Y-m-d H:i:s') : NULL,
                            'number_of_broadcast_clicks' => isset($data1['properties']['hs_social_num_broadcast_clicks']['value']) ? $data1['properties']['hs_social_num_broadcast_clicks']['value'] : 0,
                            'email_recipient_fatigue_recovery_time' => isset($data1['hs_email_recipient_fatigue_recovery_time']['degree']['value']) ? $data1['properties']['hs_email_recipient_fatigue_recovery_time']['value'] : '',
                            'linkedin_clicks' => isset($data1['properties']['linkedin_clicks']['value']) ? $data1['properties']['linkedin_clicks']['value'] : 0,
                            'google_plus_clicks' => isset($data1['properties']['google_plus_clicks']['value']) ? $data1['properties']['google_plus_clicks']['value'] : 0,
                            'facebook_clicks' => isset($data1['properties']['facebook_clicks']['value']) ? $data1['properties']['facebook_clicks']['value'] : 0,
                            'twitter_clicks' => isset($data1['properties']['twitter_clicks']['value']) ? $data1['properties']['twitter_clicks']['value'] : 0,
                            'created_at' => Carbon::now()->toDateTimeString(),
                            'updated_at' => Carbon::now()->toDateTimeString()
                        ];

                        //check if contact is already inserted or not

                        if ($check_contact === null) {
                            
                        } else {

                            $last_modified_date = Carbon::createFromTimestampMs($data1['properties']['lastmodifieddate']['value'])->format('Y-m-d H:i:s');

                            //check if any data is changed
                            if ($last_modified_date !== $check_contact->last_modified_date) {


                                $updated_rows +=1;
                            }
                        }
                    }
                }
                //insert contacts in bulk
                Contact::insert($create_contacts);
                $created_rows += count($create_contacts);
            }
        }
        if (isset($data['has-more']) && !empty($data['has-more'] == true)) {
            return $this->getContactsData($headers, $data['vid-offset'], $client, $backup_progress, $user, $hubspot_contact_ids, $status, $error_msg, $created_rows, $updated_rows);
        } else {

            $data = ['created_rows' => $created_rows, 'updated_rows' => $updated_rows,
                'hubspot_contact_ids' => $hubspot_contact_ids, 'status' => $status, 'error_msg' => $error_msg];
            return $data;
        }
    }

}
