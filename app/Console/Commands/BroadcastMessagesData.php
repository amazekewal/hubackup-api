<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
// models
use App\BroadcastMessage;
use App\BackupHistory;
use App\User;
use Illuminate\Support\Facades\Log;
use App\BackupProgress;
use App\Notifications\Backup;
use App\Notification;
use App\SmartAlert;
use App\SmartAlertHistory;
use Carbon\Carbon;

class BroadcastMessagesData extends Command {

    /**
     * The name of the apiUrl
     * @var type 
     */
    protected $api_url;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'broadcast-messages:data {userId}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command will get all Broadcast messages data';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();

        //get api key from env file
        $this->api_url = env('HUBSPOT_URL');
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        $this->info('Broadcast messages Data cron started....' . $this->api_url);

        $status = '';
        $error_msg = '';
        $created_rows = 0;
        $updated_rows = 0;
        $hubspot_broadcast_messages_ids = [];

        $userId = $this->argument('userId');
        $user = User::find($userId);

        $backup_progress = BackupProgress::where('user_id', $user['id'])->where('status', 'Inprogress')->orderByDesc('id')->first();

        try {

            Log::error('Fetching Broadcast messages data started |||| Start >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> userId - ' . $userId);


            //start time
            $time_start = microtime(true);


            //user token
            $token = $user->accesstoken;

            //using GuzzleHttp client to get request
            $client = new \GuzzleHttp\Client(['http_errors' => false]);
            $headers = [
                'Authorization' => 'Bearer ' . $token,
                'Accept' => 'application/json',
            ];

            $get_data = $this->getBroadcastMessagesData($headers, 0, $client, $backup_progress, $user, $hubspot_broadcast_messages_ids, $status, $error_msg, $created_rows, $updated_rows);

            $created_rows = $get_data['created_rows'];
            $updated_rows = $get_data['updated_rows'];
            $hubspot_broadcast_messages_ids = $get_data['hubspot_broadcast_messages_ids'];
            $error_msg = $get_data['error_msg'];
            $status = $get_data['status'];


            //end time
            $time_end = microtime(true);
            //execution time
            $execution_time = ($time_end - $time_start);

            Log::error('time taken for Broadcast messages data ' . $execution_time);
            Log::error('Fetching Broadcast messages data completed |||| End <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< userId - ' . $userId);
        } catch (BadResponseException $ex) {
            //if request is invalid
            $response = $ex->getResponse();
            $jsonBody = (string) $response->getBody();
            $this->error($jsonBody);

            $data = json_decode($response->getBody(), true);
            $status = $data['status'];
            $error_msg = $data['message'];
            Log::error('CMS Broadcast messages data  |||| Error - ' . $jsonBody);
        }

        //get broadcast messages from database
        $b_messages_in_database = BroadcastMessage::where('backup_progress_id', $backup_progress['id'] - 1)->get()->pluck('broadcast_guid')->toArray();

        //check if empty result 
        if (count($hubspot_broadcast_messages_ids) == 0) {
            $removed_rows = 0;
        } else {
            $removed_b_messages = array_diff($b_messages_in_database, $hubspot_broadcast_messages_ids);
            $removed_rows = count($removed_b_messages);
        }

        //creating BackupHistory record
        $backup_history = BackupHistory::create([
                    'backup_progress_id' => $backup_progress['id'],
                    'user_id' => $user->id,
                    'object_name' => 'Fetched Broadcast messages data',
                    'num_of_records' => $created_rows + $removed_rows,
                    'removed_records' => $removed_rows,
                    'added_records' => $created_rows,
                    'changed_records' => $updated_rows,
                    'num_of_API_calls' => 1,
                    'status' => $status,
                    'error_message' => $error_msg]);

        //get smart alerts for action 'added'
        $added_alert = SmartAlert::where('user_id', $user['id'])
                        ->where('s_object', 'Broadcast Messages')
                        ->where('action', 'Added')->first();

        if (!empty($added_alert)) {
            if ($created_rows > $added_alert->amount) {

                //notification when Broadcast message added
                $add_notificatoion = Notification::create(['user_id' => $user['id'],
                            'backup_progress_id' => $backup_progress['id'],
                            'status' => 'Broadcast Messages Added',
                            'type' => 'email',
                            'num_of_records' => $created_rows]);

                //create history
                SmartAlertHistory::create(['user_id' => $user['id'], 'smart_alert_id' => $added_alert->id]);

                //notify user
                //$user->notify(new Backup($notificatoion));
            }
        }

        //get smart alerts for action 'Changed'
        $changed_alert = SmartAlert::where('user_id', $user['id'])
                        ->where('s_object', 'Broadcast Messages')
                        ->where('action', 'Changed')->first();

        if (!empty($changed_alert)) {
            if ($updated_rows > $changed_alert->amount) {

                //notification when Broadcast message Changed
                $change_notificatoion = Notification::create(['user_id' => $user['id'],
                            'backup_progress_id' => $backup_progress['id'],
                            'status' => 'Broadcast Messages Changed',
                            'type' => 'email',
                            'num_of_records' => $updated_rows]);

                //create history
                SmartAlertHistory::create(['user_id' => $user['id'], 'smart_alert_id' => $changed_alert->id]);

                //notify user
                //$user->notify(new Backup($notificatoion));
            }
        }

        //get smart alerts for action 'Removed'
        $removed_alert = SmartAlert::where('user_id', $user['id'])
                        ->where('s_object', 'Broadcast Messages')
                        ->where('action', 'Removed')->first();

        if (!empty($removed_alert)) {
            if ($removed_rows > $removed_alert->amount) {


                //notification when Broadcast message Removed
                $remove_notificatoion = Notification::create(['user_id' => $user['id'],
                            'backup_progress_id' => $backup_progress['id'],
                            'status' => 'Broadcast Messages Removed',
                            'type' => 'email',
                            'num_of_records' => $removed_rows]);

                //create history
                SmartAlertHistory::create(['user_id' => $user['id'], 'smart_alert_id' => $removed_alert->id]);

                //notify user
                //$user->notify(new Backup($notificatoion));
            }
        }
    }

    public function getBroadcastMessagesData($headers, $offset, $client, $backup_progress, $user, $hubspot_broadcast_messages_ids, $status, $error_msg, $created_rows, $updated_rows) {


        $url = $this->api_url . '/broadcast/v1/broadcasts?count=100&offset=' . $offset;

        //get all Broadcast messages data
        $response = $client->request('GET', $url, ['headers' => $headers]);


        $body = $response->getBody();
        $results = json_decode($body, true);


        if (!empty($results['status']) == 'error') {
            $status = $results['status'];
            $error_msg = $results['message'];
            Log::error('Broadcast messages data  |||| Error - ' . $error_msg);
        } else {
            $status = 'successed';
            if (!empty($results)) {
                $create_broadcast_message = [];
                foreach ($results as $value) {

                    //get broadcast messages ids
                    $hubspot_broadcast_messages_ids[] = $value['broadcastGuid'];

                    $check_broadcast_message = BroadcastMessage::where('user_id', $user->id)
                                    ->where('broadcast_guid', $value['broadcastGuid'])
                                    ->where('backup_progress_id', $backup_progress['id'] - 1)->first();

                    $create_broadcast_message[] = [
                        'user_id' => $user->id,
                        'backup_progress_id' => $backup_progress['id'],
                        'portal_id' => isset($value['portalId']) ? $value['portalId'] : 0,
                        'broadcast_guid' => isset($value['broadcastGuid']) ? $value['broadcastGuid'] : "",
                        'group_guid' => isset($value['groupGuid']) ? $value['groupGuid'] : "",
                        'campaign_guid' => isset($value['campaignGuid']) ? $value['campaignGuid'] : "",
                        'channel_guid' => isset($value['channelGuid']) ? $value['channelGuid'] : "",
                        'client_tag' => isset($value['clientTag']) ? $value['clientTag'] : "",
                        'status' => isset($value['status']) ? $value['status'] : "",
                        'message' => isset($value['message']) ? $value['message'] : "",
                        'body' => isset($value['content']['body']) ? $value['content']['body'] : "",
                        'link_preview_suppressed' => isset($value['content']['linkPreviewSuppressed']) ? $value['content']['linkPreviewSuppressed'] : 0,
                        'link' => isset($value['content']['originalLink']) ? $value['content']['originalLink'] : "",
                        'original_body' => isset($value['content']['originalBody']) ? $value['content']['originalBody'] : "",
                        'uncompressed_links' => isset($value['content']['uncompressedLinks']) ? $value['content']['uncompressedLinks'] : "",
                        'link_guid' => isset($value['linkGuid']) ? $value['linkGuid'] : "",
                        'message_url' => isset($value['messageUrl']) ? $value['messageUrl'] : "",
                        'foreign_id' => isset($value['foreignId']) ? $value['foreignId'] : 0,
                        'task_queue_id' => isset($value['taskQueueId']) ? $value['taskQueueId'] : "",
                        'link_task_queue_id' => isset($value['linkTaskQueueId']) ? $value['linkTaskQueueId'] : "",
                        'remote_content_id' => isset($value['remoteContentId']) ? $value['remoteContentId'] : "",
                        'remote_content_type' => isset($value['remoteContentType']) ? $value['remoteContentType'] : "",
                        'clicks' => isset($value['clicks']) ? $value['clicks'] : 0,
                        'created_by' => isset($value['createdBy']) ? $value['createdBy'] : "",
                        'updated_by' => isset($value['updatedBy']) ? $value['updatedBy'] : "",
                        'interactions' => isset($value['interactions']) ? $value['interactions'] : "",
                        'interaction_counts' => isset($value['interactionCounts']) ? $value['interactionCounts'] : "",
                        'created_on' => isset($value['createdAt']) ? Carbon::createFromTimestampMs($value['createdAt'])->format('Y-m-d H:i:s') : NULL,
                        'trigger_at' => isset($value['triggerAt']) ? Carbon::createFromTimestampMs($value['triggerAt'])->format('Y-m-d H:i:s') : NULL,
                        'finished_at' => isset($value['finishedAt']) ? Carbon::createFromTimestampMs($value['finishedAt'])->format('Y-m-d H:i:s') : NULL,
                        'created_at' => Carbon::now()->toDateTimeString(),
                        'updated_at' => Carbon::now()->toDateTimeString()
                    ];
                    //check if broadcast message  is already inserted or not
                    if ($check_broadcast_message === null) {
                        
                    } else {

                        //check if any data is changed
                        if ($check_event->group_guid != $value['groupGuid'] || $check_event->campaign_guid != $value['campaignGuid'] ||
                                $check_event->channel_guid != $value['channelGuid'] || $check_event->client_tag != $value['clientTag'] ||
                                $check_event->status != $value['status'] || $check_event->message != $value['message'] ||
                                $check_event->body != $value['content']['body'] || $check_event->link_preview_suppressed != $value['content']['linkPreviewSuppressed'] ||
                                $check_event->link != $value['originalLink'] || $check_event->original_body != $value['content']['originalBody'] ||
                                $check_event->uncompressed_links != $value['content']['uncompressedLinks'] || $check_event->link_guid != $value['linkGuid'] ||
                                $check_event->message_url != $value['messageUrl'] || $check_event->foreign_id != $value['foreignId'] ||
                                $check_event->task_queue_id != $value['taskQueueId'] || $check_event->link_task_queue_id != $value['linkTaskQueueId'] ||
                                $check_event->remote_content_id != $value['remoteContentId'] || $check_event->remote_content_type != $value['remoteContentType'] ||
                                $check_event->clicks != $value['clicks'] || $check_event->created_by != $value['createdBy'] ||
                                $check_event->updated_by != $value['updatedBy'] || $check_event->interactions != $value['interactions'] ||
                                $check_event->interaction_counts != $value['interactionCounts'] || $check_event->created_on != $value['createdAt'] ||
                                $check_event->trigger_at != $value['triggerAt'] || $check_event->finished_at != $value['finishedAt']
                        ) {
                            $updated_rows +=1;
                        }
                    }
                }
                //insert broadcast messages in bulk
                BroadcastMessage::insert($create_broadcast_message);
                $created_rows += count($create_broadcast_message);
            }
            if (count($results) == 0) {
                $data_offset = 0;
            } else {
                $i = $offset;
                $maxResults = 10000;
                $data_offset = 0;
                for ($i; $i < $maxResults; $i = $i + 100) {
                    $data_offset = $i;
                    break;
                }
            }
            if (!empty($data_offset !== 0)) {

                return $this->getBroadcastMessagesData($headers, $data_offset, $client, $backup_progress, $user, $hubspot_broadcast_messages_ids, $status, $error_msg, $created_rows, $updated_rows);
            } else {

                $data = ['created_rows' => $created_rows, 'updated_rows' => $updated_rows,
                    'hubspot_broadcast_messages_ids' => $hubspot_broadcast_messages_ids, 'status' => $status, 'error_msg' => $error_msg];

                return $data;
            }
        }
        $data = ['created_rows' => $created_rows, 'updated_rows' => $updated_rows,
            'hubspot_broadcast_messages_ids' => $hubspot_broadcast_messages_ids, 'status' => $status, 'error_msg' => $error_msg];

        return $data;
    }

}
