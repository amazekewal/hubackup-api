<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\BadResponseException;
use Illuminate\Support\Facades\Artisan;
// models
use App\CmsLayout;
use App\CmsLayoutAttachedJs;
use App\CmsLayoutAttachedStylesheet;
use App\CmsLayoutContentTag;
use App\CmsLayoutRow;
use App\BackupHistory;
use App\User;
use Illuminate\Support\Facades\Log;
use App\BackupProgress;
use App\Notifications\Backup;
use App\Notification;
use App\SmartAlert;
use App\SmartAlertHistory;
use Carbon\Carbon;

class CmsLayoutsData extends Command {

    /**
     * The name of the apiUrl
     * @var type 
     */
    protected $api_url;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cms-layouts:data {userId}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command will get all CMS Layouts data';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();

        //get api key from env file
        $this->api_url = env('HUBSPOT_URL');
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        $this->info('CMS Layouts Data cron started....' . $this->api_url);

        $status = '';
        $error_msg = '';
        $created_rows = 0;
        $updated_rows = 0;
        $hubspot_cms_layout_ids = [];

        $userId = $this->argument('userId');
        $user = User::find($userId);

        $backup_progress = BackupProgress::where('user_id', $user['id'])->where('status', 'Inprogress')->orderByDesc('id')->first();


        try {

            Log::error('Fetching CMS Layouts data started |||| Start >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> userId - ' . $userId);

            //start time
            $time_start = microtime(true);


            //user token
            $token = $user->accesstoken;

            //using GuzzleHttp client to get request
            $client = new \GuzzleHttp\Client(['http_errors' => false]);
            $headers = [
                'Authorization' => 'Bearer ' . $token,
                'Accept' => 'application/json',
            ];

            $get_data = $this->getCmsLayoutsData($headers, 0, $client, $backup_progress, $user, $hubspot_cms_layout_ids, $status, $error_msg, $created_rows, $updated_rows);

            $created_rows = $get_data['created_rows'];
            $updated_rows = $get_data['updated_rows'];
            $hubspot_cms_layout_ids = $get_data['hubspot_cms_layout_ids'];
            $error_msg = $get_data['error_msg'];
            $status = $get_data['status'];

            //end time
            $time_end = microtime(true);
            //execution time
            $execution_time = ($time_end - $time_start);

            Log::error('time taken for CMS Layouts data ' . $execution_time);
            Log::error('Fetching CMS Layouts data completed |||| End <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< userId - ' . $userId);
        } catch (BadResponseException $ex) {
            //if request is invalid
            $response = $ex->getResponse();
            $jsonBody = (string) $response->getBody();
            $this->error($jsonBody);

            $data = json_decode($response->getBody(), true);
            $status = $data['status'];
            $error_msg = $data['message'];
            Log::error('CMS Layouts data  |||| Error - ' . $jsonBody);
        }

        //get cms layouts from database
        $cms_layouts_in_database = CmsLayout::where('backup_progress_id', $backup_progress['id'] - 1)->get()->pluck('layout_id')->toArray();

        //check if empty result 
        if (count($hubspot_cms_layout_ids) == 0) {
            $removed_rows = 0;
        } else {
            $removed_cms_layouts = array_diff($cms_layouts_in_database, $hubspot_cms_layout_ids);
            $removed_rows = count($removed_cms_layouts);
        }

        //creating BackupHistory record
        $backup_history = BackupHistory::create([
                    'backup_progress_id' => $backup_progress['id'],
                    'user_id' => $user->id,
                    'object_name' => 'Fetched CMS Layouts data',
                    'num_of_records' => $created_rows + $removed_rows,
                    'removed_records' => $removed_rows,
                    'added_records' => $created_rows,
                    'changed_records' => $updated_rows,
                    'num_of_API_calls' => 1,
                    'status' => $status,
                    'error_message' => $error_msg]);

        //get smart alerts for action 'added'
        $added_alert = SmartAlert::where('user_id', $user['id'])
                        ->where('s_object', 'CMS Layouts')
                        ->where('action', 'Added')->first();

        if (!empty($added_alert)) {
            if ($created_rows > $added_alert->amount) {

                //notification when CMS Layout added
                $add_notificatoion = Notification::create(['user_id' => $user['id'], 'backup_progress_id' => $backup_progress['id'],
                            'status' => 'CMS Layouts Added', 'type' => 'email', 'num_of_records' => $created_rows]);

                //create history
                SmartAlertHistory::create(['user_id' => $user['id'], 'smart_alert_id' => $added_alert->id]);

                //notify user
                //$user->notify(new Backup($notificatoion));
            }
        }

        //get smart alerts for action 'Changed'
        $changed_alert = SmartAlert::where('user_id', $user['id'])
                        ->where('s_object', 'CMS Layouts')
                        ->where('action', 'Changed')->first();

        if (!empty($changed_alert)) {
            if ($updated_rows > $changed_alert->amount) {

                //notification when CMS Layout Changed
                $change_notificatoion = Notification::create(['user_id' => $user['id'], 'backup_progress_id' => $backup_progress['id'],
                            'status' => 'CMS Layouts Changed', 'type' => 'email', 'num_of_records' => $updated_rows]);

                //create history
                SmartAlertHistory::create(['user_id' => $user['id'], 'smart_alert_id' => $changed_alert->id]);

                //notify user
                //$user->notify(new Backup($notificatoion));
            }
        }

        //get smart alerts for action 'Removed'
        $removed_alert = SmartAlert::where('user_id', $user['id'])
                        ->where('s_object', 'CMS Layouts')
                        ->where('action', 'Removed')->first();

        if (!empty($removed_alert)) {
            if ($removed_rows > $removed_alert->amount) {

                //notification when CMS Layout Removed
                $remove_notificatoion = Notification::create(['user_id' => $user['id'], 'backup_progress_id' => $backup_progress['id'],
                            'status' => 'CMS Layouts Removed', 'type' => 'email', 'num_of_records' => $removed_rows]);

                //create history
                SmartAlertHistory::create(['user_id' => $user['id'], 'smart_alert_id' => $removed_alert->id]);

                //notify user
                //$user->notify(new Backup($notificatoion));
            }
        }
    }

    public function getCmsLayoutsData($headers, $offset, $client, $backup_progress, $user, $hubspot_cms_layout_ids, $status, $error_msg, $created_rows, $updated_rows) {

        $url = $this->api_url . '/content/api/v2/layouts?limit=100&offset=' . $offset;

        //get all cms Layouts data
        $response = $client->request('GET', $url, ['headers' => $headers]);


        $body = $response->getBody();
        $data = json_decode($body, true);

        if (!empty($data['status']) == 'error') {
            $status = $data['status'];
            $error_msg = $data['message'];
            Log::error('CMS Layouts data  |||| Error - ' . $error_msg);
        } else {
            $results = $data['objects'];
            $status = 'successed';
            if (!empty($results)) {
                foreach ($results as $value) {

                    //get all cms layout ids
                    $hubspot_cms_layout_ids[] = $value['id'];


                    $check_cms_layout = CmsLayout::where('user_id', $user->id)
                                    ->where('layout_id', $value['id'])
                                    ->where('backup_progress_id', $backup_progress['id'] - 1)->first();

                    $cms_layout = CmsLayout::create([
                                'user_id' => $user->id,
                                'backup_progress_id' => $backup_progress['id'],
                                'layout_id' => $value['id'],
                                'portal_id' => $value['portal_id'],
                                'attached_amp_stylesheet' => isset($value['attached_amp_stylesheet']) ? $value['attached_amp_stylesheet'] : 0,
                                'author' => isset($value['author']) ? $value['author'] : "",
                                'author_at' => isset($value['author_at']) ? Carbon::createFromTimestampMs($value['author_at'])->format('Y-m-d H:i:s') : NULL,
                                'enable_domain_stylesheets' => isset($value['enable_domain_stylesheets']) ? $value['enable_domain_stylesheets'] : 0,
                                'include_default_custom_css' => isset($value['include_default_custom_css']) ? $value['include_default_custom_css'] : 0,
                                'folder' => isset($value['folder']) ? $value['folder'] : "",
                                'folder_id' => isset($value['folder_id']) ? $value['folder_id'] : 0,
                                'cdn_purge_embargo_time' => isset($value['body']['cdn_purge_embargo_time']) ? Carbon::createFromTimestampMs($value['body']['cdn_purge_embargo_time'])->format('Y-m-d H:i:s') : NULL,
                                'custom_head' => isset($value['custom_head']) ? $value['custom_head'] : "",
                                'body_css_id' => isset($value['body_css_id']) ? $value['body_css_id'] : "",
                                'custom_footer' => isset($value['custom_footer']) ? $value['custom_footer'] : "",
                                'body_class' => isset($value['body_class']) ? $value['body_class'] : "",
                                'body_css' => isset($value['body_css']) ? $value['body_css'] : "",
                                'builtin' => isset($value['builtin']) ? $value['builtin'] : 0,
                                'purchased' => isset($value['purchased']) ? $value['purchased'] : 0,
                                'generator' => isset($value['generator']) ? $value['generator'] : "",
                                'generated_template_id' => isset($value['generated_template_id']) ? $value['generated_template_id'] : 0,
                                'marketplace_good_id' => isset($value['marketplace_good_id']) ? $value['marketplace_good_id'] : 0,
                                'cloned_from' => isset($value['cloned_from']) ? $value['cloned_from'] : "",
                                'body_editable' => isset($value['body_editable']) ? $value['body_editable'] : 0,
                                'last_edit_session_id' => isset($value['last_edit_session_id']) ? $value['last_edit_session_id'] : "",
                                'last_edit_update_id' => isset($value['last_edit_update_id']) ? $value['last_edit_update_id'] : "",
                                'has_user_changes' => isset($value['has_user_changes']) ? $value['has_user_changes'] : 0,
                                'marketplace_delivery_id' => isset($value['marketplace_delivery_id']) ? $value['marketplace_delivery_id'] : 0,
                                'is_read_only' => isset($value['is_read_only']) ? $value['is_read_only'] : 0,
                                'host_template_types' => isset($value['host_template_types']) ? $value['host_template_types'] : NULL,
                                'marketplace_version' => isset($value['marketplace_version']) ? $value['marketplace_version'] : 0,
                                'should_create_hubshot' => isset($value['should_create_hubshot']) ? $value['should_create_hubshot'] : 0,
                                'is_default' => isset($value['is_default']) ? $value['is_default'] : 0,
                                'category_id' => isset($value['category_id']) ? $value['category_id'] : "",
                                'deleted_at' => isset($value['deleted_at']) ? $value['deleted_at'] : 0,
                                'is_available_for_new_content' => isset($value['is_available_for_new_content']) ? $value['is_available_for_new_content'] : 0,
                                'languages' => isset($value['languages']) ? $value['languages'] : NULL,
                                'label' => isset($value['label']) ? $value['label'] : "",
                                'has_buffered_changes' => isset($value['meta']['hasBufferedChanges']) ? $value['meta']['hasBufferedChanges'] : 0,
                                'path' => isset($value['path']) ? $value['path'] : "",
                                'template_type' => isset($value['template_type']) ? $value['template_type'] : 0,
                                'updated_by' => isset($value['updated_by']) ? $value['updated_by'] : "",
                                'updated_by_id' => isset($value['updated_by_id']) ? $value['updated_by_id'] : 0,
                                'layout_data_cell' => isset($value['layout_data']['cell']) ? $value['layout_data']['cell'] : 0,
                                'layout_data_cells' => isset($value['layout_data']['cells']) ? $value['layout_data']['cells'] : NULL,
                                'layout_data_css_class' => isset($value['layout_data']['css_class']) ? $value['layout_data']['css_class'] : "",
                                'layout_data_css_id' => isset($value['layout_data']['css_id']) ? $value['layout_data']['css_id'] : "",
                                'layout_data_css_id_str' => isset($value['layout_data']['css_id_str']) ? $value['layout_data']['css_id_str'] : "",
                                'layout_data_css_style' => isset($value['layout_data']['css_style']) ? $value['layout_data']['css_style'] : "",
                                'layout_data_editable' => isset($value['layout_data']['editable']) ? $value['layout_data']['editable'] : 0,
                                'layout_data_id' => isset($value['layout_data']['id']) ? $value['layout_data']['id'] : "",
                                'layout_data_is_container' => isset($value['layout_data']['is_container']) ? $value['layout_data']['is_container'] : 0,
                                'layout_data_is_content_overridden' => isset($value['layout_data']['is_content_overridden']) ? $value['layout_data']['is_content_overridden'] : 0,
                                'layout_data_is_in_container' => isset($value['layout_data']['is_in_container']) ? $value['layout_data']['is_in_container'] : 0,
                                'layout_data_language_overrides' => isset($value['layout_data']['language_overrides']) ? $value['layout_data']['language_overrides'] : NULL,
                                'layout_data_order' => isset($value['layout_data']['order']) ? $value['layout_data']['order'] : "",
                                'layout_data_params' => isset($value['layout_data']['params']) ? $value['layout_data']['params'] : NULL,
                                'layout_data_root' => isset($value['layout_data']['root']) ? $value['layout_data']['root'] : 0,
                                'layout_data_row' => isset($value['layout_data']['row']) ? $value['layout_data']['row'] : 0,
                                'layout_data_row_meta_data' => isset($value['layout_data']['row_meta_data']) ? $value['layout_data']['row_meta_data'] : NULL,
                                'layout_data_type' => isset($value['layout_data']['type']) ? $value['layout_data']['type'] : "",
                                'layout_data_w' => isset($value['layout_data']['w']) ? $value['layout_data']['w'] : 0,
                                'layout_data_widgets' => isset($value['layout_data']['widgets']) ? $value['layout_data']['widgets'] : NULL,
                                'layout_data_x' => isset($value['layout_data']['x']) ? $value['layout_data']['x'] : "",
                                'created' => isset($value['created']) ? Carbon::createFromTimestampMs($value['created'])->format('Y-m-d H:i:s') : NULL,
                                'updated' => isset($value['updated']) ? Carbon::createFromTimestampMs($value['updated'])->format('Y-m-d H:i:s') : NULL,
                    ]);

                    if (!empty($value['attached_js'])) {
                        foreach ($value['attached_js'] as $js) {

                            $attached_js = CmsLayoutAttachedJs::create([
                                        'cms_layout_db_id' => $cms_layout->id,
                                        'js_id' => $js['id'],
                                        'type' => $js['type'],
                            ]);
                        }
                    }

                    if (!empty($value['attached_stylesheets'])) {
                        foreach ($value['attached_stylesheets'] as $stylesheet) {

                            $attached_stylesheets = CmsLayoutAttachedStylesheet::create([
                                        'cms_layout_db_id' => $cms_layout->id,
                                        'stylesheet_id' => $stylesheet['id'],
                                        'type' => $stylesheet['type'],
                            ]);
                        }
                    }

                    if (!empty($value['content_tags'])) {
                        foreach ($value['content_tags'] as $content_tag) {

                            $content_tags = CmsLayoutContentTag::create([
                                        'cms_layout_db_id' => $cms_layout->id,
                                        'name' => $content_tag['name'],
                                        'source' => $content_tag['source'],
                            ]);
                        }
                    }

                    if (!empty($value['layout_data']['rows'])) {
                        foreach ($value['layout_data']['rows'] as $row) {

                            $rows = CmsLayoutRow::create([
                                        'cms_layout_db_id' => $cms_layout->id,
                                        'row_id' => $row[0]['id'],
                                        'cell' => isset($row[0]['cell']) ? $row[0]['cell'] : 0,
                                        'cells' => isset($row[0]['cells']) ? $row[0]['cells'] : NULL,
                                        'css_class' => isset($row[0]['css_class']) ? $row[0]['css_class'] : "",
                                        'css_id' => isset($row[0]['css_id']) ? $row[0]['css_id'] : "",
                                        'css_id_str' => isset($row[0]['css_id_str']) ? $row[0]['css_id_str'] : "",
                                        'css_style' => isset($row[0]['css_style']) ? $row[0]['css_style'] : "",
                                        'editable' => isset($row[0]['editable']) ? $row[0]['editable'] : 0,
                                        'is_container' => isset($row[0]['is_container']) ? $row[0]['is_container'] : 0,
                                        'is_content_overridden' => isset($row[0]['is_content_overridden']) ? $row[0]['is_content_overridden'] : 0,
                                        'is_in_container' => isset($row[0]['is_in_container']) ? $row[0]['is_in_container'] : 0,
                                        'language_overrides' => isset($row[0]['language_overrides']) ? $row[0]['language_overrides'] : NULL,
                                        'order' => isset($row[0]['order']) ? $row[0]['order'] : "",
                                        'params' => isset($row[0]['params']) ? $row[0]['params'] : NULL,
                                        'root' => isset($row[0]['root']) ? $row[0]['root'] : 0,
                                        'row' => isset($row[0]['row']) ? $row[0]['row'] : 0,
                                        'row_meta_data' => isset($row[0]['row_meta_data']) ? $row[0]['row_meta_data'] : NULL,
                                        'type' => isset($row[0]['type']) ? $row[0]['type'] : "",
                                        'w' => isset($row[0]['w']) ? $row[0]['w'] : 0,
                                        'widgets' => isset($row[0]['widgets']) ? $row[0]['widgets'] : NULL,
                                        'x' => isset($row[0]['x']) ? $row[0]['x'] : "",
                            ]);
                        }
                    }

                    //if layout created
                    if ($cms_layout) {
                        $created_rows +=1;
                    }

                    //check if cms Layout  is already inserted or not
                    if ($check_cms_layout === null) {
                        
                    } else {
                        $last_updated_date = Carbon::createFromTimestampMs($value['updated'])->format('Y-m-d H:i:s');

                        //check if any data is changed
                        if ($last_updated_date != $check_cms_layout->updated) {

                            $updated_rows +=1;
                        }
                    }
                }
            }
        }

        if (isset($data['offset']) && !empty($data['offset'] !== 0)) {

            return $this->getCmsLayoutsData($headers, $data['offset'], $client, $backup_progress, $user, $hubspot_cms_layout_ids, $status, $error_msg, $created_rows, $updated_rows);
        } else {

            $data = ['created_rows' => $created_rows, 'updated_rows' => $updated_rows,
                'hubspot_cms_layout_ids' => $hubspot_cms_layout_ids, 'status' => $status, 'error_msg' => $error_msg];

            return $data;
        }
    }

}
