<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\BadResponseException;
use Illuminate\Support\Facades\Artisan;
// models
use App\CmsFile;
use App\BackupHistory;
use App\User;
use Illuminate\Support\Facades\Log;
use App\BackupProgress;
use App\Notifications\Backup;
use App\Notification;
use App\SmartAlert;
use App\SmartAlertHistory;
use Carbon\Carbon;

class CmsFilesData extends Command {

    /**
     * The name of the apiUrl
     * @var type 
     */
    protected $api_url;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cms-files:data {userId}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command will get all CMS Files data';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();

        //get api key from env file
        $this->api_url = env('HUBSPOT_URL');
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        $this->info('CMS Files Data cron started....' . $this->api_url);

        $status = '';
        $error_msg = '';
        $created_rows = 0;
        $updated_rows = 0;
        $hubspot_cms_file_ids = [];

        $userId = $this->argument('userId');
        $user = User::find($userId);

        $backup_progress = BackupProgress::where('user_id', $user['id'])->where('status', 'Inprogress')->orderByDesc('id')->first();


        try {

            Log::error('Fetching CMS Files data started |||| Start >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> userId - ' . $userId);

            //start time
            $time_start = microtime(true);


            //user token
            $token = $user->accesstoken;

            //using GuzzleHttp client to get request
            $client = new \GuzzleHttp\Client(['http_errors' => false]);
            $headers = [
                'Authorization' => 'Bearer ' . $token,
                'Accept' => 'application/json',
            ];

            $get_data = $this->getCmsFilesData($headers, 0, $client, $backup_progress, $user, $hubspot_cms_file_ids, $status, $error_msg, $created_rows, $updated_rows);

            $created_rows = $get_data['created_rows'];
            $updated_rows = $get_data['updated_rows'];
            $hubspot_cms_file_ids = $get_data['hubspot_cms_file_ids'];
            $error_msg = $get_data['error_msg'];
            $status = $get_data['status'];

            //end time
            $time_end = microtime(true);
            //execution time
            $execution_time = ($time_end - $time_start);

            Log::error('time taken for CMS Files data ' . $execution_time);
            Log::error('Fetching CMS Files data completed |||| End <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< userId - ' . $userId);
        } catch (BadResponseException $ex) {
            //if request is invalid
            $response = $ex->getResponse();
            $jsonBody = (string) $response->getBody();
            $this->error($jsonBody);

            $data = json_decode($response->getBody(), true);
            $status = $data['status'];
            $error_msg = $data['message'];
            Log::error('CMS Files data  |||| Error - ' . $jsonBody);
        }


        //get cms files from database
        $cms_files_in_database = CmsFile::where('backup_progress_id', $backup_progress['id'] - 1)->get()->pluck('cms_file_id')->toArray();

        //check if empty result 
        if (count($hubspot_cms_file_ids) == 0) {
            $removed_rows = 0;
        } else {
            $removed_cms_files = array_diff($cms_files_in_database, $hubspot_cms_file_ids);
            $removed_rows = count($removed_cms_files);
        }

        //creating BackupHistory record
        $backup_history = BackupHistory::create([
                    'backup_progress_id' => $backup_progress['id'],
                    'user_id' => $user->id,
                    'object_name' => 'Fetched CMS Files data',
                    'num_of_records' => $created_rows + $removed_rows,
                    'removed_records' => $removed_rows,
                    'added_records' => $created_rows,
                    'changed_records' => $updated_rows,
                    'num_of_API_calls' => 1,
                    'status' => $status,
                    'error_message' => $error_msg]);

        //get smart alerts for action 'added'
        $added_alert = SmartAlert::where('user_id', $user['id'])
                        ->where('s_object', 'CMS Files')
                        ->where('action', 'Added')->first();

        if (!empty($added_alert)) {
            if ($created_rows > $added_alert->amount) {

                //notification when CMS File added
                $add_notificatoion = Notification::create(['user_id' => $user['id'],
                            'backup_progress_id' => $backup_progress['id'],
                            'status' => 'CMS Files Added',
                            'type' => 'email',
                            'num_of_records' => $created_rows]);

                //create history
                SmartAlertHistory::create(['user_id' => $user['id'], 'smart_alert_id' => $added_alert->id]);

                //notify user
                //$user->notify(new Backup($notificatoion));
            }
        }

        //get smart alerts for action 'Changed'
        $changed_alert = SmartAlert::where('user_id', $user['id'])
                        ->where('s_object', 'CMS Files')
                        ->where('action', 'Changed')->first();

        if (!empty($changed_alert)) {
            if ($updated_rows > $changed_alert->amount) {


                //notification when CMS File Changed
                $change_notificatoion = Notification::create(['user_id' => $user['id'],
                            'backup_progress_id' => $backup_progress['id'],
                            'status' => 'CMS Files Changed',
                            'type' => 'email',
                            'num_of_records' => $updated_rows]);

                //create history
                SmartAlertHistory::create(['user_id' => $user['id'], 'smart_alert_id' => $changed_alert->id]);

                //notify user
                //$user->notify(new Backup($notificatoion));
            }
        }

        //get smart alerts for action 'Removed'
        $removed_alert = SmartAlert::where('user_id', $user['id'])
                        ->where('s_object', 'CMS Files')
                        ->where('action', 'Removed')->first();

        if (!empty($removed_alert)) {
            if ($removed_rows > $removed_alert->amount) {

                //notification when CMS File Removed
                $remove_notificatoion = Notification::create(['user_id' => $user['id'],
                            'backup_progress_id' => $backup_progress['id'],
                            'status' => 'CMS Files Removed',
                            'type' => 'email',
                            'num_of_records' => $removed_rows]);

                //create history
                SmartAlertHistory::create(['user_id' => $user['id'], 'smart_alert_id' => $removed_alert->id]);

                //notify user
                //$user->notify(new Backup($notificatoion));
            }
        }
    }

    public function getCmsFilesData($headers, $offset, $client, $backup_progress, $user, $hubspot_cms_file_ids, $status, $error_msg, $created_rows, $updated_rows) {

        $url = $this->api_url . '/filemanager/api/v2/files?limit=100&offset=' . $offset;

        //get all cms blog Files data
        $response = $client->request('GET', $url, ['headers' => $headers]);


        $body = $response->getBody();
        $data = json_decode($body, true);


        if (!empty($data['status']) == 'error') {
            $status = $data['status'];
            $error_msg = $data['message'];
            Log::error('CMS Files data  |||| Error - ' . $error_msg);
        } else {
            $results = $data['objects'];
            $status = 'successed';
            if (!empty($results)) {
                $create_files = [];
                foreach ($results as $value) {

                    //get all cms file ids
                    $hubspot_cms_file_ids[] = $value['id'];



                    $check_cms_file = CmsFile::where('user_id', $user->id)
                                    ->where('cms_file_id', $value['id'])
                                    ->where('backup_progress_id', $backup_progress['id'] - 1)->first();

                    $create_files[] = [
                        'user_id' => $user->id,
                        'backup_progress_id' => $backup_progress['id'],
                        'cms_file_id' => $value['id'],
                        'portal_id' => isset($value['portal_id']) ? $value['portal_id'] : 0,
                        'title' => isset($value['title']) ? $value['title'] : "",
                        'alt_key' => isset($value['alt_key']) ? $value['alt_key'] : "",
                        'alt_key_hash' => isset($value['alt_key_hash']) ? $value['alt_key_hash'] : "",
                        'alt_url' => isset($value['alt_url']) ? $value['alt_url'] : "",
                        'archived' => isset($value['archived']) ? $value['archived'] : 0,
                        'deleted_at' => isset($value['deleted_at']) ? $value['deleted_at'] : 0,
                        'extension' => isset($value['extension']) ? $value['extension'] : "",
                        'folder_id' => isset($value['folder_id']) ? $value['folder_id'] : "",
                        'height' => isset($value['height']) ? $value['height'] : 0,
                        'encoding' => isset($value['encoding']) ? $value['encoding'] : "",
                        'is_cta_image' => isset($value['is_cta_image']) ? $value['is_cta_image'] : 0,
                        'size' => isset($value['size']) ? $value['size'] : 0,
                        'type' => isset($value['type']) ? $value['type'] : "",
                        'url' => isset($value['url']) ? $value['url'] : "",
                        'version' => isset($value['version']) ? $value['version'] : 0,
                        'width' => isset($value['width']) ? $value['width'] : 0,
                        'cloud_key' => isset($value['cloud_key']) ? $value['cloud_key'] : "",
                        's3_url' => isset($value['s3_url']) ? $value['s3_url'] : "",
                        'friendly_url' => isset($value['friendly_url']) ? $value['friendly_url'] : "",
                        'url_scheme' => isset($value['meta']['url_scheme']) ? $value['meta']['url_scheme'] : "",
                        'allows_anonymous_access' => isset($value['meta']['allows_anonymous_access']) ? $value['meta']['allows_anonymous_access'] : 0,
                        'hidden' => isset($value['hidden']) ? $value['hidden'] : 0,
                        'cloud_key_hash' => isset($value['cloud_key_hash']) ? $value['cloud_key_hash'] : "",
                        'created_by' => isset($value['created_by']) ? $value['created_by'] : "",
                        'deleted_by' => isset($value['deleted_by']) ? $value['deleted_by'] : "",
                        'replaceable' => isset($value['replaceable']) ? $value['replaceable'] : 0,
                        'cdn_purge_embargo_time' => isset($value['cdn_purge_embargo_time']) ? $value['cdn_purge_embargo_time'] : "",
                        'file_hash' => isset($value['file_hash']) ? $value['file_hash'] : "",
                        'meta_thumbs_medium_cloud_key' => isset($value['meta']['thumbs']['medium']['cloud_key']) ? $value['meta']['thumbs']['medium']['cloud_key'] : "",
                        'meta_thumbs_medium_friendly_url' => isset($value['meta']['thumbs']['medium']['friendly_url']) ? $value['meta']['thumbs']['medium']['friendly_url'] : "",
                        'meta_thumbs_medium_s3_url' => isset($value['meta']['thumbs']['medium']['s3_url']) ? $value['meta']['thumbs']['medium']['s3_url'] : "",
                        'meta_thumbs_medium_image_name' => isset($value['meta']['thumbs']['medium']['image_name']) ? $value['meta']['thumbs']['medium']['image_name'] : "",
                        'meta_thumbs_thumb_cloud_key' => isset($value['meta']['thumbs']['thumb']['cloud_key']) ? $value['meta']['thumbs']['thumb']['cloud_key'] : "",
                        'meta_thumbs_thumb_friendly_url' => isset($value['meta']['thumbs']['thumb']['friendly_url']) ? $value['meta']['thumbs']['thumb']['friendly_url'] : "",
                        'meta_thumbs_thumb_s3_url' => isset($value['meta']['thumbs']['thumb']['s3_url']) ? $value['meta']['thumbs']['thumb']['s3_url'] : "",
                        'meta_thumbs_thumb_image_name' => isset($value['meta']['thumbs']['thumb']['image_name']) ? $value['meta']['thumbs']['thumb']['image_name'] : "",
                        'meta_thumbs_icon_cloud_key' => isset($value['meta']['thumbs']['icon']['cloud_key']) ? $value['meta']['thumbs']['icon']['cloud_key'] : "",
                        'meta_thumbs_icon_friendly_url' => isset($value['meta']['thumbs']['icon']['friendly_url']) ? $value['meta']['thumbs']['icon']['friendly_url'] : "",
                        'meta_thumbs_icon_s3_url' => isset($value['meta']['thumbs']['icon']['s3_url']) ? $value['meta']['thumbs']['icon']['s3_url'] : "",
                        'meta_thumbs_icon_image_name' => isset($value['meta']['thumbs']['icon']['image_name']) ? $value['meta']['thumbs']['icon']['image_name'] : "",
                        'created' => isset($value['created']) ? Carbon::createFromTimestampMs($value['created'])->format('Y-m-d H:i:s') : NULL,
                        'updated' => isset($value['updated']) ? Carbon::createFromTimestampMs($value['updated'])->format('Y-m-d H:i:s') : NULL,
                        'created_at' => Carbon::now()->toDateTimeString(),
                        'updated_at' => Carbon::now()->toDateTimeString()
                    ];

                    //check if blog file  is already inserted or not
                    if ($check_cms_file === null) {
                        
                    } else {
                        $last_updated_date = Carbon::createFromTimestampMs($value['updated'])->format('Y-m-d H:i:s');

                        //check if any data is changed
                        if ($last_updated_date != $check_cms_file->updated) {

                            $updated_rows +=1;
                        }
                    }
                }
                //insert cms files in bulk
                CmsFile::insert($create_files);
                $created_rows += count($create_files);
            }
        }

        if (isset($data['offset']) && !empty($data['offset'] !== 0)) {

            return $this->getCmsFilesData($headers, $data['offset'], $client, $backup_progress, $user, $hubspot_cms_file_ids, $status, $error_msg, $created_rows, $updated_rows);
        } else {

            $data = ['created_rows' => $created_rows, 'updated_rows' => $updated_rows,
                'hubspot_cms_file_ids' => $hubspot_cms_file_ids, 'status' => $status, 'error_msg' => $error_msg];

            return $data;
        }
    }

}
