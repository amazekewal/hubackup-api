<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
// models
use App\EmailSubscription;
use App\EmailSubscriptionTimeline;
use App\EmailSubscriptionTimelineChange;
use App\EmailStatus;
use App\BackupHistory;
use App\User;
use Illuminate\Support\Facades\Log;
use App\BackupProgress;
use App\Notifications\Backup;
use App\Notification;
use App\SmartAlert;
use App\SmartAlertHistory;
use Carbon\Carbon;

class EmailSubscriptionsData extends Command {

    /**
     * The name of the apiUrl
     * @var type 
     */
    protected $api_url;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'email-subscriptions:data {userId}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command will get all Email Subscriptions data';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();

        //get api key from env file
        $this->api_url = env('HUBSPOT_URL');
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        $this->info('Email Subscriptions Data cron started....' . $this->api_url);

        $status = '';
        $error_msg = '';
        $created_rows = 0;
        $updated_rows = 0;
        $hubspot_email_subscription_ids = [];

        $userId = $this->argument('userId');
        $user = User::find($userId);

        $backup_progress = BackupProgress::where('user_id', $user['id'])->where('status', 'Inprogress')->orderByDesc('id')->first();

        try {

            Log::error('Fetching Email Subscriptions data started |||| Start >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> userId - ' . $userId);

            //start time
            $time_start = microtime(true);


            //user token
            $token = $user->accesstoken;

            //using GuzzleHttp client to get request
            $client = new \GuzzleHttp\Client(['http_errors' => false]);
            $headers = [
                'Authorization' => 'Bearer ' . $token,
                'Accept' => 'application/json',
            ];

            $url = $this->api_url . '/email/public/v1/subscriptions';

            //get all Email Subscriptions data
            $response = $client->request('GET', $url, ['headers' => $headers]);


            $body = $response->getBody();
            $data = json_decode($body, true);

            if (!empty($data['status']) == 'error') {
                $status = $data['status'];
                $error_msg = $data['message'];
                Log::error('Email Subscriptions data  |||| Error - ' . $error_msg);
            } else {
                $status = 'successed';

                $all_results = $data['subscriptionDefinitions'];

                //turn data into collection
                $results_collection = collect($all_results);
                $results_chunks = $results_collection->chunk(500); //chunk into smaller pieces

                $create_email_subscription = [];
                foreach ($results_chunks as $results_chunk) {

                    $results = $results_chunk->toArray();

                    if (!empty($results)) {
                        foreach ($results as $value) {

                            //get all email subscriptions ids
                            $hubspot_email_subscription_ids[] = $value['id'];



                            $check_subscription = EmailSubscription::where('user_id', $user->id)
                                            ->where('email_subscription_id', $value['id'])
                                            ->where('backup_progress_id', $backup_progress['id'] - 1)->first();

                            $create_email_subscription[] = [
                                'user_id' => $user->id,
                                'backup_progress_id' => $backup_progress['id'],
                                'email_subscription_id' => $value['id'],
                                'portal_id' => isset($value['portalId']) ? $value['portalId'] : 0,
                                'name' => isset($value['name']) ? $value['name'] : "",
                                'active' => isset($value['active']) ? $value['active'] : 0,
                                'description' => isset($value['description']) ? $value['description'] : "",
                                'internal' => isset($value['internal']) ? $value['internal'] : "",
                                'category' => isset($value['category']) ? $value['category'] : "",
                                'channel' => isset($value['channel']) ? $value['channel'] : "",
                                'internal_name' => isset($value['internalName']) ? $value['internalName'] : "",
                                'created_at' => Carbon::now()->toDateTimeString(),
                                'updated_at' => Carbon::now()->toDateTimeString()
                            ];



                            //check if email subscription  is already inserted or not
                            if ($check_subscription === null) {
                                
                            } else {

                                //check if any data is changed
                                if ($check_subscription->name != $value['name'] || $check_subscription->active != $value['active'] ||
                                        $check_subscription->description != $value['description'] || $check_subscription->internal != $value['internal']) {

                                    $updated_rows +=1;
                                }
                            }
                        }
                        //insert email subscriptions in bulk
                        EmailSubscription::insert($create_email_subscription);
                        $created_rows += count($create_email_subscription);
                    }
                }
            }
            //for email subscription status
            $subscription_status_url = $this->api_url . '/email/public/v1/subscriptions/' . $user->email;

            //get status details
            $response_subscription_status = $client->request('GET', $subscription_status_url, ['headers' => $headers]);
            $body_subscription_status = $response_subscription_status->getBody();
            $data_subscription_status = json_decode($body_subscription_status, true);

            if (!empty($data_subscription_status)) {
                $check_status = EmailStatus::where('email', $user->email)->first();


                if ($check_status === null) {

                    $status = EmailStatus::create([
                                'user_id' => $user->id,
                                'portal_id' => isset($data_subscription_status['portalId']) ? $data_subscription_status['portalId'] : "",
                                'email' => isset($data_subscription_status['email']) ? $data_subscription_status['email'] : "",
                                'status' => isset($data_subscription_status['status']) ? $data_subscription_status['status'] : "",
                                'subscribed' => isset($data_subscription_status['subscribed']) ? $data_subscription_status['subscribed'] : 0,
                                'marked_as_spam' => isset($data_subscription_status['markedAsSpam']) ? $data_subscription_status['markedAsSpam'] : 0,
                                'bounced' => isset($data_subscription_status['bounced']) ? $data_subscription_status['bounced'] : 0,
                                'subscription_statuses' => !empty($data_subscription_status['subscriptionStatuses']) ? $data_subscription_status['subscriptionStatuses'] : NULL,
                    ]);
                } else {
                    //check if any data is changed
                    if ($check_status->status != $data_subscription_status['status'] || $check_status->subscribed != $data_subscription_status['subscribed'] ||
                            $check_status->marked_as_spam != $data_subscription_status['markedAsSpam'] || $check_status->bounced != $data_subscription_status['bounced'] ||
                            $check_status->subscription_statuses != $data_subscription_status['subscriptionStatuses']) {


                        $check_status->update([
                            'status' => isset($data_subscription_status['status']) ? $data_subscription_status['status'] : "",
                            'subscribed' => isset($data_subscription_status['subscribed']) ? $data_subscription_status['subscribed'] : 0,
                            'marked_as_spam' => isset($data_subscription_status['markedAsSpam']) ? $data_subscription_status['markedAsSpam'] : 0,
                            'bounced' => isset($data_subscription_status['bounced']) ? $data_subscription_status['bounced'] : 0,
                            'subscription_statuses' => !empty($data_subscription_status['subscriptionStatuses']) ? $data_subscription_status['subscriptionStatuses'] : NULL,
                        ]);
                    }
                }
            }

            //end time
            $time_end = microtime(true);
            //execution time
            $execution_time = ($time_end - $time_start);

            Log::error('time taken for Email Subscriptions data ' . $execution_time);
            Log::error('Fetching Email Subscriptions data completed |||| End <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< userId - ' . $userId);
        } catch (BadResponseException $ex) {
            //if request is invalid
            $response = $ex->getResponse();
            $jsonBody = (string) $response->getBody();
            $this->error($jsonBody);

            $data = json_decode($response->getBody(), true);
            $status = $data['status'];
            $error_msg = $data['message'];
            Log::error('Email Subscriptions data  |||| Error - ' . $jsonBody);
        }

        //get email subscriptions from database
        $email_subscriptions_in_database = EmailSubscription::where('backup_progress_id', $backup_progress['id'] - 1)->get()->pluck('email_subscription_id')->toArray();

        //check if empty result 
        if (count($hubspot_email_subscription_ids) == 0) {
            $removed_rows = 0;
        } else {
            $removed_email_subscriptions = array_diff($email_subscriptions_in_database, $hubspot_email_subscription_ids);
            $removed_rows = count($removed_email_subscriptions);
        }

        $backup_progress = BackupProgress::where('user_id', $user['id'])->where('status', 'Inprogress')->orderByDesc('id')->first();

        //creating BackupHistory record
        $backup_history = BackupHistory::create([
                    'backup_progress_id' => $backup_progress['id'],
                    'user_id' => $user->id,
                    'object_name' => 'Fetched Email Subscriptions data',
                    'num_of_records' => $created_rows + $removed_rows,
                    'removed_records' => $removed_rows,
                    'added_records' => $created_rows,
                    'changed_records' => $updated_rows,
                    'num_of_API_calls' => 2,
                    'status' => $status,
                    'error_message' => $error_msg]);

        //get smart alerts for action 'added'
        $added_alert = SmartAlert::where('user_id', $user['id'])
                        ->where('s_object', 'Email Subscriptions')
                        ->where('action', 'Added')->first();

        if (!empty($added_alert)) {
            if ($created_rows > $added_alert->amount) {

                //notification when Email Subscription added
                $add_notificatoion = Notification::create(['user_id' => $user['id'], 'backup_progress_id' => $backup_progress['id'],
                            'status' => 'Email Subscriptions Added', 'type' => 'email', 'num_of_records' => $created_rows]);

                //create history
                SmartAlertHistory::create(['user_id' => $user['id'], 'smart_alert_id' => $added_alert->id]);

                //notify user
                //$user->notify(new Backup($notificatoion));
            }
        }

        //get smart alerts for action 'Changed'
        $changed_alert = SmartAlert::where('user_id', $user['id'])
                        ->where('s_object', 'Email Subscriptions')
                        ->where('action', 'Changed')->first();

        if (!empty($changed_alert)) {
            if ($updated_rows > $changed_alert->amount) {

                //notification when Email Subscription Changed
                $change_notificatoion = Notification::create(['user_id' => $user['id'], 'backup_progress_id' => $backup_progress['id'],
                            'status' => 'Email Subscriptions Changed', 'type' => 'email', 'num_of_records' => $updated_rows]);

                //create history
                SmartAlertHistory::create(['user_id' => $user['id'], 'smart_alert_id' => $changed_alert->id]);

                //notify user
                //$user->notify(new Backup($notificatoion));
            }
        }

        //get smart alerts for action 'Removed'
        $removed_alert = SmartAlert::where('user_id', $user['id'])
                        ->where('s_object', 'Email Subscriptions')
                        ->where('action', 'Removed')->first();

        if (!empty($removed_alert)) {
            if ($removed_rows > $removed_alert->amount) {

                //notification when Email Subscription Removed
                $remove_notificatoion = Notification::create(['user_id' => $user['id'], 'backup_progress_id' => $backup_progress['id'],
                            'status' => 'Email Subscriptions Removed', 'type' => 'email', 'num_of_records' => $removed_rows]);

                //create history
                SmartAlertHistory::create(['user_id' => $user['id'], 'smart_alert_id' => $removed_alert->id]);

                //notify user
                //$user->notify(new Backup($notificatoion));
            }
        }
    }

}
