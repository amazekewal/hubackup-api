<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\BadResponseException;
use Illuminate\Support\Facades\Artisan;
// models
use App\CmsBlogAuthor;
use App\BackupHistory;
use App\User;
use Illuminate\Support\Facades\Log;
use App\BackupProgress;
use App\Notifications\Backup;
use App\Notification;
use App\SmartAlert;
use App\SmartAlertHistory;
use Carbon\Carbon;

class CmsBlogAuthorsData extends Command {

    /**
     * The name of the apiUrl
     * @var type 
     */
    protected $api_url;

    /**
     * The name of the apiKey
     * @var type 
     */
    protected $api_key;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cms-blog-authors:data {userId}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command will get all CMS Blog Authors data';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();

        //get api key from env file
        $this->api_url = env('HUBSPOT_URL');
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        $this->info('CMS Blog Authors Data cron started....' . $this->api_url);

        $status = '';
        $error_msg = '';
        $created_rows = 0;
        $updated_rows = 0;
        $hubspot_blog_author_ids = [];

        $userId = $this->argument('userId');
        $user = User::find($userId);

        $backup_progress = BackupProgress::where('user_id', $user['id'])->where('status', 'Inprogress')->orderByDesc('id')->first();

        try {

            Log::error('Fetching CMS Blog Authors data started |||| Start >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> userId - ' . $userId);

            //start time
            $time_start = microtime(true);


            //user token
            $token = $user->accesstoken;

            //using GuzzleHttp client to get request
            $client = new \GuzzleHttp\Client(['http_errors' => false]);
            $headers = [
                'Authorization' => 'Bearer ' . $token,
                'Accept' => 'application/json',
            ];

            $get_data = $this->getCmsBlogAuthorsData($headers, 0, $client, $backup_progress, $user, $hubspot_blog_author_ids, $status, $error_msg, $created_rows, $updated_rows);

            $created_rows = $get_data['created_rows'];
            $updated_rows = $get_data['updated_rows'];
            $hubspot_blog_author_ids = $get_data['hubspot_blog_author_ids'];
            $error_msg = $get_data['error_msg'];
            $status = $get_data['status'];


            //end time
            $time_end = microtime(true);
            //execution time
            $execution_time = ($time_end - $time_start);

            Log::error('time taken for CMS Blog Authors data ' . $execution_time);
            Log::error('Fetching CMS Blog Authors data completed |||| End <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< userId - ' . $userId);
        } catch (BadResponseException $ex) {
            //if request is invalid
            $response = $ex->getResponse();
            $jsonBody = (string) $response->getBody();
            $this->error($jsonBody);

            $data = json_decode($response->getBody(), true);
            $status = $data['status'];
            $error_msg = $data['message'];
            Log::error('CMS Blog Authors data  |||| Error - ' . $jsonBody);
        }

        //get blog authors from database
        $blog_authors_in_database = CmsBlogAuthor::where('backup_progress_id', $backup_progress['id'] - 1)->get()->pluck('blog_author_id')->toArray();

        //check if empty result 
        if (count($hubspot_blog_author_ids) == 0) {
            $removed_rows = 0;
        } else {
            $removed_blog_authors = array_diff($blog_authors_in_database, $hubspot_blog_author_ids);
            $removed_rows = count($removed_blog_authors);
        }

        if (!empty($backup_progress)) {
            //creating BackupHistory record
            $backup_history = BackupHistory::create([
                        'backup_progress_id' => $backup_progress['id'],
                        'user_id' => $user->id,
                        'object_name' => 'Fetched CMS Blog Authors data',
                        'num_of_records' => $created_rows + $removed_rows,
                        'removed_records' => $removed_rows,
                        'added_records' => $created_rows,
                        'changed_records' => $updated_rows,
                        'num_of_API_calls' => 1,
                        'status' => $status,
                        'error_message' => $error_msg]);
        }

        //get smart alerts for action 'added'
        $added_alert = SmartAlert::where('user_id', $user['id'])
                        ->where('s_object', 'CMS Blog Authors')
                        ->where('action', 'Added')->first();

        if (!empty($added_alert)) {
            if ($created_rows > $added_alert->amount) {

                //notification when CMS Blog Author added
                $add_notificatoion = Notification::create(['user_id' => $user['id'],
                            'backup_progress_id' => $backup_progress['id'],
                            'status' => 'CMS Blog Authors Added',
                            'type' => 'email',
                            'num_of_records' => $created_rows]);

                //create history
                SmartAlertHistory::create(['user_id' => $user['id'], 'smart_alert_id' => $added_alert->id]);

                //notify user
                //$user->notify(new Backup($notificatoion));
            }
        }

        //get smart alerts for action 'Changed'
        $changed_alert = SmartAlert::where('user_id', $user['id'])
                        ->where('s_object', 'CMS Blog Authors')
                        ->where('action', 'Changed')->first();

        if (!empty($changed_alert)) {
            if ($updated_rows > $changed_alert->amount) {

                //notification when Blog Author Changed
                $change_notificatoion = Notification::create(['user_id' => $user['id'],
                            'backup_progress_id' => $backup_progress['id'],
                            'status' => 'CMS Blog Authors Changed',
                            'type' => 'email',
                            'num_of_records' => $updated_rows]);

                //create history
                SmartAlertHistory::create(['user_id' => $user['id'], 'smart_alert_id' => $changed_alert->id]);

                //notify user
                //$user->notify(new Backup($notificatoion));
            }
        }

        //get smart alerts for action 'Removed'
        $removed_alert = SmartAlert::where('user_id', $user['id'])
                        ->where('s_object', 'CMS Blog Authors')
                        ->where('action', 'Removed')->first();

        if (!empty($removed_alert)) {
            if ($removed_rows > $removed_alert->amount) {

                //notification when CMS Blog Author Removed
                $remove_notificatoion = Notification::create(['user_id' => $user['id'],
                            'backup_progress_id' => $backup_progress['id'],
                            'status' => 'CMS Blog Authors Removed',
                            'type' => 'email',
                            'num_of_records' => $removed_rows]);

                //create history
                SmartAlertHistory::create(['user_id' => $user['id'], 'smart_alert_id' => $removed_alert->id]);

                //notify user
                //$user->notify(new Backup($notificatoion));
            }
        }
    }

    public function getCmsBlogAuthorsData($headers, $offset, $client, $backup_progress, $user, $hubspot_blog_author_ids, $status, $error_msg, $created_rows, $updated_rows) {

        $url = $this->api_url . '/blogs/v3/blog-authors?limit=100&offset=' . $offset;

        //get all cms blog Authors data
        $response = $client->request('GET', $url, ['headers' => $headers]);


        $body = $response->getBody();
        $data = json_decode($body, true);


        if (!empty($data['status']) == 'error') {
            $status = $data['status'];
            $error_msg = $data['message'];
            Log::error('CMS Authors data  |||| Error - ' . $error_msg);
        } else {
            $results = $data['objects'];
            $status = 'successed';
            if (!empty($results)) {
                $create_blog_authors = [];
                foreach ($results as $value) {

                    //get all blog author ids
                    $hubspot_blog_author_ids[] = $value['id'];

                    $check_blog_author = CmsBlogAuthor::where('user_id', $user->id)
                                    ->where('blog_author_id', $value['id'])
                                    ->where('backup_progress_id', $backup_progress['id'] - 1)->first();

                    $create_blog_authors[] = [
                        'user_id' => $user->id,
                        'backup_progress_id' => $backup_progress['id'],
                        'blog_author_id' => $value['id'],
                        'portal_id' => isset($value['portalId']) ? $value['portalId'] : 0,
                        'created' => isset($value['created']) ? Carbon::createFromTimestampMs($value['created'])->format('Y-m-d H:i:s') : NULL,
                        'updated' => isset($value['updated']) ? Carbon::createFromTimestampMs($value['updated'])->format('Y-m-d H:i:s') : NULL,
                        'deleted_at' => isset($value['deleted_at']) ? $value['deleted_at'] : 0,
                        'full_name' => isset($value['full_name']) ? $value['full_name'] : "",
                        'email' => isset($value['email']) ? $value['email'] : "",
                        'user_name' => isset($value['user_name']) ? $value['user_name'] : "",
                        'slug' => isset($value['slug']) ? $value['slug'] : "",
                        'display_name' => isset($value['display_name']) ? $value['display_name'] : "",
                        'google_plus' => isset($value['google_plus']) ? $value['google_plus'] : "",
                        'bio' => isset($value['bio']) ? $value['bio'] : "",
                        'website' => isset($value['website']) ? $value['website'] : "",
                        'twitter' => isset($value['twitter']) ? $value['twitter'] : "",
                        'linkedin' => isset($value['linkedin']) ? $value['linkedin'] : "",
                        'facebook' => isset($value['facebook']) ? $value['facebook'] : "",
                        'avatar' => isset($value['avatar']) ? $value['avatar'] : "",
                        'gravatar_url' => isset($value['gravatar_url']) ? $value['gravatar_url'] : "",
                        'twitter_username' => isset($value['twitter_username']) ? $value['twitter_username'] : "",
                        'has_social_profiles' => isset($value['has_social_profiles']) ? $value['has_social_profiles'] : 0,
                        'created_at' => Carbon::now()->toDateTimeString(),
                        'updated_at' => Carbon::now()->toDateTimeString()
                    ];

                    //check if blog author  is already inserted or not
                    if ($check_blog_author === null) {
                        
                    } else {

                        $last_updated_date = Carbon::createFromTimestampMs($value['updated'])->format('Y-m-d H:i:s');

                        //check if any data is changed
                        if ($last_updated_date != $check_blog_author->updated) {

                            $updated_rows +=1;
                        }
                    }
                }
                //insert Cms Blog Author in bulk
                CmsBlogAuthor::insert($create_blog_authors);
                $created_rows += count($create_blog_authors);
            }
        }
        if (isset($data['offset']) && !empty($data['offset'] !== 0)) {

            return $this->getCmsBlogAuthorsData($headers, $data['offset'], $client, $backup_progress, $user, $hubspot_blog_author_ids, $status, $error_msg, $created_rows, $updated_rows);
        } else {

            $data = ['created_rows' => $created_rows, 'updated_rows' => $updated_rows,
                'hubspot_blog_author_ids' => $hubspot_blog_author_ids, 'status' => $status, 'error_msg' => $error_msg];

            return $data;
        }
    }

}
