<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\BadResponseException;
use Illuminate\Support\Facades\Artisan;
// models
use App\Ticket;
use App\TicketAssociatedContact;
use App\TicketAssociatedCompany;
use App\TicketAssociatedDeal;
use App\TicketAssociatedEngagement;
use App\Engagement;
use App\TaskEngagement;
use App\NoteEngagement;
use App\NoteAttachment;
use App\EmailEngagement;
use App\CallEngagement;
use App\MeetingEngagement;
use App\BackupHistory;
use App\User;
use Illuminate\Support\Facades\Log;
use App\BackupProgress;
use App\Notifications\Backup;
use App\Notification;
use App\SmartAlert;
use App\SmartAlertHistory;
use Carbon\Carbon;

class TicketsData extends Command {

    /**
     * The name of the apiUrl
     * @var type 
     */
    protected $api_url;

    /**
     * The name of the apiKey
     * @var type 
     */
    protected $api_key;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'tickets:data {userId}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command will get all Tickets data';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
        //get api key from env file
        $this->api_url = env('HUBSPOT_URL');
        $this->api_key = env('HUBSPOT_KEY');
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {

        $this->info('Tickets Data cron started....' . $this->api_url);

        $status = '';
        $error_msg = '';
        $created_rows = 0;
        $updated_rows = 0;
        $hubspot_ticket_ids = [];

        $userId = $this->argument('userId');
        $user = User::find($userId);

        $backup_progress = BackupProgress::where('user_id', $user['id'])->where('status', 'Inprogress')->orderByDesc('id')->first();


        try {
            Log::error('Fetching Tickets data started |||| Start >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> userId - ' . $userId);

            //start time
            $time_start = microtime(true);


            //user token
            $token = $user->accesstoken;

            //using GuzzleHttp client to get request
            $client = new \GuzzleHttp\Client(['http_errors' => false]);
            $headers = [
                'Authorization' => 'Bearer ' . $token,
                'Accept' => 'application/json',
            ];

            $get_data = $this->getTicketsData($headers, 0, $client, $backup_progress, $user, $hubspot_ticket_ids, $status, $error_msg, $created_rows, $updated_rows);

            $created_rows = $get_data['created_rows'];
            $updated_rows = $get_data['updated_rows'];
            $hubspot_ticket_ids = $get_data['hubspot_ticket_ids'];
            $error_msg = $get_data['error_msg'];
            $status = $get_data['status'];

            //end time
            $time_end = microtime(true);

            //execution time
            $execution_time = ($time_end - $time_start);

            Log::error('time taken for Tickets data ' . $execution_time);
            Log::error('Fetching Tickets data completed |||| End <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< userId - ' . $userId);
        } catch (BadResponseException $ex) {
            //if request is invalid
            $response = $ex->getResponse();
            $jsonBody = (string) $response->getBody();
            $this->error($jsonBody);

            $data = json_decode($response->getBody(), true);
            $status = $data['status'];
            $error_msg = $data['message'];
            Log::error('Tickets data  |||| Error - ' . $jsonBody);
        }

        //get tickets from database
        $tickets_in_database = Ticket::where('backup_progress_id', $backup_progress['id'] - 1)->get()->pluck('ticket_id')->toArray();
        
        //check if empty result 
        if (count($hubspot_ticket_ids) == 0) {
            $removed_rows = 0;
        } else {
        $removed_tickets = array_diff($tickets_in_database, $hubspot_ticket_ids);
        $removed_rows = count($removed_tickets);
        }

        //creating BackupHistory record
        $backup_history = BackupHistory::create([
                    'backup_progress_id' => $backup_progress['id'],
                    'user_id' => $user->id,
                    'object_name' => 'Fetched Tickets data',
                    'num_of_records' => $created_rows + $removed_rows,
                    'removed_records' => $removed_rows,
                    'added_records' => $created_rows,
                    'changed_records' => $updated_rows,
                    'num_of_API_calls' => 2,
                    'status' => $status,
                    'error_message' => $error_msg]);


        //get smart alerts for action 'added'
        $added_alert = SmartAlert::where('user_id', $user['id'])
                        ->where('s_object', 'Tickets')
                        ->where('action', 'Added')->first();

        if (!empty($added_alert)) {
            if ($created_rows > $added_alert->amount) {

                //notification when Ticket added
                $add_notificatoion = Notification::create(['user_id' => $user['id'], 'backup_progress_id' => $backup_progress['id'],
                            'status' => 'Tickets Added', 'type' => 'email', 'num_of_records' => $created_rows]);

                //create history
                SmartAlertHistory::create(['user_id' => $user['id'], 'smart_alert_id' => $added_alert->id]);

                //notify user
                //$user->notify(new Backup($notificatoion));
            }
        }
        //get smart alerts for action 'Changed'
        $changed_alert = SmartAlert::where('user_id', $user['id'])
                        ->where('s_object', 'Tickets')
                        ->where('action', 'Changed')->first();

        if (!empty($changed_alert)) {
            if ($updated_rows > $changed_alert->amount) {

                //notification when Deal Changed
                $change_notificatoion = Notification::create(['user_id' => $user['id'], 'backup_progress_id' => $backup_progress['id'],
                            'status' => 'Tickets Changed', 'type' => 'email', 'num_of_records' => $updated_rows]);

                //create history
                SmartAlertHistory::create(['user_id' => $user['id'], 'smart_alert_id' => $changed_alert->id]);

                //notify user
                //$user->notify(new Backup($notificatoion));
            }
        }

        //get smart alerts for action 'Removed'
        $removed_alert = SmartAlert::where('user_id', $user['id'])
                        ->where('s_object', 'Tickets')
                        ->where('action', 'Removed')->first();

        if (!empty($removed_alert)) {
            if ($removed_rows > $removed_alert->amount) {


                //notification when Ticket Removed
                $remove_notificatoion = Notification::create(['user_id' => $user['id'], 'backup_progress_id' => $backup_progress['id'],
                            'status' => 'Tickets Removed', 'type' => 'email', 'num_of_records' => $removed_rows]);

                //create history
                SmartAlertHistory::create(['user_id' => $user['id'], 'smart_alert_id' => $removed_alert->id]);

                //notify user
                //$user->notify(new Backup($notificatoion));
            }
        }
    }

    public function getTicketsData($headers, $offset, $client, $backup_progress, $user, $hubspot_ticket_ids, $status, $error_msg, $created_rows, $updated_rows) {

        $url = $this->api_url . '/crm-objects/v1/objects/tickets/paged?offset=' . $offset;

        //get all tickets data
        $response = $client->request('GET', $url, ['headers' => $headers]);


        $body = $response->getBody();
        $data = json_decode($body, true);


        if (!empty($data['status']) == 'error') {
            $status = $data['status'];
            $error_msg = $data['message'];
            Log::error('Tickets data  |||| Error - ' . $error_msg);
        } else {
            $results = $data['objects'];
            $status = 'successed';
            if (!empty($results)) {
                foreach ($results as $value) {

                    //get all ticket ids
                    $hubspot_ticket_ids[] = $value['objectId'];


                    $property_text = "";

                    $properties = ['subject',
                        'content',
                        'created_by',
                        'hs_pipeline',
                        'hs_pipeline_stage',
                        'status',
                        'source_type',
                        'createdate',
                        'hs_lastmodifieddate',
                        'closedate'];
                    //last value
                    $last_value = end($properties);
                    $create_tickets = [];
                    foreach ($properties as $property) {

                        //check if value is not last value
                        if ($property != $last_value) {
                            $property_text .= 'properties=' . $property . '&';
                        } else {
                            $property_text .= 'properties=' . $property;
                        }
                    }
                    $ticket_meta_url = $this->api_url . '/crm-objects/v1/objects/tickets/' . $value['objectId'] . '&' . $property_text;

                    //get complete details of each ticket
                    $response_ticket_meta = $client->request('GET', $ticket_meta_url, ['headers' => $headers]);
                    $body_ticket_meta = $response_ticket_meta->getBody();
                    $data_ticket_meta = json_decode($body_ticket_meta, true);

                    if (!empty($data_ticket_meta['status']) == 'error') {
                        $status = 'warning';
                        $error_msg = $data_ticket_meta['message'];
                        Log::error('Tickets data  |||| Warning - ' . $error_msg);
                    } else {
                        $check_ticket = Ticket::where('user_id', $user->id)
                                        ->where('ticket_id', $data_ticket_meta['objectId'])
                                        ->where('backup_progress_id', $backup_progress['id'] - 1)->first();


                        $create_tickets[] = [
                            'user_id' => $user->id,
                            'backup_progress_id' => $backup_progress['id'],
                            'ticket_id' => $data_ticket_meta['objectId'],
                            'subject' => isset($data_ticket_meta['properties']['subject']['value']) ? $data_ticket_meta['properties']['subject']['value'] : '',
                            'pipeline' => isset($data_ticket_meta['properties']['hs_pipeline']['value']) ? $data_ticket_meta['properties']['hs_pipeline']['value'] : '',
                            'pipeline_stage' => isset($data_ticket_meta['properties']['hs_pipeline_stage']['value']) ? $data_ticket_meta['properties']['hs_pipeline_stage']['value'] : '',
                            'content' => isset($data_ticket_meta['properties']['content']['value']) ? $data_ticket_meta['properties']['content']['value'] : '',
                            'created_by' => isset($data_ticket_meta['properties']['created_by']['value']) ? $data_ticket_meta['properties']['created_by']['value'] : '',
                            'source_type' => isset($data_ticket_meta['properties']['source_type']['value']) ? $data_ticket_meta['properties']['source_type']['value'] : '',
                            'status' => isset($data_ticket_meta['properties']['status']['value']) ? $data_ticket_meta['properties']['status']['value'] : '',
                            'createdate' => isset($data_ticket_meta['properties']['createdate']['value']) ? Carbon::createFromTimestampMs($data_ticket_meta['properties']['createdate']['value'])->format('Y-m-d H:i:s') : NULL,
                            'last_modified_date' => isset($data_ticket_meta['properties']['hs_lastmodifieddate']['value']) ? Carbon::createFromTimestampMs($data_ticket_meta['properties']['hs_lastmodifieddate']['value'])->format('Y-m-d H:i:s') : NULL,
                            'closedate' => isset($data_ticket_meta['properties']['closedate']['value']) ? Carbon::createFromTimestampMs($data_ticket_meta['properties']['closedate']['value'])->format('Y-m-d H:i:s') : NULL,
                            'created_at' => Carbon::now()->toDateTimeString(),
                            'updated_at' => Carbon::now()->toDateTimeString()
                        ];
                        //check if ticket is already inserted or not

                        if ($check_ticket === null) {
                            
                        } else {
                            $hs_lastmodifieddate = Carbon::createFromTimestampMs($data_ticket_meta['properties']['hs_lastmodifieddate']['value'])->format('Y-m-d H:i:s');

                            //check if any data is changed
                            if ($hs_lastmodifieddate != $check_ticket->last_modified_date) {
                                $updated_rows +=1;
                            }
                        }
                    }
                }

                //insert tickets in bulk
                Ticket::insert($create_tickets);
                $created_rows += count($create_tickets);
            }
        }

        if (isset($data['hasMore']) && !empty($data['hasMore'] == true)) {

            return $this->getTicketsData($headers, $data['offset'], $client, $backup_progress, $user, $hubspot_ticket_ids, $status, $error_msg, $created_rows, $updated_rows);
        } else {

            $data = ['created_rows' => $created_rows, 'updated_rows' => $updated_rows,
                'hubspot_ticket_ids' => $hubspot_ticket_ids, 'status' => $status, 'error_msg' => $error_msg];

            return $data;
        }
    }

}
