<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\BadResponseException;
use Illuminate\Support\Facades\Artisan;
// models
use App\CmsBlogPost;
use App\CmsBlogPostWidget;
use App\BackupHistory;
use App\User;
use Illuminate\Support\Facades\Log;
use App\BackupProgress;
use App\Notifications\Backup;
use App\Notification;
use App\SmartAlert;
use App\SmartAlertHistory;
use Carbon\Carbon;

class CmsBlogPostsData extends Command {

    /**
     * The name of the apiUrl
     * @var type 
     */
    protected $api_url;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cms-blog-posts:data {userId}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command will get all CMS Blog Posts data';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();

        //get api key from env file
        $this->api_url = env('HUBSPOT_URL');
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        $this->info('CMS Blog Posts Data cron started....' . $this->api_url);

        $status = '';
        $error_msg = '';
        $created_rows = 0;
        $updated_rows = 0;
        $hubspot_blog_post_ids = [];

        $userId = $this->argument('userId');
        $user = User::find($userId);

        $backup_progress = BackupProgress::where('user_id', $user['id'])->where('status', 'Inprogress')->orderByDesc('id')->first();

        try {

            Log::error('Fetching CMS Blog Posts data started |||| Start >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> userId - ' . $userId);

            //start time
            $time_start = microtime(true);


            //user token
            $token = $user->accesstoken;

            //using GuzzleHttp client to get request
            $client = new \GuzzleHttp\Client(['http_errors' => false]);
            $headers = [
                'Authorization' => 'Bearer ' . $token,
                'Accept' => 'application/json',
            ];

            $get_data = $this->getCmsBlogPostsData($headers, 0, $client, $backup_progress, $user, $hubspot_blog_post_ids, $status, $error_msg, $created_rows, $updated_rows);

            $created_rows = $get_data['created_rows'];
            $updated_rows = $get_data['updated_rows'];
            $hubspot_blog_post_ids = $get_data['hubspot_blog_post_ids'];
            $error_msg = $get_data['error_msg'];
            $status = $get_data['status'];

            //end time
            $time_end = microtime(true);
            //execution time
            $execution_time = ($time_end - $time_start);

            Log::error('time taken for  CMS Blog Posts data ' . $execution_time);
            Log::error('Fetching CMS Blog Posts data completed |||| End <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< userId - ' . $userId);
        } catch (BadResponseException $ex) {
            //if request is invalid
            $response = $ex->getResponse();
            $jsonBody = (string) $response->getBody();
            $this->error($jsonBody);

            $data = json_decode($response->getBody(), true);
            $status = $data['status'];
            $error_msg = $data['message'];
            Log::error('CMS Blog Posts data  |||| Error - ' . $jsonBody);
        }

        //get CMS Blog Posts from database
        $cms_blog_post_in_database = CmsBlogPost::where('backup_progress_id', $backup_progress['id'] - 1)->get()->pluck('blog_post_id')->toArray();

        //check if empty result 
        if (count($hubspot_blog_post_ids) == 0) {
            $removed_rows = 0;
        } else {
            $removed_cms_blog_posts = array_diff($cms_blog_post_in_database, $hubspot_blog_post_ids);
            $removed_rows = count($removed_cms_blog_posts);
        }

        //creating BackupHistory record
        $backup_history = BackupHistory::create([
                    'backup_progress_id' => $backup_progress['id'],
                    'user_id' => $user->id,
                    'object_name' => 'Fetched CMS Blog Posts data',
                    'num_of_records' => $created_rows + $removed_rows,
                    'removed_records' => $removed_rows,
                    'added_records' => $created_rows,
                    'changed_records' => $updated_rows,
                    'num_of_API_calls' => 2,
                    'status' => $status,
                    'error_message' => $error_msg]);

        //get smart alerts for action 'added'
        $added_alert = SmartAlert::where('user_id', $user['id'])
                        ->where('s_object', 'CMS Blog Posts')
                        ->where('action', 'Added')->first();

        if (!empty($added_alert)) {
            if ($created_rows > $added_alert->amount) {

                //notification when CMS Blog Post added
                $add_notificatoion = Notification::create(['user_id' => $user['id'],
                            'backup_progress_id' => $backup_progress['id'],
                            'status' => 'CMS Blog Posts Added',
                            'type' => 'email',
                            'num_of_records' => $created_rows]);

                //create history
                SmartAlertHistory::create(['user_id' => $user['id'], 'smart_alert_id' => $added_alert->id]);

                //notify user
                //$user->notify(new Backup($notificatoion));
            }
        }

        //get smart alerts for action 'Changed'
        $changed_alert = SmartAlert::where('user_id', $user['id'])
                        ->where('s_object', 'CMS Blog Posts')
                        ->where('action', 'Changed')->first();

        if (!empty($changed_alert)) {
            if ($updated_rows > $changed_alert->amount) {



                //notification when CMS  Blog Posts Changed
                $change_notificatoion = Notification::create(['user_id' => $user['id'],
                            'backup_progress_id' => $backup_progress['id'],
                            'status' => 'CMS Blog Posts Changed',
                            'type' => 'email',
                            'num_of_records' => $updated_rows]);

                //create history
                SmartAlertHistory::create(['user_id' => $user['id'], 'smart_alert_id' => $changed_alert->id]);

                //notify user
                //$user->notify(new Backup($notificatoion));
            }
        }

        //get smart alerts for action 'Removed'
        $removed_alert = SmartAlert::where('user_id', $user['id'])
                        ->where('s_object', 'CMS Blog Posts')
                        ->where('action', 'Removed')->first();

        if (!empty($removed_alert)) {
            if ($removed_rows > $removed_alert->amount) {

                //notification when CMS Blog Post Removed
                $remove_notificatoion = Notification::create(['user_id' => $user['id'],
                            'backup_progress_id' => $backup_progress['id'],
                            'status' => 'CMS Blog Posts Removed',
                            'type' => 'email',
                            'num_of_records' => $removed_rows]);

                //create history
                SmartAlertHistory::create(['user_id' => $user['id'], 'smart_alert_id' => $removed_alert->id]);

                //notify user
                //$user->notify(new Backup($notificatoion));
            }
        }
    }

    public function getCmsBlogPostsData($headers, $offset, $client, $backup_progress, $user, $hubspot_blog_post_ids, $status, $error_msg, $created_rows, $updated_rows) {

        $url = $this->api_url . '/content/api/v2/blog-posts?limit=100&offset=' . $offset;

        //get all cms Blog Posts data
        $response = $client->request('GET', $url, ['headers' => $headers]);


        $body = $response->getBody();
        $data = json_decode($body, true);


        if (!empty($data['status']) == 'error') {
            $status = $data['status'];
            $error_msg = $data['message'];
            Log::error('CMS Blog Posts data  |||| Error - ' . $error_msg);
        } else {
            $results = $data['objects'];
            $status = 'successed';

            if (!empty($results)) {
                foreach ($results as $value) {

                    //get all CMS Blog Posts  ids
                    $hubspot_blog_post_ids[] = $value['id'];


                    $check_post = CmsBlogPost::where('user_id', $user->id)
                                    ->where('blog_post_id', $value['id'])
                                    ->where('backup_progress_id', $backup_progress['id'] - 1)->first();

                    $post_meta_url = $this->api_url . '/content/api/v2/blog-posts/' . $value['id'];

                    //get complete details of each post
                    $response_post_meta = $client->request('GET', $post_meta_url, ['headers' => $headers]);
                    $body_post_meta = $response_post_meta->getBody();
                    $data_post_meta = json_decode($body_post_meta, true);

                    if (!empty($data_post_meta['status']) == 'error') {
                        $status = 'warning';
                        $error_msg = $data_post_meta['message'];
                        Log::error('CMS Blog Posts data  |||| Warning - ' . $error_msg);
                    } else {
                        $cms_blog_post = CmsBlogPost::create([
                                    'user_id' => $user->id,
                                    'backup_progress_id' => $backup_progress['id'],
                                    'blog_post_id' => $value['id'],
                                    'ab' => isset($value['ab']) ? $value['ab'] : 0,
                                    'ab_variation' => isset($value['ab_variation']) ? $value['ab_variation'] : 0,
                                    'absolute_url' => isset($value['absolute_url']) ? $value['absolute_url'] : "",
                                    'analytics_page_id' => isset($value['analytics_page_id']) ? $value['analytics_page_id'] : 0,
                                    'analytics_page_type' => isset($value['analytics_page_type']) ? $value['analytics_page_type'] : "",
                                    'archived' => isset($value['archived']) ? $value['archived'] : 0,
                                    'are_comments_allowed' => isset($value['are_comments_allowed']) ? $value['are_comments_allowed'] : 0,
                                    'attached_stylesheets' => isset($value['attached_stylesheets']) ? $value['attached_stylesheets'] : NULL,
                                    'author' => isset($value['author']) ? $value['author'] : "",
                                    'author_at' => isset($value['author_at']) ? Carbon::createFromTimestampMs($value['author_at'])->format('Y-m-d H:i:s') : NULL,
                                    'author_email' => isset($value['author_email']) ? $value['author_email'] : "",
                                    'author_name' => isset($value['author_name']) ? $value['author_name'] : "",
                                    'author_user_id' => isset($value['author_user_id']) ? $value['author_user_id'] : 0,
                                    'author_username' => isset($value['author_username']) ? $value['author_username'] : "",
                                    'blog_author_id' => isset($value['blog_author_id']) ? $value['blog_author_id'] : 0,
                                    'blog_post_author_id' => isset($value['blog_post_author']['id']) ? $value['blog_post_author']['id'] : 0,
                                    'blueprint_type_id' => isset($value['blueprint_type_id']) ? $value['blueprint_type_id'] : 0,
                                    'campaign' => isset($value['campaign']) ? $value['campaign'] : "",
                                    'campaign_name' => isset($value['campaign_name']) ? $value['campaign_name'] : "",
                                    'category_id' => isset($value['category_id']) ? $value['category_id'] : 0,
                                    'comment_count' => isset($value['comment_count']) ? $value['comment_count'] : 0,
                                    'composition_id' => isset($value['composition_id']) ? $value['composition_id'] : 0,
                                    'content_group_id' => isset($value['content_group_id']) ? $value['content_group_id'] : 0,
                                    'content_type_category_id' => isset($value['content_type_category_id']) ? $value['content_type_category_id'] : 0,
                                    'created_by_id' => isset($value['created_by_id']) ? $value['created_by_id'] : 0,
                                    'created_time' => isset($value['created_time']) ? Carbon::createFromTimestampMs($value['created_time'])->format('Y-m-d H:i:s') : NULL,
                                    'css' => isset($value['css']) ? $value['css'] : NULL,
                                    'css_text' => isset($value['css_text']) ? $value['css_text'] : "",
                                    'ctas' => isset($value['ctas']) ? $value['ctas'] : "",
                                    'current_state' => isset($value['current_state']) ? $value['current_state'] : "",
                                    'currently_published' => isset($value['currently_published']) ? $value['currently_published'] : "",
                                    'deleted_at' => isset($value['deleted_at']) ? $value['deleted_at'] : 0,
                                    'domain' => isset($value['domain']) ? $value['domain'] : "",
                                    'enable_google_amp_output_override' => isset($value['enable_google_amp_output_override']) ? $value['enable_google_amp_output_override'] : "",
                                    'featured_image' => isset($value['featured_image']) ? $value['featured_image'] : "",
                                    'featured_image_alt_text' => isset($value['featured_image_alt_text']) ? $value['featured_image_alt_text'] : "",
                                    'featured_image_height' => isset($value['featured_image_height']) ? $value['featured_image_height'] : 0,
                                    'featured_image_length' => isset($value['featured_image_length']) ? $value['featured_image_length'] : 0,
                                    'featured_image_width' => isset($value['featured_image_width']) ? $value['featured_image_width'] : 0,
                                    'flex_areas' => isset($value['flex_areas']) ? $value['flex_areas'] : NULL,
                                    'freeze_date' => isset($value['freeze_date']) ? Carbon::createFromTimestampMs($value['freeze_date'])->format('Y-m-d H:i:s') : NULL,
                                    'has_content_access_rules' => isset($value['has_content_access_rules']) ? $value['has_content_access_rules'] : 0,
                                    'has_user_changes' => isset($value['has_user_changes']) ? $value['has_user_changes'] : 0,
                                    'html_title' => isset($value['html_title']) ? $value['html_title'] : "",
                                    'is_captcha_required' => isset($value['is_captcha_required']) ? $value['is_captcha_required'] : 0,
                                    'is_draft' => isset($value['is_draft']) ? $value['is_draft'] : 0,
                                    'is_instant_email_enabled' => isset($value['is_instant_email_enabled']) ? $value['is_instant_email_enabled'] : 0,
                                    'is_published' => isset($value['is_published']) ? $value['is_published'] : 0,
                                    'is_social_publishing_enabled' => isset($value['is_social_publishing_enabled']) ? $value['is_social_publishing_enabled'] : 0,
                                    'keywords' => isset($value['keywords']) ? $value['keywords'] : NULL,
                                    'label' => isset($value['label']) ? $value['label'] : "",
                                    'layout_sections' => isset($value['layout_sections']) ? $value['layout_sections'] : NULL,
                                    'link_rel_canonical_url' => isset($value['link_rel_canonical_url']) ? $value['link_rel_canonical_url'] : "",
                                    'list_template' => isset($value['list_template']) ? $value['list_template'] : "",
                                    'live_domain' => isset($value['live_domain']) ? $value['live_domain'] : "",
                                    'mab' => isset($value['mab']) ? $value['mab'] : 0,
                                    'mab_master' => isset($value['mab_master']) ? $value['mab_master'] : 0,
                                    'mab_variant' => isset($value['mab_variant']) ? $value['mab_variant'] : 0,
                                    'meta_post_summary' => isset($value['meta_post_summary']) ? $value['meta_post_summary'] : "",
                                    'meta_last_edit_session_id' => isset($value['meta']['last_edit_session_id']) ? $value['meta']['last_edit_session_id'] : "",
                                    'meta_last_edit_update_id' => isset($value['meta']['last_edit_update_id']) ? $value['meta']['last_edit_update_id'] : "",
                                    'tag_ids' => isset($value['tag_ids']) ? $value['tag_ids'] : NULL,
                                    'topic_ids' => isset($value['topic_ids']) ? $value['topic_ids'] : NULL,
                                    'page_redirected' => isset($value['page_redirected']) ? $value['page_redirected'] : 0,
                                    'personas' => isset($value['personas']) ? $value['personas'] : NULL,
                                    'placement_guids' => isset($value['placement_guids']) ? $value['placement_guids'] : NULL,
                                    'public_access_rules' => isset($value['public_access_rules']) ? $value['public_access_rules'] : NULL,
                                    'public_access_rules_enabled' => isset($value['public_access_rules_enabled']) ? $value['public_access_rules_enabled'] : 0,
                                    'tweet_immediately' => isset($value['tweet_immediately']) ? $value['tweet_immediately'] : 0,
                                    'unpublished_at' => isset($value['unpublished_at']) ? Carbon::createFromTimestampMs($value['unpublished_at'])->format('Y-m-d H:i:s') : NULL,
                                    'use_featured_image' => isset($value['use_featured_image']) ? $value['use_featured_image'] : 0,
                                    'rss_summary' => isset($value['rss_summary']) ? $value['rss_summary'] : "",
                                    'blog_publish_to_social_media_task' => isset($value['blog_publish_to_social_media_task']) ? $value['blog_publish_to_social_media_task'] : "",
                                    'blog_post_schedule_task_uid' => isset($value['blog_post_schedule_task_uid']) ? $value['blog_post_schedule_task_uid'] : "",
                                    'meta_description' => isset($value['meta_description']) ? $value['meta_description'] : "",
                                    'name' => isset($value['name']) ? $value['name'] : "",
                                    'page_title' => isset($value['page_title']) ? $value['page_title'] : "",
                                    'parent_blog_id' => isset($value['parent_blog']['id']) ? $value['parent_blog']['id'] : 0,
                                    'past_mab_experiment_ids' => isset($value['past_mab_experiment_ids']) ? $value['past_mab_experiment_ids'] : NULL,
                                    'portal_id' => isset($value['portal_id']) ? $value['portal_id'] : 0,
                                    'post_body_rss' => isset($value['post_body_rss']) ? $value['post_body_rss'] : "",
                                    'post_email_content' => isset($value['post_email_content']) ? $value['post_email_content'] : "",
                                    'post_list_summary_featured_image' => isset($value['post_list_summary_featured_image']) ? $value['post_list_summary_featured_image'] : "",
                                    'post_rss_content' => isset($value['post_rss_content']) ? $value['post_rss_content'] : "",
                                    'post_template' => isset($value['post_template']) ? $value['post_template'] : "",
                                    'preview_key' => isset($value['preview_key']) ? $value['preview_key'] : "",
                                    'processing_status' => isset($value['processing_status']) ? $value['processing_status'] : "",
                                    'publish_date' => isset($value['publish_date']) ? Carbon::createFromTimestampMs($value['publish_date'])->format('Y-m-d H:i:s') : NULL,
                                    'publish_date_local_time' => isset($value['publish_date_local_time']) ? Carbon::createFromTimestampMs($value['publish_date_local_time'])->format('Y-m-d H:i:s') : NULL,
                                    'publish_date_local_format' => isset($value['publish_date_localized']['format']) ? $value['publish_date_localized']['format'] : "",
                                    'publish_immediately' => isset($value['publish_immediately']) ? $value['publish_immediately'] : 0,
                                    'published_at' => isset($value['published_at']) ? Carbon::createFromTimestampMs($value['published_at'])->format('Y-m-d H:i:s') : NULL,
                                    'published_by_email' => isset($value['published_by_email']) ? $value['published_by_email'] : "",
                                    'published_by_id' => isset($value['published_by_id']) ? $value['published_by_id'] : 0,
                                    'published_by_name' => isset($value['published_by_name']) ? $value['published_by_name'] : "",
                                    'published_url' => isset($value['published_url']) ? $value['published_url'] : "",
                                    'resolved_domain' => isset($value['resolved_domain']) ? $value['resolved_domain'] : "",
                                    'rss_body' => isset($value['rss_body']) ? $value['rss_body'] : "",
                                    'rss_summary_featured_image' => isset($value['rss_summary_featured_image']) ? $value['rss_summary_featured_image'] : "",
                                    'slug' => isset($value['slug']) ? $value['slug'] : "",
                                    'state' => isset($value['state']) ? $value['state'] : "",
                                    'subcategory' => isset($value['subcategory']) ? $value['subcategory'] : "",
                                    'synced_with_blog_root' => isset($value['synced_with_blog_root']) ? $value['synced_with_blog_root'] : 0,
                                    'tag_list' => isset($value['tag_list']) ? $value['tag_list'] : NULL,
                                    'tag_names' => isset($value['tag_names']) ? $value['tag_names'] : NULL,
                                    'team_perms' => isset($value['team_perms']) ? $value['team_perms'] : NULL,
                                    'template_path' => isset($value['template_path']) ? $value['template_path'] : "",
                                    'template_path_for_render' => isset($value['template_path_for_render']) ? $value['template_path_for_render'] : "",
                                    'title' => isset($value['title']) ? $value['title'] : "",
                                    'topic_list' => isset($value['topic_list']) ? $value['topic_list'] : NULL,
                                    'topic_names' => isset($value['topic_names']) ? $value['topic_names'] : NULL,
                                    'translated_content' => isset($value['translated_content']) ? $value['translated_content'] : NULL,
                                    'updated_by_id' => isset($value['updated_by_id']) ? $value['updated_by_id'] : 0,
                                    'upsize_featured_image' => isset($value['upsize_featured_image']) ? $value['upsize_featured_image'] : 0,
                                    'url' => isset($value['url']) ? $value['url'] : "",
                                    'views' => isset($value['views']) ? $value['views'] : 0,
                                    'user_perms' => isset($value['user_perms']) ? $value['user_perms'] : NULL,
                                    'widget_containers' => isset($value['widget_containers']) ? $value['widget_containers'] : NULL,
                                    'widgetcontainers' => isset($value['widgetcontainers']) ? $value['widgetcontainers'] : NULL,
                                    'created' => isset($data_post_meta['created']) ? Carbon::createFromTimestampMs($data_post_meta['created'])->format('Y-m-d H:i:s') : NULL,
                                    'updated' => isset($data_post_meta['updated']) ? Carbon::createFromTimestampMs($data_post_meta['updated'])->format('Y-m-d H:i:s') : NULL,
                        ]);

                        if (!empty($value['widgets'])) {
                            foreach ($value['widgets'] as $key => $widget) {

                                $cms_blog_post_widgets = CmsBlogPostWidget::create([
                                            'cms_blog_post_db_id' => $cms_blog_post->id,
                                            'body_html' => isset($widget['body']['html']) ? $widget['body']['html'] : "",
                                            'child_css' => isset($widget['child_css']) ? $widget['child_css'] : NULL,
                                            'css' => isset($widget['css']) ? $widget['css'] : NULL,
                                            'label' => isset($widget['label']) ? $widget['label'] : "",
                                            'name' => isset($key) ? $key : "",
                                            'order' => isset($widget['order']) ? $widget['order'] : 0,
                                            'smart_type' => isset($widget['smart_type']) ? $widget['smart_type'] : "",
                                            'type' => isset($widget['type']) ? $widget['type'] : "",
                                ]);
                            }
                        }
                        if ($cms_blog_post) {
                            $created_rows +=1;
                        }

                        //check if post  is already inserted or not
                        if ($check_post === null) {
                            
                        } else {

                            $last_updated_date = Carbon::createFromTimestampMs($value['updated'])->format('Y-m-d H:i:s');

                            //check if any data is changed
                            if ($last_updated_date != $check_post->updated) {

                                $updated_rows +=1;
                            }
                        }
                    }
                }
            }
        }
        if (isset($data['offset']) && !empty($data['offset'] !== 0)) {

            return $this->getCmsBlogPostsData($headers, $data['offset'], $client, $backup_progress, $user, $hubspot_blog_post_ids, $status, $error_msg, $created_rows, $updated_rows);
        } else {

            $data = ['created_rows' => $created_rows, 'updated_rows' => $updated_rows,
                'hubspot_blog_post_ids' => $hubspot_blog_post_ids, 'status' => $status, 'error_msg' => $error_msg];

            return $data;
        }
    }

}
