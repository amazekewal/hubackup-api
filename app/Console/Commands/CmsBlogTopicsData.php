<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\BadResponseException;
use Illuminate\Support\Facades\Artisan;
// models
use App\CmsBlogTopic;
use App\BackupHistory;
use App\User;
use Illuminate\Support\Facades\Log;
use App\BackupProgress;
use App\Notifications\Backup;
use App\Notification;
use App\SmartAlert;
use App\SmartAlertHistory;
use Carbon\Carbon;

class CmsBlogTopicsData extends Command {

    /**
     * The name of the apiUrl
     * @var type 
     */
    protected $api_url;

    /**
     * The name of the apiKey
     * @var type 
     */
    protected $api_key;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cms-blog-topics:data {userId}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command will get all CMS Blog Topics data';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();

        //get api key from env file
        $this->api_url = env('HUBSPOT_URL');
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        $this->info('CMS Blog Topics Data cron started....' . $this->api_url);

        $status = '';
        $error_msg = '';
        $created_rows = 0;
        $updated_rows = 0;
        $hubspot_blog_topic_ids = [];

        $userId = $this->argument('userId');
        $user = User::find($userId);

        $backup_progress = BackupProgress::where('user_id', $user['id'])->where('status', 'Inprogress')->orderByDesc('id')->first();

        try {

            Log::error('Fetching CMS Blog Topics data started |||| Start >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> userId - ' . $userId);

            //start time
            $time_start = microtime(true);


            //user token
            $token = $user->accesstoken;

            //using GuzzleHttp client to get request
            $client = new \GuzzleHttp\Client(['http_errors' => false]);
            $headers = [
                'Authorization' => 'Bearer ' . $token,
                'Accept' => 'application/json',
            ];

            $get_data = $this->getCmsBlogTopicsData($headers, 0, $client, $backup_progress, $user, $hubspot_blog_topic_ids, $status, $error_msg, $created_rows, $updated_rows);

            $created_rows = $get_data['created_rows'];
            $updated_rows = $get_data['updated_rows'];
            $hubspot_blog_topic_ids = $get_data['hubspot_blog_topic_ids'];
            $error_msg = $get_data['error_msg'];
            $status = $get_data['status'];

            //end time
            $time_end = microtime(true);
            //execution time
            $execution_time = ($time_end - $time_start);

            Log::error('time taken for CMS Blog Topics data ' . $execution_time);
            Log::error('Fetching CMS Blog Topics data completed |||| End <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< userId - ' . $userId);
        } catch (BadResponseException $ex) {
            //if request is invalid
            $response = $ex->getResponse();
            $jsonBody = (string) $response->getBody();
            $this->error($jsonBody);

            $data = json_decode($response->getBody(), true);
            $status = $data['status'];
            $error_msg = $data['message'];
            Log::error('CMS Blog Topics data  |||| Error - ' . $jsonBody);
        }

        //get blog topics from database
        $blog_topic_in_database = CmsBlogTopic::where('backup_progress_id', $backup_progress['id'] - 1)->get()->pluck('blog_topic_id')->toArray();

        //check if empty result 
        if (count($hubspot_blog_topic_ids) == 0) {
            $removed_rows = 0;
        } else {
            $removed_blog_topics = array_diff($blog_topic_in_database, $hubspot_blog_topic_ids);
            $removed_rows = count($removed_blog_topics);
        }

        //creating BackupHistory record
        $backup_history = BackupHistory::create([
                    'backup_progress_id' => $backup_progress['id'],
                    'user_id' => $user->id,
                    'object_name' => 'Fetched CMS Blog Topics data',
                    'num_of_records' => $created_rows + $removed_rows,
                    'removed_records' => $removed_rows,
                    'added_records' => $created_rows,
                    'changed_records' => $updated_rows,
                    'num_of_API_calls' => 2,
                    'status' => $status,
                    'error_message' => $error_msg]);

        //get smart alerts for action 'added'
        $added_alert = SmartAlert::where('user_id', $user['id'])
                        ->where('s_object', 'CMS Blog Topics')
                        ->where('action', 'Added')->first();

        if (!empty($added_alert)) {
            if ($created_rows > $added_alert->amount) {

                //notification when CMS Blog Topic added
                $add_notificatoion = Notification::create(['user_id' => $user['id'],
                            'backup_progress_id' => $backup_progress['id'],
                            'status' => 'CMS Blog Topics Added',
                            'type' => 'email',
                            'num_of_records' => $created_rows]);

                //create history
                SmartAlertHistory::create(['user_id' => $user['id'], 'smart_alert_id' => $added_alert->id]);

                //notify user
                //$user->notify(new Backup($notificatoion));
            }
        }

        //get smart alerts for action 'Changed'
        $changed_alert = SmartAlert::where('user_id', $user['id'])
                        ->where('s_object', 'CMS Blog Topics')
                        ->where('action', 'Changed')->first();

        if (!empty($changed_alert)) {
            if ($updated_rows > $changed_alert->amount) {

                //notification when Blog Topic Changed
                $change_notificatoion = Notification::create(['user_id' => $user['id'],
                            'backup_progress_id' => $backup_progress['id'],
                            'status' => 'CMS Blog Topics Changed',
                            'type' => 'email',
                            'num_of_records' => $updated_rows]);

                //create history
                SmartAlertHistory::create(['user_id' => $user['id'], 'smart_alert_id' => $changed_alert->id]);

                //notify user
                //$user->notify(new Backup($notificatoion));
            }
        }

        //get smart alerts for action 'Removed'
        $removed_alert = SmartAlert::where('user_id', $user['id'])
                        ->where('s_object', 'CMS Blog Topics')
                        ->where('action', 'Removed')->first();

        if (!empty($removed_alert)) {
            if ($removed_rows > $removed_alert->amount) {

                //notification when Cms Blog Topic Removed
                $remove_notificatoion = Notification::create(['user_id' => $user['id'],
                            'backup_progress_id' => $backup_progress['id'],
                            'status' => 'Cms Blog Topics Removed',
                            'type' => 'email',
                            'num_of_records' => $removed_rows]);

                //create history
                SmartAlertHistory::create(['user_id' => $user['id'], 'smart_alert_id' => $removed_alert->id]);

                //notify user
                //$user->notify(new Backup($notificatoion));
            }
        }
    }

    public function getCmsBlogTopicsData($headers, $offset, $client, $backup_progress, $user, $hubspot_blog_topic_ids, $status, $error_msg, $created_rows, $updated_rows) {

        $url = $this->api_url . '/blogs/v3/topics?limit=100&offset=' . $offset;

        //get all cms blog Topics data
        $response = $client->request('GET', $url, ['headers' => $headers]);


        $body = $response->getBody();
        $data = json_decode($body, true);


        if (!empty($data['status']) == 'error') {
            $status = $data['status'];
            $error_msg = $data['message'];
            Log::error('CMS Blog Topics data  |||| Error - ' . $error_msg);
        } else {
            $results = $data['objects'];
            $status = 'successed';
            if (!empty($results)) {
                $create_blog_topics = [];
                foreach ($results as $value) {

                    //get all blog Topic ids
                    $hubspot_blog_topic_ids[] = $value['id'];

                    $blog_topic_meta_url = $this->api_url . '/blogs/v3/topics/' . $value['id'];

                    //get complete details of each blog topic
                    $response_blog_topic_meta = $client->request('GET', $blog_topic_meta_url, ['headers' => $headers]);
                    $body_blog_topic_meta = $response_blog_topic_meta->getBody();
                    $data_blog_topic_meta = json_decode($body_blog_topic_meta, true);

                    if (!empty($data_blog_topic_meta['status']) == 'error') {
                        $status = 'warning';
                        $error_msg = $data_blog_topic_meta['message'];
                        Log::error('CMS Blog Topics data  |||| Warning - ' . $error_msg);
                    } else {
                        $check_blog_topic = CmsBlogTopic::where('user_id', $user->id)
                                        ->where('blog_topic_id', $data_blog_topic_meta['id'])
                                        ->where('backup_progress_id', $backup_progress['id'] - 1)->first();

                        $create_blog_topics[] = [
                            'user_id' => $user->id,
                            'backup_progress_id' => $backup_progress['id'],
                            'blog_topic_id' => $data_blog_topic_meta['id'],
                            'portal_id' => isset($data_blog_topic_meta['portalId']) ? $data_blog_topic_meta['portalId'] : 0,
                            'name' => isset($data_blog_topic_meta['name']) ? $data_blog_topic_meta['name'] : "",
                            'slug' => isset($data_blog_topic_meta['slug']) ? $data_blog_topic_meta['slug'] : "",
                            'description' => isset($data_blog_topic_meta['description']) ? $data_blog_topic_meta['description'] : "",
                            'created' => isset($data_blog_topic_meta['created']) ? Carbon::createFromTimestampMs($data_blog_topic_meta['created'])->format('Y-m-d H:i:s') : NULL,
                            'updated' => isset($data_blog_topic_meta['updated']) ? Carbon::createFromTimestampMs($data_blog_topic_meta['updated'])->format('Y-m-d H:i:s') : NULL,
                            'deleted_at' => isset($data_blog_topic_meta['deleted_at']) ? $data_blog_topic_meta['deleted_at'] : 0,
                            'total_posts' => isset($data_blog_topic_meta['total_posts']) ? $data_blog_topic_meta['total_posts'] : 0,
                            'live_posts' => isset($data_blog_topic_meta['live_posts']) ? $data_blog_topic_meta['live_posts'] : "",
                            'last_used' => isset($data_blog_topic_meta['last_used']) ? $data_blog_topic_meta['last_used'] : "",
                            'associated_blog_ids' => isset($data_blog_topic_meta['associated_blog_ids']) ? $data_blog_topic_meta['associated_blog_ids'] : NULL,
                            'public_url' => isset($data_blog_topic_meta['public_url']) ? $data_blog_topic_meta['public_url'] : "",
                            'status' => isset($data_blog_topic_meta['status']) ? $data_blog_topic_meta['status'] : "",
                            'created_at' => Carbon::now()->toDateTimeString(),
                            'updated_at' => Carbon::now()->toDateTimeString()
                        ];

                        //check if blog topic  is already inserted or not
                        if ($check_blog_topic === null) {
                            
                        } else {

                            $last_updated_date = Carbon::createFromTimestampMs($value['updated'])->format('Y-m-d H:i:s');

                            //check if any data is changed
                            if ($last_updated_date != $check_blog_topic->updated) {
                                $updated_rows +=1;
                            }
                        }
                    }
                }
                //insert blog topics  in bulk
                CmsBlogTopic::insert($create_blog_topics);
                $created_rows += count($create_blog_topics);
            }
        }
        if (isset($data['offset']) && !empty($data['offset'] !== 0)) {

            return $this->getCmsBlogTopicsData($headers, $data['offset'], $client, $backup_progress, $user, $hubspot_blog_topic_ids, $status, $error_msg, $created_rows, $updated_rows);
        } else {

            $data = ['created_rows' => $created_rows, 'updated_rows' => $updated_rows,
                'hubspot_blog_topic_ids' => $hubspot_blog_topic_ids, 'status' => $status, 'error_msg' => $error_msg];

            return $data;
        }
    }

}
