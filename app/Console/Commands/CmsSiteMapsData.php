<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\BadResponseException;
use Illuminate\Support\Facades\Artisan;
// models
use App\CmsSiteMap;
use App\BackupHistory;
use App\User;
use Illuminate\Support\Facades\Log;
use App\BackupProgress;
use App\Notifications\Backup;
use App\Notification;
use App\SmartAlert;
use App\SmartAlertHistory;
use Carbon\Carbon;

class CmsSiteMapsData extends Command {

    /**
     * The name of the apiUrl
     * @var type 
     */
    protected $api_url;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cms-site-maps:data {userId}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command will get all CMS Site Maps data';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();

        //get api key from env file
        $this->api_url = env('HUBSPOT_URL');
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        $this->info('CMS Site Maps Data cron started....' . $this->api_url);

        $status = '';
        $error_msg = '';
        $created_rows = 0;
        $updated_rows = 0;
        $hubspot_cms_site_map_ids = [];

        $userId = $this->argument('userId');
        $user = User::find($userId);

        $backup_progress = BackupProgress::where('user_id', $user['id'])->where('status', 'Inprogress')->orderByDesc('id')->first();


        try {

            Log::error('Fetching CMS Site maps data started |||| Start >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> userId - ' . $userId);

            //start time
            $time_start = microtime(true);


            //user token
            $token = $user->accesstoken;

            //using GuzzleHttp client to get request
            $client = new \GuzzleHttp\Client(['http_errors' => false]);

            $headers = [
                'Authorization' => 'Bearer ' . $token,
                'Accept' => 'application/json',
            ];

            $get_data = $this->getCmsSiteMapsData($headers, 0, $client, $backup_progress, $user, $hubspot_cms_site_map_ids, $status, $error_msg, $created_rows, $updated_rows);

            $created_rows = $get_data['created_rows'];
            $updated_rows = $get_data['updated_rows'];
            $hubspot_cms_site_map_ids = $get_data['hubspot_cms_site_map_ids'];
            $error_msg = $get_data['error_msg'];
            $status = $get_data['status'];

            //end time
            $time_end = microtime(true);
            //execution time
            $execution_time = ($time_end - $time_start);

            Log::error('time taken for CMS Site maps data ' . $execution_time);
            Log::error('Fetching CMS Site maps data completed |||| End <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< userId - ' . $userId);
        } catch (BadResponseException $ex) {
            //if request is invalid
            $response = $ex->getResponse();
            $jsonBody = (string) $response->getBody();
            $this->error($jsonBody);

            $data = json_decode($response->getBody(), true);
            $status = $data['status'];
            $error_msg = $data['message'];
            Log::error('CMS Site maps data  |||| Error - ' . $jsonBody);
        }

        //get cms site maps from database
        $cms_site_maps_in_database = CmsSiteMap::where('backup_progress_id', $backup_progress['id'] - 1)->get()->pluck('cms_site_map_id')->toArray();

        //check if empty result 
        if (count($hubspot_cms_site_map_ids) == 0) {
            $removed_rows = 0;
        } else {
            $removed_cms_site_maps = array_diff($cms_site_maps_in_database, $hubspot_cms_site_map_ids);
            $removed_rows = count($removed_cms_site_maps);
        }

        //creating BackupHistory record
        $backup_history = BackupHistory::create([
                    'backup_progress_id' => $backup_progress['id'],
                    'user_id' => $user->id,
                    'object_name' => 'Fetched CMS Site maps data',
                    'num_of_records' => $created_rows + $removed_rows,
                    'removed_records' => $removed_rows,
                    'added_records' => $created_rows,
                    'changed_records' => $updated_rows,
                    'num_of_API_calls' => 1,
                    'status' => $status,
                    'error_message' => $error_msg]);

        //get smart alerts for action 'added'
        $added_alert = SmartAlert::where('user_id', $user['id'])
                        ->where('s_object', 'CMS Site Maps')
                        ->where('action', 'Added')->first();

        if (!empty($added_alert)) {
            if ($created_rows > $added_alert->amount) {

                //notification when CMS Site map added
                $add_notificatoion = Notification::create(['user_id' => $user['id'], 'backup_progress_id' => $backup_progress['id'],
                            'status' => 'CMS Site Maps Added', 'type' => 'email', 'num_of_records' => $created_rows]);

                //create history
                SmartAlertHistory::create(['user_id' => $user['id'], 'smart_alert_id' => $added_alert->id]);

                //notify user
                //$user->notify(new Backup($notificatoion));
            }
        }

        //get smart alerts for action 'Changed'
        $changed_alert = SmartAlert::where('user_id', $user['id'])
                        ->where('s_object', 'CMS Site Maps')
                        ->where('action', 'Changed')->first();

        if (!empty($changed_alert)) {
            if ($updated_rows > $changed_alert->amount) {

                //notification when Site map Changed
                $change_notificatoion = Notification::create(['user_id' => $user['id'], 'backup_progress_id' => $backup_progress['id'],
                            'status' => 'CMS Site Maps Changed', 'type' => 'email', 'num_of_records' => $updated_rows]);

                //create history
                SmartAlertHistory::create(['user_id' => $user['id'], 'smart_alert_id' => $changed_alert->id]);

                //notify user
                //$user->notify(new Backup($notificatoion));
            }
        }

        //get smart alerts for action 'Removed'
        $removed_alert = SmartAlert::where('user_id', $user['id'])
                        ->where('s_object', 'CMS Site Maps')
                        ->where('action', 'Removed')->first();

        if (!empty($removed_alert)) {
            if ($removed_rows > $removed_alert->amount) {

                //notification when CMS Site map Removed
                $remove_notificatoion = Notification::create(['user_id' => $user['id'], 'backup_progress_id' => $backup_progress['id'],
                            'status' => 'CMS Site Maps Removed', 'type' => 'email', 'num_of_records' => $removed_rows]);

                //create history
                SmartAlertHistory::create(['user_id' => $user['id'], 'smart_alert_id' => $removed_alert->id]);

                //notify user
                //$user->notify(new Backup($notificatoion));
            }
        }
    }

    public function getCmsSiteMapsData($headers, $offset, $client, $backup_progress, $user, $hubspot_cms_site_map_ids, $status, $error_msg, $created_rows, $updated_rows) {

        $url = $this->api_url . '/content/api/v2/site-maps?limit=100&offset=' . $offset;

        //get all cms site maps data
        $response = $client->request('GET', $url, ['headers' => $headers]);


        $body = $response->getBody();
        $data = json_decode($body, true);

        if (!empty($data['status']) == 'error') {
            $status = $data['status'];
            $error_msg = $data['message'];
            Log::error('CMS Site Maps data  |||| Error - ' . $error_msg);
        } else {
            $results = $data['objects'];
            $status = 'successed';

            if (!empty($results)) {
                $create_site_maps = [];
                foreach ($results as $value) {

                    //get all cms site map ids
                    $hubspot_cms_site_map_ids[] = $value['id'];



                    $check_site_map = CmsSiteMap::where('user_id', $user->id)
                                    ->where('cms_site_map_id', $value['id'])
                                    ->where('backup_progress_id', $backup_progress['id'] - 1)->first();

                    $create_site_maps[] = [
                        'user_id' => $user->id,
                        'backup_progress_id' => $backup_progress['id'],
                        'cms_site_map_id' => $value['id'],
                        'portal_id' => isset($value['portal_id']) ? $value['portal_id'] : 0,
                        'name' => isset($value['name']) ? $value['name'] : "",
                        'blog_posts' => isset($value['blog_posts']) ? $value['blog_posts'] : NULL,
                        'content_options' => isset($value['content_options']) ? $value['content_options'] : NULL,
                        'domain' => isset($value['domain']) ? $value['domain'] : "",
                        'has_user_changes' => isset($value['has_user_changes']) ? $value['has_user_changes'] : 0,
                        'image_metas' => isset($value['image_metas']) ? $value['image_metas'] : NULL,
                        'initiated_at' => isset($value['initiated_at']) ? Carbon::createFromTimestampMs($value['initiated_at'])->format('Y-m-d H:i:s') : NULL,
                        'knowledge_articles' => isset($value['knowledge_articles']) ? $value['knowledge_articles'] : NULL,
                        'landing_pages' => isset($value['landing_pages']) ? $value['landing_pages'] : NULL,
                        'site_pages' => isset($value['site_pages']) ? $value['site_pages'] : NULL,
                        'video_metas' => isset($value['video_metas']) ? $value['video_metas'] : NULL,
                        'deleted_at' => isset($value['deleted_at']) ? $value['deleted_at'] : 0,
                        'created' => isset($value['created']) ? Carbon::createFromTimestampMs($value['created'])->format('Y-m-d H:i:s') : NULL,
                        'updated' => isset($value['updated']) ? Carbon::createFromTimestampMs($value['updated'])->format('Y-m-d H:i:s') : NULL,
                        'created_at' => Carbon::now()->toDateTimeString(),
                        'updated_at' => Carbon::now()->toDateTimeString()
                    ];
                    //check if site map  is already inserted or not
                    if ($check_site_map === null) {
                        
                    } else {
                        $last_updated_date = Carbon::createFromTimestampMs($value['updated'])->format('Y-m-d H:i:s');

                        //check if any data is changed
                        if ($last_updated_date != $check_site_map->updated) {

                            $updated_rows +=1;
                        }
                    }
                }

                //insert cms site maps in bulk
                CmsSiteMap::insert($create_site_maps);
                $created_rows += count($create_site_maps);
            }
        }

        if (isset($data['offset']) && !empty($data['offset'] !== 0)) {

            return $this->getCmsSiteMapsData($headers, $data['offset'], $client, $backup_progress, $user, $hubspot_cms_site_map_ids, $status, $error_msg, $created_rows, $updated_rows);
        } else {

            $data = ['created_rows' => $created_rows, 'updated_rows' => $updated_rows,
                'hubspot_cms_site_map_ids' => $hubspot_cms_site_map_ids, 'status' => $status, 'error_msg' => $error_msg];

            return $data;
        }
    }

}
