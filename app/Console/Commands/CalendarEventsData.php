<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
// models
use App\CalendarEvent;
use App\BackupHistory;
use App\User;
use Illuminate\Support\Facades\Log;
use App\BackupProgress;
use App\Notifications\Backup;
use App\Notification;
use App\SmartAlert;
use App\SmartAlertHistory;
use Carbon\Carbon;

class CalendarEventsData extends Command {

    /**
     * The name of the apiUrl
     * @var type 
     */
    protected $api_url;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'calendar-events:data {userId}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command will get all Calendar events data';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();

        //get api key from env file
        $this->api_url = env('HUBSPOT_URL');
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        $this->info('Calendar Events Data cron started....' . $this->api_url);

        $status = '';
        $error_msg = '';
        $created_rows = 0;
        $updated_rows = 0;
        $hubspot_calendar_events_ids = [];

        $userId = $this->argument('userId');
        $user = User::find($userId);

        $backup_progress = BackupProgress::where('user_id', $user['id'])->where('status', 'Inprogress')->orderByDesc('id')->first();


        try {

            Log::error('Fetching Calendar  Events data started |||| Start >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> userId - ' . $userId);

            //start time
            $time_start = microtime(true);


            //user token
            $token = $user->accesstoken;

            //using GuzzleHttp client to get request
            $client = new \GuzzleHttp\Client(['http_errors' => false]);
            $headers = [
                'Authorization' => 'Bearer ' . $token,
                'Accept' => 'application/json',
            ];

            $get_data = $this->getCalendarEventsData($headers, 0, $client, $backup_progress, $user, $hubspot_calendar_events_ids, $status, $error_msg, $created_rows, $updated_rows);

            $created_rows = $get_data['created_rows'];
            $updated_rows = $get_data['updated_rows'];
            $hubspot_calendar_events_ids = $get_data['hubspot_calendar_events_ids'];
            $error_msg = $get_data['error_msg'];
            $status = $get_data['status'];

            //end time
            $time_end = microtime(true);
            //execution time
            $execution_time = ($time_end - $time_start);

            Log::error('time taken for Calendar Events data ' . $execution_time);
            Log::error('Fetching Calendar Events data completed |||| End <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< userId - ' . $userId);
        } catch (BadResponseException $ex) {
            //if request is invalid
            $response = $ex->getResponse();
            $jsonBody = (string) $response->getBody();
            $this->error($jsonBody);

            $data = json_decode($response->getBody(), true);
            $status = $data['status'];
            $error_msg = $data['message'];
            Log::error('CMS Calendar Events data  |||| Error - ' . $jsonBody);
        }

        //get calendar events from database
        $calendar_events_in_database = CalendarEvent::where('backup_progress_id', $backup_progress['id'] - 1)->get()->pluck('event_id')->toArray();

        //check if empty result 
        if (count($hubspot_calendar_events_ids) == 0) {
            $removed_rows = 0;
        } else {
            $removed_calendar_events = array_diff($calendar_events_in_database, $hubspot_calendar_events_ids);
            $removed_rows = count($removed_calendar_events);
        }

        //creating BackupHistory record
        $backup_history = BackupHistory::create([
                    'backup_progress_id' => $backup_progress['id'],
                    'user_id' => $user->id,
                    'object_name' => 'Fetched Calendar Events data',
                    'num_of_records' => $created_rows + $removed_rows,
                    'removed_records' => $removed_rows,
                    'added_records' => $created_rows,
                    'changed_records' => $updated_rows,
                    'num_of_API_calls' => 4,
                    'status' => $status,
                    'error_message' => $error_msg]);

        //get smart alerts for action 'added'
        $added_alert = SmartAlert::where('user_id', $user['id'])
                        ->where('s_object', 'Calendar Events')
                        ->where('action', 'Added')->first();

        if (!empty($added_alert)) {
            if ($created_rows > $added_alert->amount) {

                //notification when Event added
                $add_notificatoion = Notification::create(['user_id' => $user['id'],
                            'backup_progress_id' => $backup_progress['id'],
                            'status' => 'Calendar Events Added',
                            'type' => 'email',
                            'num_of_records' => $created_rows]);

                //create history
                SmartAlertHistory::create(['user_id' => $user['id'], 'smart_alert_id' => $added_alert->id]);

                //notify user
                //$user->notify(new Backup($notificatoion));
            }
        }

        //get smart alerts for action 'Changed'
        $changed_alert = SmartAlert::where('user_id', $user['id'])
                        ->where('s_object', 'Calendar Events')
                        ->where('action', 'Changed')->first();

        if (!empty($changed_alert)) {
            if ($updated_rows > $changed_alert->amount) {

                //notification when Calendar Event Changed
                $change_notificatoion = Notification::create(['user_id' => $user['id'],
                            'backup_progress_id' => $backup_progress['id'],
                            'status' => 'Calendar Events Changed',
                            'type' => 'email',
                            'num_of_records' => $updated_rows]);

                //create history
                SmartAlertHistory::create(['user_id' => $user['id'], 'smart_alert_id' => $changed_alert->id]);

                //notify user
                //$user->notify(new Backup($notificatoion));
            }
        }

        //get smart alerts for action 'Removed'
        $removed_alert = SmartAlert::where('user_id', $user['id'])
                        ->where('s_object', 'Calendar Events')
                        ->where('action', 'Removed')->first();

        if (!empty($removed_alert)) {
            if ($removed_rows > $removed_alert->amount) {

                //notification when Calendar Event Removed
                $remove_notificatoion = Notification::create(['user_id' => $user['id'],
                            'backup_progress_id' => $backup_progress['id'],
                            'status' => 'Calendar Events Removed',
                            'type' => 'email',
                            'num_of_records' => $removed_rows]);

                //create history
                SmartAlertHistory::create(['user_id' => $user['id'], 'smart_alert_id' => $removed_alert->id]);

                //notify user
                //$user->notify(new Backup($notificatoion));
            }
        }
    }

    public function getCalendarEventsData($headers, $offset, $client, $backup_progress, $user, $hubspot_calendar_events_ids, $status, $error_msg, $created_rows, $updated_rows) {

        $enddate = Carbon::now()->timestamp;
        $startdate = Carbon::now()->subYears(1)->timestamp;
        $url = $this->api_url . '/calendar/v1/events?startDate=' . $startdate . '&endDate=' . $enddate . '&limit=100&offset=' . $offset;

        //get all Calendar events data
        $response = $client->request('GET', $url, ['headers' => $headers]);


        $body = $response->getBody();
        $results = json_decode($body, true);

        if (!empty($results['status']) == 'error') {
            $status = $results['status'];
            $error_msg = $results['message'];
            Log::error('CMS Calendar Events data  |||| Error - ' . $error_msg);
        } else {
            $status = 'successed';
            if (!empty($results)) {
                $create_events = [];
                foreach ($results as $value) {

                    //get all calendar events ids
                    $hubspot_calendar_events_ids [] = $value['id'];


                    $check_event = CalendarEvent::where('user_id', $user->id)
                                    ->where('event_id', $value['id'])
                                    ->where('backup_progress_id', $backup_progress['id'] - 1)->first();

                    if ($value['eventType'] == 'PUBLISHING_TASK') {

                        $get_event_url = $this->api_url . '/calendar/v1/events/task/' . $value['id'];
                    } elseif ($value['eventType'] == 'CONTENT') {

                        $get_event_url = $this->api_url . '/calendar/v1/events/content/' . $value['id'];
                    } elseif ($value['eventType'] == 'SOCIAL') {

                        $get_event_url = $this->api_url . '/calendar/v1/events/social/' . $value['id'];
                    }

                    //get event
                    $response_event = $client->request('GET', $get_event_url, ['headers' => $headers]);
                    $body_event = $response_event->getBody();
                    $data_event = json_decode($body_event, true);

                    $create_events[] = [
                        'user_id' => $user->id,
                        'backup_progress_id' => $backup_progress['id'],
                        'event_id' => $value['id'],
                        'portal_id' => isset($value['portalId']) ? $value['portalId'] : "",
                        'event_type' => isset($value['eventType']) ? $value['eventType'] : "",
                        'event_date' => isset($value['eventDate']) ? Carbon::createFromTimestampMs($value['eventDate'])->format('Y-m-d H:i:s') : NULL,
                        'category' => isset($value['category']) ? $value['category'] : "",
                        'category_id' => isset($value['categoryId']) ? $value['categoryId'] : 0,
                        'content_id' => isset($value['contentId']) ? $value['contentId'] : 0,
                        'state' => isset($value['state']) ? $value['state'] : "",
                        'campaign_guid' => isset($value['campaignGuid']) ? $value['campaignGuid'] : "",
                        'name' => isset($value['name']) ? $value['name'] : "",
                        'description' => isset($value['description']) ? $value['description'] : "",
                        'url' => isset($value['url']) ? $value['url'] : "",
                        'owner_id' => isset($value['ownerId']) ? $value['ownerId'] : 0,
                        'created_by' => isset($value['createdBy']) ? $value['createdBy'] : 0,
                        'create_content' => isset($value['createContent']) ? $value['createContent'] : 0,
                        'preview_key' => isset($value['previewKey']) ? $value['previewKey'] : "",
                        'template_path' => isset($value['templatePath']) ? $value['templatePath'] : "",
                        'social_username' => isset($value['socialUsername']) ? $value['socialUsername'] : "",
                        'social_display_name' => isset($value['socialDisplayName']) ? $value['socialDisplayName'] : "",
                        'avatar_url' => isset($value['avatarUrl']) ? $value['avatarUrl'] : "",
                        'is_recurring' => isset($value['isRecurring']) ? $value['isRecurring'] : 0,
                        'topic_ids' => isset($value['topicIds']) ? $value['topicIds'] : NULL,
                        'content_group_id' => isset($value['contentGroupId']) ? $value['contentGroupId'] : "",
                        'group_id' => isset($value['groupId']) ? $value['groupId'] : "",
                        'group_order' => isset($value['groupOrder']) ? $value['groupOrder'] : "",
                        'remote_id' => isset($value['remoteId']) ? $value['remoteId'] : "",
                        'started_date' => isset($data_event['startedDate']) ? Carbon::createFromTimestampMs($data_event['startedDate'])->format('Y-m-d H:i:s') : NULL,
                        'published_date' => isset($data_event['publishedDate']) ? Carbon::createFromTimestampMs($data_event['publishedDate'])->format('Y-m-d H:i:s') : NULL,
                        'created_at' => Carbon::now()->toDateTimeString(),
                        'updated_at' => Carbon::now()->toDateTimeString()
                    ];
                    //check if event  is already inserted or not
                    if ($check_event === null) {
                        
                    } else {

                        //check if any data is changed
                        if ($check_event->event_type != $value['eventType'] || $check_event->event_date != $value['eventDate'] ||
                                $check_event->category != $value['category'] || $check_event->category_id != $value['categoryId'] ||
                                $check_event->content_id != $value['contentId'] || $check_event->state != $value['state'] ||
                                $check_event->campaign_guid != $value['campaignGuid'] || $check_event->name != $value['name'] ||
                                $check_event->description != $value['description'] || $check_event->url != $value['url'] ||
                                $check_event->owner_id != $value['ownerId'] || $check_event->created_by != $value['createdBy'] ||
                                $check_event->create_content != $value['createContent'] || $check_event->preview_key != $value['previewKey'] ||
                                $check_event->template_path != $value['templatePath'] || $check_event->social_username != $value['socialUsername'] ||
                                $check_event->social_display_name != $value['socialDisplayName'] || $check_event->avatar_url != $value['avatarUrl'] ||
                                $check_event->is_recurring != $value['isRecurring'] || $check_event->topic_ids != $value['topicIds'] ||
                                $check_event->content_group_id != $value['contentGroupId'] || $check_event->group_id != $value['groupId'] ||
                                $check_event->group_order != $value['groupOrder'] || $check_event->remote_id != $value['remoteId'] ||
                                $check_event->started_date != $value['startedDate'] || $check_event->published_date != $value['publishedDate']
                        ) {
                            $updated_rows +=1;
                        }
                    }
                }

                //insert caledar events in bulk
                CalendarEvent::insert($create_events);
                $created_rows += count($create_events);
            }
            if (count($results) == 0) {
                $data_offset = 0;
            } else {
                $data_offset = end($hubspot_calendar_events_ids);
            }
            if (!empty($data_offset !== 0)) {

                return $this->getCalendarEventsData($headers, $data_offset, $client, $backup_progress, $user, $hubspot_calendar_events_ids, $status, $error_msg, $created_rows, $updated_rows);
            } else {

                $data = ['created_rows' => $created_rows, 'updated_rows' => $updated_rows,
                    'hubspot_calendar_events_ids' => $hubspot_calendar_events_ids, 'status' => $status, 'error_msg' => $error_msg];

                return $data;
            }
        }
        $data = ['created_rows' => $created_rows, 'updated_rows' => $updated_rows,
            'hubspot_calendar_events_ids' => $hubspot_calendar_events_ids, 'status' => $status, 'error_msg' => $error_msg];

        return $data;
    }

}
