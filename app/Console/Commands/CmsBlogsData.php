<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\BadResponseException;
use Illuminate\Support\Facades\Artisan;
// models
use App\CmsBlog;
use App\BackupHistory;
use App\User;
use Illuminate\Support\Facades\Log;
use App\BackupProgress;
use App\Notifications\Backup;
use App\Notification;
use App\SmartAlert;
use App\SmartAlertHistory;
use Carbon\Carbon;

class CmsBlogsData extends Command {

    /**
     * The name of the apiUrl
     * @var type 
     */
    protected $api_url;

    /**
     * The name of the apiKey
     * @var type 
     */
    protected $api_key;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cms-blogs:data {userId}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command will get all CMS Blogs data';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
        //get api key from env file
        $this->api_url = env('HUBSPOT_URL');
        $this->api_key = env('HUBSPOT_KEY');
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        $this->info('CMS Blogs Data cron started....' . $this->api_url);

        $status = '';
        $error_msg = '';
        $created_rows = 0;
        $updated_rows = 0;
        $hubspot_blog_ids = [];

        $userId = $this->argument('userId');
        $user = User::find($userId);

        $backup_progress = BackupProgress::where('user_id', $user['id'])->where('status', 'Inprogress')->orderByDesc('id')->first();

        try {

            Log::error('Fetching CMS Blogs data started |||| Start >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> userId - ' . $userId);

            //start time
            $time_start = microtime(true);


            //user token
            $token = $user->accesstoken;

            //using GuzzleHttp client to get request
            $client = new \GuzzleHttp\Client(['http_errors' => false]);
            $headers = [
                'Authorization' => 'Bearer ' . $token,
                'Accept' => 'application/json',
            ];

            $get_data = $this->getCmsBlogsData($headers, 0, $client, $backup_progress, $user, $hubspot_blog_ids, $status, $error_msg, $created_rows, $updated_rows);

            $created_rows = $get_data['created_rows'];
            $updated_rows = $get_data['updated_rows'];
            $hubspot_blog_ids = $get_data['hubspot_blog_ids'];
            $error_msg = $get_data['error_msg'];
            $status = $get_data['status'];


            //end time
            $time_end = microtime(true);
            //execution time
            $execution_time = ($time_end - $time_start);

            Log::error('time taken for CMS Blogs data ' . $execution_time);
            Log::error('Fetching CMS Blogs data completed |||| End <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< userId - ' . $userId);
        } catch (BadResponseException $ex) {
            //if request is invalid
            $response = $ex->getResponse();
            $jsonBody = (string) $response->getBody();
            $this->error($jsonBody);

            $data = json_decode($response->getBody(), true);
            $status = $data['status'];
            $error_msg = $data['message'];
            Log::error('CMS Blogs data  |||| Error - ' . $jsonBody);
        }

        //get blogs from database
        $blogs_in_database = CmsBlog::where('backup_progress_id', $backup_progress['id'] - 1)->get()->pluck('cms_blog_id')->toArray();

        //check if empty result 
        if (count($hubspot_blog_ids) == 0) {
            $removed_rows = 0;
        } else {
            $removed_blogs = array_diff($blogs_in_database, $hubspot_blog_ids);
            $removed_rows = count($removed_blogs);
        }

        //creating BackupHistory record
        $backup_history = BackupHistory::create([
                    'backup_progress_id' => $backup_progress['id'],
                    'user_id' => $user->id,
                    'object_name' => 'Fetched CMS Blogs data',
                    'num_of_records' => $created_rows + $removed_rows,
                    'removed_records' => $removed_rows,
                    'added_records' => $created_rows,
                    'changed_records' => $updated_rows,
                    'num_of_API_calls' => 2,
                    'status' => $status,
                    'error_message' => $error_msg]);

        //get smart alerts for action 'added'
        $added_alert = SmartAlert::where('user_id', $user['id'])
                        ->where('s_object', 'CMS Blogs')
                        ->where('action', 'Added')->first();

        if (!empty($added_alert)) {
            if ($created_rows > $added_alert->amount) {

                //notification when CMS Blog added
                $add_notificatoion = Notification::create(['user_id' => $user['id'],
                            'backup_progress_id' => $backup_progress['id'],
                            'status' => 'CMS Blogs Added',
                            'type' => 'email',
                            'num_of_records' => $created_rows]);

                //create history
                SmartAlertHistory::create(['user_id' => $user['id'], 'smart_alert_id' => $added_alert->id]);

                //notify user
                //$user->notify(new Backup($notificatoion));
            }
        }

        //get smart alerts for action 'Changed'
        $changed_alert = SmartAlert::where('user_id', $user['id'])
                        ->where('s_object', 'CMS Blogs')
                        ->where('action', 'Changed')->first();

        if (!empty($changed_alert)) {
            if ($updated_rows > $changed_alert->amount) {

                //notification when Blog Changed
                $change_notificatoion = Notification::create(['user_id' => $user['id'],
                            'backup_progress_id' => $backup_progress['id'],
                            'status' => 'CMS Blogs Changed',
                            'type' => 'email',
                            'num_of_records' => $updated_rows]);

                //create history
                SmartAlertHistory::create(['user_id' => $user['id'], 'smart_alert_id' => $changed_alert->id]);

                //notify user
                //$user->notify(new Backup($notificatoion));
            }
        }

        //get smart alerts for action 'Removed'
        $removed_alert = SmartAlert::where('user_id', $user['id'])
                        ->where('s_object', 'Cms Blogs')
                        ->where('action', 'Removed')->first();

        if (!empty($removed_alert)) {
            if ($removed_rows > $removed_alert->amount) {

                //notification when Cms Blog  Removed
                $remove_notificatoion = Notification::create(['user_id' => $user['id'],
                            'backup_progress_id' => $backup_progress['id'],
                            'status' => 'Cms Blogs Removed',
                            'type' => 'email',
                            'num_of_records' => $removed_rows]);

                //create history
                SmartAlertHistory::create(['user_id' => $user['id'], 'smart_alert_id' => $removed_alert->id]);

                //notify user
                //$user->notify(new Backup($notificatoion));
            }
        }
    }

    public function getCmsBlogsData($headers, $offset, $client, $backup_progress, $user, $hubspot_blog_ids, $status, $error_msg, $created_rows, $updated_rows) {

        $url = $this->api_url . '/content/api/v2/blogs?limit=100&offset=' . $offset;

        //get all cms blogs data
        $response = $client->request('GET', $url, ['headers' => $headers]);


        $body = $response->getBody();
        $data = json_decode($body, true);


        if (!empty($data['status']) == 'error') {
            $status = $data['status'];
            $error_msg = $data['message'];
            Log::error('CMS Blogs data  |||| Error - ' . $error_msg);
        } else {
            $results = $data['objects'];
            $status = 'successed';
            if (!empty($results)) {
                $create_blogs = [];
                foreach ($results as $value) {

                    //get all blog ids
                    $hubspot_blog_ids[] = $value['id'];


                    $blog_meta_url = $this->api_url . '/content/api/v2/blogs/' . $value['id'];

                    //get complete details of each blog
                    $response_blog_meta = $client->request('GET', $blog_meta_url, ['headers' => $headers]);
                    $body_blog_meta = $response_blog_meta->getBody();
                    $data_blog_meta = json_decode($body_blog_meta, true);

                    if (!empty($data_blog_meta['status']) == 'error') {
                        $status = 'warning';
                        $error_msg = $data_blog_meta['message'];
                        Log::error('CMS Blogs data  |||| Warning - ' . $error_msg);
                    } else {
                        $check_blog = CmsBlog::where('user_id', $user->id)
                                        ->where('cms_blog_id', $data_blog_meta['id'])
                                        ->where('backup_progress_id', $backup_progress['id'] - 1)->first();

                        $create_blogs[] = [
                            'user_id' => $user->id,
                            'backup_progress_id' => $backup_progress['id'],
                            'cms_blog_id' => $data_blog_meta['id'],
                            'portal_id' => isset($data_blog_meta['portal_id']) ? $data_blog_meta['portal_id'] : 0,
                            'absolute_url' => isset($data_blog_meta['absolute_url']) ? $data_blog_meta['absolute_url'] : '',
                            'allow_comments' => isset($data_blog_meta['allow_comments']) ? $data_blog_meta['allow_comments'] : 0,
                            'amp_body_color' => isset($data_blog_meta['amp_body_color']) ? $data_blog_meta['amp_body_color'] : '',
                            'amp_body_font' => isset($data_blog_meta['amp_body_font']) ? $data_blog_meta['amp_body_font'] : '',
                            'amp_body_font_size' => isset($data_blog_meta['amp_body_font_size']) ? $data_blog_meta['amp_body_font_size'] : 0,
                            'amp_custom_css' => isset($data_blog_meta['amp_custom_css']) ? $data_blog_meta['amp_custom_css'] : '',
                            'amp_header_background_color' => isset($data_blog_meta['amp_header_background_color']) ? $data_blog_meta['amp_header_background_color'] : '',
                            'amp_header_color' => isset($data_blog_meta['amp_header_color']) ? $data_blog_meta['amp_header_color'] : '',
                            'amp_header_font' => isset($data_blog_meta['amp_header_font']) ? $data_blog_meta['amp_header_font'] : '',
                            'amp_header_font_size' => isset($data_blog_meta['amp_header_font_size']) ? $data_blog_meta['amp_header_font_size'] : 0,
                            'amp_link_color' => isset($data_blog_meta['amp_link_color']) ? $data_blog_meta['amp_link_color'] : '',
                            'amp_logo_alt' => isset($data_blog_meta['amp_logo_alt']) ? $data_blog_meta['amp_logo_alt'] : '',
                            'amp_logo_height' => isset($data_blog_meta['amp_logo_height']) ? $data_blog_meta['amp_logo_height'] : 0,
                            'amp_logo_src' => isset($data_blog_meta['amp_logo_src']) ? $data_blog_meta['amp_logo_src'] : '',
                            'amp_logo_width' => isset($data_blog_meta['amp_logo_width']) ? $data_blog_meta['amp_logo_width'] : 0,
                            'analytics_page_id' => isset($data_blog_meta['analytics_page_id']) ? $data_blog_meta['analytics_page_id'] : 0,
                            'attached_stylesheets' => isset($data_blog_meta['attached_stylesheets']) ? $data_blog_meta['attached_stylesheets'] : NULL,
                            'captcha_after_days' => isset($data_blog_meta['captcha_after_days']) ? $data_blog_meta['captcha_after_days'] : 0,
                            'captcha_always' => isset($data_blog_meta['captcha_always']) ? $data_blog_meta['captcha_always'] : 0,
                            'category_id' => isset($data_blog_meta['category_id']) ? $data_blog_meta['category_id'] : 0,
                            'close_comments_older' => isset($data_blog_meta['close_comments_older']) ? $data_blog_meta['close_comments_older'] : 0,
                            'comment_date_format' => isset($data_blog_meta['comment_date_format']) ? $data_blog_meta['comment_date_format'] : '',
                            'comment_form_guid' => isset($data_blog_meta['comment_form_guid']) ? $data_blog_meta['comment_form_guid'] : '',
                            'comment_max_thread_depth' => isset($data_blog_meta['comment_max_thread_depth']) ? $data_blog_meta['comment_max_thread_depth'] : 0,
                            'comment_moderation' => isset($data_blog_meta['comment_moderation']) ? $data_blog_meta['comment_moderation'] : 0,
                            'comment_notification_emails' => isset($data_blog_meta['comment_notification_emails']) ? $data_blog_meta['comment_notification_emails'] : NULL,
                            'comment_should_create_contact' => isset($data_blog_meta['comment_should_create_contact']) ? $data_blog_meta['comment_should_create_contact'] : 0,
                            'comment_verification_text' => isset($data_blog_meta['comment_verification_text']) ? $data_blog_meta['comment_verification_text'] : '',
                            'cos_object_type' => isset($data_blog_meta['cos_object_type']) ? $data_blog_meta['cos_object_type'] : '',
                            'created_date_time' => isset($data_blog_meta['created_date_time']) ? Carbon::createFromTimestampMs($data_blog_meta['created_date_time'])->format('Y-m-d H:i:s') : NULL,
                            'daily_notification_email_id' => isset($data_blog_meta['daily_notification_email_id']) ? $data_blog_meta['daily_notification_email_id'] : '',
                            'default_group_style_id' => isset($data_blog_meta['default_group_style_id']) ? $data_blog_meta['default_group_style_id'] : 0,
                            'default_notification_from_name' => isset($data_blog_meta['default_notification_from_name']) ? $data_blog_meta['default_notification_from_name'] : '',
                            'default_notification_reply_to' => isset($data_blog_meta['default_notification_reply_to']) ? $data_blog_meta['default_notification_reply_to'] : '',
                            'deleted_at' => isset($data_blog_meta['deleted_at']) ? $data_blog_meta['deleted_at'] : 0,
                            'description' => isset($data_blog_meta['description']) ? $data_blog_meta['description'] : '',
                            'domain' => isset($data_blog_meta['domain']) ? $data_blog_meta['domain'] : '',
                            'domain_when_published' => isset($data_blog_meta['domain_when_published']) ? $data_blog_meta['domain_when_published'] : '',
                            'email_api_subscription_id' => isset($data_blog_meta['email_api_subscription_id']) ? $data_blog_meta['email_api_subscription_id'] : 0,
                            'enable_google_amp_output' => isset($data_blog_meta['enable_google_amp_output']) ? $data_blog_meta['enable_google_amp_output'] : 0,
                            'enable_social_auto_publishing' => isset($data_blog_meta['enable_social_auto_publishing']) ? $data_blog_meta['enable_social_auto_publishing'] : 0,
                            'html_footer' => isset($data_blog_meta['html_footer']) ? $data_blog_meta['html_footer'] : '',
                            'html_footer_is_shared' => isset($data_blog_meta['html_footer_is_shared']) ? $data_blog_meta['html_footer_is_shared'] : 0,
                            'html_head_is_shared' => isset($data_blog_meta['html_head_is_shared']) ? $data_blog_meta['html_head_is_shared'] : 0,
                            'html_keywords' => isset($data_blog_meta['html_keywords']) ? $data_blog_meta['html_keywords'] : NULL,
                            'html_title' => isset($data_blog_meta['html_title']) ? $data_blog_meta['html_title'] : '',
                            'instant_notification_email_id' => isset($data_blog_meta['instant_notification_email_id']) ? $data_blog_meta['instant_notification_email_id'] : 0,
                            'item_layout_id' => isset($data_blog_meta['item_layout_id']) ? $data_blog_meta['item_layout_id'] : 0,
                            'item_template_is_shared' => isset($data_blog_meta['item_template_is_shared']) ? $data_blog_meta['item_template_is_shared'] : 0,
                            'item_template_path' => isset($data_blog_meta['item_template_path']) ? $data_blog_meta['item_template_path'] : '',
                            'label' => isset($data_blog_meta['label']) ? $data_blog_meta['label'] : '',
                            'listing_layout_id' => isset($data_blog_meta['listing_layout_id']) ? $data_blog_meta['listing_layout_id'] : 0,
                            'listing_template_path' => isset($data_blog_meta['listing_template_path']) ? $data_blog_meta['listing_template_path'] : '',
                            'live_domain' => isset($data_blog_meta['live_domain']) ? $data_blog_meta['live_domain'] : '',
                            'month_filter_format' => isset($data_blog_meta['month_filter_format']) ? $data_blog_meta['month_filter_format'] : '',
                            'monthly_notification_email_id' => isset($data_blog_meta['monthly_notification_email_id']) ? $data_blog_meta['monthly_notification_email_id'] : 0,
                            'name' => isset($data_blog_meta['name']) ? $data_blog_meta['name'] : '',
                            'post_html_footer' => isset($data_blog_meta['post_html_footer']) ? $data_blog_meta['post_html_footer'] : '',
                            'post_html_head' => isset($data_blog_meta['post_html_head']) ? $data_blog_meta['post_html_head'] : '',
                            'posts_per_listing_page' => isset($data_blog_meta['posts_per_listing_page']) ? $data_blog_meta['posts_per_listing_page'] : 0,
                            'posts_per_rss_feed' => isset($data_blog_meta['posts_per_rss_feed']) ? $data_blog_meta['posts_per_rss_feed'] : 0,
                            'public_access_rules' => isset($data_blog_meta['public_access_rules']) ? $data_blog_meta['public_access_rules'] : NULL,
                            'public_access_rules_enabled' => isset($data_blog_meta['public_access_rules_enabled']) ? $data_blog_meta['public_access_rules_enabled'] : 0,
                            'public_title' => isset($data_blog_meta['public_title']) ? $data_blog_meta['public_title'] : '',
                            'publish_date_format' => isset($data_blog_meta['publish_date_format']) ? $data_blog_meta['publish_date_format'] : '',
                            'resolved_domain' => isset($data_blog_meta['resolved_domain']) ? $data_blog_meta['resolved_domain'] : '',
                            'root_url' => isset($data_blog_meta['root_url']) ? $data_blog_meta['root_url'] : '',
                            'show_social_link_facebook' => isset($data_blog_meta['show_social_link_facebook']) ? $data_blog_meta['show_social_link_facebook'] : 0,
                            'show_social_link_linkedin' => isset($data_blog_meta['show_social_link_linkedin']) ? $data_blog_meta['show_social_link_linkedin'] : 0,
                            'show_social_link_twitter' => isset($data_blog_meta['show_social_link_twitter']) ? $data_blog_meta['show_social_link_twitter'] : 0,
                            'show_summary_in_emails' => isset($data_blog_meta['show_summary_in_emails']) ? $data_blog_meta['show_summary_in_emails'] : 0,
                            'show_summary_in_listing' => isset($data_blog_meta['show_summary_in_listing']) ? $data_blog_meta['show_summary_in_listing'] : 0,
                            'show_summary_in_rss' => isset($data_blog_meta['show_summary_in_rss']) ? $data_blog_meta['show_summary_in_rss'] : 0,
                            'slug' => isset($data_blog_meta['slug']) ? $data_blog_meta['slug'] : '',
                            'social_account_twitter' => isset($data_blog_meta['social_account_twitter']) ? $data_blog_meta['social_account_twitter'] : '',
                            'subscription_contacts_property' => isset($data_blog_meta['subscription_contacts_property']) ? $data_blog_meta['subscription_contacts_property'] : '',
                            'subscription_form_guid' => isset($data_blog_meta['subscription_form_guid']) ? $data_blog_meta['subscription_form_guid'] : '',
                            'subscription_lists_by_type_daily' => isset($data_blog_meta['subscription_lists_by_type']['daily']) ? $data_blog_meta['subscription_lists_by_type']['daily'] : '',
                            'subscription_lists_by_type_instant' => isset($data_blog_meta['subscription_lists_by_type']['instant']) ? $data_blog_meta['subscription_lists_by_type']['instant'] : '',
                            'subscription_lists_by_type_monthly' => isset($data_blog_meta['subscription_lists_by_type']['monthly']) ? $data_blog_meta['subscription_lists_by_type']['monthly'] : '',
                            'subscription_lists_by_type_weekly' => isset($data_blog_meta['subscription_lists_by_type']['weekly']) ? $data_blog_meta['subscription_lists_by_type']['weekly'] : '',
                            'updated_date_time' => isset($data_blog_meta['updated_date_time']) ? Carbon::createFromTimestampMs($data_blog_meta['updated_date_time'])->format('Y-m-d H:i:s') : NULL,
                            'url_base' => isset($data_blog_meta['url_base']) ? $data_blog_meta['url_base'] : '',
                            'url_segments' => isset($data_blog_meta['url_segments']) ? $data_blog_meta['url_segments'] : NULL,
                            'use_featured_image_in_summary' => isset($data_blog_meta['use_featured_image_in_summary']) ? $data_blog_meta['use_featured_image_in_summary'] : 0,
                            'uses_default_template' => isset($data_blog_meta['uses_default_template']) ? $data_blog_meta['uses_default_template'] : 0,
                            'social_sharing_linkedin' => isset($data_blog_meta['social_sharing_linkedin']) ? $data_blog_meta['social_sharing_linkedin'] : '',
                            'header' => isset($data_blog_meta['header']) ? $data_blog_meta['header'] : '',
                            'legacy_tab_id' => isset($data_blog_meta['legacy_tab_id']) ? $data_blog_meta['legacy_tab_id'] : 0,
                            'subscription_email_type' => isset($data_blog_meta['subscription_email_type']) ? $data_blog_meta['subscription_email_type'] : '',
                            'social_sharing_digg' => isset($data_blog_meta['social_sharing_digg']) ? $data_blog_meta['social_sharing_digg'] : '',
                            'social_sharing_twitter' => isset($data_blog_meta['social_sharing_twitter']) ? $data_blog_meta['social_sharing_twitter'] : '',
                            'rss_item_header' => isset($data_blog_meta['rss_item_header']) ? $data_blog_meta['rss_item_header'] : '',
                            'social_sharing_twitter_account' => isset($data_blog_meta['social_sharing_twitter_account']) ? $data_blog_meta['social_sharing_twitter_account'] : '',
                            'social_sharing_reddit' => isset($data_blog_meta['social_sharing_reddit']) ? $data_blog_meta['social_sharing_reddit'] : '',
                            'weekly_notification_email_id' => isset($data_blog_meta['weekly_notification_email_id']) ? $data_blog_meta['weekly_notification_email_id'] : 0,
                            'legacy_guid' => isset($data_blog_meta['legacy_guid']) ? $data_blog_meta['legacy_guid'] : '',
                            'social_sharing_googleplusone' => isset($data_blog_meta['social_sharing_googleplusone']) ? $data_blog_meta['social_sharing_googleplusone'] : '',
                            'rss_custom_feed' => isset($data_blog_meta['rss_custom_feed']) ? $data_blog_meta['rss_custom_feed'] : '',
                            'social_sharing_googlebuzz' => isset($data_blog_meta['social_sharing_googlebuzz']) ? $data_blog_meta['social_sharing_googlebuzz'] : '',
                            'social_sharing_email' => isset($data_blog_meta['social_sharing_email']) ? $data_blog_meta['social_sharing_email'] : '',
                            'social_sharing_facebook_like' => isset($data_blog_meta['social_sharing_facebook_like']) ? $data_blog_meta['social_sharing_facebook_like'] : '',
                            'legacy_module_id' => isset($data_blog_meta['legacy_module_id']) ? $data_blog_meta['legacy_module_id'] : 0,
                            'language' => isset($data_blog_meta['language']) ? $data_blog_meta['language'] : '',
                            'social_publishing_slug' => isset($data_blog_meta['social_publishing_slug']) ? $data_blog_meta['social_publishing_slug'] : '',
                            'rss_description' => isset($data_blog_meta['rss_description']) ? $data_blog_meta['rss_description'] : '',
                            'rss_item_footer' => isset($data_blog_meta['rss_item_footer']) ? $data_blog_meta['rss_item_footer'] : '',
                            'social_sharing_facebook_send' => isset($data_blog_meta['social_sharing_facebook_send']) ? $data_blog_meta['social_sharing_facebook_send'] : '',
                            'social_sharing_delicious' => isset($data_blog_meta['social_sharing_delicious']) ? $data_blog_meta['social_sharing_delicious'] : '',
                            'social_sharing_stumbleupon' => isset($data_blog_meta['social_sharing_stumbleupon']) ? $data_blog_meta['social_sharing_stumbleupon'] : '',
                            'created_at' => Carbon::now()->toDateTimeString(),
                            'updated_at' => Carbon::now()->toDateTimeString()
                        ];
                        //check if blog is already inserted or not
                        if ($check_blog === null) {
                            
                        } else {
                            $last_updated_date = Carbon::createFromTimestampMs($data_blog_meta['updated_date_time'])->format('Y-m-d H:i:s');

                            //check if any data is changed
                            if ($last_updated_date != $check_blog->updated_date_time) {
                                $updated_rows +=1;
                            }
                        }
                    }
                }
                //insert blogs in bulk
                CmsBlog::insert($create_blogs);
                $created_rows += count($create_blogs);
            }
        }
        if (isset($data['offset']) && !empty($data['offset'] !== 0)) {

            return $this->getCmsBlogsData($headers, $data['offset'], $client, $backup_progress, $user, $hubspot_blog_ids, $status, $error_msg, $created_rows, $updated_rows);
        } else {

            $data = ['created_rows' => $created_rows, 'updated_rows' => $updated_rows,
                'hubspot_blog_ids' => $hubspot_blog_ids, 'status' => $status, 'error_msg' => $error_msg];

            return $data;
        }
    }

}
