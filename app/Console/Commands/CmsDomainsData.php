<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\BadResponseException;
use Illuminate\Support\Facades\Artisan;
// models
use App\CmsDomain;
use App\BackupHistory;
use App\User;
use Illuminate\Support\Facades\Log;
use App\BackupProgress;
use App\Notifications\Backup;
use App\Notification;
use App\SmartAlert;
use App\SmartAlertHistory;
use Carbon\Carbon;

class CmsDomainsData extends Command {

    /**
     * The name of the apiUrl
     * @var type 
     */
    protected $api_url;

    /**
     * The name of the apiKey
     * @var type 
     */
    protected $api_key;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cms-domains:data {userId}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command will get all CMS Domains data';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();

        //get api key from env file
        $this->api_url = env('HUBSPOT_URL');
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        $this->info('CMS Domains Data cron started....' . $this->api_url);

        $status = '';
        $error_msg = '';
        $created_rows = 0;
        $updated_rows = 0;
        $hubspot_cms_domain_ids = [];

        $userId = $this->argument('userId');
        $user = User::find($userId);

        $backup_progress = BackupProgress::where('user_id', $user['id'])->where('status', 'Inprogress')->orderByDesc('id')->first();

        try {

            Log::error('Fetching CMS Domains data started |||| Start >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> userId - ' . $userId);

            //start time
            $time_start = microtime(true);


            //user token
            $token = $user->accesstoken;

            //using GuzzleHttp client to get request
            $client = new \GuzzleHttp\Client(['http_errors' => false]);
            $headers = [
                'Authorization' => 'Bearer ' . $token,
                'Accept' => 'application/json',
            ];

            $get_data = $this->getCmsDomainsData($headers, 0, $client, $backup_progress, $user, $hubspot_cms_domain_ids, $status, $error_msg, $created_rows, $updated_rows);

            $created_rows = $get_data['created_rows'];
            $updated_rows = $get_data['updated_rows'];
            $hubspot_cms_domain_ids = $get_data['hubspot_cms_domain_ids'];
            $error_msg = $get_data['error_msg'];
            $status = $get_data['status'];

            //end time
            $time_end = microtime(true);
            //execution time
            $execution_time = ($time_end - $time_start);

            Log::error('time taken for CMS Domains data ' . $execution_time);
            Log::error('Fetching CMS Domains data completed |||| End <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< userId - ' . $userId);
        } catch (BadResponseException $ex) {
            //if request is invalid
            $response = $ex->getResponse();
            $jsonBody = (string) $response->getBody();
            $this->error($jsonBody);

            $data = json_decode($response->getBody(), true);
            $status = $data['status'];
            $error_msg = $data['message'];
            Log::error('CMS Domains data  |||| Error - ' . $jsonBody);
        }


        //get cms domains from database
        $cms_domains_in_database = CmsDomain::where('backup_progress_id', $backup_progress['id'] - 1)->get()->pluck('cms_domain_id')->toArray();

        //check if empty result 
        if (count($hubspot_cms_domain_ids) == 0) {
            $removed_rows = 0;
        } else {
            $removed_cms_domains = array_diff($cms_domains_in_database, $hubspot_cms_domain_ids);
            $removed_rows = count($removed_cms_domains);
        }

        //creating BackupHistory record
        $backup_history = BackupHistory::create([
                    'backup_progress_id' => $backup_progress['id'],
                    'user_id' => $user->id,
                    'object_name' => 'Fetched CMS Domains data',
                    'num_of_records' => $created_rows + $removed_rows,
                    'removed_records' => $removed_rows,
                    'added_records' => $created_rows,
                    'changed_records' => $updated_rows,
                    'num_of_API_calls' => 1,
                    'status' => $status,
                    'error_message' => $error_msg]);

        //get smart alerts for action 'added'
        $added_alert = SmartAlert::where('user_id', $user['id'])
                        ->where('s_object', 'CMS Domains')
                        ->where('action', 'Added')->first();

        if (!empty($added_alert)) {
            if ($created_rows > $added_alert->amount) {

                //notification when CMS Blog Domain added
                $add_notificatoion = Notification::create(['user_id' => $user['id'],
                            'backup_progress_id' => $backup_progress['id'],
                            'status' => 'CMS Domains Added',
                            'type' => 'email',
                            'num_of_records' => $created_rows]);

                //create history
                SmartAlertHistory::create(['user_id' => $user['id'], 'smart_alert_id' => $added_alert->id]);

                //notify user
                //$user->notify(new Backup($notificatoion));
            }
        }

        //get smart alerts for action 'Changed'
        $changed_alert = SmartAlert::where('user_id', $user['id'])
                        ->where('s_object', 'CMS Domains')
                        ->where('action', 'Changed')->first();

        if (!empty($changed_alert)) {
            if ($updated_rows > $changed_alert->amount) {

                //notification when Blog Domain Changed
                $change_notificatoion = Notification::create(['user_id' => $user['id'],
                            'backup_progress_id' => $backup_progress['id'],
                            'status' => 'CMS Domains Changed',
                            'type' => 'email',
                            'num_of_records' => $updated_rows]);

                //create history
                SmartAlertHistory::create(['user_id' => $user['id'], 'smart_alert_id' => $changed_alert->id]);

                //notify user
                //$user->notify(new Backup($notificatoion));
            }
        }

        //get smart alerts for action 'Removed'
        $removed_alert = SmartAlert::where('user_id', $user['id'])
                        ->where('s_object', 'CMS Domains')
                        ->where('action', 'Removed')->first();

        if (!empty($removed_alert)) {
            if ($removed_rows > $removed_alert->amount) {

                //notification when CMS Domain Removed
                $remove_notificatoion = Notification::create(['user_id' => $user['id'],
                            'backup_progress_id' => $backup_progress['id'],
                            'status' => 'CMS Domains Removed',
                            'type' => 'email',
                            'num_of_records' => $removed_rows]);

                //create history
                SmartAlertHistory::create(['user_id' => $user['id'], 'smart_alert_id' => $removed_alert->id]);

                //notify user
                //$user->notify(new Backup($notificatoion));
            }
        }
    }

    public function getCmsDomainsData($headers, $offset, $client, $backup_progress, $user, $hubspot_cms_domain_ids, $status, $error_msg, $created_rows, $updated_rows) {

        $url = $this->api_url . '/cos-domains/v1/domains?limit=100&offset=' . $offset;

        //get all cms blog Domains data
        $response = $client->request('GET', $url, ['headers' => $headers]);


        $body = $response->getBody();
        $data = json_decode($body, true);


        if (!empty($data['status']) == 'error') {
            $status = $data['status'];
            $error_msg = $data['message'];
            Log::error('CMS Domains data  |||| Error - ' . $error_msg);
        } else {
            $results = $data['objects'];
            $status = 'successed';
            if (!empty($results)) {
                $create_domains = [];
                foreach ($results as $value) {

                    //get all cms domain ids
                    $hubspot_cms_domain_ids[] = $value['id'];


                    $check_cms_domain = CmsDomain::where('user_id', $user->id)
                                    ->where('cms_domain_id', $value['id'])
                                    ->where('backup_progress_id', $backup_progress['id'] - 1)->first();

                    $create_domains[] = [
                        'user_id' => $user->id,
                        'backup_progress_id' => $backup_progress['id'],
                        'cms_domain_id' => $value['id'],
                        'portal_id' => isset($value['portalId']) ? $value['portalId'] : 0,
                        'actual_cname' => isset($value['actualCname']) ? $value['actualCname'] : "",
                        'correct_cname' => isset($value['correctCname']) ? $value['correctCname'] : "",
                        'actual_ip' => isset($value['actualIp']) ? $value['actualIp'] : "",
                        'consecutive_non_resolving_count' => isset($value['consecutiveNonResolvingCount']) ? $value['consecutiveNonResolvingCount'] : 0,
                        'domain' => isset($value['domain']) ? $value['domain'] : "",
                        'full_category_key' => isset($value['fullCategoryKey']) ? $value['fullCategoryKey'] : "",
                        'name' => isset($value['name']) ? $value['name'] : "",
                        'is_any_primary' => isset($value['isAnyPrimary']) ? $value['isAnyPrimary'] : 0,
                        'is_dns_correct' => isset($value['isDnsCorrect']) ? $value['isDnsCorrect'] : 0,
                        'is_internal_domain' => isset($value['isInternalDomain']) ? $value['isInternalDomain'] : 0,
                        'is_legacy' => isset($value['isLegacy']) ? $value['isLegacy'] : 0,
                        'is_legacy_domain' => isset($value['isLegacyDomain']) ? $value['isLegacyDomain'] : 0,
                        'is_resolving' => isset($value['isResolving']) ? $value['isResolving'] : 0,
                        'is_ssl_enabled' => isset($value['isSslEnabled']) ? $value['isSslEnabled'] : 0,
                        'is_ssl_only' => isset($value['isSslOnly']) ? $value['isSslOnly'] : 0,
                        'is_used_for_blog_post' => isset($value['isUsedForBlogPost']) ? $value['isUsedForBlogPost'] : 0,
                        'is_used_for_site_page' => isset($value['isUsedForSitePage']) ? $value['isUsedForSitePage'] : 0,
                        'is_used_for_landing_page' => isset($value['isUsedForLandingPage']) ? $value['isUsedForLandingPage'] : 0,
                        'is_used_for_email' => isset($value['isUsedForEmail']) ? $value['isUsedForEmail'] : 0,
                        'is_used_for_knowledge' => isset($value['isUsedForKnowledge']) ? $value['isUsedForKnowledge'] : 0,
                        'is_setup_complete' => isset($value['isSetupComplete']) ? $value['isSetupComplete'] : 0,
                        'manually_marked_as_resolving' => isset($value['manuallyMarkedAsResolving']) ? $value['manuallyMarkedAsResolving'] : 0,
                        'primary_blog' => isset($value['primaryBlog']) ? $value['primaryBlog'] : 0,
                        'primary_blog_post' => isset($value['primaryBlogPost']) ? $value['primaryBlogPost'] : 0,
                        'primary_email' => isset($value['primaryEmail']) ? $value['primaryEmail'] : 0,
                        'primary_landing_page' => isset($value['primaryLandingPage']) ? $value['primaryLandingPage'] : 0,
                        'primary_legacy_page' => isset($value['primaryLegacyPage']) ? $value['primaryLegacyPage'] : 0,
                        'primary_knowledge' => isset($value['primaryKnowledge']) ? $value['primaryKnowledge'] : 0,
                        'primary_site_page' => isset($value['primarySitePage']) ? $value['primarySitePage'] : 0,
                        'secondary_to_domain' => isset($value['secondaryToDomain']) ? $value['secondaryToDomain'] : "",
                        'primary_click_tracking' => isset($value['primaryclickTracking']) ? $value['primaryclickTracking'] : 0,
                        'apex_domain' => isset($value['apexDomain']) ? $value['apexDomain'] : "",
                        'public_suffix' => isset($value['publicSuffix']) ? $value['publicSuffix'] : "",
                        'site_id' => isset($value['siteId']) ? $value['siteId'] : 0,
                        'brand_id' => isset($value['brandId']) ? $value['brandId'] : "",
                        'deletable' => isset($value['deletable']) ? $value['deletable'] : 0,
                        'cos_object_type' => isset($value['cosObjectType']) ? $value['cosObjectType'] : "",
                        'is_resolving_internal_property' => isset($value['isResolvingInternalProperty']) ? $value['isResolvingInternalProperty'] : 0,
                        'is_resolving_ignoring_manually_marked_as_resolving' => isset($value['isResolvingIgnoringManuallyMarkedAsResolving']) ? $value['isResolvingIgnoringManuallyMarkedAsResolving'] : 0,
                        'is_used_for_any_content_type' => isset($value['isUsedForAnyContentType']) ? $value['isUsedForAnyContentType'] : 0,
                        'created' => isset($value['created']) ? Carbon::createFromTimestampMs($value['created'])->format('Y-m-d H:i:s') : NULL,
                        'updated' => isset($value['updated']) ? Carbon::createFromTimestampMs($value['updated'])->format('Y-m-d H:i:s') : NULL,
                        'created_at' => Carbon::now()->toDateTimeString(),
                        'updated_at' => Carbon::now()->toDateTimeString()
                    ];

                    //check if blog Domain  is already inserted or not
                    if ($check_cms_domain === null) {
                        
                    } else {
                        $last_updated_date = Carbon::createFromTimestampMs($value['updated'])->format('Y-m-d H:i:s');

                        //check if any data is changed
                        if ($last_updated_date != $check_cms_domain->updated) {

                            $updated_rows +=1;
                        }
                    }
                }

                //insert cms domains in bulk
                CmsDomain::insert($create_domains);
                $created_rows += count($create_domains);
            }
        }
        if (isset($data['offset']) && !empty($data['offset'] !== 0)) {

            return $this->getCmsDomainsData($headers, $data['offset'], $client, $backup_progress, $user, $hubspot_cms_domain_ids, $status, $error_msg, $created_rows, $updated_rows);
        } else {

            $data = ['created_rows' => $created_rows, 'updated_rows' => $updated_rows,
                'hubspot_cms_domain_ids' => $hubspot_cms_domain_ids, 'status' => $status, 'error_msg' => $error_msg];

            return $data;
        }
    }

}
