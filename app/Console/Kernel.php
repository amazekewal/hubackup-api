<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use App\User;
use GuzzleHttp\Client;

class Kernel extends ConsoleKernel {

    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        Commands\AnalyticsData::class,
        Commands\BroadcastMessagesData::class,
        Commands\CalendarEventsData::class,
        Commands\CampaignsData::class,
        Commands\CmsBlogAuthorsData::class,
        Commands\CmsBlogCommentsData::class,
        Commands\CmsBlogPostsData::class,
        Commands\CmsBlogTopicsData::class,
        Commands\CmsBlogsData::class,
        Commands\CmsDomainsData::class,
        Commands\CmsFilesData::class,
        Commands\CmsHubdbTablesData::class,
        Commands\CmsLayoutsData::class,
        Commands\CmsPagePublshingData::class,
        Commands\CmsPipelinesData::class,
        Commands\CmsSiteMapsData::class,
        Commands\CmsTemplatesData::class,
        Commands\CmsUrlMappingsData::class,
        Commands\CompaniesData::class,
        Commands\ContactsData::class,
        Commands\DealsData::class,
        Commands\EmailEventsData::class,
        Commands\EmailSubscriptionsData::class,
        Commands\EngagementsData::class,
        Commands\EventsData::class,
        Commands\FormsData::class,
        Commands\MarketingEmailsData::class,
        Commands\OwnersData::class,
        Commands\ProductsData::class,
        Commands\PublishingChannelsData::class,
        Commands\TicketsData::class,
        Commands\WorkflowsData::class,
        Commands\BackupStarted::class,
        Commands\BackupEnded::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule) {
        $users = User::where('accesstoken', '!=', null)->get();
        $api_url = env('HUBSPOT_URL');
        
        if(!empty($api_url)){
        if(count($users)>0) {
        foreach ($users as $user) {
            //user token
            $token = $user->accesstoken;
            $refreshtoken = $user->refreshtoken;
                   //using GuzzleHttp client to get request
                    $client = new \GuzzleHttp\Client(['http_errors' => false]);
                    $headers = [
                        'Content-Type' => 'application/x-www-form-urlencoded;charset=utf-8;'
                    ];
                    $body = [
                        'grant_type' => 'refresh_token',
                        'client_id' => '889a2161-1aa0-4842-8e05-b7e87cf4a39f',
                        'client_secret' => '8ce8de3e-f210-45d5-83c0-94555b0b9f30',
                        'redirect_uri' => 'https://app.hubackup.com/dashboard/backup',
                        'refresh_token' => $refreshtoken,
                    ];
                  
                    $url = $api_url . '/oauth/v1/token';

                    $response = $client->request('POST', $url, ['headers' => $headers,'form_params'=> $body]);

                    $body = $response->getBody();
                    $data = json_decode($body, true);
                    
                    $user_update = User::where('id',$user->id)->update(['accesstoken' => $data['access_token']]);
                    
                    
                    //if user update
                    if($user_update){
                        
                    //schedule commands
                    $schedule->command('backup:started ' . $user->id)->everyMinute();
                    $schedule->command('analytics:data ' . $user->id)->everyMinute();
                    $schedule->command('broadcast-messages:data ' . $user->id)->everyMinute();
                    $schedule->command('calendar-events:data ' . $user->id)->everyMinute();
                    $schedule->command('email-campaigns:data ' . $user->id)->everyMinute();
                    $schedule->command('cms-blog-authors:data ' . $user->id)->everyMinute();
                    $schedule->command('cms-blog-comments:data ' . $user->id)->everyMinute();
                    $schedule->command('cms-blog-posts:data ' . $user->id)->everyMinute();
                    $schedule->command('cms-blog-topics:data ' . $user->id)->everyMinute();
                    $schedule->command('cms-blogs:data ' . $user->id)->everyMinute();
                    $schedule->command('cms-domains:data ' . $user->id)->everyMinute();
                    $schedule->command('cms-files:data ' . $user->id)->everyMinute();
                    $schedule->command('cms-hubdb:data ' . $user->id)->everyMinute();
                    $schedule->command('cms-layouts:data ' . $user->id)->everyMinute();
                    $schedule->command('cms-page-publishing:data ' . $user->id)->everyMinute();
                    $schedule->command('cms-pipelines:data ' . $user->id)->everyMinute();
                    $schedule->command('cms-site-maps:data ' . $user->id)->everyMinute();
                    $schedule->command('cms-templates:data ' . $user->id)->everyMinute();
                    $schedule->command('cms-url-mappings:data ' . $user->id)->everyMinute();
                    $schedule->command('companies:data ' . $user->id)->everyMinute();
                    $schedule->command('contacts:data ' . $user->id)->everyMinute();
                    $schedule->command('deals:data ' . $user->id)->everyMinute();
                    $schedule->command('email-events:data ' . $user->id)->everyMinute();
                    $schedule->command('email-subscriptions:data ' . $user->id)->everyMinute();
                    $schedule->command('engagements:data ' . $user->id)->everyMinute();
                    $schedule->command('events:data ' . $user->id)->everyMinute();
                    $schedule->command('forms:data ' . $user->id)->everyMinute();
                    $schedule->command('marketing-emails:data ' . $user->id)->everyMinute();
                    $schedule->command('owners:data ' . $user->id)->everyMinute();
                    $schedule->command('products:data ' . $user->id)->everyMinute();
                    $schedule->command('publishing-channels:data ' . $user->id)->everyMinute();
                    $schedule->command('tickets:data ' . $user->id)->everyMinute();
                    $schedule->command('workflows:data ' . $user->id)->everyMinute();
                    $schedule->command('backup:ended ' . $user->id)->everyMinute();
                    }
              
                }
        }
      }
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands() {
        $this->load(__DIR__ . '/Commands');
        require base_path('routes/console.php');
    }

}
