<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CmsPipelineStage extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'cms_pipeline_stages';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['pipeline_db_id', 'stage_id','label','display_order','ticket_state','probability',
        'is_closed','active','created_date','updated_date'];
    
    protected $hidden = ['id','created_at','updated_at','pipeline_db_id'];

    
    public function pipeline() {
        return $this->belongsTo('App\CmsPipeline', 'pipeline_db_id', 'id');
    }
}
