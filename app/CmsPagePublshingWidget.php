<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CmsPagePublshingWidget extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'cms_page_publishing_widgets';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['page_publishing_db_id','widget_container','widget_id','body_name','body_is_widget_block',
                          'body_type','body_tag','body_label','body_widget_type','body_html','body_overrideable',
                          'body_export_to_template_context','body_org_tag','body_no_wrapper','group','body_fields_initial',
                          'body_fields_type','body_fields_name','body_fields_label','label','meta','common_name',
                          'common_is_widget_block','common_type','common_tag','common_label','common_widget_type',
                          'common_html','common_overrideable','common_export_to_template_context','common_org_tag',
                          'common_no_wrapper','key','order','type'];
    
    protected $hidden = ['page_publishing_db_id','created_at','updated_at'];
    
    public function page_publshing() {
        return $this->belongsTo('App\PagePublshing', 'page_publishing_db_id', 'id');
    }
}
