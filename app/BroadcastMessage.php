<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BroadcastMessage extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'broadcast_messages';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id','backup_progress_id','portal_id','broadcast_guid','group_guid','campaign_guid','channel_guid',
        'client_tag','status','message','body','link_preview_suppressed','link','original_body','uncompressed_links',
        'link_guid','message_url','foreign_id','task_queue_id','link_task_queue_id','remote_content_id','remote_content_type',
        'clicks','created_by','updated_by','interactions','interaction_counts','created_on','trigger_at','finished_at'];
    
    protected $hidden = ['id','user_id','created_at', 'updated_at','backup_progress_id'];
    
    public function user() {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }
}
