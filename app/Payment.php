<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
     /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'payments';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id', 'json_response'];
    
    public function user() {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }
}
