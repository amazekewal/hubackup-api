<?php

namespace App\Api\V1\Controllers;

use Config;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use JWTAuth;
use DB;
use App\User;

class AdminController extends Controller {

    /**
     * get users.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {

        $users = User::all();

        return response()->json([
                    'users' => $users
        ]);
    }

    public function user($id) {

        $user = User::where('id', $id)->first();

        return response()->json([
                    'user' => $user
        ]);
    }

    public function destroy(Request $request) {

        if (!empty($request->input('id'))) {
            User::find($request->input('id'))->delete();

            return response()->json([
                        'message' => 'User deleted successfully']);
        }
    }

}
