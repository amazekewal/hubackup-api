<?php

namespace App\Api\V1\Controllers;

use Symfony\Component\HttpKernel\Exception\HttpException;
use Tymon\JWTAuth\JWTAuth;
use App\Http\Controllers\Controller;
use App\Api\V1\Requests\LoginRequest;
use Tymon\JWTAuth\Exceptions\JWTException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Auth;

class LoginController extends Controller {

    /**
     * Log the user in
     *
     * @param LoginRequest $request
     * @param JWTAuth $JWTAuth
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(LoginRequest $request, JWTAuth $JWTAuth) {

        $credentials = $request->only(['email', 'password']);
        $user = \App\User::where('email',$request->input('email'))->first();
        if($user['lastLogin'] == null){
            $first_login = true;
        }else{
            $first_login = false;
        }      
        try {

            //$token = Auth::guard()->attempt($credentials);
            $token = $JWTAuth->attempt($credentials);
            

            if (!$token) {
                
                abort(401, "invalid email and password");
                //throw new AccessDeniedHttpException();
            }else{
                $user->lastLogin = \Carbon\Carbon::now()->toDateTimeString();
                $user->save();
            }
        } catch (JWTException $e) {
            throw new HttpException(500);
        }
        
        return response()
                        ->json([
                            'status' => 'ok',
                            'token' => $token,
                            'first_login' => $first_login,
                           // 'expires_in' => Auth::guard()->factory()->getTTL() * 60,
                            'user' => $JWTAuth->user(),
        ]);
    }

}
