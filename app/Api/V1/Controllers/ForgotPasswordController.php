<?php

namespace App\Api\V1\Controllers;

use DB;
use Hash;
use Carbon\Carbon;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Password;
use App\Api\V1\Requests\ForgotPasswordRequest;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use App\Notifications\PasswordReset;

class ForgotPasswordController extends Controller {

    public function sendResetEmail(ForgotPasswordRequest $request) {
        $user = User::where('email', '=', $request->get('email'))->first();
        if (!$user) {
            throw new HttpException(500, 'Email not exist.');
        }
//create a new token to be sent to the user. 
        DB::table('password_resets')->insert([
            'email' => $request->email,
            'token' => str_random(60), //change 60 to any length you want
            'created_at' => Carbon::now()
        ]);

        $tokenData = DB::table('password_resets')
                        ->where('email', $request->email)->orderByDesc('email')->first();

        $token = $tokenData->token;
        $email = $request->email; // or $email = $tokenData->email;
        $user->notify(new PasswordReset($token));
        return response()->json([
                    'status' => 'ok',
                    'message' => 'Email send successfully'
                        ], 200);

        /**
         * Send email to the email above with a link to your password reset
         * something like url('password-reset/' . $token)
         * Sending email varies according to your Laravel version. Very easy to implement
         */
    }

}
