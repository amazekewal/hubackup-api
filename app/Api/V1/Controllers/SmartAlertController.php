<?php

namespace App\Api\V1\Controllers;
use JWTAuth;
use App\SmartAlert;
use App\SmartAlertHistory;
use App\HubackupObject;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SmartAlertController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('jwt.auth');
    }
    
    /**
     * Display a listing of the Smart Alerts.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(JWTAuth::user()->payment == null){
            $user_id = 0;
        }else{
            $user_id = JWTAuth::user()->id;
        }
        
        $smartalerts = SmartAlert::where('user_id',$user_id)->get();
        
        return response()->json([
                        'smartalerts' => $smartalerts]);
    }

    

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       $check_alert = SmartAlert::where('user_id',JWTAuth::user()->id)
                                  ->where('s_object',$request->s_object)
                                  ->where('action',$request->action)
                                  ->first();
       if(!empty($check_alert)){
           return response()->json([
                        'status' => 'error',
                        'message' => "Alert is already added",
            ]);
           
       }else{
       $smartalert =  SmartAlert::create([
                            'user_id' => JWTAuth::user()->id,
                            's_object'=>$request->s_object,
                            'action' => $request->action,
                            'operator' => $request->operator,
                            'amount' => $request->amount,
                            'unit' => 'records']);
       if($smartalert){
           return response()->json([
                        'status' => 'ok',
                        'message' => "Alert added successfully",
            ]);
       }
       }
    }

   
    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        if(!empty($request->input('id'))){
        SmartAlert::find($request->input('id'))->delete();
        
        return response()->json([
                        'message' => 'Alert deleted successfully']);
        }
    }
    
    /**
     * Display  history of  Smart Alerts.
     *
     * @return \Illuminate\Http\Response
     */
    public function history()
    {
        $alert_history = [];
        
        if(JWTAuth::user()->payment == null){
            $user_id = 0;
        }else{
            $user_id = JWTAuth::user()->id;
        }
        
        $smart_alert_history = SmartAlertHistory::where('user_id',$user_id)->get();
       
        foreach($smart_alert_history as $history){
            $smartalert = SmartAlert::where('id',$history->smart_alert_id)->first();
            $alert_history[] = ['id' => $history->id,
                        's_object'=>$smartalert->s_object,
                        'action' => $smartalert->action,
                        'operator' => $smartalert->operator,
                        'amount' => $smartalert->amount,
                        'unit' => $smartalert->unit]; 
        }
        
        return response()->json([
                        'smartalerts' => $alert_history]);
    }
    
    /**
     * Destroy  history of  Smart Alerts.
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function history_destroy(Request $request)
    {
        
        if(!empty($request->input('id'))){
            SmartAlertHistory::find($request->input('id'))->delete();
        
        return response()->json([
                        'message' => 'History deleted successfully']);
        }
        
    }
    
    /**
     * get all hubackup objects.
     *
     * @return \Illuminate\Http\Response
     */
    public function hubackup_objects()
    {
        $objects = HubackupObject::all();
        return response()->json([
                        'objects' => $objects]);
    }
}
