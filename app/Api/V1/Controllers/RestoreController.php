<?php

namespace App\Api\V1\Controllers;

use Illuminate\Http\Request;

class RestoreController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('jwt.auth');
    }
    
    /**
     * find functionality
     * @param Request $request
     * @return type
     */
    public function restore(Request $request){
        
        $table = $request->table;
        $backup_progress_id = $request->backup_progress_id;
        $record_ids = $request->ids;
        $action = $request->action;
        
            //user token
            $token = JWTAuth::user()->accesstoken;

            //using GuzzleHttp client to get request
            $client = new \GuzzleHttp\Client(['http_errors' => false]);
            $headers = [
                'Authorization' => 'Bearer ' . $token,
                'Accept' => 'application/json',
            ];
            
        if($action == 'insert'){
            $url = $this->api_url . '/companies/v2/companies/recent/modified?limit=100&offset=' . $offset;
            
            $body = [];
            //$data = json_encode($body);
            //get all companies data
           // $response = $client->request('POST', $url, ['headers' => $headers,\GuzzleHttp\RequestOptions::JSON => $body]);


           // $body = $response->getBody();
           // $data = json_decode($body, true);
        }
    }
}
