<?php

namespace App\Api\V1\Controllers;
use JWTAuth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Stripe\Error\Card;
use Stripe;
use App\User;
use App\Payment;
class StripeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('jwt.auth');
    }
    
    /**
     * Stripe payment
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        
        try {
        $token = $request->token;
        $amount = $request->amount;
        if (!isset($token)) {
             return response()->json([
                        'message' => 'token not provided']);
        }
        Stripe\Stripe::setApiKey('sk_test_i3554Sp8ZFXxhJ1BFoin15a2');
        $charge = Stripe\Charge::create([
                    'source' => $token,
                    'currency' => 'USD',
                    'amount' => $amount,
                    'description' => 'wallet',
        ]);
        
        if($charge['status'] == 'succeeded') {
            
            $user = JWTAuth::user();
            $user->payment = 1;
            $user->save();
            
            //store payment details
            $payment =  Payment::create(['user_id' => $user->id,
                             'json_response' => $charge]);
            if($payment){
               return response()->json([
                            'message' => 'Payment done successfully']);
            }
        } else {
            return response()->json([
                        'error' => 'Money not add in wallet!!']);
        }
        } catch (Exception $e) {
            return response()->json([
                        'error' => $e->getMessage()]);
        } catch(\Cartalyst\Stripe\Exception\CardErrorException $e) {
            return response()->json([
                        'error' => $e->getMessage()]);
        } catch(\Cartalyst\Stripe\Exception\MissingParameterException $e) {
            return response()->json([
                        'error' => $e->getMessage()]);
        }
        
        
    }

    

   
}
