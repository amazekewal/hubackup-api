<?php

namespace App\Api\V1\Controllers;

use Config;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use JWTAuth;
use DB;
use App\User;
use App\BackupHistory;
use App\BackupProgress;
use Artisan;


class DashboardController extends Controller {

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('jwt.auth');
    }

    /**
     * All backups for dashboard page 
     * @param Request $request
     * @return type
     */
    public function all_backups(Request $request) {

        $page = $request->page;

        if ($page == 0) {
            $skip = 0;
        } else {
            $skip = $page;
        }
        
        if(JWTAuth::user()->payment == null){
            $user_id = 0;
        }else{
            $user_id = JWTAuth::user()->id;
        }
        
        $backup_progress = BackupProgress::select('id')
                            ->selectRaw('DATE_FORMAT(start_date,"%M %d, %Y") as date')
                            ->selectRaw('DATE_FORMAT(start_date,"%l:%i %p") as time')
                            ->where('user_id', $user_id)
                            ->where('status', 'done')
                            ->orderByDesc('id')
                            ->skip($skip * 6)
                            ->take(6)
                            ->get();
                            
        

        //all backups count
        $backup_progress_count = BackupProgress::where('user_id', $user_id)->where('status','done')->get();
        $backup_all_count = count($backup_progress_count);

        //if last data is true then there is no more data
        $last_data = false;

        $total_pages = $backup_all_count / 6;
        if ($total_pages == 0) {
            $last_data = true;
        } else {
            if (strpos($total_pages, ".") === 1) {
                $explode = explode('.', $total_pages);
                if ($page == $explode[0]) {
                    $last_data = true;
                } else {
                    $last_data = false;
                }
            } else {
                if ($page + 1 == $total_pages) {
                    $last_data = true;
                }
            }
        }


//        $error_backup = [];
//        $success_backup = [];
//        $warning_backup = [];
//
//        if (!empty($backup_progress_count)) {
//            foreach ($backup_progress_count as $progress_count) {
//
//                $backup_history = \App\BackupHistory::where('backup_progress_id', $progress_count->id)
//                        ->get();
//                $msg = '';
//                foreach ($backup_history as $history) {
//
//                    if ($history->status == 'successed') {
//                        $msg = 'successed';
//                    } elseif ($history->status == 'error') {
//                        $msg = 'error';
//                    } elseif ($history->status == 'warning') {
//                        $msg = 'warning';
//                    }
//                }
//                if ($msg == 'successed') {
//                    $success_backup[] = ['date' => \Carbon\Carbon::parse($progress_count['created_at'])->format('M d, Y'),
//                        'time' => \Carbon\Carbon::parse($progress_count['created_at'])->format('g:i A')];
//                } elseif ($msg == 'error') {
//                    $error_backup[] = ['date' => \Carbon\Carbon::parse($progress_count['created_at'])->format('M d, Y'),
//                        'time' => \Carbon\Carbon::parse($progress_count['created_at'])->format('g:i A')];
//                } elseif ($msg == 'warning') {
//                    $warning_backup[] = ['date' => \Carbon\Carbon::parse($progress_count['created_at'])->format('M d, Y'),
//                        'time' => \Carbon\Carbon::parse($progress_count['created_at'])->format('g:i A')];
//                }
//            }
//        }

        //backup count
//        $backup_success_count = sizeof($success_backup);
//        $backup_error_count = sizeof($error_backup);
//        $backup_warning_count = sizeof($warning_backup);


        $backup_running_count = BackupProgress::selectRaw('DATE_FORMAT(created_at,"%M %d, %Y") as date')
                ->selectRaw('DATE_FORMAT(created_at,"%l:%i %p") as time')
                ->where('user_id', JWTAuth::user()->id)
                ->where('status', 'Inprogress')
                ->count();

        
        return response()->json(['dates' => $backup_progress,
                    'all_count' => $backup_all_count,
//                    'success_count' => $backup_success_count,
//                    'warning_count' => $backup_warning_count,
//                    'error_count' => $backup_error_count,
                    'running_count' => $backup_running_count,
                    'last_data' => $last_data]);
    }

    /**
     * Successfull backups for dashboard page 
     * @param Request $request
     * @return type
     */
//    public function success_backups(Request $request) {
//
//        $page = $request->page;
//
//        if ($page == 0) {
//            $skip = 0;
//        } else {
//            $skip = $page;
//        }
//
//        $backup_progress = BackupProgress::where('user_id', JWTAuth::user()->id)
//                ->orderByDesc('id')
//                ->skip($skip * 6)
//                ->take(6)
//                ->get();
//
//        $success_backup = [];
//
//        if (!empty($backup_progress)) {
//            foreach ($backup_progress as $progress) {
//
//
//                $backup_history = \App\BackupHistory::where('backup_progress_id', $progress->id)
//                        ->get();
//
//                $success = false;
//                foreach ($backup_history as $history) {
//
//                    if ($history->status == 'successed') {
//                        $success = true;
//                    } else {
//                        $success = false;
//                    }
//                }
//                if ($success == true) {
//                    $success_backup[] = [
//                        'id' => $progress['id'],
//                        'date' => \Carbon\Carbon::parse($progress['created_at'])->format('M d, Y'),
//                        'time' => \Carbon\Carbon::parse($progress['created_at'])->format('g:i A')];
//                }
//            }
//        }
//
//
//        //for view more data
//        $success_backup_all = [];
//        $backup_progress_count = BackupProgress::where('user_id', JWTAuth::user()->id)->get();
//
//        if (!empty($backup_progress_count)) {
//            foreach ($backup_progress_count as $progress_count) {
//
//                $backup_history_success = \App\BackupHistory::where('backup_progress_id', $progress_count->id)
//                        ->get();
//                $msg = false;
//                foreach ($backup_history_success as $history_success) {
//
//                    if ($history_success->status == 'successed') {
//                         $msg = true;
//                    }else{
//                        $msg = false;
//                    }
//                }
//                if ($msg == true) {
//                    $success_backup_all[] = ['date' => \Carbon\Carbon::parse($progress_count['created_at'])->format('M d, Y'),
//                        'time' => \Carbon\Carbon::parse($progress_count['created_at'])->format('g:i A')];
//                }
//            }
//        }
//
//
//        // success backup count
//        $backup_success_count = sizeof($success_backup_all);
//
//        //if last data is true then there is no more data
//        $last_data = false;
//
//        $total_pages = $backup_success_count / 6;
//
//        if ($total_pages == 0) {
//            $last_data = true;
//        } else {
//            if (strpos($total_pages, ".") === 1) {
//                $explode = explode('.', $total_pages);
//                if ($page == $explode[0]) {
//                    $last_data = true;
//                } else {
//                    $last_data = false;
//                }
//            } else {
//                if ($page + 1 == $total_pages) {
//                    $last_data = true;
//                }
//            }
//        }
//
//
//
//        return response()->json(['dates' => $success_backup, 'last_data' => $last_data]);
//    }

    /**
     * warning backups for dashboard page 
     * @param Request $request
     * @return type
     */
//    public function warning_backups(Request $request) {
//
//        $page = $request->page;
//
//        if ($page == 0) {
//            $skip = 0;
//        } else {
//            $skip = $page;
//        }
//
//        $backup_progress = BackupProgress::where('user_id', JWTAuth::user()->id)
//                ->orderByDesc('id')
//                ->skip($skip * 6)
//                ->take(6)
//                ->get();
//
//
//        $warning_backup = [];
//        if (!empty($backup_progress)) {
//            foreach ($backup_progress as $progress) {
//
//
//                $backup_history = \App\BackupHistory::where('backup_progress_id', $progress->id)
//                        ->get();
//
//                $warning = false;
//                foreach ($backup_history as $history) {
//
//                    if ($history->status == 'warning') {
//                        $warning = true;
//                    } else {
//                        $warning = false;
//                    }
//                }
//
//                if ($warning == true) {
//                    $warning_backup[] = [
//                        'id' => $progress['id'],
//                        'date' => \Carbon\Carbon::parse($progress['created_at'])->format('M d, Y'),
//                        'time' => \Carbon\Carbon::parse($progress['created_at'])->format('g:i A')];
//                }
//            }
//        }
//
//        //for view more data
//        $warning_backup_all = [];
//        $backup_progress_count = BackupProgress::where('user_id', JWTAuth::user()->id)->get();
//
//        if (!empty($backup_progress_count)) {
//            foreach ($backup_progress_count as $progress_count) {
//
//                $backup_history_warning = \App\BackupHistory::where('backup_progress_id', $progress_count->id)
//                        ->get();
//                $msg = false;
//                foreach ($backup_history_warning as $history_warning) {
//
//                    if ($history_warning->status == 'warning') {
//                        $msg = true;
//                    }else{
//                        $msg = false;
//                    }
//                }
//                if ($msg == true) {
//                    $warning_backup_all[] = ['date' => \Carbon\Carbon::parse($progress_count['created_at'])->format('M d, Y'),
//                        'time' => \Carbon\Carbon::parse($progress_count['created_at'])->format('g:i A')];
//                }
//            }
//        }
//
//
//        // success backup count
//        $backup_warning_count = sizeof($warning_backup_all);
//
//        //if last data is true then there is no more data
//        $last_data = false;
//
//        $total_pages = $backup_warning_count / 6;
//
//        if ($total_pages == 0) {
//            $last_data = true;
//        } else {
//            if (strpos($total_pages, ".") === 1) {
//                $explode = explode('.', $total_pages);
//                if ($page == $explode[0]) {
//                    $last_data = true;
//                } else {
//                    $last_data = false;
//                }
//            } else {
//                if ($page + 1 == $total_pages) {
//                    $last_data = true;
//                }
//            }
//        }
//        return response()->json(['dates' => $warning_backup, 'last_data' => $last_data]);
//    }
//
//    /**
//     * Failed backups for dashboard page 
//     * @param Request $request
//     * @return type
//     */
//    public function error_backups(Request $request) {
//
//        $page = $request->page;
//
//        if ($page == 0) {
//            $skip = 0;
//        } else {
//            $skip = $page;
//        }
//
//
//        $backup_progress = BackupProgress::where('user_id', JWTAuth::user()->id)
//                ->orderByDesc('id')
//                ->skip($skip * 6)
//                ->take(6)
//                ->get();
//
//
//        $error_backup = [];
//
//        if (!empty($backup_progress)) {
//            foreach ($backup_progress as $progress) {
//
//                $backup_history = \App\BackupHistory::where('backup_progress_id', $progress->id)
//                        ->get();
//
//                $error = false;
//                foreach ($backup_history as $history) {
//
//                    if ($history->status == 'error') {
//                        $error = true;
//                    } else {
//                        $error = false;
//                    }
//                }
//                if ($error == true) {
//                    $error_backup[] = [
//                        'id' => $progress['id'],
//                        'date' => \Carbon\Carbon::parse($progress['created_at'])->format('M d, Y'),
//                        'time' => \Carbon\Carbon::parse($progress['created_at'])->format('g:i A')];
//                }
//            }
//        }
//
//
//        //for view more data
//        $error_backup_all = [];
//        $backup_progress_count = BackupProgress::where('user_id', JWTAuth::user()->id)->get();
//
//        if (!empty($backup_progress_count)) {
//            foreach ($backup_progress_count as $progress_count) {
//
//                $backup_history_error = \App\BackupHistory::where('backup_progress_id', $progress_count->id)
//                        ->get();
//                $msg = false;
//                foreach ($backup_history_error as $history_error) {
//
//                    if ($history_error->status == 'error') {
//                        $msg = true;
//                    }else{
//                        $msg = false;
//                    }
//                }
//                if ($msg == true) {
//                    $error_backup_all[] = ['date' => \Carbon\Carbon::parse($progress_count['created_at'])->format('M d, Y'),
//                        'time' => \Carbon\Carbon::parse($progress_count['created_at'])->format('g:i A')];
//                }
//            }
//        }
//
//
//        // success backup count
//        $backup_error_count = sizeof($error_backup_all);
//
//        //if last data is true then there is no more data
//        $last_data = false;
//
//        $total_pages = $backup_error_count / 6;
//        
//        if ($total_pages == 0) {
//            $last_data = true;
//        } else {
//            if (strpos($total_pages, ".") === 1) {
//                $explode = explode('.', $total_pages);
//                if ($page == $explode[0]) {
//                    $last_data = true;
//                } else {
//                    $last_data = false;
//                }
//            } else {
//                if ($page + 1 == $total_pages) {
//                    $last_data = true;
//                }
//            }
//        }
//
//        return response()->json(['dates' => $error_backup, 'last_data' => $last_data]);
//    }

    /**
     * Running backups for dashboard page 
     * @param Request $request
     * @return type
     */
    public function running_backups(Request $request) {

        //running backups
        $running_backups = BackupProgress::select('id')
                        ->selectRaw('DATE_FORMAT(created_at,"%M %d, %Y") as date')
                        ->selectRaw('DATE_FORMAT(created_at,"%l:%i %p") as time')
                        ->where('user_id', JWTAuth::user()->id)
                        ->where('status', 'Inprogress')
                        ->orderByDesc('id')
                        ->get()->toArray();

        return response()->json(['dates' => $running_backups]);
    }

    /**
     * Get specific Backup 
     * @param Request $request
     * @return type
     */
    public function get_backup($id) {
        

        $progress = BackupProgress::select('id')
                            ->selectRaw('DATE_FORMAT(start_date,"%M %d, %Y") as date')
                            ->selectRaw('DATE_FORMAT(start_date,"%l:%i %p") as time')
                            ->where('id', $id)->first();

        if (!empty($progress)) {
            
            $last_backup_history = BackupHistory::where('backup_progress_id', $progress->id)->get();
            $msg = '';
            $last_status = '';
            foreach ($last_backup_history as $history) {

                if ($history->status == 'successed') {
                    $msg = 'successed';
                } elseif ($history->status == 'error') {
                    $msg = false;
                } elseif ($history->status == 'warning') {
                    $msg = false;
                }
            }
            if ($progress->status == 'Inprogress') {
                $last_status = 'Inprogress';
            } elseif ($msg == 'successed') {
                $last_status = 'Successed';
            } elseif ($msg == 'error') {
                $last_status = 'Failed';
            } elseif ($msg == 'warning') {
                $last_status = 'Warning';
            }

            return response()->json([
                        'status' => $last_status,
                        'backup_date' => $progress->date,
                        'backup_time' => $progress->time
            ]);
        } else {
            return response()->json([
                        'status' => 'no data'
            ]);
        }
    }

}
