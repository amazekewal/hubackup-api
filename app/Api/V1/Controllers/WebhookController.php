<?php

namespace App\Api\V1\Controllers;

use Config;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use JWTAuth;
use DB;
use App\User;
use App\SmartAlert;
use App\Webhook;
use App\Notification;
use App\SmartAlertHistory;
class WebhookController extends Controller {

    /**
     * get webhooks from hubspot.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {
        $data = $request->json()->all();

        $user_id = '';
        $contact_deletion = 0;
        $company_deletion = 0;
        $deal_deletion = 0;

        foreach ($data as $webhook) {
            $users = User::where('app_id',$webhook['appId'])->first();
            $user_id = $users['id'];
            Webhook::create(['user_id' => $user_id,
                'object_id' => isset($webhook['objectId']) ? $webhook['objectId'] : null,
                'property_name' => isset($webhook['propertyName']) ? $webhook['propertyName'] : null,
                'property_value' => isset($webhook['propertyValue']) ? $webhook['propertyValue'] : null,
                'change_source' => isset($webhook['changeSource']) ? $webhook['changeSource'] : null,
                'event_id' => isset($webhook['eventId']) ? $webhook['eventId'] : null,
                'subscription_id' => isset($webhook['subscriptionId']) ? $webhook['subscriptionId'] : null,
                'portal_id' => isset($webhook['portalId']) ? $webhook['portalId'] : null,
                'app_id' => isset($webhook['appId']) ? $webhook['appId'] : null,
                'occurred_at' => isset($webhook['occurredAt']) ? \Carbon\Carbon::createFromTimestampMs($webhook['occurredAt'], 'Asia/Kolkata')->format('Y-m-d H:i:s') : null,
                'subscription_type' => isset($webhook['subscriptionType']) ? $webhook['subscriptionType'] : null,
                'attempt_number' => isset($webhook['attemptNumber']) ? $webhook['attemptNumber'] : null,
                'change_flag' => isset($webhook['changeFlag']) ? $webhook['changeFlag'] : null]);

            if ($webhook['subscriptionType'] == 'contact.deletion' && $webhook['changeFlag'] == 'DELETED') {
                $contact_deletion += 1;
            }
            if ($webhook['subscriptionType'] == 'company.deletion' && $webhook['changeFlag'] == 'DELETED') {
                $company_deletion += 1;
            }
            if ($webhook['subscriptionType'] == 'deal.deletion' && $webhook['changeFlag'] == 'DELETED') {
                $deal_deletion += 1;
            }
        }



        //check smart alert for  contact removed is added or not
        $contact_removed_alert = SmartAlert::where('user_id', $user_id)
                        ->where('s_object', 'Contacts')
                        ->where('action', 'Removed')->first();

        if (!empty($contact_removed_alert)) {
            if ($contact_deletion > $contact_removed_alert->amount) {
                if ($contact_deletion == 1) {
                    $contact_deletion_data = 'Contact';
                } else {
                    $contact_deletion_data = 'Contacts';
                }

                //notification when contact removed
                $add_notificatoion = Notification::create(['user_id' => $user_id,
                            'status' => $contact_deletion_data . ' Removed', 'type' => 'email', 'num_of_records' => $contact_deletion]);

                //create history
                SmartAlertHistory::create(['user_id' => $user_id, 'smart_alert_id' => $contact_removed_alert->id]);

                //notify user
                //$user->notify(new Backup($notificatoion));
            }
        }
        
        //check smart alert for  company removed is added or not
        $company_removed_alert = SmartAlert::where('user_id', $user_id)
                        ->where('s_object', 'Companies')
                        ->where('action', 'Removed')->first();

        if (!empty($company_removed_alert)) {
            if ($company_deletion > $company_removed_alert->amount) {
                if ($company_deletion == 1) {
                    $company_deletion_data = 'Company';
                } else {
                    $company_deletion_data = 'Companies';
                }

                //notification when contact removed
                $add_notificatoion = Notification::create(['user_id' => $user_id,
                            'status' => $company_deletion_data . ' Removed', 'type' => 'email', 'num_of_records' => $company_deletion]);

                //create history
                SmartAlertHistory::create(['user_id' => $user_id, 'smart_alert_id' => $company_removed_alert->id]);

                //notify user
                //$user->notify(new Backup($notificatoion));
            }
        }
        
        //check smart alert for  contact removed is added or not
        $deal_removed_alert = SmartAlert::where('user_id', $user_id)
                        ->where('s_object', 'Deals')
                        ->where('action', 'Removed')->first();

        if (!empty($deal_removed_alert)) {
            if ($deal_deletion > $deal_removed_alert->amount) {
                if ($deal_deletion == 1) {
                    $deal_deletion_data = 'Deal';
                } else {
                    $deal_deletion_data = 'Deals';
                }

                //notification when deal removed
                $add_notificatoion = Notification::create(['user_id' => $user_id,
                            'status' => $deal_deletion_data . ' Removed', 'type' => 'email', 'num_of_records' => $deal_deletion]);

                //create history
                SmartAlertHistory::create(['user_id' => $user_id, 'smart_alert_id' => $deal_removed_alert->id]);

                //notify user
                //$user->notify(new Backup($notificatoion));
            }
        }
    }

}
