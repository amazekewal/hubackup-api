<?php

namespace App\Api\V1\Controllers;

use Config;
use DB;
use Hash;
use Carbon\Carbon;
use App\User;
use Tymon\JWTAuth\JWTAuth;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Password;
use App\Api\V1\Requests\ResetPasswordRequest;
use Symfony\Component\HttpKernel\Exception\HttpException;

class ResetPasswordController extends Controller {

    public function resetPassword(ResetPasswordRequest $request, JWTAuth $JWTAuth) {
        $password = $request->password;
        $tokenData = DB::table('password_resets')
                ->where('token', $request->token)
                ->where('created_at', '>', Carbon::now()->subHours(24))
                ->first();
        if (!$tokenData) {
            throw new HttpException(500, 'Password reset token is invalid or has expired.');
        }
        $user = User::where('email', $tokenData->email)->first();
        if (!$user) {
            throw new HttpException(500, 'Email Not found');
        }
        $this->reset($user, $password);
        // If the user shouldn't reuse the token later, delete the token 
        DB::table('password_resets')->where('email', $user->email)->delete();

        return response()->json([
                    'status' => 'ok',
                    'token' => $JWTAuth->fromUser($user),
        ]);
    }

    /**
     * Reset the given user's password.
     *
     * @param  \Illuminate\Contracts\Auth\CanResetPassword  $user
     * @param  string  $password
     * @return void
     */
    protected function reset($user, $password) {
        $user->password = $password;
        $user->save();
    }

}
