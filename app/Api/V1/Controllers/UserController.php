<?php

namespace App\Api\V1\Controllers;

use Symfony\Component\HttpKernel\Exception\HttpException;
use Tymon\JWTAuth\JWTAuth;
use App\Http\Controllers\Controller;
use App\Api\V1\Requests\LoginRequest;
use Tymon\JWTAuth\Exceptions\JWTException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Auth;
use App\User;
use Illuminate\Http\Request;
use Dingo\Api\Routing\Helpers;

class UserController extends Controller {

    use Helpers;

    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('jwt.auth', []);
    }

    /**
     * Get the authenticated User
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function me() {
        return response()->json(Auth::guard()->user());
    }

    /**
     * Update user
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request) {
        // current user
        $currentUser = Auth::user();
        // find user
        $user = User::find($currentUser->id);
        if (!$user) {
            throw new NotFoundHttpException;
        }
        $user->fill($request->all());
        // save
        if ($user->save()) {
          return response()->json($user);
        }
        return $this->response->error('could_not_update_user', 500);
    }

}
