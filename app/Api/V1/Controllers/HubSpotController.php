<?php

namespace App\Api\V1\Controllers;

use App\BackupHistory;
use App\BackupProgress;
use App\Connection;
use App\Http\Controllers\Controller;
use App\Notification;
use App\User;
use App\Analytic;
use App\BroadcastMessage;
use App\CalendarEvent;
use App\Contact;
use App\Company;
use App\Deal;
use App\CmsFile;
use App\CmsPipeline;
use App\Form;
use App\Engagement;
use App\CmsBlog;
use App\CmsBlogAuthor;
use App\CmsBlogComment;
use App\CmsBlogPost;
use App\CmsBlogTopic;
use App\CmsDomain;
use App\CmsHubdbTable;
use App\CmsLayout;
use App\CmsPagePublshing;
use App\CmsSiteMap;
use App\CmsTemplate;
use App\CmsUrlMapping;
use App\EmailCampaign;
use App\EmailEvent;
use App\EmailSubscription;
use App\Event;
use App\MarketingEmail;
use App\Owner;
use App\Product;
use App\PublishingChannel;
use App\Ticket;
use App\Workflow;
use App\WorkflowMeta;
use Artisan;
use Auth;
use DB;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use JWTAuth;
use DataTables;

class HubSpotController extends Controller {

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('jwt.auth');
    }

    /**
     *
     */
    public function getConnection(Request $request) {
        $connection = User::with('connection')->find(JWTAuth::user()->id);
        if (!empty($connection)) {
            return response()->json([
                        'success' => true,
                        'data' => $connection->connection,
            ]);
        }
        return response()->json([
                    'success' => false,
                    'data' => null,
        ]);
    }

    /**
     * Hubspot acees token get
     * @param Request $request
     * @return type
     */
    public function authentication(Request $request) {

        $token = $request->token;
        $refresh = $request->refresh;
        $api_url = env('HUBSPOT_URL');
        $hapikey = '43324f29-f717-4934-afd2-2561ce5ab4c6';
        $client_secret = '8ce8de3e-f210-45d5-83c0-94555b0b9f30';
        $client = new \GuzzleHttp\Client();
        if ($token) {
            $user = JWTAuth::user();
            if (!empty($user)) {

                $user->accesstoken = $token;
                $user->refreshtoken = $refresh;

                if ($user->app_id == null) {
                    //using GuzzleHttp client to get request

                    $headers = [
                        'Authorization' => 'Bearer ' . $token,
                        'Accept' => 'application/json',
                    ];

                    $url = $api_url . '/oauth/v1/access-tokens/' . $token;

                    $response = $client->request('GET', $url, ['headers' => $headers]);

                    $body = $response->getBody();
                    $data = json_decode($body, true);

                    if (!empty($data['app_id'])) {
                        $user->app_id = $data['app_id'];
                        $user->hub_id = $data['hub_id'];
                    }
                }
                $user->save();
                $connections = Connection::where('user_id', $user->id)->first();

                if (empty($connections)) {
                    $connection = Connection::create([
                                'user_id' => $user->id,
                                'connectionName' => $request->connectionName,
                                'accesstoken' => $request->token,
                                'refreshtoken' => $request->refresh,
                    ]);
                } else {
                    $connection = Connection::find($connections->id);
                    $connection->connectionName = $request->connectionName;
                    $connection->accesstoken = $request->token;
                    $connection->refreshtoken = $request->refresh;
                    $connection->save();
                    return response()->json([
                                'status' => true,
                                'message' => "Connection add successfully!",
                                'data' => $connection,
                    ]);
                }

       
            } else {
                return response()->json([
                            'status' => 'error',
                            'message' => "User not found",
                ]);
            }
        } else {
            return response()->json([
                        'status' => 'error',
                        'message' => "Token not found",
            ]);
        }
    }

    /**
     * Dashboard
     * @param Request $request
     * @return type
     */
    public function dashboard(Request $request) {

        $page = $request->page;

        if ($page == 0) {
            $skip = 0;
        } else {
            $skip = $page;
        }

        $backup_progress = BackupProgress::where('user_id', JWTAuth::user()->id)
                ->orderByDesc('id')
                ->skip($skip * 6)
                ->take(6)
                ->get();

        $backup_all = [];
        $error_backup = [];
        $success_backup = [];
        $warning_backup = [];
        if (!empty($backup_progress)) {
            foreach ($backup_progress as $progress) {

                if ($progress !== reset($backup_progress)) {
                    $backup_all[] = ['date' => \Carbon\Carbon::parse($progress['created_at'])->format('M d, Y'),
                        'time' => \Carbon\Carbon::parse($progress['created_at'])->format('g:i A')];
                }
                $backup_history = \App\BackupHistory::where('backup_progress_id', $progress->id)
                        ->get();
                $msg = '';
                foreach ($backup_history as $history) {

                    if ($history->status == 'successed') {
                        $msg = 'successed';
                    } elseif ($history->status == 'error') {
                        $msg = 'error';
                    } elseif ($history->status == 'warning') {
                        $msg = 'warning';
                    }
                }
                if ($msg == 'successed') {
                    $success_backup[] = ['date' => \Carbon\Carbon::parse($progress['created_at'])->format('M d, Y'),
                        'time' => \Carbon\Carbon::parse($progress['created_at'])->format('g:i A')];
                } elseif ($msg == 'error') {
                    $error_backup[] = ['date' => \Carbon\Carbon::parse($progress['created_at'])->format('M d, Y'),
                        'time' => \Carbon\Carbon::parse($progress['created_at'])->format('g:i A')];
                } elseif ($msg == 'warning') {
                    $warning_backup[] = ['date' => \Carbon\Carbon::parse($progress['created_at'])->format('M d, Y'),
                        'time' => \Carbon\Carbon::parse($progress['created_at'])->format('g:i A')];
                }
            }
        }

        //running backups
        $running_backups = \App\BackupProgress::selectRaw('DATE_FORMAT(created_at,"%M %d, %Y") as date')
                        ->selectRaw('DATE_FORMAT(created_at,"%l:%i %p") as time')
                        ->where('user_id', JWTAuth::user()->id)
                        ->where('status', 'Inprogress')
                        ->orderByDesc('id')
                        ->get()->toArray();

        return response()->json(['all' => ['dates' => $backup_all],
                    'success' => ['dates' => $success_backup],
                    'error' => ['dates' => $error_backup],
                    'warning' => ['dates' => $warning_backup],
                    'running' => ['dates' => $running_backups]]);
    }

    /**
     * Backup Progress
     * @param Request $request
     * @return type
     */
    public function backup_progress() {

        $progress = BackupProgress::where('user_id', JWTAuth::user()->id)->orderByDesc('id')->first();

        if (!empty($progress)) {
            $start = new \Carbon\Carbon($progress->start_time);
            $end = new \Carbon\Carbon($progress->end_time);

            $last_backup_date = \Carbon\Carbon::parse($start)->format('M d, Y');
            $last_backup_time = \Carbon\Carbon::parse($start)->format('g:i A');

            $time = $start->diff($end)->format('%H:%I:%S') . 'hrs';
            return response()->json([
                        'status' => $progress->status,
                        'execution_time' => $time,
                        'last_backup_date' => $last_backup_date,
                        'last_backup_time' => $last_backup_time,
            ]);
        } else {
            return response()->json([
                        'status' => 'no data',
            ]);
        }
    }

    /**
     * Get auth user backup data
     * @param Request $request
     * @return type
     */
    public function backup(Request $request) {

        if (JWTAuth::user()->payment == null) {
            $user_id = 0;
        } else {
            $user_id = JWTAuth::user()->id;
        }

        $backup_dates = BackupHistory::selectRaw('DATE(backup_history.created_at) as  day')
                        ->where('user_id', $user_id)
                        ->groupBy('day')
                        ->orderByDesc('backup_history.created_at')
                        ->get()->toArray();

        if (count($backup_dates) > 0) {
            $added_records = [];
            $changed_records = [];
            $b_dates = [];
            foreach ($backup_dates as $date) {

                $backup = BackupHistory::selectRaw('SUM(backup_history.num_of_records) as num_of_records')
                        ->selectRaw('SUM(backup_history.added_records) as added_records')
                        ->selectRaw('SUM(backup_history.changed_records) as changed_records')
                        ->where('user_id', $user_id)
                        ->whereRaw('DATE(backup_history.created_at)= ?', $date)
                        ->first();
                $b_dates[] = $date['day'];
                $added_records[] = $backup['added_records'];
                $changed_records[] = $backup['changed_records'];
            }

            $last_date = BackupHistory::select('backup_history.created_at as last_created_date')
                            ->where('user_id', $user_id)
                            ->orderByDesc('backup_history.created_at')
                            ->pluck('last_created_date')->first();
            //last progress
            $last_progress = BackupProgress::where('user_id', $user_id)->orderByDesc('id')->first();
            $last_backup_history = BackupHistory::where('backup_progress_id', $last_progress->id)->get();
            $last_no_of_records = BackupHistory::selectRaw('SUM(backup_history.num_of_records) as num_of_records')
                            ->where('user_id', $user_id)
                            ->where('backup_progress_id', $last_progress->id)
                            ->pluck('num_of_records')->first();
            $msg = '';

            foreach ($last_backup_history as $history) {

                if ($history->status == 'successed') {
                    $msg = 'successed';
                } elseif ($history->status == 'error') {
                    $msg = 'error';
                } elseif ($history->status == 'warning') {
                    $msg = 'warning';
                }
            }
            if ($last_progress->status == 'Inprogress') {
                $last_status = 'Inprogress';
            } elseif ($msg == 'successed') {
                $last_status = 'Successed';
            } elseif ($msg == 'error') {
                $last_status = 'Failed';
            } elseif ($msg == 'warning') {
                $last_status = 'Warning';
            }
            $last_backup_date = \Carbon\Carbon::parse($last_progress->start_date)->format('M d, Y');
            $last_backup_time = \Carbon\Carbon::parse($last_progress->start_date)->format('g:i A');

            $max_num_added_records = max($added_records);
            $max_num_changed_records = max($changed_records);
            if ($max_num_added_records > $max_num_changed_records) {
                $max_num = $max_num_added_records;
            } else {
                $max_num = $max_num_changed_records;
            }
            return response()->json([
                        'status' => 'ok',
                        'backup_chunk_dates' => isset($backup_dates) ? array_chunk($backup_dates, 2) : null,
                        'backup_dates' => isset($b_dates) ? $b_dates : null,
//                       'added_records' => isset($added_records) ? $added_records : null,
                        //                        'changed_records' => isset($changed_records) ? $changed_records : null,
                        'added_records' => [20, 30, 40, 50, 60],
                        'changed_records' => [10, 40, 60, 20, 30],
                        'last_status' => $last_status,
                        'max_num' => $max_num,
                        'last_backup_date' => isset($last_backup_date) ? $last_backup_date : null,
                        'last_backup_time' => isset($last_backup_time) ? $last_backup_time : null,
                        'last_no_of_records' => isset($last_no_of_records) ? $last_no_of_records : null,
            ]);
        } else {
            return response()->json([
                        'status' => 'no data']);
        }
    }

    /**
     * Get auth user backup  history data
     * @param Request $request
     * @return type
     */
    public function backup_history($id) {

        $progress = BackupProgress::select('created_at')
                        ->selectRaw('DATE_FORMAT(start_date,"%M %d, %Y") as date')
                        ->selectRaw('DATE_FORMAT(start_date,"%l:%i %p") as time')
                        ->where('id', $id)->first();

        if (!empty($progress)) {

            $backup_history = BackupHistory::where('backup_progress_id', $id)->get();

            $last_record = BackupHistory::selectRaw('SUM(backup_history.num_of_records) as num_of_records')
                    ->selectRaw('SUM(backup_history.num_of_API_calls) as num_of_API_calls')
                    ->where('backup_progress_id', $id)
                    ->first();

            return response()->json([
                        'status' => 'ok',
                        'backup_history' => isset($backup_history) ? $backup_history : null,
                        'backup_date' => isset($progress->date) ? $progress->date : null,
                        'backup_time' => isset($progress->time) ? $progress->time : null,
                        'no_of_records' => isset($last_record['num_of_records']) ? $last_record['num_of_records'] : 0,
                        'no_of_api' => isset($last_record['num_of_API_calls']) ? $last_record['num_of_API_calls'] : 0,
            ]);
        } else {
            return response()->json([
                        'status' => 'no data']);
        }
    }

    /**
     * Get auth user backup  history data
     * @param Request $request
     * @return type
     */
    public function backup_now(Request $request) {

        $user = JWTAuth::user();
        $api_url = env('HUBSPOT_URL');
        $access_token = $user->accesstoken;

        if ($access_token == null) {
            return response()->json([
                        'status' => 'Token is null',
            ]);
        } else {
            //using GuzzleHttp client to get request
            $client = new \GuzzleHttp\Client(['http_errors' => false]);
            $headers = [
                'Authorization' => 'Bearer ' . $access_token,
                'Accept' => 'application/json',
            ];

            $url = $api_url . '/oauth/v1/access-tokens/' . $access_token;

            $response = $client->request('GET', $url, ['headers' => $headers]);

            $body = $response->getBody();
            $data = json_decode($body, true);

            if (!empty($data['status']) == 'error') {
                return response()->json([
                            'status' => 'Authentication error. Please Re-authenticate with Hubspot',
                ]);
            } else {

                //run Analytics command
                Artisan::call("backup:started", ["userId" => $user->id]);
                try {
                    Artisan::call("analytics:data", ["userId" => $user->id]);
                } catch (\Exception $ex) {
                    Log::error("Analytics data Error: " . $ex->getMessage());
                }

                //run broadcast messages command
                try {
                    Artisan::call("broadcast-messages:data", ["userId" => $user->id]);
                } catch (\Exception $ex) {
                    Log::error("broadcast messages data Error: " . $ex->getMessage());
                }

                //run calendar events command
                try {
                    Artisan::call("calendar-events:data", ["userId" => $user->id]);
                } catch (\Exception $ex) {
                    Log::error("calendar events data Error: " . $ex->getMessage());
                }
                
                //run email campaigns command
                try {
                    Artisan::call("email-campaigns:data", ["userId" => $user->id]);
                } catch (\Exception $ex) {
                    Log::error("calendar events data Error: " . $ex->getMessage());
                }
                

                //run cms blog authors command
                try {
                    Artisan::call("cms-blog-authors:data", ["userId" => $user->id]);
                } catch (\Exception $ex) {
                    Log::error("cms blog authors data Error: " . $ex->getMessage());
                }

                //run blog comments command
                try {
                    Artisan::call("cms-blog-comments:data", ["userId" => $user->id]);
                } catch (\Exception $ex) {
                    Log::error("cms blog comments data Error: " . $ex->getMessage());
                }

                //run blog posts command
                try {
                    Artisan::call("cms-blog-posts:data", ["userId" => $user->id]);
                } catch (\Exception $ex) {
                    Log::error("cms blog posts data Error: " . $ex->getMessage());
                }

                //run cms blog topics command
                try {
                    Artisan::call("cms-blog-topics:data", ["userId" => $user->id]);
                } catch (\Exception $ex) {
                    Log::error("cms blog topics data Error: " . $ex->getMessage());
                }

                //run cms blogs command
                try {
                    Artisan::call("cms-blogs:data", ["userId" => $user->id]);
                } catch (\Exception $ex) {
                    Log::error("cms blogs data Error: " . $ex->getMessage());
                }

                //run cms domains command
                try {
                    Artisan::call("cms domains:data", ["userId" => $user->id]);
                } catch (\Exception $ex) {
                    Log::error("cms domains:data Error: " . $ex->getMessage());
                }

                //run cms files command
                try {
                    Artisan::call("cms-files:data", ["userId" => $user->id]);
                } catch (\Exception $ex) {
                    Log::error("cms files data Error: " . $ex->getMessage());
                }

                //run cms hubdb command
                try {
                    Artisan::call("cms-hubdb:data", ["userId" => $user->id]);
                } catch (\Exception $ex) {
                    Log::error("cms hubdb data Error: " . $ex->getMessage());
                }

                //run cms layouts command
                try {
                    Artisan::call("cms-layouts:data", ["userId" => $user->id]);
                } catch (\Exception $ex) {
                    Log::error("cms layouts:data Error: " . $ex->getMessage());
                }

                //run  cms page publishing command
                try {
                    Artisan::call("cms-page-publishing:data", ["userId" => $user->id]);
                } catch (\Exception $ex) {
                    Log::error("Cms Page publishing data Error: " . $ex->getMessage());
                }

                //run cms pipelines command
                try {
                    Artisan::call("cms-pipelines:data", ["userId" => $user->id]);
                } catch (\Exception $ex) {
                    Log::error("cms pipelines:data Error: " . $ex->getMessage());
                }

                //run cms site maps command
                try {
                    Artisan::call("cms-site-maps:data", ["userId" => $user->id]);
                } catch (\Exception $ex) {
                    Log::error("cms site maps data Error: " . $ex->getMessage());
                }

                //run cms templates command
                try {
                    Artisan::call("cms-templates:data", ["userId" => $user->id]);
                } catch (\Exception $ex) {
                    Log::error("cms templates data Error: " . $ex->getMessage());
                }

                //run cms url mappings command
                try {
                    Artisan::call("cms-url-mappings:data", ["userId" => $user->id]);
                } catch (\Exception $ex) {
                    Log::error("cms url mappings data Error: " . $ex->getMessage());
                }

                //run companies command
                try {
                    Artisan::call("companies:data", ["userId" => $user->id]);
                } catch (\Exception $ex) {
                    Log::error("companies data Error: " . $ex->getMessage());
                }

                //run contacts command
                try {
                    Artisan::call("contacts:data", ["userId" => $user->id]);
                } catch (\Exception $ex) {
                    Log::error("contacts data Error: " . $ex->getMessage());
                }

                //run deals command
                try {
                    Artisan::call("deals:data", ["userId" => $user->id]);
                } catch (\Exception $ex) {
                    Log::error("deals data Error: " . $ex->getMessage());
                }

                //run email events command
                try {
                    Artisan::call("email-events:data", ["userId" => $user->id]);
                } catch (\Exception $ex) {
                    Log::error("email events data Error: " . $ex->getMessage());
                }

                //run email subscriptions command
                try {
                    Artisan::call("email-subscriptions:data", ["userId" => $user->id]);
                } catch (\Exception $ex) {
                    Log::error("email subscriptions data Error: " . $ex->getMessage());
                }
                
                //run engagements command
                try {
                    Artisan::call("engagements:data", ["userId" => $user->id]);
                } catch (\Exception $ex) {
                    Log::error("Engagements data Error: " . $ex->getMessage());
                }

                //run events command
                try {
                    Artisan::call("events:data", ["userId" => $user->id]);
                } catch (\Exception $ex) {
                    Log::error("events data Error: " . $ex->getMessage());
                }

                //run forms command
                try {
                    Artisan::call("forms:data", ["userId" => $user->id]);
                } catch (\Exception $ex) {
                    Log::error("forms data Error: " . $ex->getMessage());
                }

                //run marketing emails command
                try {
                    Artisan::call("marketing-emails:data", ["userId" => $user->id]);
                } catch (\Exception $ex) {
                    Log::error("marketing emails data Error: " . $ex->getMessage());
                }

                //run owners command
                try {
                    Artisan::call("owners:data", ["userId" => $user->id]);
                } catch (\Exception $ex) {
                    Log::error("owners data Error: " . $ex->getMessage());
                }

                //run products command
                try {
                    Artisan::call("products:data", ["userId" => $user->id]);
                } catch (\Exception $ex) {
                    Log::error("products data Error: " . $ex->getMessage());
                }

                //run publishing channels command
                try {
                    Artisan::call("publishing-channels:data", ["userId" => $user->id]);
                } catch (\Exception $ex) {
                    Log::error("publishing channels data Error: " . $ex->getMessage());
                }

                //run tickets command
                try {
                    Artisan::call("tickets:data", ["userId" => $user->id]);
                } catch (\Exception $ex) {
                    Log::error("tickets data Error: " . $ex->getMessage());
                }

                //run workflows command
                try {
                    Artisan::call("workflows:data", ["userId" => $user->id]);
                } catch (\Exception $ex) {
                    Log::error("workflows data Error: " . $ex->getMessage());
                }
                Artisan::call("backup:ended", ["userId" => $user->id]);

                return response()->json([
                            'status' => 'Backup completed',
                ]);
            }
        }
    }

    /**
     * Get auth user notifications
     * @param Request $request
     * @return type
     */
    public function notifications() {

        if (JWTAuth::user()->payment == null) {
            $user_id = 0;
        } else {
            $user_id = JWTAuth::user()->id;
        }

        $notifications = Notification::where('user_id', $user_id)->orderByDesc('id')->get();

        return response()->json([
                    'notifications' => $notifications]);
    }

    /**
     * Destroy notifications
     * @param Request $request
     * @return type
     */
    public function notifications_destroy(Request $request) {

        $checked = $request->input('checked');

        Notification::destroy($checked);

        if (is_array($checked)) {
            $message = 'Notifications';
        } else {
            $message = 'Notification';
        }
        return response()->json([
                    'message' => $message . ' deleted successfully']);
    }

    /**
     * show notification
     * @param Request $request
     * @return type
     */
    public function show_notification($id) {

        $notification = Notification::where('id', $id)->first();
        if ($notification->is_read == 0) {
            $notification->update(['is_read' => 1]);
        }
        return response()->json([
                    'notification' => $notification]);
    }

    /**
     * show unread notifications
     * @param Request $request
     * @return type
     */
    public function unread_notifications() {

        $notifications = Notification::where('user_id', JWTAuth::user()->id)->where('is_read', 0)->get();

        return response()->json([
                    'notifications' => $notifications,
                    'count' => count($notifications)]);
    }

    public function get_find_backup_dates() {

        $backup_dates = BackupProgress::select('id')
                        ->selectRaw('DATE_FORMAT(start_date,"%M %d, %Y") as date')
                        ->where('user_id', JWTAuth::user()->id)
                        ->where('status', 'done')
                        ->groupBy('date')
                        ->orderByDesc('created_at')
                        ->get()->toArray();
        return response()->json([
                    'backup_dates' => $backup_dates]);
    }
    
    /**
     * find functionality
     * @param Request $request
     * @return type
     */
    
    public function search(Request $request) {

        //table 
        $search_table = $request->search_table;

        //start date
        
        $start_date = \Carbon\Carbon::parse($request->date[0])->toDateString();

        //end date
        $end_date = \Carbon\Carbon::parse($request->date[1])->toDateString();

        //search term
        $search_term = $request->search_term;
        
        //last backup
        $backup_progress = BackupProgress::where('user_id', JWTAuth::user()->id)->orderByDesc('id')->first();
        
        //last backup start date
        $startdate = \Carbon\Carbon::parse($backup_progress['start_date'])->format('M d, Y g:i A');
        
        //last backup end date
        $enddate = \Carbon\Carbon::parse($backup_progress['end_date'])->format('M d, Y g:i A');
        
        if ($search_table == 'Analytics') { //search if table is Analytics
            
            //get Analytics columns
            $analytics = new Analytic();
            $columns = $analytics->getFillable();

            $key_not = '';

            $query = Analytic::query();

            //search term in all columns
            foreach ($columns as $column) {
                $query->orWhere($column, 'LIKE', '%' . $search_term . '%')
                      ->where('backup_progress_id',$backup_progress['id']);
            }
        }else if ($search_table == 'Broadcast Messages') { //search if table is Broadcast Messages
            
            //get Broadcast Messages columns
            $broadcast_message = new BroadcastMessage();
            $columns = $broadcast_message->getFillable();

            $key_not = '';

            $query = BroadcastMessage::query();

            //search term in all columns
            foreach ($columns as $column) {
                $query->orWhere($column, 'LIKE', '%' . $search_term . '%')
                      ->where('backup_progress_id',$backup_progress['id']);
            }
        }else if ($search_table == 'Campaigns') { //search if table is EmailCampaign
           
            //get EmailCampaign columns
            $campaigns = new EmailCampaign();
            $columns = $campaigns->getFillable();

            $key_not = '';

            $query = EmailCampaign::query();

            //search term in all columns
            foreach ($columns as $column) {
                $query->orWhere($column, 'LIKE', '%' . $search_term . '%')
                      ->where('backup_progress_id',$backup_progress['id']);
            }
        }else if ($search_table == 'Calendar Events') { //search if table is Calendar Events
           
            //get Calendar Events columns
            $calendar_event = new CalendarEvent();
            $columns = $calendar_event->getFillable();

            $key_not = '';

            $query = CalendarEvent::query();

            //search term in all columns
            foreach ($columns as $column) {
                $query->orWhere($column, 'LIKE', '%' . $search_term . '%')
                      ->where('backup_progress_id',$backup_progress['id']);
            }
        }else if ($search_table == 'CMS Blog Authors') { //search if table is CMS Blogs Authors

            //get blog  authors columns
            $blog_author = new CmsBlogAuthor();
            $columns = $blog_author->getFillable();

            $key_not = '';

            $query = CmsBlogAuthor::query();


            //search term in all columns
            foreach ($columns as $column) {
                $query->orWhere($column, 'LIKE', '%' . $search_term . '%')
                      ->where('backup_progress_id',$backup_progress['id']);
            }
        } else if ($search_table == 'CMS Blog Comments') { //search if table is CMS Blogs Comments
            
            //get blog comments columns
            $blog_comment = new CmsBlogComment();
            $columns = $blog_comment->getFillable();

            $key_not = '';

            $query = CmsBlogComment::query();


            //search term in all columns
            foreach ($columns as $column) {
                $query->orWhere($column, 'LIKE', '%' . $search_term . '%')
                      ->where('backup_progress_id',$backup_progress['id']);
            }
        } else if ($search_table == 'CMS Blog Posts') { //search if table is CMS Blogs Posts
            
            //get blog post columns
            $blog_post = new CmsBlogPost();
            $post_columns = $blog_post->getFillable();

            $key_not = '';
            
            $widgets = ['Widgets'];
            $columns = array_merge($post_columns, $widgets);

            $query = CmsBlogPost::with('post_widgets');


            //search term in all columns
            foreach ($post_columns as $column) {
                $query->orWhere($column, 'LIKE', '%' . $search_term . '%')
                      ->where('backup_progress_id',$backup_progress['id']);
            }
        } else if ($search_table == 'CMS Blog Topics') { //search if table is CMS Blog Topics
            
            //get blog  topic columns
            $blog_topic = new CmsBlogTopic();
            $columns = $blog_topic->getFillable();

            $key_not = '';

            $query = CmsBlogTopic::query();

            //search term in all columns
            foreach ($columns as $column) {
                $query->orWhere($column, 'LIKE', '%' . $search_term . '%')
                      ->where('backup_progress_id',$backup_progress['id']);
            }
        }else if ($search_table == 'CMS Blogs') { //search if table is CMS Blogs
            //get blog columns
            $blog = new CmsBlog();
            $columns = $blog->getFillable();

            $key_not = '';

            $query = CmsBlog::query();

            //search term in all columns
            foreach ($columns as $column) {
                $query->orWhere($column, 'LIKE', '%' . $search_term . '%')
                      ->where('backup_progress_id',$backup_progress['id']);
            }
        }else if ($search_table == 'CMS Domains') { //search if table is CMS Domains
           
            //get blog columns
            $domain = new CmsDomain();
            $columns = $domain->getFillable();

            $key_not = '';

            $query = CmsDomain::query();

            //search term in all columns
            foreach ($columns as $column) {
                $query->orWhere($column, 'LIKE', '%' . $search_term . '%')
                      ->where('backup_progress_id',$backup_progress['id']);
            }
        } else if ($search_table == 'CMS Files') { //search if table is CMS Files
            //get deal columns
            $file = new CmsFile();
            $columns = $file->getFillable();

            $key_not = '';

            $query = CmsFile::query();

            //search term in all columns
            foreach ($columns as $column) {
                $query->orWhere($column, 'LIKE', '%' . $search_term . '%')
                      ->where('backup_progress_id',$backup_progress['id']);
            }
        } else if ($search_table == 'CMS HubDB') { //search if table is CMS HubDB
            
            //get HubDB columns
            $hubdb = new CmsHubdbTable();
            $hubdb_columns = $hubdb->getFillable();

            $key_not = '';
            
            $feild_columns = ['Columns'];
            $columns = array_merge($hubdb_columns, $feild_columns);
            
            $query = CmsHubdbTable::with('table_columns');

            //search term in all columns
            foreach ($hubdb_columns as $column) {
                $query->orWhere($column, 'LIKE', '%' . $search_term . '%')
                      ->where('backup_progress_id',$backup_progress['id']);
            }
        } else if ($search_table == 'CMS Layouts') { //search if table is CMS Layouts
            
            //get CMS Layouts columns
            $layout = new CmsLayout();
            $layout_columns = $layout->getFillable();

            $key_not = '';
            
            $attachs = ['attached_js','attached_stylesheets','content_tags','rows'];
            $columns = array_merge($layout_columns, $attachs);
            
            $query = CmsLayout::with('layout_js')
                                    ->with('layout_stylesheet')
                                    ->with('layout_content_tags')
                                    ->with('layout_rows');

            //search term in all columns
            foreach ($layout_columns as $column) {
                $query->orWhere($column, 'LIKE', '%' . $search_term . '%')
                      ->where('backup_progress_id',$backup_progress['id']);
            }
        } else if ($search_table == 'CMS Page Publishing') { //search if table is CMS Page Publishing
            
            //get CMS Page Publishing columns
            $publishing = new CmsPagePublshing();
            $publishing_columns = $publishing->getFillable();

            $key_not = '';
            
            $widgets = ['Widgets'];
            $columns = array_merge($publishing_columns, $widgets);
            
            $query = CmsPagePublshing::with('widgets');

            //search term in all columns
            foreach ($publishing_columns as $column) {
                $query->orWhere($column, 'LIKE', '%' . $search_term . '%')
                      ->where('backup_progress_id',$backup_progress['id']);
            }
        }else if ($search_table == 'CMS Pipelines') { //search if table is CMS Pipelines
            //get pipeline columns
            $pipeline = new CmsPipeline();
            $pipeline_columns = $pipeline->getFillable();

            $stages = ['Stages'];
            $columns = array_merge($pipeline_columns, $stages);

            $key_not = '';

            $query = CmsPipeline::with('pipeline_stages');

            //search term in all columns
            foreach ($pipeline_columns as $column) {
                $query->orWhere($column, 'LIKE', '%' . $search_term . '%')
                      ->where('backup_progress_id',$backup_progress['id']);
            }
        } else if ($search_table == 'CMS Site Maps') { //search if table is CMS Site Maps
        
            //get Site Maps columns
            $site_maps = new CmsSiteMap();
            $columns = $site_maps->getFillable();

            $key_not = '';

            $query = CmsSiteMap::query();

            //search term in all columns
            foreach ($columns as $column) {
                $query->orWhere($column, 'LIKE', '%' . $search_term . '%')
                      ->where('backup_progress_id',$backup_progress['id']);
            }
        }else if ($search_table == 'CMS Templates') { //search if table is CMS Templates
            
            //get CMS Templates columns
            $template = new CmsTemplate();
            $template_columns = $template->getFillable();

            $key_not = '';
            
            $attachs = ['attached_js','attached_stylesheets','content_tags'];
            $columns = array_merge($template_columns, $attachs);
            
            $query = CmsTemplate::with('template_js')
                                    ->with('template_stylesheet')
                                    ->with('template_content_tags');

            //search term in all columns
            foreach ($template_columns as $column) {
                $query->orWhere($column, 'LIKE', '%' . $search_term . '%')
                      ->where('backup_progress_id',$backup_progress['id']);
            }
        }else if ($search_table == 'CMS Url Mappings') { //search if table is CMS Url Mappings
        
            //get Url Mappings columns
            $url_mapping = new CmsUrlMapping();
            $columns = $url_mapping->getFillable();

            $key_not = '';

            $query = CmsUrlMapping::query();

            //search term in all columns
            foreach ($columns as $column) {
                $query->orWhere($column, 'LIKE', '%' . $search_term . '%')
                      ->where('backup_progress_id',$backup_progress['id']);
            }
        }else if ($search_table == 'Email Events') { //search if table is Email Events
        
            //get Email Events columns
            $email_event = new EmailEvent();
            $columns = $email_event->getFillable();

            $key_not = '';

            $query = EmailEvent::query();

            //search term in all columns
            foreach ($columns as $column) {
                $query->orWhere($column, 'LIKE', '%' . $search_term . '%')
                      ->where('backup_progress_id',$backup_progress['id']);
            }
        }else if ($search_table == 'Email Subscriptions') { //search if table is Email Subscriptions
        
            //get Email Subscriptions columns
            $email_subscriptions = new EmailSubscription();
            $columns = $email_subscriptions->getFillable();

            $key_not = '';

            $query = EmailSubscription::query();

            //search term in all columns
            foreach ($columns as $column) {
                $query->orWhere($column, 'LIKE', '%' . $search_term . '%')
                      ->where('backup_progress_id',$backup_progress['id']);
            }
        }else if ($search_table == 'Events') { //search if table is Events
        
            //get Events columns
            $event = new Event();
            $columns = $event->getFillable();

            $key_not = '';

            $query = Event::query();

            //search term in all columns
            foreach ($columns as $column) {
                $query->orWhere($column, 'LIKE', '%' . $search_term . '%')
                      ->where('backup_progress_id',$backup_progress['id']);
            }
        } else if ($search_table == 'Forms') { //search if table is Forms
            //get form columns
            $form = new Form();
            $form_columns = $form->getFillable();

            $fields = ['Fields'];
            $columns = array_merge($form_columns, $fields);

            $key_not = '';

            $query = Form::with('form_fields');

            //search term in all columns
            foreach ($form_columns as $column) {
                $query->orWhere($column, 'LIKE', '%' . $search_term . '%')
                      ->where('backup_progress_id',$backup_progress['id']);
            }
        }else if ($search_table == 'Marketing Emails') { //search if table is Marketing Emails
            
            //get Marketing Emails columns
            $m_email = new MarketingEmail();
            $m_email_columns = $m_email->getFillable();

            $widgets = ['Widgets'];
            $columns = array_merge($m_email_columns, $widgets);

            $key_not = '';

            $query = MarketingEmail::with('widgets');

            //search term in all columns
            foreach ($m_email_columns as $column) {
                $query->orWhere($column, 'LIKE', '%' . $search_term . '%')
                      ->where('backup_progress_id',$backup_progress['id']);
            }
        }   else if ($search_table == 'Contacts') { //search if table is Contacts
            //get contact columns
            $contact = new Contact();
            $columns = $contact->getFillable();
            $key_not = '';

            $query = Contact::query();

            //search term in all columns
            foreach ($columns as $column) {
                $query->orWhere($column, 'LIKE', '%' . $search_term . '%')
                      ->where('backup_progress_id',$backup_progress['id']);
            }
           
        } else if ($search_table == 'Companies') { //search if table is Companies
            //get Companies columns
            $company = new Company();
            $columns = $company->getFillable();

            $query = Company::query();
                        
            $key_not = '';
            //search term in all columns
            foreach ($columns as $column) {
                $query->orWhere($column, 'LIKE', '%' . $search_term . '%')
                       ->where('backup_progress_id',$backup_progress['id']);
                
            }
            
        } else if ($search_table == 'Deals') { //search if table is Deals
            //get deal columns
            $deal = new Deal();
            $deal_columns = $deal->getFillable();
            
            $associated = ['associated_companies','associated_contacts','associated_tickets']; 
            
            $columns = array_merge($deal_columns,$associated);

            //key not equal to deal db id
            $key_not = 'deal_db_id';

            $query = Deal::with('deal_associated_companies')
                           ->with('deal_associated_contacts')
                           ->with('deal_associated_tickets');

            //search term in all columns
            foreach ($deal_columns as $deal_column) {
                $query->orWhere('deals.' . $deal_column, 'LIKE', '%' . $search_term . '%')
                       ->where('backup_progress_id',$backup_progress['id']);
            }
        }else if ($search_table == 'Owners') { //search if table is Owners
           
            //get Owners columns
            $owner = new Owner();
            $columns = $owner->getFillable();

           
            $key_not = '';

            $query = Owner::query();

            //search term in all columns
            foreach ($columns as $column) {
                $query->orWhere($column, 'LIKE', '%' . $search_term . '%')
                      ->where('backup_progress_id',$backup_progress['id']);
            }
        }else if ($search_table == 'Products') { //search if table is Products
           
            //get Products columns
            $product = new Product();
            $product_columns = $product->getFillable();

            $properties = ['Properties'];
            $columns = array_merge($product_columns, $properties);
            
            $key_not = '';

            $query = Product::query();

            //search term in all columns
            foreach ($product_columns as $column) {
                $query->orWhere($column, 'LIKE', '%' . $search_term . '%')
                      ->where('backup_progress_id',$backup_progress['id']);
            }
        }else if ($search_table == 'Publishing Channels') { //search if table is Publishing Channels
            
            //get Publishing Channels columns
            $publishing_channel = new PublishingChannel();
            $columns = $publishing_channel->getFillable();

            $key_not = '';

            $query = PublishingChannel::query();

            //search term in all columns
            foreach ($columns as $column) {
                $query->orWhere($column, 'LIKE', '%' . $search_term . '%')
                      ->where('backup_progress_id',$backup_progress['id']);
            }
        }else if ($search_table == 'Tickets') { //search if table is Tickets
            
            //get Ticket columns
            $ticket = new Ticket();
            $columns = $ticket->getFillable();

            
            $key_not = '';

            $query = Ticket::query();

            //search term in all columns
            foreach ($columns as $column) {
                $query->orWhere($column, 'LIKE', '%' . $search_term . '%')
                      ->where('backup_progress_id',$backup_progress['id']);
            }
        }   else if ($search_table == 'Workflows') { //search if table is Workflows
            
            //get Workflows columns
            $workflow = new Workflow();
            $workflow_columns = $workflow->getFillable();

            //get contact meta columns
            $workflow_meta = new WorkflowMeta();
            $workflow_meta_columns = $workflow_meta->getFillable();

           
            $columns = array_merge($workflow_columns, $workflow_meta_columns);

            //key not equal to workflow db id
            $key_not = 'workflow_db_id';

            $query = Workflow::leftJoin('workflow_metas', 'workflows.id', '=', 'workflow_metas.workflow_db_id');
                     

            //search term in all columns
            foreach ($workflow_columns as $column) {
                $query->orWhere('workflows.' . $column, 'LIKE', '%' . $search_term . '%')
                      ->where('backup_progress_id',$backup_progress['id']);
            }
            foreach ($workflow_meta_columns as $column) {
                $query->orWhere('workflow_metas.' . $column, 'LIKE', '%' . $search_term . '%')
                      ->whereBetween('workflow_metas.created_at', [$startdate, $enddate]);
            }
        } else if ($search_table == 'Engagements') { //search if table is Engagements
            //get engagement columns
            $engagement = new Engagement();
            $engagement_columns = $engagement->getFillable();

            $key_not = ''; 
            
            $associated = ['metadata']; 
            
            $columns = array_merge($engagement_columns,$associated);
            
            $query = Engagement::with('note_engagements')
                                ->with('email_engagements')
                                ->with('task_engagements')
                                ->with('call_engagements')
                                ->with('meeting_engagements');

            //search term in all columns
            foreach ($engagement_columns as $column) {
                $query->orWhere($column, 'LIKE', '%' . $search_term . '%')
                      ->where('backup_progress_id',$backup_progress['id']);
            }
        }

        
        
        $query->where('user_id', JWTAuth::user()->id)
              ->select('*');

        $data = $query->get()->toArray();


        $data_only = [];
        $i = 0;
       
                
        foreach ($data as $data_value) {
            $data_only[$i] = array();
            foreach ($data_value as $key => $value) {
                if($key == 'deal_associated_contacts' || $key == 'deal_associated_companies' ||
                    $key == 'deal_associated_tickets'){
                    $ass_ids = [];
                    foreach($value as $associated_values){
                        foreach($associated_values as $associated_ids){
                            $ass_ids[] = $associated_ids;
                        }
                        
                    }
                 $data_only[$i][] = $ass_ids;   
                }elseif($key == 'note_engagements' || $key == 'email_engagements' ||
                        $key == 'task_engagements' || $key == 'call_engagements' ||
                        $key == 'meeting_engagements'){
                    if($data_value[$key] != null ){
                        if(is_array($value)){
                            if(strlen(json_encode($value)) > 200){
                                $text = substr(json_encode($value),0,200).'...';
                            }else{
                                $text = json_encode($value);
                            }
                            $data_only[$i][] = $text;
                        }else{
                            $data_only[$i][] = $value;
                        }
                        
                    }
                    
                }else{
                   if(is_array($value)){
                            if(strlen(json_encode($value)) > 200){
                                $text_value = substr(json_encode($value),0,200).'...';
                            }else{
                                $text_value = json_encode($value);
                            }
                            $data_only[$i][] = $text_value;
                        }else{
                            $data_only[$i][] = $value;
                        }
                }
                
            }
            $i++;
        }
        $key_column = [];
        foreach ($columns as $getcolumn) {

            if ($getcolumn != 'user_id' && $getcolumn != $key_not && $getcolumn != 'backup_progress_id') {
                $key_column[] = strtoupper(str_replace('_', ' ', $getcolumn));
            }
        }

        $json_data = array(
            "recordsTotal" => count($data),
            "data" => $data_only,
            "fields" => $key_column,
            "startDate" => $startdate,
            "endDate" => $enddate
        );


        return response()->json([
                    'data' => $json_data]);
    }

}
