<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CmsTemplate extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'cms_templates';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id','backup_progress_id', 'cms_template_id','portal_id','amp_validated','attached_amp_stylesheet','category_id',
        'cdn_minified_url','cdn_url','cloned_from','generated_from_layout_id','linked_style_id','thumbnail_width','created_by_id',
        'css_or_js_template','default','description_i18n_key','deleted_at','folder','folder_id','google_fonts','has_style_tag',
        'host_template_types','is_available_for_new_content','is_from_layout','is_read_only','marketplace_delivery_id',
        'marketplace_version','has_buffered_changes','missing','purchased','release_id','size','source','template_type','type',
        'updated'];
    
    protected $hidden = ['id','user_id','created_at','updated_at','backup_progress_id'];
    
    public function user() {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }
    
     public function template_js() {
        return $this->hasMany('App\CmsTemplateAttachedJs', 'cms_template_db_id', 'id');
    }
    
    public function template_stylesheet() {
        return $this->hasMany('App\CmsTemplateAttachedStylesheet', 'cms_template_db_id', 'id');
    }
    
    public function template_content_tags() {
        return $this->hasMany('App\CmsTemplateContentTag', 'cms_template_db_id', 'id');
    }
}
