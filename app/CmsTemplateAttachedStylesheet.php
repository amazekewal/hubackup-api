<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CmsTemplateAttachedStylesheet extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'cms_template_attached_stylesheets';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['cms_template_db_id','stylesheet_id','type'];
    
    protected $hidden = ['cms_template_db_id','created_at','updated_at'];
    
    public function template() {
        return $this->belongsTo('App\cms_templates', 'cms_template_db_id', 'id');
    }
}
