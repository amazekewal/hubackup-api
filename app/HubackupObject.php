<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HubackupObject extends Model
{
     /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'hubackup_objects';
    
    protected $fillable = ['object'];
}
