<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Company extends Model {

      

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id','backup_progress_id', 'company_id', 'name', 'website','about_us','facebook_fans','first_deal_created_date',
                          'founded_year','time_first_seen','first_touch_converting_campaign','time_of_first_visit','time_last_seen',
                           'last_touch_converting_campaign','time_of_last_session','number_of_pageviews','number_of_visits',
                           'original_source_type','original_source_data_1','original_source_data_2','avatar_filemanager_key',
                           'last_modified_date','target_account','owner_assigned_date','is_public','associated_contacts',
                           'associated_deals','recent_deal_amount','recent_deal_close_date','timezone','total_money_raised',
                           'total_revenue','twitter_handle','phone_number','twitter_bio','twitterfollowers','address','address2',
                           'facebook_company_page','city','linkedin_company_page','linkedin_bio','state_or_region','googleplus_page',
                           'last_meeting_booked','last_meeting_booked_campaign','last_meeting_booked_medium','last_meeting_booked_source',
                           'recent_sales_email_replied_date','contact_owner','last_contacted','last_activity_date','next_activity_date',
                           'number_of_times_contacted','number_of_sales_activities','zip','country','hubspot_team','all_owner_ids',
                           'website_url','company_domain_name','all_team_ids','all_accessible_team_ids','number_of_employees',
                           'industry','annual_revenue','lifecycle_stage','lead_status','parent_company','type','description',
                           'number_of_child_companies','createdate','closedate','first_contact_createdate','days_to_close',
                           'web_technologies'];
    
            
    protected $hidden = ['id','user_id','created_at', 'updated_at','backup_progress_id'];

    public function user() {
        
        return $this->belongsTo('App\User', 'user_id', 'id');
    }
      
    
}
