<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CmsFile extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'cms_files';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id','backup_progress_id', 'cms_file_id','portal_id','alt_key','alt_key_hash','alt_url','archived',
        'deleted_at','extension','folder_id','height','is_cta_image','encoding','size','title','type','url','version',
        'width','cloud_key','s3_url','friendly_url','url_scheme','allows_anonymous_access','hidden','cloud_key_hash',
        'created_by','deleted_by','replaceable','cdn_purge_embargo_time','file_hash','meta_thumbs_medium_cloud_key',
        'meta_thumbs_medium_friendly_url','meta_thumbs_medium_s3_url','meta_thumbs_medium_image_name','meta_thumbs_thumb_cloud_key',
        'meta_thumbs_thumb_friendly_url','meta_thumbs_thumb_s3_url','meta_thumbs_thumb_image_name','meta_thumbs_icon_cloud_key',
        'meta_thumbs_icon_friendly_url','meta_thumbs_icon_s3_url','meta_thumbs_icon_image_name','created', 'updated'];
    
    protected $hidden = ['id','user_id','created_at','updated_at','backup_progress_id'];
    
    public function user() {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }
}
