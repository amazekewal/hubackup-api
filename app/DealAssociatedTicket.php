<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DealAssociatedTicket extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'deal_associated_tickets';
    
     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['deal_db_id', 'deal_associated_ticket_id'];
    
    protected $hidden = ['id','deal_db_id','created_at','updated_at'];
    
    public function deal() {
        return $this->belongsTo('App\Deal', 'deal_db_id', 'id');
    }
    
    public function ticket() {
        return $this->belongsTo('App\Ticket', 'deal_associated_ticket_id', 'ticket_id');
    }
}
