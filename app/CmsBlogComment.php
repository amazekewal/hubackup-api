<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CmsBlogComment extends Model
{
     /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'cms_blog_comments';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id','backup_progress_id', 'cms_blog_comment_id','portal_id','content_id','content_title','content_permalink',
        'collection_id','deleted_at','user_name','first_name','last_name','user_email','comment','user_url','state',
        'user_ip','user_referrer','user_agent','content_author_email','content_author_name','thread_id','replying_to',
        'parent_id','legacy_id','extra_context','created_date','content_created_at'];
    
    protected $hidden = ['id','user_id','created_at','updated_at','backup_progress_id'];

    
    public function user() {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }
}
