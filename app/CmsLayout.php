<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CmsLayout extends Model
{
   /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'cms_layouts';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id','backup_progress_id', 'layout_id','portal_id','attached_amp_stylesheet','author','author_at',
        'enable_domain_stylesheets','include_default_custom_css','folder','folder_id','cdn_purge_embargo_time','custom_head',
        'body_css_id','custom_footer','body_class','body_css','builtin','purchased','generator','generated_template_id',
        'marketplace_good_id','cloned_from','body_editable','last_edit_session_id','last_edit_update_id','has_user_changes',
        'marketplace_delivery_id','is_read_only','host_template_types','marketplace_version','should_create_hubshot',
        'is_default','category_id','deleted_at','is_available_for_new_content','languages','label','has_buffered_changes',
        'path','template_type','updated','updated_by','updated_by_id','layout_data_cell','layout_data_cells','layout_data_css_class',
        'layout_data_css_id','layout_data_css_id_str','layout_data_css_style','layout_data_editable','layout_data_id',
        'layout_data_is_container','layout_data_is_content_overridden','layout_data_is_in_container','layout_data_language_overrides',
        'layout_data_order','layout_data_params','layout_data_root','layout_data_row','layout_data_row_meta_data',
        'layout_data_type','layout_data_w','layout_data_widgets','layout_data_x'];
    
    
    protected $hidden = ['id','user_id','created_at','updated_at','backup_progress_id'];
        
    public function user() {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }
    
    public function layout_js() {
        return $this->hasMany('App\CmsLayoutAttachedJs', 'cms_layout_db_id', 'id');
    }
    
    public function layout_stylesheet() {
        return $this->hasMany('App\CmsLayoutAttachedStylesheet', 'cms_layout_db_id', 'id');
    }
    
    public function layout_content_tags() {
        return $this->hasMany('App\CmsLayoutContentTag', 'cms_layout_db_id', 'id');
    }
    
    public function layout_rows() {
        return $this->hasMany('App\CmsLayoutRow', 'cms_layout_db_id', 'id');
    }
    
    
}
