<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmailEvent extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'email_events';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id','backup_progress_id', 'email_event_id','app_id','portal_id','app_name','browser_family',
        'browser_name','browser_producer','browser_producer_url','browser_type','browser_url','browser_version','created',
        'email_campaign_id','city','country','state','recipient','type','user_agent'];
    
    protected $hidden = ['id','user_id','created_at','updated_at','backup_progress_id'];
    
    public function user() {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }
}
