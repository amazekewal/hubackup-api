<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model {

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'contacts';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id','backup_progress_id', 'vid', 'company_size', 'date_of_birth', 'days_to_close','degree','field_of_study',
                        'first_conversion_date','first_conversion','first_deal_created_date','gender','graduation_date',
                        'additional_email_addresses','all_vids_for_a_contact','first_touch_converting_campaign',
                        'last_touch_converting_campaign','avatar_filemanager_key','all_form_submissions_for_a_contact',
                        'merged_vids_with_timestamps_of_a_contact','calculated_mobile_number_in_international_format',
                        'calculated_phone_number_in_international_format','calculated_phone_number_area_code',
                        'calculated_phone_number_country_code','calculated_phone_number_region_code','email_confirmed',
                        'membership_notes','registered_at','domain_to_which_registration_email_was_sent',
                        'time_registration_email_was_sent','status','conversations_visitor_email','created_by_conversations',
                        'recent_document_revisit_date','email_domain','email_address_quarantined','sends_since_last_engagement',
                        'marketing_email_confirmation_status','clicked_facebook_ad','facebook_id','google_ad_click_id',
                        'googleplus_id','ip_timezone','is_a_contact','lead_status','legal_basis_for_processing_contacts_data',
                        'linkedin_id','recent_sales_email_clicked_date','recent_sales_email_opened_date',
                        'calculated_mobile_number_with_country_code','calculated_phone_number_with_country_code',
                        'calculated_mobile_number_without_country_code','calculated_phone_number_without_country_code',
                        'currently_in_sequence','twitter_id','owner_assigned_date','ip_city','ip_country','ip_country_code',
                        'ip_latitude_and_longitude','ip_state_or_region','ip_state_code_or_region_code','ip_zipcode','job_function',
                        'last_modified_date','marital_status','military_status','associated_deals','number_of_form_submissions',
                        'number_of_unique_forms_submitted','recent_conversion_date','recent_conversion','recent_deal_amount',
                        'recent_deal_close_date','relationship_status','school','seniority','start_date','total_revenue','work_email',
                        'first_name','first_page_seen','marketing_emails_delivered','opted_out_of_email:_one_to_one','twitter_username',
                        'follower_count','last_page_seen','marketing_emails_opened','last_name','number_of_pageviews',
                        'marketing_emails_clicked','salutation','twitter_profile_photo','email','number_of_visits',
                        'marketing_emails_bounced','persona','number_of_event_completions','unsubscribed_from_all_email',
                        'mobile_phone_number','phone_number','fax_number','time_first_seen','last_marketing_email_name',
                        'last_marketing_email_send_date','address','last_meeting_booked','last_meeting_booked_campaign',
                        'last_meeting_booked_medium','last_meeting_booked_source','last_marketing_email_open_date',
                        'recent_sales_email_replied_date','contact_owner','last_contacted','last_activity_date',
                        'next_activity_date','number_of_times_contacted','number_of_sales_activities',
                        'surveyMonkey_event_last_updated','webinar_event_last_updated','city','time_last_seen',
                        'last_marketing_email_click_date','hubspot_team','linkedin_bio','twitter_bio','all_owner_ids',
                        'time_of_last_visit','first_marketing_email_send_date','state_or_region','all_team_ids',
                        'original_source','first_marketing_email_open_date','zip','country','all_accessible_team_ids',
                        'original_source_drill-down_1','first_marketing_email_click_date','linkedIn_connections',
                        'original_source_drill-down_2','is_globally_ineligible','preferred_language','klout_score',
                        'first_referring_site','jobtitle','photo','last_referring_site','message','closedate','average_page_views',
                        'event_revenue','became_a_lead_date','became_a_marketing_qualified_lead_date','became_an_opportunity_date',
                        'lifecycle_stage','became_a_sales_qualified_lead_date','createdate','became_an_evangelist_date',
                        'became_a_customer_date','hubspotscore','company_name','became_a_subscriber_date',
                        'became_an_other_lifecycle_date','website_url','number_of_employees','annual_revenue','industry',
                        'associated_company_id','associated_company_last_updated','number_of_broadcast_clicks',
                        'email_recipient_fatigue_recovery_time','linkedin_clicks','google_plus_clicks','facebook_clicks',
                        'twitter_clicks'
                        
        ];
     

    protected $hidden = ['id','user_id','created_at', 'updated_at','backup_progress_id'];
    
    public function user() {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }
       
}
