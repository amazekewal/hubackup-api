<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MarketingEmailWidget extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'marketing_email_widgets';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['marketing_email_db_id', 'name','label','body','child_css','css','order','smart_type','type'];
    
    protected $hidden = ['marketing_email_db_id','created_at','updated_at'];

    
    public function marketing_email() {
        return $this->belongsTo('App\MarketingEmail', 'marketing_email_db_id', 'id');
    }
}
