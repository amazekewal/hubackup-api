<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Analytic extends Model
{
     /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'analytics';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id','backup_progress_id','breakdown','raw_views', 'visits', 'visitors','leads','contacts',
                           'subscribers','marketing_qualified_leads','sales_qualified_leads','opportunities',
                           'customers','pageviews_per_session','bounce_rate','time_per_session',
                           'new_visitor_session_rate','session_to_contact_rate','contact_to_customer_rate'];
    
    protected $hidden = ['id','user_id','created_at', 'updated_at','backup_progress_id'];
     
    public function user() {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }
   
}
