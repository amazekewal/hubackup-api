<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CmsBlog extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'cms_blogs';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id','backup_progress_id', 'cms_blog_id','portal_id','absolute_url', 'allow_comments', 'amp_body_color','amp_body_font','amp_body_font_size',
        'amp_custom_css','amp_header_background_color','amp_header_color','amp_header_font','amp_header_font_size','amp_link_color',
        'amp_logo_alt','amp_logo_height','amp_logo_src','amp_logo_width','analytics_page_id','attached_stylesheets','captcha_after_days',
        'captcha_always','category_id','close_comments_older','comment_date_format','comment_form_guid','comment_max_thread_depth',
        'comment_moderation','comment_notification_emails','comment_should_create_contact','comment_verification_text','cos_object_type',
        'created_date_time','daily_notification_email_id','default_group_style_id','default_notification_from_name',
        'default_notification_reply_to','deleted_at','description','domain','domain_when_published','email_api_subscription_id',
        'enable_google_amp_output','enable_social_auto_publishing','html_footer','html_footer_is_shared','html_head',
        'html_head_is_shared','html_keywords','html_title','instant_notification_email_id','item_layout_id','item_template_is_shared',
        'item_template_path','label','listing_layout_id','listing_template_path','live_domain','month_filter_format',
        'monthly_notification_email_id','name','post_html_footer','post_html_head','posts_per_listing_page','posts_per_rss_feed',
        'public_access_rules','public_access_rules_enabled','public_title','publish_date_format','resolved_domain','root_url',
        'show_social_link_facebook','show_social_link_linkedin','show_social_link_twitter','show_summary_in_emails','show_summary_in_listing',
        'show_summary_in_rss','slug','social_account_twitter','subscription_contacts_property','subscription_form_guid',
        'subscription_lists_by_type_daily','subscription_lists_by_type_instant','subscription_lists_by_type_monthly',
        'subscription_lists_by_type_weekly','updated_date_time','url_base','url_segments','use_featured_image_in_summary',
        'uses_default_template','social_sharing_linkedin','header','legacy_tab_id','subscription_email_type','social_sharing_digg',
        'social_sharing_twitter','rss_item_header','social_sharing_twitter_account','social_sharing_reddit','weekly_notification_email_id',
        'legacy_guid','social_sharing_googleplusone','rss_custom_feed','social_sharing_googlebuzz','social_sharing_email',
        'social_sharing_facebook_like','legacy_module_id','language','social_publishing_slug','rss_description','rss_item_footer',
        'social_sharing_facebook_send','social_sharing_delicious','social_sharing_stumbleupon'];
    
    
    protected $hidden = ['id','user_id','created_at','updated_at','backup_progress_id'];

    public function user() {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }
}
