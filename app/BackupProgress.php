<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BackupProgress extends Model {

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'backup_progress';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id','status','start_date', 'end_date'];
    
    public function user() {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }
   
}
