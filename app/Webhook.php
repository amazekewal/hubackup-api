<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Webhook extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id','object_id','property_name','property_value','change_source',
                           'event_id','subscription_id','portal_id','app_id','occurred_at',
                           'subscription_type','attempt_number','change_flag'];
    
    public function user() {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }
}
