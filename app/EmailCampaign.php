<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmailCampaign extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'email_campaigns';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id','backup_progress_id', 'email_campaign_id','app_id','app_name','counters_delivered','counters_open',
        'counters_processed','counters_sent','name','num_included','num_queued','sub_type','subject','type'];
    
    protected $hidden = ['id','user_id','created_at', 'updated_at','backup_progress_id'];
    
    public function user() {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }
}
