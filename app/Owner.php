<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Owner extends Model
{
     /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'owners';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id','backup_progress_id', 'owner_id','portal_id','first_name','last_name','email','has_contacts_access',
        'active_user_id','is_active','user_id_including_inactive','remote_list_id','remote_list_portal_id','remote_list_owner_id',
        'remote_list_remote_id','remote_list_remote_type','remote_list_active','created','updated'];
    
    protected $hidden = ['id','user_id','created_at','updated_at','backup_progress_id'];

    
    public function user() {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }
}
