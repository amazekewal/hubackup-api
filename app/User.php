<?php

namespace App;

use Hash;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'firstName', 'email', 'password','lastName','accesstoken','refreshtoken','app_id','hub_id','region','companyName','address','address1','address2','billingAddress',
        'billingAddress1','billingAddress2','city','zipcode','country','phoneNumber','state','timeZone','lastLogin'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    
    public function connection(){
        return $this->hasOne('App\Connection','user_id','id'); 
    }
    public function contacts()
    {
        return $this->hasMany('App\Contact');
    }
    
    public function companies()
    {
        return $this->hasMany('App\Company');
    }
    
    public function deals()
    {
        return $this->hasMany('App\Deal');
    }
    
    public function tickets()
    {
        return $this->hasMany('App\Ticket');
    }
    
    public function backup_history()
    {
        return $this->hasMany('App\BackupHistory');
    }
    
    /**
     * Automatically creates hash for the user password.
     *
     * @param  string  $value
     * @return void
     */
    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = Hash::make($value);
    }

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }
}
