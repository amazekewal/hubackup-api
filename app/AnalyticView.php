<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AnalyticView extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'analytic_views';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id','backup_progress_id','analytic_view_id','title','creator_id', 'updater_id', 'contains_legacy_report_properties','deleted_at',
                            'report_property_filters_op','report_property_filters_args','report_property_filters_prop',
                            'created_date','updated_date'];
    
    public function user() {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }
}
