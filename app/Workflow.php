<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Workflow extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'workflows';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id','backup_progress_id', 'workflow_id','portal_id','name','type','enabled','persona_tag_ids','inserted_at',
        'updated_on'];
    
    protected $hidden = ['id','user_id','created_at', 'updated_at','workflow_db_id','backup_progress_id'];
    
    public function user() {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }
    
    public function workflow_meta()
    {
        return $this->hasOne('App\WorkflowMeta', 'workflow_db_id', 'id');
    }
}
