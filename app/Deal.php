<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Deal extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id','backup_progress_id', 'deal_id', 'deal_name', 'amount','amount_in_home_currency','days_to_close','original_source_type',
                          'original_source_data_1','original_source_data_2','deal_amount_calculation_preference','owner_assigned_date',
                          'deal_stage','pipeline','closedate','last_meeting_booked','last_meeting_booked_campaign',
                          'last_meeting_booked_medium','last_meeting_booked_source','recent_sales_email_replied_date','deal_owner',
                          'last_contacted','last_activity_date','next_activity_date','number_of_times_contacted','number_of_sales_activities',
                          'createdate','last_modified_date','hubspot_team','deal_type','all_owner_ids','deal_description',
                          'all_team_ids','all_accessible_team_ids','number_of_contacts','closed_lost_reason','closed_won_reason'];
         
    protected $hidden = ['id','user_id','created_at','updated_at','backup_progress_id'];

        
    public function user() {
        
        return $this->belongsTo('App\User', 'user_id', 'id');
    }
    public function deal_associated_contacts()
    {
        return $this->hasMany('App\DealAssociatedContact', 'deal_db_id', 'id');
    }
    
    public function deal_associated_companies()
    {
        return $this->hasMany('App\DealAssociatedCompany', 'deal_db_id', 'id');
    }
    
    public function deal_associated_tickets()
    {
        return $this->hasMany('App\DealAssociatedTicket', 'deal_db_id', 'id');
    }
        
    }
