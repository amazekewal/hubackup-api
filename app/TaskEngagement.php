<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TaskEngagement extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'task_engagements';
    
     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    
    protected $fillable = ['engagement_db_id','body','status','subject','task_type','reminder',
                           'send_default_reminder'];
    
    protected $hidden = ['id','engagement_db_id','created_at', 'updated_at'];
    
    public function engagement() {
        return $this->belongsTo('App\Engagement', 'engagement_db_id', 'id');
    }
}
