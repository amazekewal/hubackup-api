<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CmsSiteMap extends Model
{
   /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'cms_site_maps';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id','backup_progress_id', 'cms_site_map_id','portal_id','name','blog_posts','content_options',
        'domain','has_user_changes','image_metas','initiated_at','knowledge_articles','landing_pages',
        'site_pages','video_metas','deleted_at','created','updated'];
    
    protected $hidden = ['id','user_id','created_at','updated_at'];
    
    public function user() {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }
}
