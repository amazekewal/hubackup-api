<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Connection extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'connections';

        /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id', 'connectionName', 'accesstoken', 'refreshtoken'];


    public function user(){
        return $this->belongsTo('App\User');
    }
}


