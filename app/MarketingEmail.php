<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MarketingEmail extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'marketing_emails';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id','backup_progress_id', 'marketing_email_id','portal_id','ab','ab_hours_to_wait','ab_sample_size_default','ab_sampling_default',
        'ab_success_metric','ab_test_percentage','ab_variation','absolute_url','all_email_campaign_ids','analytics_page_id',
        'analytics_page_type','archived','author_name','author_at','author_email','author_user_id','blog_rss_settings',
        'can_spam_settings_id','category_id','content_type_category','create_page','current_state','currently_published',
        'domain','email_body','email_note','email_template_mode','email_type','feedback_email_category',
        'feedback_survey_id','flex_areas','freeze_date','from_name','html_title','is_graymail_suppression_enabled',
        'is_local_timezone_send','is_published','is_recipient_fatigue_suppression_enabled','last_edit_session_id','last_edit_update_id',
        'layout_sections','lead_flow_id','live_domain','mailing_lists_excluded','mailing_lists_included','max_rss_entries',
        'name','page_expiry_enabled','page_redirected','preview_key','processing_status','publish_date','publish_immediately',
        'published_url','reply_to','resolved_domain','rss_email_by_text','rss_email_click_through_text','rss_email_comment_text',
        'rss_email_entry_template_enabled','rss_email_image_max_width','rss_email_url','scrubs_subscription_links','slug',
        'smart_email_fields','state','style_settings','subcategory','subject','subscription','subscription_name','team_perms',
        'template_path','transactional','unpublished_at','url','use_rss_headline_as_subject','user_perms','vids_excluded',
        'vids_included','workflow_names','created','updated'];
    
    protected $hidden = ['id','user_id','created_at','updated_at','backup_progress_id'];

    
    public function user() {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }
    
    public function widgets() {
        return $this->hasMany('App\MarketingEmailWidget', 'marketing_email_db_id', 'id');
    }
}
