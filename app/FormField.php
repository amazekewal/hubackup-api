<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FormField extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'form_fields';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['form_db_id', 'name','label','type','field_type','description','group_name','display_order',
        'selected_options','options','validation_name','validation_message','validation_data','validation_use_default_block_list',
        'validation_blocked_email_addresses','hidden','enabled','default_value','is_smart_field','unselected_label','placeholder',
        'dependent_field_filters','label_hidden','default','is_smart_group','rich_text_content'];
    
    protected $hidden = ['form_db_id','created_at', 'updated_at'];
    
    protected $casts = [
        'selected_options' => 'array',
        'options' => 'array',
        'validation_blocked_email_addresses' => 'array',
        'dependent_field_filters'=> 'array',
    ];
    
    public function form() {
        return $this->belongsTo('App\Form', 'form_db_id', 'id');
    }
}
