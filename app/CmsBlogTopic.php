<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CmsBlogTopic extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'cms_blog_topics';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id','backup_progress_id', 'blog_topic_id','portal_id','name','slug','description','created', 'updated',
        'deleted_at','total_posts','live_posts','last_used','associated_blog_ids','public_url','status'];
    
    protected $hidden = ['id','user_id','created_at','updated_at','backup_progress_id'];

    public function user() {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }
}
