<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CmsHubdbTable extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'cms_hubdb_tables';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id','backup_progress_id', 'table_id','name','created_date','published_at','updated_date','deleted',
        'use_for_pages','row_count','created_by','updated_by','column_count'];
    
    protected $hidden = ['id','user_id','created_at','updated_at','backup_progress_id'];

    
    public function user() {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }
    
    public function table_columns()
    {
        return $this->hasMany('App\CmsHubdbTableColumn', 'cms_hubdb_table_db_id', 'id');
    }
}
