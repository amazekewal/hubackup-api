<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SmartAlert extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'smart_alerts';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id','s_object','action','operator','amount','unit'];
    
    public function user() {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }
}
