<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ticket extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id','backup_progress_id', 'ticket_id', 'subject', 'pipeline','pipeline_stage','content',
                           'created_by','source_type','status','createdate','last_modified_date','closedate'];
    
    protected $hidden = ['id','user_id','created_at','updated_at','backup_progress_id'];

        
    public function user() {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }
       
}
