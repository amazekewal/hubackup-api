<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DealAssociatedCompany extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'deal_associated_companies';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['deal_db_id', 'deal_associated_company_id'];
    
    protected $hidden = ['id','deal_db_id','created_at','updated_at'];

    public function deal() {
        return $this->belongsTo('App\Deal', 'deal_db_id', 'id');
    }
    
    public function company() {
        return $this->belongsTo('App\Company', 'deal_associated_company_id', 'company_id');
    }
}
