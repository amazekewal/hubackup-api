<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CmsLayoutAttachedJs extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'cms_layout_attached_js';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['cms_layout_db_id','js_id','type'];
    
    protected $hidden = ['cms_layout_db_id','created_at','updated_at'];

    
    public function layout() {
        return $this->belongsTo('App\cms_layouts', 'cms_layout_db_id', 'id');
    }
}
