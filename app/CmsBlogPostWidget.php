<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CmsBlogPostWidget extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'cms_blog_post_widgets';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['cms_blog_post_db_id','widget_id','body_html','child_css','css','label',
                            'name','order','smart_type','type'];
    
    protected $hidden = ['cms_blog_post_db_id','created_at','updated_at'];

    
    public function cms_blog_post() {
        return $this->belongsTo('App\CmsBlogPost', 'cms_blog_post_db_id', 'id');
    }
}
