<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Form extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'forms';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id','backup_progress_id', 'portal_id','guid','name','action','method','css_class','redirect','submit_text','follow_up_id',
        'notify_recipients','lead_nurturing_campaign_id','performable_html','migrated_from','ignore_current_values','meta_data',
        'deletable','inline_message','tms_id','captcha_enabled','campaign_guid','cloneable','editable','form_type','deleted_at','theme_name',
        'parent_id','style','is_published','publish_at','unpublish_at','published_at','winning_variant_id','finished','control_id',
        'kickback_emailworkflow_id','kickback_emails_json','created_on','updated_on'];
    
    protected $casts = [
        'meta_data' => 'array',
    ];
     
    protected $hidden = ['id','user_id','created_at', 'updated_at','backup_progress_id'];
    
    public function user() {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }
    
    public function form_fields()
    {
        return $this->hasMany('App\FormField', 'form_db_id', 'id');
    }
}
