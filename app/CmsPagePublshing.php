<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CmsPagePublshing extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'cms_page_publishing';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id','backup_progress_id', 'page_publishing_id','ab_status','ab_test_id','analytics_page_id','archived',
                           'attached_stylesheets','author_user_id','blueprint_type_id','campaign','campaign_name',
                           'cloned_from','compose_body','css','css_text','current_live_domain','deleted_at','deleted_by',
                           'domain','enable_domain_stylesheets','enable_layout_stylesheets','featured_image',
                           'featured_image_alt_text','flex_areas','folder_id','footer_html','freeze_date',
                           'has_smart_content','has_user_changes','head_html','html_title','include_default_custom_css',
                           'is_draft','keywords','language','last_edit_session_id','last_edit_update_id','legacy_blog_tabid',
                           'meta_description','meta_keywords','name','page_expiry_date','page_expiry_enabled',
                           'page_expiry_redirect_id','page_expiry_redirect_url','page_redirected','password','performable_url',
                           'personas','portal_id','publish_date','publish_immediately','published_url','rss_email_author_line_template',
                           'rss_email_blog_image_max_width','rss_email_by_text','rss_email_click_through_text',
                           'rss_email_comment_text','rss_email_entry_template','rss_email_entry_template_enabled',
                           'rss_email_image_max_width','scheduled_update_date','slug','staged_from','style_override_id',
                           'subcategory','template_path','tms_id','translated_from_id','unpublished_at','url','created',
                           'updated'
                            ];
    
    protected $hidden = ['id','user_id','created_at','updated_at','backup_progress_id'];
    
    public function user() {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }
    
    public function widgets() {
        return $this->hasMany('App\CmsPagePublshingWidget', 'page_publishing_db_id', 'id');
    }
}
