<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmailSubscriptionTimeline extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'email_subscription_timelines';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id','backup_progress_id', 'portal_id','recipient','date'];
    
    public function user() {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }
}
