<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CalendarEvent extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'calendar_events';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id','backup_progress_id', 'event_id','portal_id','event_type','event_date','category','category_id','content_id',
        'state','campaign_guid','name','description','url','owner_id','created_by','create_content','preview_key','template_path',
        'social_username','social_display_name','avatar_url','is_recurring','topic_ids','content_group_id','group_id',
        'group_order','remote_id','started_date','published_date'];
    
    protected $hidden = ['id','user_id','created_at', 'updated_at','backup_progress_id'];
    
    public function user() {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }
}
