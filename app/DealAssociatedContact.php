<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DealAssociatedContact extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'deal_associated_contacts';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['deal_db_id', 'deal_associated_vid'];
    
    protected $hidden = ['id','deal_db_id','created_at','updated_at'];
    
    public function deal() {
        return $this->belongsTo('App\Deal', 'deal_db_id', 'id');
    }
    
    public function contact() {
        return $this->belongsTo('App\Contact', 'deal_associated_vid', 'vid');
    }
}
