<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WorkflowMeta extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'workflow_metas';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['workflow_db_id', 'internal','only_exec_on_biz_days','nurture_time_range_enabled','nurture_time_range_start_hour',
        'nurture_time_range_stop_hour','listening','allow_contact_to_trigger_multiple_times','unenrollment_setting_type',
        'unenrollment_setting_type_excluded_workflows','recurring_setting_type','enroll_on_criteria_update','only_enrolls_manually',
        'goal_criteria_property_object_type','goal_criteria_filter_family','goal_criteria_within_time_mode','goal_criteria_property',
        'goal_criteria_value','goal_criteria_type','goal_criteria_operator','re_enrollment_trigger_sets','suppression_list_ids',
        'last_updated_by','segment_criteria','triggered_by_workflow_ids','succeeded_list_id','contact_list_ids_enrolled','contact_list_ids_active','contact_list_ids_steps','contact_list_ids_succeeded',
        'contact_list_ids_completed'];
    
    public function workflow() {
        return $this->belongsTo('App\workflows', 'workflow_db_id', 'id');
    }
}
