<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BackupHistory extends Model {

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'backup_history';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['backup_progress_id','user_id','object_name','num_of_records', 'object_size', 'removed_records','added_records',
        'changed_records','num_of_API_calls','status','error_message'];
    
    public function user() {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }
   
}

