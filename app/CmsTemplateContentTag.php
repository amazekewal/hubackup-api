<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CmsTemplateContentTag extends Model
{
     /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'cms_template_content_tags';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['cms_template_db_id','name','source'];
    
    protected $hidden = ['cms_template_db_id','created_at','updated_at'];
    
    public function template() {
        return $this->belongsTo('App\cms_templates', 'cms_template_db_id', 'id');
    }
}
