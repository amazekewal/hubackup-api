<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'products';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id','backup_progress_id', 'product_id','object_type','portal_id','version','is_deleted'];
    
    protected $hidden = ['id','user_id','created_at','updated_at','backup_progress_id'];
    
    public function user() {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }
    
    public function properties() {
        return $this->hasMany('App\ProductProperty', 'product_db_id', 'id');
    }
}
