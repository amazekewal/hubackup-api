<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmailEngagement extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'email_engagements';
    
     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
     
    protected $fillable = ['engagement_db_id','sender_email','sender_first_name','sender_last_name',
                           'recipient_email','subject','html','text'];
    
    protected $hidden = ['id','engagement_db_id','created_at', 'updated_at'];
    
    public function engagement() {
        return $this->belongsTo('App\Engagement', 'engagement_db_id', 'id');
    }
}
