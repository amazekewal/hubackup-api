<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SmartAlertHistory extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'smart_alert_histories';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id','smart_alert_id'];
    
    public function smartalert() {
        return $this->belongsTo('App\SmartAlert', 'smart_alert_id', 'id');
    }
}
