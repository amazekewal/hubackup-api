<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NoteEngagement extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'note_engagements';
    
     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    
    protected $fillable = ['engagement_db_id','body'];
    
    protected $hidden = ['id','engagement_db_id','created_at', 'updated_at'];
    
    public function engagement() {
        return $this->belongsTo('App\Engagement', 'engagement_db_id', 'id');
    }
    
    public function note_attachments()
    {
        return $this->hasMany('App\NoteAttachment', 'note_engagement_db_id', 'id');
    }
}
