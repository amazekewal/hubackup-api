<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PublishingChannel extends Model
{
   /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'publishing_channels';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id','backup_progress_id', 'channel_id','portal_id','channel_guid','account_guid','name','settings_publish',
        'type','image_url','picture','user_name','real_name','last_name','page_name','profile_url','email','page_id','user',
        'full_name','first_name','page_category','created_on','updated_on'];
    
    protected $hidden = ['id','user_id','created_at','updated_at','backup_progress_id'];

    
    public function user() {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }
}
