<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmailSubscriptionTimelineChange extends Model
{
     /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'email_subscription_timeline_changes';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['timeline_db_id','portal_id','subscription_id','change','source','change_type','caused_by_event_id',
        'created'];
    
    public function email_subscription_timeline() {
        return $this->belongsTo('App\EmailSubscriptionTimeline', 'timeline_db_id', 'id');
    }
}
