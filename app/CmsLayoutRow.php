<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CmsLayoutRow extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'cms_layout_rows';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['cms_layout_db_id','row_id','cell','cells','css_class','css_id','css_id_str',
        'css_style','editable','is_container','is_content_overridden','is_in_container','label','language_overrides','name',
        'order','params','root','row','row_meta_data','type','w','widgets',
        'x'];
    
    protected $hidden = ['cms_layout_db_id','created_at','updated_at'];
    
    public function layout() {
        return $this->belongsTo('App\cms_layouts', 'cms_layout_db_id', 'id');
    }
}
